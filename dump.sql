-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.7.9 - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura do banco de dados para mobly
CREATE DATABASE IF NOT EXISTS `mobly` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mobly`;


-- Copiando estrutura para tabela mobly.ativacoes
CREATE TABLE IF NOT EXISTS `ativacoes` (
  `ati_id` int(11) NOT NULL AUTO_INCREMENT,
  `ati_idlicenca` int(11) NOT NULL,
  `ati_idusuario` int(11) NOT NULL,
  `ati_idpedido` int(11) NOT NULL,
  `ati_valor` decimal(10,2) NOT NULL,
  `ati_data_criacao` datetime DEFAULT NULL,
  `ati_data_validade` date DEFAULT NULL,
  PRIMARY KEY (`ati_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.ativacoes: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ativacoes` DISABLE KEYS */;
/*!40000 ALTER TABLE `ativacoes` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.bancos
CREATE TABLE IF NOT EXISTS `bancos` (
  `ban_id` int(11) NOT NULL AUTO_INCREMENT,
  `ban_nome` varchar(255) NOT NULL,
  PRIMARY KEY (`ban_id`)
) ENGINE=InnoDB AUTO_INCREMENT=746 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Copiando dados para a tabela mobly.bancos: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `bancos` DISABLE KEYS */;
INSERT INTO `bancos` (`ban_id`, `ban_nome`) VALUES
	(1, 'Banco do Brasil'),
	(33, 'Banco Santander'),
	(104, 'Caixa Econômica Federal'),
	(341, 'Itáu Unibanco'),
	(399, 'HSBC Bank Brasil'),
	(745, 'Banco Citibank');
/*!40000 ALTER TABLE `bancos` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.banner_titulo
CREATE TABLE IF NOT EXISTS `banner_titulo` (
  `ban_id` int(11) NOT NULL AUTO_INCREMENT,
  `ban_nome` varchar(255) DEFAULT '0',
  `ban_descricao` text NOT NULL,
  `ban_promocao` text NOT NULL,
  `ban_link` text NOT NULL,
  `ban_acao_botao` varchar(255) DEFAULT NULL,
  `ban_extensao` varchar(4) DEFAULT '0',
  PRIMARY KEY (`ban_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.banner_titulo: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `banner_titulo` DISABLE KEYS */;
INSERT INTO `banner_titulo` (`ban_id`, `ban_nome`, `ban_descricao`, `ban_promocao`, `ban_link`, `ban_acao_botao`, `ban_extensao`) VALUES
	(35, 'ESTAMOS PRONTOS PRA ATENDER', 'FEIRA RECIFE', 'compre tudo em casa ', 'http://localhost:8080/mlm/produto/15/prod-1', 'COMPRAR', 'jpg'),
	(36, 'O que está esperando?', 'PRA SUA MESA ', 'DIRETO DA ROÇA', 'http://localhost:8080/mlm/produto/15/prod-1', 'COMPRAR', 'jpg');
/*!40000 ALTER TABLE `banner_titulo` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.bonificacao_por_nivel
CREATE TABLE IF NOT EXISTS `bonificacao_por_nivel` (
  `bpn_id` int(11) NOT NULL AUTO_INCREMENT,
  `bpn_idlicenca` int(11) NOT NULL,
  `bpn_percentual` decimal(10,2) NOT NULL,
  `bpn_nivel` int(11) NOT NULL,
  `bpn_tipo_bonus` int(11) NOT NULL,
  PRIMARY KEY (`bpn_id`),
  UNIQUE KEY `bpn_idlicenca_bpn_nivel_bpn_tipo_bonus` (`bpn_idlicenca`,`bpn_nivel`,`bpn_tipo_bonus`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.bonificacao_por_nivel: ~48 rows (aproximadamente)
/*!40000 ALTER TABLE `bonificacao_por_nivel` DISABLE KEYS */;
INSERT INTO `bonificacao_por_nivel` (`bpn_id`, `bpn_idlicenca`, `bpn_percentual`, `bpn_nivel`, `bpn_tipo_bonus`) VALUES
	(1, 1, 0.00, 1, 2),
	(2, 1, 0.00, 2, 2),
	(3, 1, 0.00, 3, 2),
	(4, 1, 0.00, 4, 2),
	(5, 2, 0.05, 1, 2),
	(6, 2, 0.10, 2, 2),
	(7, 2, 0.03, 3, 2),
	(8, 2, 0.05, 4, 2),
	(9, 3, 0.05, 1, 2),
	(10, 3, 0.10, 2, 2),
	(11, 3, 0.03, 3, 2),
	(12, 3, 0.05, 4, 2),
	(13, 4, 0.05, 1, 2),
	(14, 4, 0.10, 2, 2),
	(15, 4, 0.03, 3, 2),
	(16, 4, 0.05, 4, 2),
	(23, 1, 0.00, 1, 3),
	(24, 1, 0.00, 2, 3),
	(25, 1, 0.00, 3, 3),
	(26, 1, 0.00, 4, 3),
	(27, 2, 0.05, 1, 3),
	(28, 2, 0.10, 2, 3),
	(29, 2, 0.03, 3, 3),
	(30, 2, 0.05, 4, 3),
	(31, 3, 0.05, 1, 3),
	(32, 3, 0.10, 2, 3),
	(33, 3, 0.03, 3, 3),
	(34, 3, 0.05, 4, 3),
	(35, 4, 0.05, 1, 3),
	(36, 4, 0.10, 2, 3),
	(37, 4, 0.03, 3, 3),
	(38, 4, 0.05, 4, 3),
	(39, 1, 0.00, 1, 1),
	(40, 2, 0.10, 1, 1),
	(41, 3, 0.15, 1, 1),
	(42, 4, 0.20, 1, 1),
	(43, 1, 0.00, 2, 1),
	(44, 2, 0.10, 2, 1),
	(46, 3, 0.10, 2, 1),
	(47, 4, 0.10, 2, 1),
	(48, 1, 0.00, 3, 1),
	(49, 2, 0.03, 3, 1),
	(50, 3, 0.03, 3, 1),
	(51, 4, 0.03, 3, 1),
	(52, 1, 0.00, 4, 1),
	(53, 2, 0.05, 4, 1),
	(54, 3, 0.05, 4, 1),
	(55, 4, 0.05, 4, 1);
/*!40000 ALTER TABLE `bonificacao_por_nivel` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.carrinho
CREATE TABLE IF NOT EXISTS `carrinho` (
  `car_id` int(11) NOT NULL AUTO_INCREMENT,
  `car_datacompra` datetime DEFAULT NULL,
  `car_valorcompra` decimal(10,2) NOT NULL,
  `car_valorfrete` decimal(10,2) DEFAULT NULL,
  `car_valorcomprafinal` decimal(10,2) NOT NULL,
  `car_situacao` enum('A','T','E','C') NOT NULL,
  `car_idformaentrega` int(11) NOT NULL,
  `car_idformapagamento` int(11) NOT NULL,
  `car_idcliente` int(11) NOT NULL,
  `car_endentrega` varchar(100) NOT NULL,
  `car_cidadeentrega` varchar(100) NOT NULL,
  `car_ufentrega` varchar(2) NOT NULL,
  `car_cepentrega` varchar(8) NOT NULL,
  PRIMARY KEY (`car_id`),
  KEY `fk_carrinho_forma_entrega_fpag_id` (`car_idformapagamento`),
  KEY `fk_carrinho_forma_entrega_fent_id` (`car_idformaentrega`),
  KEY `fk_carrinho_forma_cliente_cli_id` (`car_idcliente`),
  CONSTRAINT `fk_carrinho_forma_cliente_cli_id` FOREIGN KEY (`car_idcliente`) REFERENCES `cliente` (`cli_id`),
  CONSTRAINT `fk_carrinho_forma_entrega_fent_id` FOREIGN KEY (`car_idformaentrega`) REFERENCES `forma_entrega` (`fent_id`),
  CONSTRAINT `fk_carrinho_forma_entrega_fpag_id` FOREIGN KEY (`car_idformapagamento`) REFERENCES `forma_pagamento` (`fpag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.carrinho: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `carrinho` DISABLE KEYS */;
/*!40000 ALTER TABLE `carrinho` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.carrinho_item
CREATE TABLE IF NOT EXISTS `carrinho_item` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_valor` decimal(10,2) NOT NULL,
  `item_quantidade` int(11) NOT NULL,
  `item_idcarrinho` int(11) NOT NULL,
  `item_idsku` int(11) NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `fk_Carrinho_Item_Carrinho1_idx` (`item_idcarrinho`),
  KEY `fk_Carrinho_Item_Produto1_idx` (`item_idsku`),
  CONSTRAINT `fk_Carrinho_Item_Carrinho1` FOREIGN KEY (`item_idcarrinho`) REFERENCES `carrinho` (`car_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Carrinho_Item_Produto1` FOREIGN KEY (`item_idsku`) REFERENCES `sku` (`sku_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.carrinho_item: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `carrinho_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `carrinho_item` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.ci_sessions
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) DEFAULT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Note: This is the default CodeIgniter session table.';

-- Copiando dados para a tabela mobly.ci_sessions: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.cliente
CREATE TABLE IF NOT EXISTS `cliente` (
  `cli_id` int(11) NOT NULL AUTO_INCREMENT,
  `cli_nome` varchar(100) NOT NULL,
  `cli_sobrenome` varchar(100) DEFAULT NULL,
  `cli_endereco` varchar(100) NOT NULL,
  `cli_cidade` varchar(100) NOT NULL,
  `cli_uf` char(2) NOT NULL,
  `cli_cep` varchar(8) NOT NULL,
  `cli_email` varchar(100) NOT NULL,
  `cli_telefone` varchar(14) NOT NULL,
  `cli_cpf` varchar(11) NOT NULL,
  `cli_sexo` enum('M','F') NOT NULL,
  `cli_senha` varchar(300) NOT NULL,
  PRIMARY KEY (`cli_id`),
  UNIQUE KEY `cli_email_UNIQUE` (`cli_email`),
  UNIQUE KEY `cli_cpf_UNIQUE` (`cli_cpf`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.cliente: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.config_mmn
CREATE TABLE IF NOT EXISTS `config_mmn` (
  `LIMITE_MINIMO_BOLETO` decimal(10,2) DEFAULT '0.00',
  `TAXA_ADMINISTRACAO` decimal(10,2) DEFAULT '0.00',
  `PRAZO_LIMITE_ATIVACAO` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela mobly.config_mmn: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `config_mmn` DISABLE KEYS */;
INSERT INTO `config_mmn` (`LIMITE_MINIMO_BOLETO`, `TAXA_ADMINISTRACAO`, `PRAZO_LIMITE_ATIVACAO`) VALUES
	(0.00, 0.10, 30);
/*!40000 ALTER TABLE `config_mmn` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.conta_banco
CREATE TABLE IF NOT EXISTS `conta_banco` (
  `cba_id` int(11) NOT NULL AUTO_INCREMENT,
  `cba_idpessoa` int(11) NOT NULL,
  `cba_idbanco` int(11) NOT NULL,
  `cba_agencia` varchar(100) NOT NULL DEFAULT '',
  `cba_conta` varchar(100) NOT NULL DEFAULT '',
  `cba_titular` varchar(100) NOT NULL DEFAULT '',
  `cba_cpf` varchar(100) NOT NULL DEFAULT '',
  `cba_idtipo` int(11) NOT NULL,
  `cba_data_criacao` datetime DEFAULT NULL,
  `cba_data_alteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`cba_id`),
  UNIQUE KEY `cba_idpessoa` (`cba_idpessoa`),
  KEY `cba_id` (`cba_id`),
  KEY `fk_idbanco` (`cba_idbanco`),
  CONSTRAINT `fk_idbanco` FOREIGN KEY (`cba_idbanco`) REFERENCES `bancos` (`ban_id`),
  CONSTRAINT `fk_idpessoa` FOREIGN KEY (`cba_idpessoa`) REFERENCES `pessoa` (`pes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Copiando dados para a tabela mobly.conta_banco: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `conta_banco` DISABLE KEYS */;
/*!40000 ALTER TABLE `conta_banco` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.cotacao
CREATE TABLE IF NOT EXISTS `cotacao` (
  `cot_id` int(11) NOT NULL AUTO_INCREMENT,
  `cot_valor` decimal(10,2) NOT NULL,
  `cot_idlicenca` int(11) NOT NULL,
  `cot_taxa_venda_percent` decimal(10,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`cot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.cotacao: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `cotacao` DISABLE KEYS */;
INSERT INTO `cotacao` (`cot_id`, `cot_valor`, `cot_idlicenca`, `cot_taxa_venda_percent`) VALUES
	(1, 250.00, 1, 0.0000),
	(2, 250.00, 2, 0.0000),
	(3, 250.00, 3, 0.0000),
	(4, 250.00, 4, 0.0000);
/*!40000 ALTER TABLE `cotacao` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.creditos_aprovisionados
CREATE TABLE IF NOT EXISTS `creditos_aprovisionados` (
  `cap_id` int(11) NOT NULL AUTO_INCREMENT,
  `cap_idusuario` int(11) NOT NULL,
  `cap_idpedido` int(11) NOT NULL,
  `cap_cotacao` decimal(10,2) NOT NULL,
  `cap_pontos` decimal(10,2) NOT NULL,
  `cap_descricao` varchar(255) DEFAULT NULL,
  `cap_data_criacao` datetime DEFAULT NULL,
  `cap_inicio_vigencia` datetime DEFAULT NULL,
  PRIMARY KEY (`cap_id`),
  KEY `cap_idusuario` (`cap_idusuario`),
  KEY `cap_idpedido` (`cap_idpedido`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Copiando dados para a tabela mobly.creditos_aprovisionados: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `creditos_aprovisionados` DISABLE KEYS */;
/*!40000 ALTER TABLE `creditos_aprovisionados` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.departamento
CREATE TABLE IF NOT EXISTS `departamento` (
  `dep_id` int(11) NOT NULL AUTO_INCREMENT,
  `dep_nome` varchar(50) NOT NULL,
  `dep_departamentopai` int(11) DEFAULT NULL,
  PRIMARY KEY (`dep_id`),
  KEY `fk_Departamento_Departamento1_idx` (`dep_departamentopai`),
  CONSTRAINT `fk_Departamento_Departamento1` FOREIGN KEY (`dep_departamentopai`) REFERENCES `departamento` (`dep_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.departamento: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `departamento` DISABLE KEYS */;
INSERT INTO `departamento` (`dep_id`, `dep_nome`, `dep_departamentopai`) VALUES
	(2, 'categ1', NULL),
	(3, 'categ2', NULL);
/*!40000 ALTER TABLE `departamento` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.departamento_foto
CREATE TABLE IF NOT EXISTS `departamento_foto` (
  `depfot_id` int(11) NOT NULL AUTO_INCREMENT,
  `depfot_extensao` varchar(4) NOT NULL DEFAULT '0',
  `depfot_iddepartamento` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`depfot_id`),
  KEY `fk_dpfoto_departamento_depfot_iddp` (`depfot_iddepartamento`),
  CONSTRAINT `fk_dpfoto_departamento_depfot_iddp` FOREIGN KEY (`depfot_iddepartamento`) REFERENCES `departamento` (`dep_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.departamento_foto: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `departamento_foto` DISABLE KEYS */;
INSERT INTO `departamento_foto` (`depfot_id`, `depfot_extensao`, `depfot_iddepartamento`) VALUES
	(1, 'gif', 2),
	(2, 'png', 3);
/*!40000 ALTER TABLE `departamento_foto` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.endereco
CREATE TABLE IF NOT EXISTS `endereco` (
  `end_id` int(11) NOT NULL AUTO_INCREMENT,
  `end_logradouro` varchar(255) DEFAULT NULL,
  `end_numero` varchar(50) DEFAULT NULL,
  `end_complemento` varchar(255) DEFAULT NULL,
  `end_cep` varchar(8) NOT NULL,
  `end_bairro` varchar(255) DEFAULT NULL,
  `end_cidade` varchar(255) DEFAULT NULL,
  `end_estado` int(11) DEFAULT NULL,
  `end_pais` varchar(255) DEFAULT NULL,
  `end_data_criacao` datetime DEFAULT NULL,
  `end_data_alteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`end_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.endereco: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `endereco` DISABLE KEYS */;
INSERT INTO `endereco` (`end_id`, `end_logradouro`, `end_numero`, `end_complemento`, `end_cep`, `end_bairro`, `end_cidade`, `end_estado`, `end_pais`, `end_data_criacao`, `end_data_alteracao`) VALUES
	(1, 'Rua 1', '1', 'comp 1', '18000000', 'bairro 1', 'Sorocaba', 35, 'Brasil', '2016-07-10 11:32:21', '2016-07-10 11:32:22'),
	(2, NULL, NULL, NULL, '', NULL, NULL, 0, NULL, '2016-07-10 11:50:12', '2016-07-10 11:50:12'),
	(3, NULL, NULL, NULL, '', NULL, NULL, 0, NULL, '2016-07-10 11:50:12', '2016-07-10 11:50:12'),
	(4, NULL, NULL, NULL, '', NULL, NULL, 0, NULL, '2016-07-10 12:47:28', '2016-07-10 12:47:28'),
	(5, NULL, NULL, NULL, '', NULL, NULL, 0, NULL, '2016-07-10 12:47:28', '2016-07-10 12:47:28'),
	(8, 'asdfasdfsadfsdaf', '541', 'asdfasdfsdaf', '18048400', 'asdfasdfasdf', 'asdfasdfasdf', 27, NULL, '2016-07-10 19:21:29', '2016-07-10 19:21:29'),
	(9, 'asfasdf', 'asdfasdf', 'asdfasdfasd', '18444444', 'asdfasdfasdf', 'asdfasdfasdf', 22, NULL, '2016-07-10 19:28:32', '2016-07-10 19:28:32'),
	(10, 'rua conceição', '251', 'proximo ao bairro', '18180000', 'bairro', 'santos', 27, NULL, '2016-07-10 19:53:13', '2016-07-10 19:53:13');
/*!40000 ALTER TABLE `endereco` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.estado
CREATE TABLE IF NOT EXISTS `estado` (
  `est_id` int(11) NOT NULL,
  `est_nome` varchar(45) NOT NULL,
  `est_sigla` varchar(2) NOT NULL,
  PRIMARY KEY (`est_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.estado: ~27 rows (aproximadamente)
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` (`est_id`, `est_nome`, `est_sigla`) VALUES
	(11, 'Rondônia', 'RO'),
	(12, 'Acre', 'AC'),
	(13, 'Amazonas', 'AM'),
	(14, 'Roraima', 'RR'),
	(15, 'Pará', 'PA'),
	(16, 'Amapá', 'AP'),
	(17, 'Tocantins', 'TO'),
	(21, 'Maranhão', 'MA'),
	(22, 'Piauí', 'PI'),
	(23, 'Ceará', 'CE'),
	(24, 'Rio Grande do Norte', 'RN'),
	(25, 'Paraíba', 'PB'),
	(26, 'Pernambuco', 'PE'),
	(27, 'Alagoas', 'AL'),
	(28, 'Sergipe', 'SE'),
	(29, 'Bahia', 'BA'),
	(31, 'Minas Gerais', 'MG'),
	(32, 'Espírito Santo', 'ES'),
	(33, 'Rio de Janeiro', 'RJ'),
	(35, 'São Paulo', 'SP'),
	(41, 'Paraná', 'PR'),
	(42, 'Santa Catarina', 'SC'),
	(43, 'Rio Grande do Sul', 'RS'),
	(50, 'Mato Grosso do Sul', 'MS'),
	(51, 'Mato Grosso', 'MT'),
	(52, 'Goiás', 'GO'),
	(53, 'Distrito Federal', 'DF');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.faixa_cep_frete_gratis
CREATE TABLE IF NOT EXISTS `faixa_cep_frete_gratis` (
  `fcepfg_id` int(11) NOT NULL AUTO_INCREMENT,
  `fcepfg_cepinicial` varchar(8) NOT NULL,
  `fcepfg_cepfinal` varchar(8) NOT NULL,
  `fcepfg_pesoinicial` decimal(10,3) DEFAULT NULL,
  `fcepfg_pesofinal` decimal(10,3) DEFAULT NULL,
  `fcepfg_min_valor` decimal(10,2) DEFAULT '0.00',
  `fcepfg_habilita` char(1) NOT NULL DEFAULT 'S',
  `fcepfg_idformaentrega` int(11) NOT NULL,
  PRIMARY KEY (`fcepfg_id`),
  KEY `fk_faixa_cep_frete_gratis_forma_entrega_idformaentrega` (`fcepfg_idformaentrega`),
  CONSTRAINT `fk_faixa_cep_frete_gratis_forma_entrega_idformaentrega` FOREIGN KEY (`fcepfg_idformaentrega`) REFERENCES `forma_entrega` (`fent_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.faixa_cep_frete_gratis: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `faixa_cep_frete_gratis` DISABLE KEYS */;
/*!40000 ALTER TABLE `faixa_cep_frete_gratis` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.forma_entrega
CREATE TABLE IF NOT EXISTS `forma_entrega` (
  `fent_id` int(11) NOT NULL AUTO_INCREMENT,
  `fent_nome` varchar(100) NOT NULL,
  `fent_codcorreios` varchar(45) DEFAULT NULL,
  `fent_habilita` char(1) NOT NULL DEFAULT 'S',
  PRIMARY KEY (`fent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.forma_entrega: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `forma_entrega` DISABLE KEYS */;
INSERT INTO `forma_entrega` (`fent_id`, `fent_nome`, `fent_codcorreios`, `fent_habilita`) VALUES
	(1, 'Transportadora', NULL, 'S'),
	(2, 'PAC', '41106', 'S'),
	(3, 'SEDEX', '40010', 'S');
/*!40000 ALTER TABLE `forma_entrega` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.forma_pagamento
CREATE TABLE IF NOT EXISTS `forma_pagamento` (
  `fpag_id` int(11) NOT NULL AUTO_INCREMENT,
  `fpag_nome` varchar(100) NOT NULL,
  `fpag_tipo` char(1) NOT NULL,
  `fpag_img` varchar(50) NOT NULL DEFAULT '''''',
  `fpag_desconto` decimal(10,2) NOT NULL,
  `fpag_nummaxparcelas` int(11) NOT NULL DEFAULT '1',
  `fpag_habilita` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fpag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.forma_pagamento: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `forma_pagamento` DISABLE KEYS */;
INSERT INTO `forma_pagamento` (`fpag_id`, `fpag_nome`, `fpag_tipo`, `fpag_img`, `fpag_desconto`, `fpag_nummaxparcelas`, `fpag_habilita`) VALUES
	(1, 'Boleto', '1', '../img/bankisp', 0.00, 1, 1),
	(2, 'Crédito Wolf', '2', '../img/wolf', 0.00, 1, 0),
	(3, 'pagseguro', '1', '../img/pagseguro', 0.00, 1, 1),
	(4, 'paypal', '1', '../img/paypal', 0.00, 1, 1);
/*!40000 ALTER TABLE `forma_pagamento` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.imagem_destaque
CREATE TABLE IF NOT EXISTS `imagem_destaque` (
  `img_id` int(11) NOT NULL AUTO_INCREMENT,
  `img_nome` varchar(255) DEFAULT '0',
  `img_descricao` text,
  `img_link` text,
  `img_extensao` varchar(4) DEFAULT '0',
  PRIMARY KEY (`img_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.imagem_destaque: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `imagem_destaque` DISABLE KEYS */;
INSERT INTO `imagem_destaque` (`img_id`, `img_nome`, `img_descricao`, `img_link`, `img_extensao`) VALUES
	(1, 'BEBIDAS ', 'TUDO NO MESMO LUGAR', 'http://localhost:8080/mlm/produto/15/prod-1', 'jpg');
/*!40000 ALTER TABLE `imagem_destaque` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.licencas
CREATE TABLE IF NOT EXISTS `licencas` (
  `lic_id` int(11) NOT NULL AUTO_INCREMENT,
  `lic_nome` varchar(255) NOT NULL,
  `lic_valor` decimal(10,2) NOT NULL,
  `lic_bonus_adesao` decimal(10,2) NOT NULL DEFAULT '0.00',
  `lic_carreira` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.licencas: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `licencas` DISABLE KEYS */;
INSERT INTO `licencas` (`lic_id`, `lic_nome`, `lic_valor`, `lic_bonus_adesao`, `lic_carreira`) VALUES
	(1, 'START', 100.00, 0.00, 0),
	(2, 'PREMIUM', 300.00, 0.00, 1),
	(3, 'SILVER', 500.00, 0.10, 1),
	(4, 'GOLD', 800.00, 0.20, 1);
/*!40000 ALTER TABLE `licencas` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.log_saldos_pagamento
CREATE TABLE IF NOT EXISTS `log_saldos_pagamento` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `log_idusuario` int(11) NOT NULL,
  `log_idsaldo` int(11) NOT NULL,
  `log_valor` decimal(10,2) NOT NULL,
  `log_descricao` varchar(255) DEFAULT NULL,
  `log_data_criacao` datetime DEFAULT NULL,
  PRIMARY KEY (`log_id`),
  KEY `fk_log_saldos_pagamento_idusuario` (`log_idusuario`),
  KEY `fk_log_saldos_pagamento_idsaldo` (`log_idsaldo`),
  CONSTRAINT `fk_log_saldos_pagamento_idsaldo` FOREIGN KEY (`log_idsaldo`) REFERENCES `saldos` (`sal_id`),
  CONSTRAINT `fk_log_saldos_pagamento_idusuario` FOREIGN KEY (`log_idusuario`) REFERENCES `usuario_mmn` (`usu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.log_saldos_pagamento: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `log_saldos_pagamento` DISABLE KEYS */;
/*!40000 ALTER TABLE `log_saldos_pagamento` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.loja_configuracao
CREATE TABLE IF NOT EXISTS `loja_configuracao` (
  `loja_idconfiguracao` int(11) NOT NULL AUTO_INCREMENT,
  `loja_nome` varchar(100) NOT NULL,
  `loja_endereco` varchar(100) NOT NULL,
  `loja_cnpj` varchar(14) NOT NULL,
  `loja_email` varchar(100) NOT NULL,
  `loja_cidade` varchar(50) NOT NULL,
  `loja_uf` char(2) NOT NULL,
  PRIMARY KEY (`loja_idconfiguracao`),
  UNIQUE KEY `loja_cnpj_UNIQUE` (`loja_cnpj`),
  UNIQUE KEY `loja_email_UNIQUE` (`loja_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.loja_configuracao: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `loja_configuracao` DISABLE KEYS */;
/*!40000 ALTER TABLE `loja_configuracao` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.menu
CREATE TABLE IF NOT EXISTS `menu` (
  `mnu_id` int(11) NOT NULL AUTO_INCREMENT,
  `mnu_status` int(11) NOT NULL,
  `mnu_tipo` int(11) DEFAULT NULL,
  `mnu_idcontroller` int(11) DEFAULT NULL,
  `mnu_display` varchar(45) NOT NULL,
  `mnu_fa_icon` varchar(50) DEFAULT NULL,
  `mnu_ordem` int(11) DEFAULT NULL,
  PRIMARY KEY (`mnu_id`),
  KEY `FK_mnu_con_idx` (`mnu_idcontroller`),
  KEY `FK_mnu_tmn_idx` (`mnu_tipo`),
  CONSTRAINT `FK_mnu_con` FOREIGN KEY (`mnu_idcontroller`) REFERENCES `m2n_permissoes_controllers` (`con_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_mnu_tmn` FOREIGN KEY (`mnu_tipo`) REFERENCES `menu_tipo` (`tmn_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.menu: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.menu_item
CREATE TABLE IF NOT EXISTS `menu_item` (
  `mni_id` int(11) NOT NULL AUTO_INCREMENT,
  `mni_status` int(11) NOT NULL,
  `mni_tipo` int(11) DEFAULT NULL,
  `mni_parent` int(11) DEFAULT NULL,
  `mni_idaction` int(11) DEFAULT NULL,
  `mni_display` varchar(45) NOT NULL,
  PRIMARY KEY (`mni_id`),
  KEY `FK_mni_mnu_idx` (`mni_parent`),
  KEY `FK_mni_act_idx` (`mni_idaction`),
  CONSTRAINT `FK_mni_mnu` FOREIGN KEY (`mni_parent`) REFERENCES `menu` (`mnu_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_mni_act` FOREIGN KEY (`mni_idaction`) REFERENCES `m2n_permissoes_actions` (`act_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.menu_item: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `menu_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_item` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.menu_tipo
CREATE TABLE IF NOT EXISTS `menu_tipo` (
  `tmn_id` int(11) NOT NULL AUTO_INCREMENT,
  `tmn_descricao` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`tmn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.menu_tipo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `menu_tipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_tipo` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.pedidos
CREATE TABLE IF NOT EXISTS `pedidos` (
  `ped_id` int(11) NOT NULL AUTO_INCREMENT,
  `ped_status` int(11) NOT NULL DEFAULT '0',
  `ped_idusu` int(11) NOT NULL DEFAULT '1',
  `ped_subtotal` decimal(10,2) DEFAULT NULL,
  `ped_desconto` decimal(10,2) DEFAULT NULL,
  `ped_frete` decimal(10,2) DEFAULT NULL,
  `ped_total` decimal(10,2) DEFAULT NULL,
  `ped_tipo_bonus` int(11) DEFAULT NULL,
  `ped_data_criacao` datetime DEFAULT NULL,
  `ped_data_alteracao` datetime DEFAULT NULL,
  `ped_data_vencimento` datetime DEFAULT NULL,
  PRIMARY KEY (`ped_id`),
  KEY `FK_pedido_usuariommn` (`ped_idusu`),
  CONSTRAINT `FK_pedido_usuariommn` FOREIGN KEY (`ped_idusu`) REFERENCES `usuario_mmn` (`usu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.pedidos: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `pedidos` DISABLE KEYS */;
INSERT INTO `pedidos` (`ped_id`, `ped_status`, `ped_idusu`, `ped_subtotal`, `ped_desconto`, `ped_frete`, `ped_total`, `ped_tipo_bonus`, `ped_data_criacao`, `ped_data_alteracao`, `ped_data_vencimento`) VALUES
	(1, 0, 1, 10.00, 0.00, 15.00, 25.00, 3, NULL, NULL, NULL),
	(2, 0, 1, 10.00, 0.00, 15.00, 25.00, 3, NULL, NULL, NULL);
/*!40000 ALTER TABLE `pedidos` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.pedidos_endereco
CREATE TABLE IF NOT EXISTS `pedidos_endereco` (
  `pedend_id` int(11) NOT NULL AUTO_INCREMENT,
  `pedend_pedido_id` int(11) NOT NULL,
  `pedend_endereco_id` int(11) NOT NULL,
  PRIMARY KEY (`pedend_id`),
  KEY `fk_pedend_pedido` (`pedend_pedido_id`),
  KEY `fk_pedend_endereco` (`pedend_endereco_id`),
  CONSTRAINT `fk_pedend_endereco` FOREIGN KEY (`pedend_endereco_id`) REFERENCES `endereco` (`end_id`),
  CONSTRAINT `fk_pedend_pedido` FOREIGN KEY (`pedend_pedido_id`) REFERENCES `pedidos` (`ped_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Copiando dados para a tabela mobly.pedidos_endereco: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pedidos_endereco` DISABLE KEYS */;
INSERT INTO `pedidos_endereco` (`pedend_id`, `pedend_pedido_id`, `pedend_endereco_id`) VALUES
	(1, 2, 5);
/*!40000 ALTER TABLE `pedidos_endereco` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.pedidos_item
CREATE TABLE IF NOT EXISTS `pedidos_item` (
  `pedite_id` int(11) NOT NULL AUTO_INCREMENT,
  `pedite_idpedido` int(11) NOT NULL,
  `pedite_idproduto` int(11) NOT NULL,
  `pedite_quantidade` decimal(10,2) NOT NULL,
  `pedite_valor_unitario` decimal(10,2) NOT NULL,
  `pedite_valor_desconto` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pedite_data_criacao` datetime DEFAULT NULL,
  `pedite_data_alteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`pedite_id`),
  KEY `fk_item_pedido_pedido` (`pedite_idpedido`),
  KEY `fk_item_pedido_produto` (`pedite_idproduto`),
  CONSTRAINT `fk_item_pedido_pedido` FOREIGN KEY (`pedite_idpedido`) REFERENCES `pedidos` (`ped_id`),
  CONSTRAINT `fk_item_pedido_produto` FOREIGN KEY (`pedite_idproduto`) REFERENCES `produto` (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.pedidos_item: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pedidos_item` DISABLE KEYS */;
INSERT INTO `pedidos_item` (`pedite_id`, `pedite_idpedido`, `pedite_idproduto`, `pedite_quantidade`, `pedite_valor_unitario`, `pedite_valor_desconto`, `pedite_data_criacao`, `pedite_data_alteracao`) VALUES
	(1, 2, 25, 1.00, 10.00, 0.00, NULL, NULL);
/*!40000 ALTER TABLE `pedidos_item` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.pessoa
CREATE TABLE IF NOT EXISTS `pessoa` (
  `pes_id` int(11) NOT NULL AUTO_INCREMENT,
  `pes_nome` varchar(255) NOT NULL,
  `pes_email` varchar(255) NOT NULL,
  `pes_rg` varchar(50) DEFAULT NULL,
  `pes_cpf_cnpj` varchar(11) NOT NULL,
  `pes_tipo` int(11) NOT NULL,
  `pes_nome_mae` varchar(255) DEFAULT NULL,
  `pes_nome_pai` varchar(255) DEFAULT NULL,
  `pes_data_criacao` datetime DEFAULT NULL,
  `pes_data_alteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`pes_id`),
  KEY `FK_pessoa_tipo_pessoa_tip_id` (`pes_tipo`),
  CONSTRAINT `FK_pessoa_tipo_pessoa_tip_id` FOREIGN KEY (`pes_tipo`) REFERENCES `tipo_pessoa` (`tip_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.pessoa: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `pessoa` DISABLE KEYS */;
INSERT INTO `pessoa` (`pes_id`, `pes_nome`, `pes_email`, `pes_rg`, `pes_cpf_cnpj`, `pes_tipo`, `pes_nome_mae`, `pes_nome_pai`, `pes_data_criacao`, `pes_data_alteracao`) VALUES
	(1, 'user', 'user@email.com', '00000', '00000000000', 1, 'mae', 'pai', '2016-07-10 11:58:02', '2016-07-10 11:58:02'),
	(13, 'paulo antunes', 'user@email.com.br', NULL, '06752369674', 1, NULL, NULL, '2016-07-10 19:21:29', '2016-07-10 19:21:29'),
	(14, 'claudia leite', 'claudia@email.com.br', NULL, '44444444444', 1, NULL, NULL, '2016-07-10 19:28:32', '2016-07-10 19:28:32'),
	(15, 'Canu Pado', 'padu@email.com.br', NULL, '15243652415', 1, NULL, NULL, '2016-07-10 19:53:13', '2016-07-10 19:53:13');
/*!40000 ALTER TABLE `pessoa` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.pessoa_endereco
CREATE TABLE IF NOT EXISTS `pessoa_endereco` (
  `pesend_id` int(11) NOT NULL AUTO_INCREMENT,
  `pesend_idpessoa` int(11) NOT NULL,
  `pesend_idendereco` int(11) NOT NULL,
  `pesend_padrao` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`pesend_id`),
  UNIQUE KEY `UK_pessoa_endereco` (`pesend_idendereco`,`pesend_idpessoa`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.pessoa_endereco: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `pessoa_endereco` DISABLE KEYS */;
INSERT INTO `pessoa_endereco` (`pesend_id`, `pesend_idpessoa`, `pesend_idendereco`, `pesend_padrao`) VALUES
	(1, 1, 1, 1),
	(4, 13, 8, 1),
	(5, 14, 9, 1),
	(6, 15, 10, 1);
/*!40000 ALTER TABLE `pessoa_endereco` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.pessoa_telefone
CREATE TABLE IF NOT EXISTS `pessoa_telefone` (
  `pestel_id` int(11) NOT NULL AUTO_INCREMENT,
  `pestel_idpessoa` int(11) NOT NULL,
  `pestel_idtelefone` int(11) NOT NULL,
  `pestel_data_criacao` datetime DEFAULT NULL,
  `pestel_data_alteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`pestel_id`),
  UNIQUE KEY `UK_pessoa_telefone` (`pestel_idpessoa`,`pestel_idtelefone`),
  KEY `FK_pessoa_telefone_telefone_tel_id` (`pestel_idtelefone`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.pessoa_telefone: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `pessoa_telefone` DISABLE KEYS */;
INSERT INTO `pessoa_telefone` (`pestel_id`, `pestel_idpessoa`, `pestel_idtelefone`, `pestel_data_criacao`, `pestel_data_alteracao`) VALUES
	(6, 13, 6, NULL, NULL),
	(7, 14, 7, NULL, NULL),
	(8, 15, 8, NULL, NULL);
/*!40000 ALTER TABLE `pessoa_telefone` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.ponto
CREATE TABLE IF NOT EXISTS `ponto` (
  `pon_id` int(11) NOT NULL AUTO_INCREMENT,
  `pon_idusuario` int(11) NOT NULL,
  `pon_idtipo` smallint(2) NOT NULL,
  `pon_pontos` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pon_cotacao` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pon_valor` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pon_data_adicionado` datetime NOT NULL,
  PRIMARY KEY (`pon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.ponto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `ponto` DISABLE KEYS */;
/*!40000 ALTER TABLE `ponto` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.pontos
CREATE TABLE IF NOT EXISTS `pontos` (
  `pon_id` int(11) NOT NULL AUTO_INCREMENT,
  `pon_idusuario` int(11) NOT NULL,
  `pon_idtipo` smallint(2) NOT NULL,
  `pon_pontos` decimal(10,2) NOT NULL,
  `pon_data_criacao` datetime NOT NULL,
  PRIMARY KEY (`pon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.pontos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pontos` DISABLE KEYS */;
/*!40000 ALTER TABLE `pontos` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.pontos_tipo
CREATE TABLE IF NOT EXISTS `pontos_tipo` (
  `tip_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `tip_nome` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`tip_id`),
  KEY `tip_id` (`tip_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.pontos_tipo: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `pontos_tipo` DISABLE KEYS */;
/*!40000 ALTER TABLE `pontos_tipo` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.preco_forma_entrega
CREATE TABLE IF NOT EXISTS `preco_forma_entrega` (
  `pfent_id` int(11) NOT NULL AUTO_INCREMENT,
  `pfent_cepinicial` varchar(8) NOT NULL,
  `pfent_cepfinal` varchar(8) NOT NULL,
  `pfent_pesoinicial` decimal(10,3) NOT NULL,
  `pfent_pesofinal` decimal(10,3) NOT NULL,
  `pfent_valor` decimal(10,2) NOT NULL DEFAULT '0.00',
  `pfent_prazo` int(11) NOT NULL,
  `pfent_idformaentrega` int(11) NOT NULL,
  PRIMARY KEY (`pfent_id`),
  KEY `fk_preco_forma_entrega_forma_entrega_idformaentrega` (`pfent_idformaentrega`),
  CONSTRAINT `fk_preco_forma_entrega_forma_entrega_idformaentrega` FOREIGN KEY (`pfent_idformaentrega`) REFERENCES `forma_entrega` (`fent_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.preco_forma_entrega: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `preco_forma_entrega` DISABLE KEYS */;
/*!40000 ALTER TABLE `preco_forma_entrega` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.produto
CREATE TABLE IF NOT EXISTS `produto` (
  `prod_id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_nome` varchar(100) DEFAULT NULL,
  `prod_resumo` text,
  `prod_ficha` varchar(50) DEFAULT NULL,
  `prod_valor` decimal(10,2) DEFAULT NULL,
  `prod_valorpromocional` decimal(10,2) DEFAULT NULL,
  `prod_foto` varchar(255) DEFAULT NULL,
  `prod_urlseo` varchar(120) DEFAULT NULL,
  `prod_peso` decimal(10,3) DEFAULT NULL,
  `prod_altura` decimal(10,3) DEFAULT NULL,
  `prod_largura` decimal(10,3) DEFAULT NULL,
  `prod_comprimento` decimal(10,3) DEFAULT NULL,
  `prod_status` int(11) NOT NULL DEFAULT '0',
  `prod_tipo` int(11) DEFAULT '0',
  `prod_ativacao` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`prod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='						';

-- Copiando dados para a tabela mobly.produto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `produto` DISABLE KEYS */;
INSERT INTO `produto` (`prod_id`, `prod_nome`, `prod_resumo`, `prod_ficha`, `prod_valor`, `prod_valorpromocional`, `prod_foto`, `prod_urlseo`, `prod_peso`, `prod_altura`, `prod_largura`, `prod_comprimento`, `prod_status`, `prod_tipo`, `prod_ativacao`) VALUES
	(25, 'prod1', 'produto 1', NULL, 10.00, 10.00, NULL, 'prod1', NULL, NULL, NULL, NULL, 1, 0, 0),
	(26, 'produto 2', 'produto 2', NULL, 20.00, 20.00, NULL, 'produto-2', NULL, NULL, NULL, NULL, 1, 0, 0);
/*!40000 ALTER TABLE `produto` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.produto_departamento
CREATE TABLE IF NOT EXISTS `produto_departamento` (
  `pdep_prod_id` int(11) NOT NULL,
  `pdep_pdep_id` int(11) NOT NULL,
  PRIMARY KEY (`pdep_pdep_id`,`pdep_prod_id`),
  KEY `fk_Produto_has_Produto_Departamento_Produto_Departamento1_idx` (`pdep_pdep_id`),
  KEY `fk_Produto_has_Produto_Departamento_Produto1_idx` (`pdep_prod_id`),
  CONSTRAINT `fk_Produto_has_Departamento_Produto1` FOREIGN KEY (`pdep_prod_id`) REFERENCES `produto` (`prod_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Produto_has_Departamento_Produto_Departamento1` FOREIGN KEY (`pdep_pdep_id`) REFERENCES `departamento` (`dep_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.produto_departamento: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `produto_departamento` DISABLE KEYS */;
INSERT INTO `produto_departamento` (`pdep_prod_id`, `pdep_pdep_id`) VALUES
	(25, 2),
	(26, 2),
	(25, 3);
/*!40000 ALTER TABLE `produto_departamento` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.produto_estoque
CREATE TABLE IF NOT EXISTS `produto_estoque` (
  `prodest_id` int(11) NOT NULL AUTO_INCREMENT,
  `prodest_idproduto` int(11) NOT NULL DEFAULT '0',
  `prodest_quantidade` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`prodest_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela mobly.produto_estoque: ~25 rows (aproximadamente)
/*!40000 ALTER TABLE `produto_estoque` DISABLE KEYS */;
INSERT INTO `produto_estoque` (`prodest_id`, `prodest_idproduto`, `prodest_quantidade`) VALUES
	(1, 1, 10),
	(2, 2, 10),
	(3, 3, 10),
	(4, 4, 10),
	(5, 5, 10),
	(6, 6, 10),
	(7, 7, 10),
	(8, 8, 10),
	(9, 9, 10),
	(10, 10, 10),
	(11, 11, 10),
	(12, 12, 10),
	(13, 13, 10),
	(14, 14, 10),
	(15, 15, 10),
	(16, 16, 10),
	(17, 17, 10),
	(18, 18, 10),
	(19, 19, 10),
	(20, 20, 10),
	(21, 21, 10),
	(22, 22, 10),
	(23, 23, 10),
	(24, 24, 10),
	(25, 25, 10),
	(26, 26, 20);
/*!40000 ALTER TABLE `produto_estoque` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.produto_foto
CREATE TABLE IF NOT EXISTS `produto_foto` (
  `profot_id` int(11) NOT NULL AUTO_INCREMENT,
  `profot_extensao` varchar(4) NOT NULL DEFAULT '0',
  `profot_idproduto` int(11) NOT NULL,
  PRIMARY KEY (`profot_id`),
  KEY `fk_produto_foto_profot_idproduto_produto_prod_id` (`profot_idproduto`),
  CONSTRAINT `fk_produto_foto_profot_idproduto_produto_prod_id` FOREIGN KEY (`profot_idproduto`) REFERENCES `produto` (`prod_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.produto_foto: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `produto_foto` DISABLE KEYS */;
INSERT INTO `produto_foto` (`profot_id`, `profot_extensao`, `profot_idproduto`) VALUES
	(18, 'png', 26);
/*!40000 ALTER TABLE `produto_foto` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.produto_licenca
CREATE TABLE IF NOT EXISTS `produto_licenca` (
  `prolic_id` int(11) NOT NULL AUTO_INCREMENT,
  `prolic_idproduto` int(11) NOT NULL,
  `prolic_idlicenca` int(11) NOT NULL,
  PRIMARY KEY (`prolic_id`),
  KEY `fk_prolic_produto` (`prolic_idproduto`),
  KEY `fk_prolic_licenca` (`prolic_idlicenca`),
  CONSTRAINT `fk_prolic_licenca` FOREIGN KEY (`prolic_idlicenca`) REFERENCES `licencas` (`lic_id`),
  CONSTRAINT `fk_prolic_produto` FOREIGN KEY (`prolic_idproduto`) REFERENCES `produto` (`prod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.produto_licenca: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `produto_licenca` DISABLE KEYS */;
/*!40000 ALTER TABLE `produto_licenca` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.redes
CREATE TABLE IF NOT EXISTS `redes` (
  `red_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `red_idusuario` int(10) unsigned NOT NULL DEFAULT '0',
  `red_parent` int(10) unsigned DEFAULT NULL,
  `red_lft` int(10) unsigned NOT NULL DEFAULT '0',
  `red_rht` int(10) unsigned NOT NULL DEFAULT '0',
  `red_lvl` int(11) NOT NULL,
  `red_data_criacao` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`red_id`),
  UNIQUE KEY `red_idusuario` (`red_idusuario`),
  KEY `trees_nav_idx` (`red_lft`,`red_rht`,`red_lvl`),
  KEY `parent` (`red_parent`),
  KEY `treelevel` (`red_lvl`),
  KEY `id` (`red_id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.redes: ~10 rows (aproximadamente)
/*!40000 ALTER TABLE `redes` DISABLE KEYS */;
INSERT INTO `redes` (`red_id`, `red_idusuario`, `red_parent`, `red_lft`, `red_rht`, `red_lvl`, `red_data_criacao`) VALUES
	(1, 1, NULL, 0, 19, 0, '2015-12-05 03:18:38'),
	(66, 16, 1, 1, 8, 1, '2016-02-01 21:25:47'),
	(67, 17, 1, 9, 10, 1, '2016-02-01 21:25:58'),
	(68, 18, 1, 11, 12, 1, '2016-02-01 21:26:07'),
	(69, 20, 1, 13, 14, 1, '2016-02-01 21:26:23'),
	(70, 19, 1, 15, 16, 1, '2016-02-01 21:26:31'),
	(71, 21, 16, 2, 3, 2, '2016-02-11 09:46:08'),
	(72, 22, 16, 4, 5, 2, '2016-02-11 09:46:16'),
	(73, 23, 1, 17, 18, 1, '2016-02-19 10:55:26'),
	(74, 24, 16, 6, 7, 2, '2016-02-19 13:00:06');
/*!40000 ALTER TABLE `redes` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.reward_points_converted
CREATE TABLE IF NOT EXISTS `reward_points_converted` (
  `rew_convert_id` int(10) NOT NULL AUTO_INCREMENT,
  `rew_convert_ord_detail_fk` int(10) NOT NULL DEFAULT '10',
  `rew_convert_discount_fk` varchar(50) NOT NULL DEFAULT '',
  `rew_convert_points` int(10) NOT NULL DEFAULT '10',
  `rew_convert_date` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`rew_convert_id`),
  UNIQUE KEY `rew_convert_id` (`rew_convert_id`) USING BTREE,
  KEY `rew_convert_discount_fk` (`rew_convert_discount_fk`),
  KEY `rew_convert_ord_detail_fk` (`rew_convert_ord_detail_fk`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.reward_points_converted: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `reward_points_converted` DISABLE KEYS */;
/*!40000 ALTER TABLE `reward_points_converted` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.saldos
CREATE TABLE IF NOT EXISTS `saldos` (
  `sal_id` int(11) NOT NULL AUTO_INCREMENT,
  `sal_idusuario` int(11) NOT NULL,
  `sal_idtipobonus` int(11) NOT NULL,
  `sal_valor` decimal(10,2) NOT NULL,
  `sal_descricao` varchar(255) DEFAULT NULL,
  `sal_data_criacao` datetime DEFAULT NULL,
  `sal_inicio_vigencia` datetime DEFAULT NULL,
  PRIMARY KEY (`sal_id`),
  KEY `fk_saldos_usuarios_idusuario` (`sal_idusuario`),
  KEY `fk_saldos_tipos_idtipo` (`sal_idtipobonus`),
  CONSTRAINT `fk_saldos_usuarios_idusuario` FOREIGN KEY (`sal_idusuario`) REFERENCES `usuario_mmn` (`usu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.saldos: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `saldos` DISABLE KEYS */;
/*!40000 ALTER TABLE `saldos` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.saldos_em_ponto
CREATE TABLE IF NOT EXISTS `saldos_em_ponto` (
  `sep_id` int(11) NOT NULL AUTO_INCREMENT,
  `sep_idusuario` int(11) NOT NULL,
  `sep_cotacao` decimal(10,2) NOT NULL,
  `sep_pontos` decimal(10,2) NOT NULL,
  `sep_descricao` varchar(255) DEFAULT NULL,
  `sep_data_criacao` datetime DEFAULT NULL,
  `sep_inicio_vigencia` datetime DEFAULT NULL,
  PRIMARY KEY (`sep_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.saldos_em_ponto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `saldos_em_ponto` DISABLE KEYS */;
/*!40000 ALTER TABLE `saldos_em_ponto` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.telefone
CREATE TABLE IF NOT EXISTS `telefone` (
  `tel_id` int(11) NOT NULL AUTO_INCREMENT,
  `tel_numero` varchar(50) NOT NULL,
  `tel_tipo` int(11) NOT NULL,
  `tel_descricao` varchar(255) DEFAULT NULL,
  `tel_data_criacao` datetime DEFAULT NULL,
  `tel_data_alteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`tel_id`),
  KEY `FK_telefone_tipo_telefone_tip_id` (`tel_tipo`),
  CONSTRAINT `FK_telefone_tipo_telefone_tip_id` FOREIGN KEY (`tel_tipo`) REFERENCES `tipo_telefone` (`tip_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.telefone: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `telefone` DISABLE KEYS */;
INSERT INTO `telefone` (`tel_id`, `tel_numero`, `tel_tipo`, `tel_descricao`, `tel_data_criacao`, `tel_data_alteracao`) VALUES
	(6, '22222222222', 1, 'Fixo', '2016-07-10 19:21:29', '2016-07-10 19:21:29'),
	(7, '44444444444', 1, 'Fixo', '2016-07-10 19:28:32', '2016-07-10 19:28:32'),
	(8, '44444444444', 1, 'Fixo', '2016-07-10 19:53:13', '2016-07-10 19:53:13');
/*!40000 ALTER TABLE `telefone` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.teste
CREATE TABLE IF NOT EXISTS `teste` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` varchar(255) DEFAULT NULL,
  `data` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.teste: ~30 rows (aproximadamente)
/*!40000 ALTER TABLE `teste` DISABLE KEYS */;
INSERT INTO `teste` (`id`, `desc`, `data`) VALUES
	(1, 'teste 2016-01-27 00:01:03', '2016-01-27 00:01:03'),
	(2, 'teste 2016-01-28 00:01:04', '2016-01-28 00:01:04'),
	(3, 'teste 2016-01-29 00:01:06', '2016-01-29 00:01:06'),
	(4, 'teste 2016-01-30 00:01:03', '2016-01-30 00:01:03'),
	(5, 'teste 2016-01-31 00:01:07', '2016-01-31 00:01:07'),
	(6, 'teste 2016-02-01 00:01:01', '2016-02-01 00:01:01'),
	(7, 'teste 2016-02-02 00:01:03', '2016-02-02 00:01:03'),
	(8, 'teste 2016-02-03 00:01:04', '2016-02-03 00:01:04'),
	(9, 'teste 2016-02-04 00:01:06', '2016-02-04 00:01:06'),
	(10, 'teste 2016-02-05 00:01:06', '2016-02-05 00:01:06'),
	(11, 'teste 2016-02-06 00:01:06', '2016-02-06 00:01:06'),
	(12, 'teste 2016-02-07 00:01:08', '2016-02-07 00:01:08'),
	(13, 'teste 2016-02-08 00:01:06', '2016-02-08 00:01:06'),
	(14, 'teste 2016-02-09 00:01:04', '2016-02-09 00:01:04'),
	(15, 'teste 2016-02-10 00:01:05', '2016-02-10 00:01:05'),
	(16, 'teste 2016-02-11 00:01:05', '2016-02-11 00:01:05'),
	(17, 'teste 2016-02-12 00:01:04', '2016-02-12 00:01:04'),
	(18, 'teste 2016-02-13 00:01:06', '2016-02-13 00:01:06'),
	(19, 'teste 2016-02-14 00:01:06', '2016-02-14 00:01:06'),
	(20, 'teste 2016-02-15 00:01:04', '2016-02-15 00:01:04'),
	(21, 'teste 2016-02-16 00:01:07', '2016-02-16 00:01:07'),
	(22, 'teste 2016-02-17 00:01:06', '2016-02-17 00:01:06'),
	(23, 'teste 2016-02-18 00:01:04', '2016-02-18 00:01:04'),
	(24, 'teste 2016-02-19 00:01:05', '2016-02-19 00:01:05'),
	(25, 'teste 2016-02-20 00:01:05', '2016-02-20 00:01:05'),
	(26, 'teste 2016-02-21 00:01:07', '2016-02-21 00:01:07'),
	(27, 'teste 2016-02-22 00:01:05', '2016-02-22 00:01:05'),
	(28, 'teste 2016-02-23 00:01:04', '2016-02-23 00:01:04'),
	(29, 'teste 2016-02-24 00:01:04', '2016-02-24 00:01:04'),
	(30, 'teste 2016-02-25 00:01:03', '2016-02-25 00:01:03');
/*!40000 ALTER TABLE `teste` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.tipo_bonus
CREATE TABLE IF NOT EXISTS `tipo_bonus` (
  `tipbon_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipbon_nome` varchar(50) NOT NULL,
  `tipbon_status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tipbon_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.tipo_bonus: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_bonus` DISABLE KEYS */;
INSERT INTO `tipo_bonus` (`tipbon_id`, `tipbon_nome`, `tipbon_status`) VALUES
	(1, 'ADESÃO', 1),
	(2, 'ATIVAÇÃO', 1),
	(3, 'RECOMPRA', 0);
/*!40000 ALTER TABLE `tipo_bonus` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.tipo_conta_banco
CREATE TABLE IF NOT EXISTS `tipo_conta_banco` (
  `tipcba_id` int(11) NOT NULL AUTO_INCREMENT,
  `tipcba_nome` varchar(50) NOT NULL,
  PRIMARY KEY (`tipcba_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.tipo_conta_banco: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_conta_banco` DISABLE KEYS */;
INSERT INTO `tipo_conta_banco` (`tipcba_id`, `tipcba_nome`) VALUES
	(1, 'Corrente'),
	(2, 'Poupança');
/*!40000 ALTER TABLE `tipo_conta_banco` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.tipo_pedido
CREATE TABLE IF NOT EXISTS `tipo_pedido` (
  `pedtip_id` int(11) NOT NULL,
  `pedtip_descricao` varchar(50) NOT NULL,
  PRIMARY KEY (`pedtip_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.tipo_pedido: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_pedido` DISABLE KEYS */;
INSERT INTO `tipo_pedido` (`pedtip_id`, `pedtip_descricao`) VALUES
	(1, 'ADESÃO'),
	(2, 'RECOMPRA');
/*!40000 ALTER TABLE `tipo_pedido` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.tipo_pessoa
CREATE TABLE IF NOT EXISTS `tipo_pessoa` (
  `tip_id` int(11) NOT NULL AUTO_INCREMENT,
  `tip_descricao` varchar(255) CHARACTER SET latin1 NOT NULL,
  `tip_data_criacao` datetime NOT NULL,
  `tip_data_alteracao` datetime NOT NULL,
  PRIMARY KEY (`tip_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.tipo_pessoa: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_pessoa` DISABLE KEYS */;
INSERT INTO `tipo_pessoa` (`tip_id`, `tip_descricao`, `tip_data_criacao`, `tip_data_alteracao`) VALUES
	(1, 'fisica', '2015-10-23 10:50:27', '2015-10-23 10:50:28'),
	(2, 'juridica', '2015-10-23 10:50:38', '2015-10-23 10:50:38');
/*!40000 ALTER TABLE `tipo_pessoa` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.tipo_telefone
CREATE TABLE IF NOT EXISTS `tipo_telefone` (
  `tip_id` int(11) NOT NULL AUTO_INCREMENT,
  `tip_descricao` varchar(255) NOT NULL,
  `tip_data_criacao` datetime NOT NULL,
  `tip_data_alteracao` datetime NOT NULL,
  PRIMARY KEY (`tip_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.tipo_telefone: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `tipo_telefone` DISABLE KEYS */;
INSERT INTO `tipo_telefone` (`tip_id`, `tip_descricao`, `tip_data_criacao`, `tip_data_alteracao`) VALUES
	(1, 'Telefone', '2015-10-23 10:50:14', '2015-10-23 10:50:16'),
	(2, 'Celular', '2015-10-24 15:12:35', '2015-10-24 15:12:36');
/*!40000 ALTER TABLE `tipo_telefone` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.tokens
CREATE TABLE IF NOT EXISTS `tokens` (
  `tok_id` int(11) NOT NULL AUTO_INCREMENT,
  `tok_salt` varchar(50) CHARACTER SET utf8 NOT NULL,
  `tok_valido` char(1) CHARACTER SET utf8 NOT NULL DEFAULT 'n',
  `tok_idusuario` int(11) NOT NULL,
  `tok_valor` decimal(10,2) NOT NULL,
  `tok_data_criacao` datetime NOT NULL,
  `tok_data_validade` datetime NOT NULL,
  PRIMARY KEY (`tok_id`),
  UNIQUE KEY `tok_salt` (`tok_salt`)
) ENGINE=MyISAM AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Copiando dados para a tabela mobly.tokens: 26 rows
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
INSERT INTO `tokens` (`tok_id`, `tok_salt`, `tok_valido`, `tok_idusuario`, `tok_valor`, `tok_data_criacao`, `tok_data_validade`) VALUES
	(1, '6E6B0BB2C06D0E8A', 's', 1, 1524.00, '2016-05-11 14:05:19', '2016-06-11 00:00:00'),
	(2, 'F3D91600602F0D7B', 's', 1, 1524.00, '2016-05-11 14:07:42', '2016-06-11 00:00:00'),
	(3, 'DB8C85CFB8612D5A', 's', 1, 1524.00, '2016-05-11 14:12:38', '2016-06-11 00:00:00'),
	(4, '0FFA9FD8AA97840A', 's', 1, 1524.00, '2016-05-11 14:13:24', '2016-06-11 00:00:00'),
	(5, 'BEB390A0C9845529', 's', 1, 1524.00, '2016-05-11 14:14:37', '2016-06-11 00:00:00'),
	(6, 'B4DFDDE1E0567478', 's', 1, 1524.00, '2016-05-11 14:16:11', '2016-06-11 00:00:00'),
	(7, '020268F2F94CEF40', 's', 1, 1524.00, '2016-05-11 14:27:04', '2016-06-11 00:00:00'),
	(8, 'F60F6642741743D2', 's', 1, 14.00, '2016-05-11 14:30:15', '2016-06-11 00:00:00'),
	(9, 'E6C963774E4A22BB', 's', 1, 1.00, '2016-05-11 14:37:03', '2016-06-11 00:00:00'),
	(10, '259AEAD50B242B4D', 's', 1, 15.00, '2016-05-11 14:38:32', '2016-06-11 00:00:00'),
	(11, '7639173F4E616AAA', 's', 1, 15.00, '2016-05-11 14:38:40', '2016-06-11 00:00:00'),
	(12, '3E4589AEF64FC794', 's', 1, 15.00, '2016-05-11 14:40:31', '2016-06-11 00:00:00'),
	(13, '3AEA7430C2D0BD25', 's', 1, 15.00, '2016-05-11 14:41:13', '2016-06-11 00:00:00'),
	(14, 'FB34520E9522C75A', 's', 1, 15.00, '2016-05-11 14:41:52', '2016-06-11 00:00:00'),
	(15, '6C96078533E9003B', 's', 1, 15.00, '2016-05-11 14:42:06', '2016-06-11 00:00:00'),
	(16, 'D2EB0930FC77F92D', 's', 1, 15.00, '2016-05-11 14:42:20', '2016-06-11 00:00:00'),
	(17, '12C4EE3A1193A8E4', 's', 1, 15.00, '2016-05-11 14:42:24', '2016-06-11 00:00:00'),
	(18, '9037A2E9F34F56C8', 's', 1, 15.00, '2016-05-11 14:43:00', '2016-06-11 00:00:00'),
	(19, '1DD42FE081FCBFBD', 's', 1, 15.00, '2016-05-11 14:44:02', '2016-06-11 00:00:00'),
	(20, '17577BCDE4CC3062', 's', 1, 1524.00, '2016-05-11 20:58:35', '2016-06-11 00:00:00'),
	(21, '7CC62D0E0EC6AA37', 's', 1, 1524.00, '2016-05-11 21:00:10', '2016-06-11 00:00:00'),
	(22, 'B23F662CDB665FB7', 's', 1, 1524.00, '2016-05-11 21:01:24', '2016-06-11 00:00:00'),
	(23, '1E39763732792C12', 's', 1, 1524.00, '2016-05-11 21:02:19', '2016-06-11 00:00:00'),
	(24, '9F62363050B752D3', 's', 1, 1525.00, '2016-05-11 21:03:58', '2016-06-11 00:00:00'),
	(25, '7A9A87101919E2EB', 's', 1, 1524.00, '2016-05-11 21:06:01', '2016-06-11 00:00:00'),
	(26, '39C239652A926CB1', 's', 1, 15.00, '2016-05-11 21:10:18', '2016-06-11 00:00:00');
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.ultima_ativacao
CREATE TABLE IF NOT EXISTS `ultima_ativacao` (
  `ultati_id` int(11) NOT NULL AUTO_INCREMENT,
  `ultati_idusuario` int(11) NOT NULL DEFAULT '0',
  `ultati_idlicenca` int(11) NOT NULL DEFAULT '0',
  `ultati_data` datetime DEFAULT NULL,
  `ultati_data_validade` date DEFAULT NULL,
  PRIMARY KEY (`ultati_id`),
  KEY `fk_usuario_idusuario` (`ultati_idusuario`),
  KEY `fk_licensa_idlicensa` (`ultati_idlicenca`),
  CONSTRAINT `fk_licensa_idlicensa` FOREIGN KEY (`ultati_idlicenca`) REFERENCES `licencas` (`lic_id`),
  CONSTRAINT `fk_usuario_idusuario` FOREIGN KEY (`ultati_idusuario`) REFERENCES `usuario_mmn` (`usu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.ultima_ativacao: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `ultima_ativacao` DISABLE KEYS */;
INSERT INTO `ultima_ativacao` (`ultati_id`, `ultati_idusuario`, `ultati_idlicenca`, `ultati_data`, `ultati_data_validade`) VALUES
	(2, 17, 3, '2016-02-01 21:25:58', '2016-03-01'),
	(3, 18, 3, '2016-02-01 21:26:07', '2016-03-01'),
	(4, 20, 3, '2016-02-01 21:26:23', '2016-03-01'),
	(6, 21, 2, '2016-02-11 09:46:08', '2016-03-11'),
	(7, 22, 3, '2016-02-11 09:46:16', '2016-03-11'),
	(8, 23, 3, '2016-02-19 10:55:26', '2016-03-19'),
	(9, 24, 3, '2016-02-19 13:00:06', '2016-03-19'),
	(11, 19, 3, '2016-03-02 11:38:46', '2016-04-02'),
	(12, 16, 3, '2016-04-06 12:54:52', '2016-05-02');
/*!40000 ALTER TABLE `ultima_ativacao` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.usuario
CREATE TABLE IF NOT EXISTS `usuario` (
  `usu_id` int(11) NOT NULL AUTO_INCREMENT,
  `usu_idempresa` int(11) NOT NULL DEFAULT '0',
  `usu_nome` varchar(100) NOT NULL,
  `usu_email` varchar(100) NOT NULL,
  `usu_senha` varchar(300) NOT NULL,
  `usu_status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`usu_id`),
  KEY `usu_senha` (`usu_senha`(255)),
  KEY `usu_email` (`usu_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.usuario: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.usuario_admin
CREATE TABLE IF NOT EXISTS `usuario_admin` (
  `usu_id` int(11) NOT NULL AUTO_INCREMENT,
  `usu_pai_id` int(11) DEFAULT NULL,
  `usu_login` varchar(255) NOT NULL,
  `usu_pessoa_id` int(11) NOT NULL,
  `usu_email` varchar(255) NOT NULL,
  `usu_senha` varchar(255) NOT NULL,
  `usu_status` int(11) NOT NULL,
  `usu_salt` varchar(255) NOT NULL,
  `usu_chave_ativacao` varchar(255) NOT NULL,
  `usu_data_criacao` datetime NOT NULL,
  `usu_data_alteracao` datetime NOT NULL,
  PRIMARY KEY (`usu_id`),
  KEY `FK_usuario_admin_pessoa_pes_id` (`usu_pessoa_id`),
  KEY `FK_usuario_admin_usuario_pai` (`usu_pai_id`),
  CONSTRAINT `FK_usuario_admin_pessoa_pes_id` FOREIGN KEY (`usu_pessoa_id`) REFERENCES `pessoa` (`pes_id`),
  CONSTRAINT `FK_usuario_admin_usuario_pai` FOREIGN KEY (`usu_pai_id`) REFERENCES `usuario_admin` (`usu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.usuario_admin: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario_admin` DISABLE KEYS */;
INSERT INTO `usuario_admin` (`usu_id`, `usu_pai_id`, `usu_login`, `usu_pessoa_id`, `usu_email`, `usu_senha`, `usu_status`, `usu_salt`, `usu_chave_ativacao`, `usu_data_criacao`, `usu_data_alteracao`) VALUES
	(1, NULL, 'admin', 1, 'user1@mail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'kykyky1', 'kxkxkx1', '2015-10-23 15:03:55', '2015-10-23 15:03:55');
/*!40000 ALTER TABLE `usuario_admin` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.usuario_mmn
CREATE TABLE IF NOT EXISTS `usuario_mmn` (
  `usu_id` int(11) NOT NULL AUTO_INCREMENT,
  `usu_admin` int(11) DEFAULT '0',
  `usu_pai_id` int(11) DEFAULT NULL,
  `usu_login` varchar(255) NOT NULL,
  `usu_pessoa_id` int(11) NOT NULL,
  `usu_email` varchar(255) NOT NULL,
  `usu_senha` varchar(255) NOT NULL,
  `usu_status` int(11) NOT NULL,
  `usu_salt` varchar(255) NOT NULL,
  `usu_chave_ativacao` varchar(255) NOT NULL,
  `usu_data_criacao` datetime NOT NULL,
  `usu_data_alteracao` datetime NOT NULL,
  PRIMARY KEY (`usu_id`),
  KEY `FK_usuario_mmn_pessoa_pes_id` (`usu_pessoa_id`),
  KEY `FK_usuario_mmn_usuario_pai` (`usu_pai_id`),
  CONSTRAINT `FK_usuario_mmn_pessoa_pes_id` FOREIGN KEY (`usu_pessoa_id`) REFERENCES `pessoa` (`pes_id`),
  CONSTRAINT `FK_usuario_mmn_usuario_pai` FOREIGN KEY (`usu_pai_id`) REFERENCES `usuario_mmn` (`usu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- Copiando dados para a tabela mobly.usuario_mmn: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `usuario_mmn` DISABLE KEYS */;
INSERT INTO `usuario_mmn` (`usu_id`, `usu_admin`, `usu_pai_id`, `usu_login`, `usu_pessoa_id`, `usu_email`, `usu_senha`, `usu_status`, `usu_salt`, `usu_chave_ativacao`, `usu_data_criacao`, `usu_data_alteracao`) VALUES
	(1, 1, NULL, 'admin', 1, 'admin@feirarecife.com.br', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'segredo', 'segredo', '2015-12-09 18:55:21', '2015-12-09 18:55:22'),
	(16, 0, 1, 'cleitoncosta', 16, 'cleitinho.costa@hotmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'cleiton pereira costa', 'Cp87283252', '2016-02-01 20:54:05', '2016-02-11 22:59:21'),
	(17, 0, 1, 'flavioalmeida', 17, 'flavio82@hotmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'Flavio correia de almeida', '12345', '2016-02-01 21:00:31', '2016-02-01 21:00:31'),
	(18, 0, 1, 'ismaelwatson', 18, 'ismaelwatson231@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'Ismaell watson xavier moreira', '12345', '2016-02-01 21:08:40', '2016-02-01 21:08:40'),
	(19, 0, 1, 'danielasoares', 19, 'danisoaressilva@hotmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'Daniela Soares da silva', '12345', '2016-02-01 21:15:51', '2016-02-01 21:15:51'),
	(20, 0, 1, 'marlos', 20, 'marlosdamasio@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'marlos vinicius', '12345', '2016-02-01 21:23:02', '2016-02-01 21:23:02'),
	(21, 0, 16, 'Pedrofelix', 21, 'Cleitinho.costa@hotmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'Ivaldo Pedro Felix', '197861', '2016-02-03 16:03:31', '2016-02-03 16:03:31'),
	(22, 0, 16, 'Franciscoandrade', 22, 'Andrade177@hotmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'Francisco Pereira de Andrade', '2105fpa', '2016-02-03 17:02:53', '2016-02-03 17:02:53'),
	(23, 0, 1, 'joãosilva', 23, 'feirarecife@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'joão silva', '12345', '2016-02-19 10:54:34', '2016-02-19 10:54:34'),
	(24, 0, 16, 'kmaria', 24, 'zaetucgs@getairmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'karol maria dos santos', '123456', '2016-02-19 12:49:48', '2016-02-19 12:49:48'),
	(36, 0, 1, 'userteste', 36, 'userteste@email.com.br', '81dc9bdb52d04dc20036dbd8313ed055', 99, 'User Teste teste', '123456', '2016-04-06 17:31:49', '2016-04-06 17:31:49'),
	(42, 0, NULL, 'ususus', 13, 'user@email.com.br', 'd41d8cd98f00b204e9800998ecf8427e', 1, 'paulo antunes', '', '2016-07-10 19:21:29', '2016-07-10 19:21:29'),
	(43, 0, NULL, 'claudialeite', 14, 'claudia@email.com.br', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'claudia leite', '1234', '2016-07-10 19:28:32', '2016-07-10 19:28:32'),
	(44, 0, NULL, 'paducado', 15, 'padu@email.com.br', '81dc9bdb52d04dc20036dbd8313ed055', 1, 'Canu Pado', '1234', '2016-07-10 19:53:13', '2016-07-10 19:53:13');
/*!40000 ALTER TABLE `usuario_mmn` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.vitrine
CREATE TABLE IF NOT EXISTS `vitrine` (
  `vit_id` int(11) NOT NULL AUTO_INCREMENT,
  `vit_idempresa` int(11) NOT NULL DEFAULT '0',
  `vit_nome` varchar(100) NOT NULL,
  `vit_ativa` char(1) NOT NULL DEFAULT 'A',
  `vit_data_inicio` date DEFAULT NULL,
  `vit_data_final` date DEFAULT NULL,
  `vit_valor` decimal(10,2) unsigned DEFAULT NULL,
  PRIMARY KEY (`vit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.vitrine: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `vitrine` DISABLE KEYS */;
/*!40000 ALTER TABLE `vitrine` ENABLE KEYS */;


-- Copiando estrutura para tabela mobly.vitrine_produto
CREATE TABLE IF NOT EXISTS `vitrine_produto` (
  `vitp_id` int(11) NOT NULL AUTO_INCREMENT,
  `vitp_idempresa` int(11) NOT NULL DEFAULT '0',
  `vitp_ordem` int(11) NOT NULL,
  `vitp_idproduto` int(11) NOT NULL,
  `vitp_idvitrine` int(11) NOT NULL,
  PRIMARY KEY (`vitp_id`,`vitp_idproduto`,`vitp_idvitrine`),
  KEY `fk_vitrine_produto_produto_idproduto` (`vitp_idproduto`),
  KEY `fk_vitrine_produto_produto_idvitrine` (`vitp_idvitrine`),
  CONSTRAINT `fk_vitrine_produto_produto_idproduto` FOREIGN KEY (`vitp_idproduto`) REFERENCES `produto` (`prod_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_vitrine_produto_produto_idvitrine` FOREIGN KEY (`vitp_idvitrine`) REFERENCES `vitrine` (`vit_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela mobly.vitrine_produto: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `vitrine_produto` DISABLE KEYS */;
/*!40000 ALTER TABLE `vitrine_produto` ENABLE KEYS */;


-- Copiando estrutura para procedure mobly.categoria_adicionar
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `categoria_adicionar`(IN `i_id` INT, IN `p_parent` INT, IN `i_nome` VARCHAR(255), IN `i_url` VARCHAR(255))
proc: BEGIN

	DECLARE e_no_id CONDITION FOR SQLSTATE '45000';
	DECLARE e_id_found  CONDITION FOR SQLSTATE '45000';
	DECLARE v_id_found INT DEFAULT NULL;
	DECLARE e_no_parent CONDITION FOR SQLSTATE '45000';
	DECLARE e_parent_not_found  CONDITION FOR SQLSTATE '45000';
	DECLARE v_parent_found INT DEFAULT NULL;
	DECLARE v_parent_lft INT DEFAULT NULL;
	DECLARE v_parent_rht INT DEFAULT NULL;
	DECLARE v_parent_lvl MEDIUMINT DEFAULT NULL;
	DECLARE v_new_id INT DEFAULT NULL;

	SELECT cat_id INTO v_id_found FROM m2n_loja_categorias WHERE cat_id = i_id;

	IF (v_id_found IS NOT NULL) THEN
		SIGNAL e_id_found
			SET MESSAGE_TEXT = 'id já existe.';
		LEAVE proc;
	END IF;

	IF (p_parent IS NULL) THEN
		SIGNAL e_no_parent
			SET MESSAGE_TEXT = 'O parent não pode ser vazio.';
		LEAVE proc;
	END IF;

	SELECT cat_id INTO v_parent_found FROM m2n_loja_categorias WHERE cat_id = p_parent;

	IF (v_parent_found IS NULL) THEN
		SIGNAL e_parent_not_found
			SET MESSAGE_TEXT = 'Pai não existe.';
		LEAVE proc;
	END IF;

	SELECT cat_id, cat_lft , cat_rht, cat_lvl
	INTO v_parent_found, v_parent_lft, v_parent_rht, v_parent_lvl
	FROM m2n_loja_categorias
	WHERE cat_id = p_parent;

	START TRANSACTION;

	UPDATE m2n_loja_categorias
	SET cat_lft = CASE WHEN cat_lft >  v_parent_rht THEN cat_lft + 2 ELSE cat_lft END,
		cat_rht = CASE WHEN cat_rht >= v_parent_rht THEN cat_rht + 2 ELSE cat_rht END
  	 WHERE cat_rht >= v_parent_rht;

	INSERT INTO m2n_loja_categorias (cat_id, cat_status, cat_nome, cat_url, cat_parent, cat_lft, cat_rht, cat_lvl)
	VALUES (i_id, 1, i_nome, i_url, p_parent, v_parent_rht, v_parent_rht + 1, v_parent_lvl + 1);

	SELECT LAST_INSERT_id() INTO v_new_id;

COMMIT;
END//
DELIMITER ;


-- Copiando estrutura para procedure mobly.categoria_editar
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `categoria_editar`(IN `i_id` INT, IN `p_parent` INT, IN `i_nome` LONGTEXT, IN `i_url` LONGTEXT)
BEGIN
  DECLARE origin_lft INT;
  DECLARE origin_rht  INT;
  DECLARE origin_lvl  INT;
  DECLARE p_parent_rht INT;
  DECLARE p_parent_lvl INT;

  SELECT a.cat_rht, a.cat_lvl, b.cat_lft, b.cat_rht, b.cat_lvl
  INTO p_parent_rht, p_parent_lvl, origin_lft, origin_rht, origin_lvl
  FROM m2n_loja_categorias a JOIN m2n_loja_categorias b
  	ON a.cat_id = p_parent or b.cat_id = i_id
  WHERE a.cat_id = p_parent  AND b.cat_id = i_id;

 	UPDATE m2n_loja_categorias
	   SET 	cat_nome = i_nome,
	   		cat_url = i_url
  	 WHERE cat_id = i_id;

  IF p_parent_rht < origin_lft THEN
 	UPDATE m2n_loja_categorias
	   SET cat_parent = p_parent
  	 WHERE cat_id = i_id;

    UPDATE m2n_loja_categorias SET
      cat_lvl =  cat_lvl +
      		CASE
      			WHEN cat_lft BETWEEN origin_lft AND origin_rht THEN
					  CASE
					    WHEN p_parent_lvl >= origin_lvl THEN
      			      (p_parent_lvl - origin_lvl) + 1
      			    WHEN p_parent_lvl < origin_lvl THEN
      			      - (origin_lvl - p_parent_lvl) + 1
      			    ELSE (p_parent_lvl - origin_lvl) + 1 END
      		ELSE 0 END,
		cat_lft = cat_lft +
            CASE
               WHEN cat_lft BETWEEN origin_lft
				    AND origin_rht  THEN
                    p_parent_rht - origin_lft
               WHEN cat_lft BETWEEN p_parent_rht
				    AND origin_lft - 1 THEN
                    origin_rht  - origin_lft + 1
            ELSE 0 END,
      cat_rht = cat_rht +
            CASE
               WHEN cat_rht BETWEEN origin_lft
				    AND origin_rht  THEN
                    p_parent_rht - origin_lft
               WHEN cat_rht BETWEEN p_parent_rht
				    AND origin_lft - 1 THEN
                    origin_rht  - origin_lft + 1
            ELSE 0 END
	    WHERE cat_lft BETWEEN p_parent_rht
			AND origin_rht
	       OR cat_rht BETWEEN p_parent_rht
			AND origin_rht ;

	  ELSEIF p_parent_rht > origin_rht  THEN
	 	UPDATE m2n_loja_categorias
		   SET cat_parent = p_parent
	  	 WHERE cat_id = i_id;
	    UPDATE m2n_loja_categorias SET
	      cat_lvl =  cat_lvl +
	      		CASE
	      			WHEN cat_lft BETWEEN origin_lft AND origin_rht THEN
						  CASE
						    WHEN p_parent_lvl >= origin_lvl THEN
	      			      (p_parent_lvl - origin_lvl) + 1
	      			    WHEN p_parent_lvl < origin_lvl THEN
	      			      - (origin_lvl - p_parent_lvl) + 1
	      			    ELSE (p_parent_lvl - origin_lvl) + 1 END
   				ELSE 0 END,
	      cat_lft = cat_lft + CASE
	        WHEN cat_lft BETWEEN origin_lft
			   AND origin_rht  THEN
	          p_parent_rht -origin_rht  - 1
	        WHEN cat_lft BETWEEN origin_rht  + 1
			   AND p_parent_rht - 1 THEN
	          origin_lft -origin_rht  - 1
	        ELSE 0 END,
	      cat_rht = cat_rht + CASE
	        WHEN cat_rht BETWEEN origin_lft
			   AND origin_rht  THEN
	          p_parent_rht -origin_rht  - 1
	        WHEN cat_rht BETWEEN origin_rht  + 1
			   AND p_parent_rht - 1 THEN
	          origin_lft -origin_rht  - 1
	        ELSE 0 END
	    WHERE cat_lft BETWEEN origin_lft
		   AND p_parent_rht
	       OR cat_rht BETWEEN origin_lft
			AND p_parent_rht;
	  ELSE
	    SELECT 'Cannot move a subtree to itself, infinite recursion';
	  END IF;
END//
DELIMITER ;


-- Copiando estrutura para procedure mobly.categoria_excluir
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `categoria_excluir`(IN `node` INTEGER)
proc: BEGIN
	DECLARE v_id_found INT DEFAULT NULL;
	DECLARE v_id_parent INTEGER DEFAULT 0;
	DECLARE e_no_id CONDITION FOR SQLSTATE '45000';
	DECLARE thisLeft, thisRight INTEGER DEFAULT 0;
	DECLARE isSubTree INTEGER DEFAULT 0;

	SELECT cat_id, cat_parent INTO v_id_found, v_id_parent FROM m2n_loja_categorias WHERE cat_id = node;

	IF ( v_id_found IS NULL) THEN
		SIGNAL e_no_id
			SET MESSAGE_TEXT = 'O id não existe.';
		LEAVE proc;
	END IF;

	SELECT cat_lft, cat_rht, cat_rht - cat_lft
	INTO thisLeft, thisRight, isSubTree
	FROM m2n_loja_categorias
	WHERE cat_id = node;

	IF isSubTree > 1 THEN
		UPDATE m2n_loja_categorias
		SET cat_lvl = cat_lvl - 1
		WHERE cat_lft BETWEEN thisLeft AND thisRight;
	END IF;

	BEGIN
		UPDATE m2n_loja_categorias
		SET cat_parent = v_id_parent
		WHERE cat_parent = node;

		UPDATE m2n_loja_categorias
		SET cat_lft = cat_lft - 1, cat_rht = cat_rht - 1
		WHERE cat_lft BETWEEN thisLeft AND thisRight;

		UPDATE m2n_loja_categorias
		SET cat_rht = cat_rht - 2
		WHERE cat_rht > thisRight;

		UPDATE m2n_loja_categorias
		SET cat_lft = cat_lft - 2
		WHERE cat_lft > thisRight;

		DELETE FROM m2n_loja_categorias
		WHERE cat_id = node;
    END;
END//
DELIMITER ;


-- Copiando estrutura para procedure mobly.categoria_listar
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `categoria_listar`(IN `p_id` INT, IN `p_depth` TINYINT, IN `p_indent` VARCHAR(20))
proc: BEGIN

	DECLARE e_no_id CONDITION FOR SQLSTATE '45000';
	DECLARE e_id_not_found  CONDITION FOR SQLSTATE '45000';
	DECLARE v_node_found INT DEFAULT NULL;
	DECLARE v_node_lft INT DEFAULT NULL;
	DECLARE v_node_rht INT DEFAULT NULL;
	DECLARE v_node_lvl MEDIUMINT DEFAULT NULL;
	DECLARE v_indent VARCHAR(20) DEFAULT NULL;
	DECLARE v_depth TINYINT DEFAULT 127;

	IF ( p_id IS NULL) THEN
		LEAVE proc;
	END IF;

	SELECT cat_id, cat_lft, cat_rht, cat_lvl
	INTO v_node_found, v_node_lft, v_node_rht, v_node_lvl
	FROM m2n_loja_categorias
	WHERE cat_id = p_id;

	IF ( v_node_found IS NULL) THEN
		LEAVE proc;
	END IF;

	SET v_indent := COALESCE( p_indent, '');
	SET v_depth  := COALESCE( p_depth, v_depth);

	SELECT  cat_id AS id_categoria
		, 	cat_status AS status
		, 	UPPER(cat_nome) AS nome
		, 	cat_url AS url
		, 	cat_parent AS id_pai
		, 	cat_lvl AS nivel
		, 	FORMAT((((cat_rht - cat_lft) -1) / 2),0) AS total
	FROM m2n_loja_categorias
	WHERE 	cat_lft >= v_node_lft
		AND cat_lft <  v_node_rht
		AND cat_lvl <= v_node_lvl + v_depth
	ORDER BY cat_lft;

END//
DELIMITER ;


-- Copiando estrutura para procedure mobly.geraAtivavoes
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `geraAtivavoes`()
proc: BEGIN

	set @count = 1;
	set @valor = floor(1+(rand()*(1001-1)));
	set @usu = floor(1+(rand()*(101-1)));
	
	WHILE @count <= 50 DO
		
		set @valor = floor(1+(rand()*(1001-1)));
		set @usu = floor(1+(rand()*(101-1)));
		
		insert into ativacoes values (null, 1, @usu, 1, @valor, now(), date(now()));
		
		set @count = @count+1;
		
	END WHILE;

END//
DELIMITER ;


-- Copiando estrutura para procedure mobly.gera_dados
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `gera_dados`()
BEGIN
	DECLARE cont INT DEFAULT 168;
	
	WHILE cont <= 1000 DO
		
		INSERT INTO produto (prod_id, prod_nome, prod_valor) SELECT cont, CONCAT("prod-", cont), ( floor(100+(rand()*(10000-100))) );
	
		INSERT INTO produto_departamento select cont, (SELECT dep_id FROM departamento ORDER BY RAND() LIMIT 1);
		
		INSERT INTO sku select NULL, '', 100, cont, NULL;
		
		SET cont = cont + 1;
	END WHILE;
	
END//
DELIMITER ;


-- Copiando estrutura para procedure mobly.gera_dados_aleatorios
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `gera_dados_aleatorios`()
proc: BEGIN

	set @count = 1;
	set @pes = 'usuario';
	set @end = 'Rua End';
	set @tel = '0000000000';
	set @rand = null;
	
	WHILE @count <= 100 DO
	
		INSERT INTO pessoa values (@count, concat('user',@count),concat('user',@count,'mail.com'),'0000',concat('0000000000',@count), 1,
									'2000-01-01', concat('mae',@count), concat('pai',@count), now(), now());
		INSERT INTO endereco values (@count, concat(@end,@count), @count, null, '0000000', concat('bairro',@count),
									concat('cidade',@count), 1, 'Brasil', now(), now());
		INSERT INTO telefone values (@count, @tel, 1, 'tipo1', now(), now());
		INSERT INTO pessoa_endereco values (@count, @count, @count, now(), now());
		INSERT INTO pessoa_telefone values (@count, @count, @count, now(), now());
		
		if @count > 1 then
			set @rand = floor(1+(rand()*(@count-1)));
			INSERT INTO usuario_mmn values (@count, @rand, concat('user',@count), @count, concat('user',@count,'mail.com'),
				md5('segredo'), 1, concat('kykyky',@count), concat('kxkxkx',@count), now(), now());
			call rede_adicionar(@count, @rand);
		else 
			INSERT INTO usuario_mmn values (@count, null, concat('user',@count), @count, concat('user',@count,'mail.com'),
				md5('segredo'), 1, concat('kykyky',@count), concat('kxkxkx',@count), now(), now());
		end if;
		
		set @count = @count+1;
		
	END WHILE;

END//
DELIMITER ;


-- Copiando estrutura para procedure mobly.proc_gera_bonus_por_nivel
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_gera_bonus_por_nivel`(IN `id_user` INT, IN `tipo_bonus` INT, IN `valor_bonus` DECIMAL(10,2), IN `log` VARCHAR(255))
proc: BEGIN

   SET @id_user = id_user;
   SET @tipo_bonus = tipo_bonus;
   SET @valor_bonus = valor_bonus;
   SET @log = log;

	INSERT INTO saldos 
	(
			 sal_idusuario
		  , sal_idtipobonus
		  , sal_valor
		  , sal_descricao
		  , sal_data_criacao
		  , sal_inicio_vigencia
	)
	SELECT p.red_idusuario
	     , @tipo_bonus
		  , (@valor_bonus*bpn_percentual)
		  , CONCAT(@log, ' (NÍVEL - ', (f.red_lvl-p.red_lvl), ')')
		  , NOW()
		  , NOW()
	  FROM redes AS p
	  JOIN redes AS f
	    ON p.red_lft < f.red_lft 
	   AND p.red_rht > f.red_rht 
	  JOIN ultima_ativacao 
	    ON p.red_idusuario = ultati_idusuario
	  JOIN bonificacao_por_nivel 
	  	 ON (f.red_lvl-p.red_lvl) = bpn_nivel 
		AND bpn_idlicenca = ultati_idlicenca
		AND bpn_percentual > 0
	  JOIN tipo_bonus
	  	 ON tipbon_id = bpn_tipo_bonus
	  	AND tipbon_status = 1
	 WHERE f.red_idusuario = @id_user
	   AND bpn_tipo_bonus = @tipo_bonus
	 ORDER BY p.red_rht ASC;

END//
DELIMITER ;


-- Copiando estrutura para procedure mobly.rede_adicionar
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rede_adicionar`(IN `i_id` INT, IN `p_parent` INT)
proc: BEGIN

	DECLARE e_no_id CONDITION FOR SQLSTATE '45000';
	DECLARE e_id_found  CONDITION FOR SQLSTATE '45000';
	DECLARE v_id_found INT DEFAULT NULL;
	DECLARE e_no_parent CONDITION FOR SQLSTATE '45000';
	DECLARE e_parent_not_found  CONDITION FOR SQLSTATE '45000';
	DECLARE v_parent_found INT DEFAULT NULL;
	DECLARE v_parent_lft INT DEFAULT NULL;
	DECLARE v_parent_rht INT DEFAULT NULL;
	DECLARE v_parent_lvl MEDIUMINT DEFAULT NULL;
	DECLARE v_new_id INT DEFAULT NULL;


	IF ( i_id IS NULL ) THEN
		LEAVE proc;
	END IF;

	IF ( p_parent IS NULL ) THEN
		LEAVE proc;
	END IF;

	SELECT red_idusuario INTO v_id_found FROM redes WHERE red_idusuario = i_id;

	IF ( v_id_found IS NOT NULL ) THEN
		LEAVE proc;
	END IF;

	SELECT red_idusuario, red_lft , red_rht ,red_lvl 
		INTO v_parent_found, v_parent_lft, v_parent_rht, v_parent_lvl 
	 FROM redes WHERE red_idusuario = p_parent;

	IF ( v_parent_found IS NULL ) THEN
		LEAVE proc;
	END IF;

	#START TRANSACTION;

		UPDATE redes
		SET  red_lft = CASE WHEN red_lft >  v_parent_rht THEN red_lft + 2 ELSE red_lft END
			 ,red_rht = CASE WHEN red_rht >= v_parent_rht THEN red_rht + 2 ELSE red_rht END
		WHERE  red_rht >= v_parent_rht;
	
		INSERT INTO redes (red_idusuario, red_parent, red_lft, red_rht, red_lvl)
		VALUES (i_id, p_parent, v_parent_rht, v_parent_rht + 1, v_parent_lvl + 1);
	
		SELECT LAST_INSERT_id() INTO v_new_id;

	#COMMIT;
END//
DELIMITER ;


-- Copiando estrutura para procedure mobly.rede_editar
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rede_editar`(IN `i_id` INT, IN `p_parent` INT)
BEGIN
	DECLARE origin_lft INT;
	DECLARE origin_rht  INT;
	DECLARE origin_lvl  INT;
	DECLARE p_parent_rht INT;
	DECLARE p_parent_lvl INT;

	SELECT a.red_rht, a.red_lvl, b.red_lft, b.red_rht, b.red_lvl
	INTO p_parent_rht, p_parent_lvl, origin_lft, origin_rht, origin_lvl
	FROM redes a
	JOIN redes b
	WHERE  a.red_id = p_parent
	   AND b.red_id = i_id;
/*
	UPDATE redes
	SET cat_nome = i_nome, cat_url = i_url
	WHERE id = i_id;
*/
	IF p_parent_rht < origin_lft THEN
		UPDATE redes
		SET red_parent = p_parent
		WHERE red_id = i_id;

		UPDATE redes SET
			red_lvl =  red_lvl +
				CASE
					WHEN red_lft BETWEEN origin_lft AND origin_rht THEN
						CASE
							WHEN p_parent_lvl >= origin_lvl THEN
								(p_parent_lvl - origin_lvl) + 1
							WHEN p_parent_lvl < origin_lvl THEN
								- (origin_lvl - p_parent_lvl) + 1
							ELSE (p_parent_lvl - origin_lvl) + 1
						END
				ELSE 0 END,
			red_lft = red_lft +
				CASE
					WHEN red_lft BETWEEN origin_lft AND origin_rht  THEN
						p_parent_rht - origin_lft
					WHEN red_lft BETWEEN p_parent_rht AND origin_lft - 1 THEN
						origin_rht  - origin_lft + 1
				ELSE 0 END,
			red_rht = red_rht +
				CASE
					WHEN red_rht BETWEEN origin_lft AND origin_rht  THEN
						p_parent_rht - origin_lft
					WHEN red_rht BETWEEN p_parent_rht AND origin_lft - 1 THEN
						origin_rht  - origin_lft + 1
				ELSE 0 END
		WHERE red_lft BETWEEN p_parent_rht
		AND origin_rht
		OR red_rht BETWEEN p_parent_rht
		AND origin_rht ;

	ELSEIF p_parent_rht > origin_rht  THEN
		UPDATE redes
		SET red_parent = p_parent
		WHERE red_id = i_id;
		UPDATE redes SET
			red_lvl =  red_lvl +
				CASE
					WHEN red_lft BETWEEN origin_lft AND origin_rht THEN
						CASE
							WHEN p_parent_lvl >= origin_lvl THEN
								(p_parent_lvl - origin_lvl) + 1
							WHEN p_parent_lvl < origin_lvl THEN
								- (origin_lvl - p_parent_lvl) + 1
							ELSE (p_parent_lvl - origin_lvl) + 1
						END
				ELSE 0 END,
			red_lft = red_lft + CASE
				WHEN red_lft BETWEEN origin_lft AND origin_rht  THEN
					p_parent_rht - origin_rht - 1
				WHEN red_lft BETWEEN origin_rht + 1 AND p_parent_rht - 1 THEN
					origin_lft - origin_rht  - 1
				ELSE 0 END,
			red_rht = red_rht + CASE
				WHEN red_rht BETWEEN origin_lft AND origin_rht  THEN
					p_parent_rht - origin_rht - 1
				WHEN red_rht BETWEEN origin_rht + 1 AND p_parent_rht - 1 THEN
					origin_lft - origin_rht  - 1
			ELSE 0 END
		WHERE red_lft BETWEEN origin_lft
		AND p_parent_rht
		OR red_rht BETWEEN origin_lft
		AND p_parent_rht;
	ELSE
		SELECT 'Não foi possivel mover a categoria.';
	END IF;
END//
DELIMITER ;


-- Copiando estrutura para procedure mobly.rede_excluir
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rede_excluir`(IN `node` INTEGER)
proc: BEGIN
	DECLARE v_id_found INT DEFAULT NULL;
	DECLARE v_id_parent INTEGER DEFAULT 0;
	DECLARE e_no_id CONDITION FOR SQLSTATE '45000';
	DECLARE thisLeft, thisRight INTEGER DEFAULT 0;
	DECLARE isSubTree INTEGER DEFAULT 0;

	SELECT red_id, red_parent INTO v_id_found, v_id_parent FROM redes WHERE red_id = node;

	IF ( v_id_found IS NULL) THEN
		SIGNAL e_no_id
			SET MESSAGE_TEXT = 'O id não existe.';
		LEAVE proc;
	END IF;

	SELECT red_lft, red_rht, red_rht - red_lft
	INTO thisLeft, thisRight, isSubTree
	FROM redes
	WHERE red_id = node;

	IF isSubTree > 1 THEN
		UPDATE redes
		SET red_lvl = red_lvl - 1
		WHERE red_lft BETWEEN thisLeft AND thisRight;
	END IF;

	BEGIN
		UPDATE redes
		SET red_parent = v_id_parent
		WHERE red_parent = node;

		UPDATE redes
		SET red_lft = red_lft - 1, red_rht = red_rht - 1
		WHERE red_lft BETWEEN thisLeft AND thisRight;

		UPDATE redes
		SET red_rht = red_rht - 2
		WHERE red_rht > thisRight;

		UPDATE redes
		SET red_lft = red_lft - 2
		WHERE red_lft > thisRight;

		DELETE FROM redes
		WHERE red_id = node;
    END;
END//
DELIMITER ;


-- Copiando estrutura para procedure mobly.rede_listar
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `rede_listar`(IN `p_id` INT, IN `p_depth` TINYINT, IN `p_indent` VARCHAR(20))
proc: BEGIN

	DECLARE e_no_id CONDITION FOR SQLSTATE '45000';
	DECLARE e_id_not_found  CONDITION FOR SQLSTATE '45000';
	DECLARE v_node_found INT DEFAULT NULL;
	DECLARE v_node_lft INT DEFAULT NULL;
	DECLARE v_node_rht INT DEFAULT NULL;
	DECLARE v_node_lvl MEDIUMINT DEFAULT NULL;
	DECLARE v_indent VARCHAR(20) DEFAULT NULL;
	DECLARE v_depth TINYINT DEFAULT 127;

	IF ( p_id IS NULL) THEN
		LEAVE proc;
	END IF;

	SELECT red_id, red_lft, red_rht, red_lvl
	INTO v_node_found, v_node_lft, v_node_rht, v_node_lvl
	FROM redes
	WHERE red_id = p_id;

	IF ( v_node_found IS NULL) THEN
		LEAVE proc;
	END IF;

	SET v_indent := COALESCE( p_indent, '');
	SET v_depth  := COALESCE( p_depth, v_depth);

	SELECT   r.red_id
			 , u.usu_status AS status
			 , UPPER(u.usu_id) AS usuario
			 , UPPER(u.usu_login) AS login
			 , r.red_parent
			 , CONCAT(REPEAT( v_indent, (r.red_lvl - v_node_lvl) ), r.red_id) AS label
			 , r.red_lvl
			 , FORMAT((((r.red_rht - r.red_lft) -1) / 2),0) AS equipe
	FROM 	redes AS r,
			usuario_mmn AS u
	WHERE 	 r.red_idusuario = u.usu_id
			AND r.red_lft >= v_node_lft
			AND r.red_lft < v_node_rht
			AND r.red_lvl <= v_node_lvl + v_depth
	 ORDER BY r.red_lft;

END//
DELIMITER ;


-- Copiando estrutura para função mobly.gera_salt
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `gera_salt`(`tam` INT) RETURNS varchar(50) CHARSET utf8 COLLATE utf8_unicode_ci
BEGIN
	 RETURN UPPER(SUBSTRING(MD5(RAND()) FROM 1 FOR tam));
	 #RETURN UPPER(SUBSTRING(MD5(RAND()), - tam));
END//
DELIMITER ;


-- Copiando estrutura para função mobly.total_produto
DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `total_produto`(`codproduto` INT) RETURNS int(11)
BEGIN
	DECLARE soma INT;
	
	SELECT SUM(prodest_quantidade) INTO soma
	 FROM produto
	 JOIN produto_estoque 
	 ON prod_id = prodest_idproduto
	 WHERE prod_id = codproduto;
	 
	 RETURN soma;
END//
DELIMITER ;


-- Copiando estrutura para trigger mobly.ai_ativacoes
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `ai_ativacoes` AFTER INSERT ON `ativacoes` FOR EACH ROW BEGIN 

	SET @cotacao = (SELECT cot_valor FROM cotacao 
					 WHERE cot_idlicenca = NEW.ati_idlicenca);

	SET @tx_administracao = (SELECT TAXA_ADMINISTRACAO FROM config_mmn);

 	INSERT INTO saldos_em_ponto (sep_idusuario, sep_cotacao, sep_pontos, sep_descricao, sep_data_criacao, sep_inicio_vigencia)
  		VALUES (NEW.ati_idusuario, @cotacao, (NEW.ati_valor*@cotacao*(1-@tx_administracao)), CONCAT('ativacao usuario - ',NEW.ati_idusuario), NOW(), NOW());
  
	DELETE FROM ultima_ativacao WHERE ultati_idusuario = NEW.ati_idusuario;
	

	INSERT INTO ultima_ativacao (ultati_idusuario, ultati_idlicenca, ultati_data, ultati_data_validade)
		VALUES (NEW.ati_idusuario, NEW.ati_idlicenca, NEW.ati_data_criacao, NEW.ati_data_validade);	
	
	SET @n_ativacoes = (SELECT count(ati_idusuario) FROM ativacoes WHERE ati_idusuario = NEW.ati_idusuario);
	
	IF (@n_ativacoes = 1) THEN
		CALL proc_gera_bonus_por_nivel(NEW.ati_idusuario, 1, NEW.ati_valor, CONCAT('ADESÃO DO USUARIO ', NEW.ati_idusuario));
  	ELSE
    	CALL proc_gera_bonus_por_nivel(NEW.ati_idusuario, 2, NEW.ati_valor, CONCAT('ATIVAÇÃO DO USUARIO ', NEW.ati_idusuario));
	END IF;

END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Copiando estrutura para trigger mobly.ai_licencas
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `ai_licencas` AFTER INSERT ON `licencas` FOR EACH ROW BEGIN 

 INSERT INTO produto 
 	SELECT NULL, NEW.lic_nome, CONCAT('ATIVAÇÃO ', NEW.lic_nome), CONCAT('PRODUTO DE ATIVAÇÃO ', NEW.lic_nome),
	 	NEW.lic_valor, NULL, NULL, NEW.lic_nome, NULL, NULL, NULL, NULL, 1, NULL, 1;

 SET @idproduto = LAST_INSERT_id();	 	
 
 INSERT INTO produto_licenca 
 	SELECT NULL, @idproduto, NEW.lic_id;
 	

END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Copiando estrutura para trigger mobly.atualiza_ordem_vitrine
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_ENGINE_SUBSTITUTION';
DELIMITER //
CREATE TRIGGER `atualiza_ordem_vitrine` BEFORE INSERT ON `vitrine_produto` FOR EACH ROW BEGIN

	DECLARE novaPosicao INT DEFAULT 0;
	
	SELECT IFNULL(MAX(vitp_ordem), 0) INTO novaPosicao
		FROM vitrine_produto
	 WHERE vitp_idvitrine = NEW.vitp_idvitrine;
	 
	SET novaPosicao = novaPosicao + 1;
	
	SET NEW.vitp_ordem = novaPosicao;

END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Copiando estrutura para trigger mobly.au_licencas
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `au_licencas` AFTER UPDATE ON `licencas` FOR EACH ROW BEGIN 

 -- UPDATE produto_licenca SET prolic_idlicenca = NEW.lic_id WHERE prolic_idlicenca = OLD.lic_id;
 
 UPDATE produto 
 	SET prod_nome = NEW.lic_nome
     , prod_resumo = CONCAT('ATIVAÇÃO ', NEW.lic_nome)
     , prod_ficha = CONCAT('PRODUTO DE ATIVAÇÃO ', NEW.lic_nome)
     , prod_valor = NEW.lic_valor
     , prod_urlseo = NEW.lic_nome
 	WHERE prod_id = (SELECT prolic_idproduto FROM produto_licenca WHERE prolic_idlicenca = OLD.lic_id);	

END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;


-- Copiando estrutura para trigger mobly.au_pedidos
SET @OLDTMP_SQL_MODE=@@SQL_MODE, SQL_MODE='';
DELIMITER //
CREATE TRIGGER `au_pedidos` AFTER UPDATE ON `pedidos` FOR EACH ROW BEGIN 
	
	IF NEW.ped_tipo_bonus = 3 THEN 
		IF (OLD.ped_status = 0) AND (NEW.ped_status = 1) THEN
			CALL proc_gera_bonus_por_nivel(NEW.ped_idusu, 3, NEW.ped_total, 
				CONCAT('RECOMPRA - APROVAÇÃO DO PEDIDO ', NEW.ped_id, ' DO USUARIO ', NEW.ped_idusu));
			
			DELETE FROM creditos_aprovisionados WHERE cap_idpedido = OLD.ped_id;
		END IF;
		
		IF (OLD.ped_status = 0) AND (NEW.ped_status = 99) THEN
			
			INSERT INTO saldos_em_ponto (sep_idusuario, sep_cotacao, sep_pontos, sep_descricao, sep_data_criacao)
				SELECT (cap_idusuario, cap_cotacao, cap_pontos, CONCAT('Cancelamento do pedido - ', sep_idpedido), NOW());
			
			DELETE FROM creditos_aprovisionados WHERE cap_idpedido = OLD.ped_id;
		END IF;
	END IF;

END//
DELIMITER ;
SET SQL_MODE=@OLDTMP_SQL_MODE;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
