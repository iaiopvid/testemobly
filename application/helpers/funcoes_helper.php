<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('dateTimeBR')) {
	function dateTimeBR() {
		date_default_timezone_set('America/Sao_Paulo');
		return date('Y-m-d : H:i:s');
	}
}

if (!function_exists('setURL')) {
	function setURL(&$data, $controller) {
		$data['URLLISTAR'] = site_url("admin/{$controller}");
		$data['ACAOFORM']  = site_url("admin/{$controller}/salvar");
	}
}

if (!function_exists('debug')) {
	function debug($data, $die = false) {
		echo "<pre>";
		print_r($data);
		echo "</pre>";
		if ($die) {
			die();
		}
	}
}

if (!function_exists('convertToInt')) {
	function convertToInt($valor) {
		
		if (!$valor) {
			return 0;
		}

		$valor = str_replace('.', NULL, $valor);
		$valor = str_replace(',', '.', $valor);
		return $valor;
	}
}

if (!function_exists('convertToValorDB')) {
	function convertToValorDB($valor) {
		
		if (!$valor) {
			return 0;
		}

		$valor = str_replace('.', NULL, $valor);
		$valor = str_replace(',', '.', $valor);
		return $valor;
	}
}

if (!function_exists('convertToValorBR')) {
	function convertToValorBR($valor) {

		if (!$valor) {
			return 0;
		}
		$valor = number_format($valor, 2, ',', '.');
		
		return $valor;
	}
}

if (!function_exists('convertToValorPesoKg')) {
	function convertToValorPesoKg($valor) {

		if (!$valor) {
			return 0;
		}

		$valor = number_format($valor, 3, ',', '.');
		return $valor;
	}
}

if (!function_exists('dateMySQL2BR2')) {
	function dateMySQL2BR2($valor) {
		$valor = explode("-", $valor);
		$valor = $valor[2].'/'.$valor[1].'/'.$valor[0];
		return $valor;
	}
}

if (!function_exists('dateMySQL2BR')) {
	function dateMySQL2BR($valor) {
		$date = date_create($valor);
		return date_format($date, 'd/m/Y');
	}
}

if (!function_exists('dateBR2MySQL')) {
	function dateBR2MySQL($valor) {
		$valor = explode("/", $valor);
		$valor = $valor[2].'-'.$valor[1].'-'.$valor[0];
		return $valor;
	}
}

if (!function_exists('limparMascara')) {
	function limparMascara($valor) {
		
		$limpo = '';
		$k = 0;
		
		for ($i = 0; $i <= strlen($valor)-1; $i++) {
                    $current = $valor[$i];
                    if (ctype_digit($current) OR ctype_alpha($current)) {
                        $limpo .= $current;
                    }
		}
		return $limpo;
	}
}

if (!function_exists('mascara')) {
	function mascara($valor, $mascara) {
		
		$mascarado = '';
		$k = 0;
		
		for ($i = 0; $i <= strlen($mascara)-1; $i++) {
			if ($mascara[$i] == '#') {
				if (isset($valor[$k])) {
					$mascarado .= $valor[$k++];
				}
			} else {
				if (isset($mascara[$i])) {
					$mascarado .= $mascara[$i];
				}
			}	
		}
		return $mascarado;
	}
}

function validaCPF($cpf) {
  if (! is_numeric ( $cpf )) {
      return false;
  }
  $aCPFsBloqueados = array (
          "00000000000",
          "11111111111",
          "22222222222",
          "33333333333",
          "44444444444",
          "55555555555",
          "66666666666",
          "77777777777",
          "88888888888",
          "99999999999" 
  );
  
  if (in_array ( $cpf, $aCPFsBloqueados )) {
      return false;
  }
  
  // DÍGITO VERIFICADOR
  
  $dv_informado = substr ( $cpf, 9, 2 );
  
  for($i = 0; $i <= 8; $i ++) {
      $digito [$i] = substr ( $cpf, $i, 1 );
  }
  
  // CALCULA O VALOR DO 10º DÍGITO DE VERIFICAÇÃO
  
  $posicao = 10;
  $soma = 0;
  
  for($i = 0; $i <= 8; $i ++) {
      $soma = $soma + $digito [$i] * $posicao;
      $posicao = $posicao - 1;
  }
  
  $digito [9] = $soma % 11;
  
  if ($digito [9] < 2) {
      $digito [9] = 0;
  } else {
      $digito [9] = 11 - $digito [9];
  }
  
  // CALCULA O VALOR DO 11º DÍGITO DE VERIFICAÇÃO
  $posicao = 11;
  $soma = 0;
  
  for($i = 0; $i <= 9; $i ++) {
      $soma = $soma + $digito [$i] * $posicao;
      $posicao = $posicao - 1;
  }
  
  $digito [10] = $soma % 11;
  
  if ($digito [10] < 2) {
      $digito [10] = 0;
  } else {
      $digito [10] = 11 - $digito [10];
  }
  
  $dv = $digito [9] * 10 + $digito [10];
  
  if ($dv != $dv_informado) {
      return false;
  }
  
  return true;
}

if (!function_exists('clienteLogado')) {
	function clienteLogado($redirecionaLogin = false) {
		$CI = &get_instance ();
		
		$sessao = $CI->session->userdata('loja');
		
		if (isset($sessao['cli_id'])) {
			return TRUE;
		} else {
      if ($redirecionaLogin) {
          redirect('conta/login');
      }
			return FALSE;
		}
	}
}

if (!function_exists('montaListaProduto')) {
	function montaListaProduto($produto, $screen = 'lista') {
		$CI = &get_instance ();
		
		$CI->load->model("ProdutoFoto_Model");

        $CI->load->config('configS3');

        $ambiente = $CI->config->config['ambiente'];
        $bucket = $CI->config->config[$ambiente]['bucket'];
        $amazon_url = $CI->config->config['amazon_url'];
        $pasta_produtos = $CI->config->config['produtos'];
        $tamanho_fotos = $CI->config->config['tamanhos'];
		
		$data = array(
			"BLC_LINHA" => array()
		);
		
		$produtosExibidos = 0;
		$coluna = array();

		foreach($produto as $p) {
			
			$filtroFoto = array("p.profot_idproduto" => $p->prod_id);
			
			$foto = $CI->ProdutoFoto_Model->get($filtroFoto, TRUE);
			
			$url = base_url("assets/img/foto-indisponivel.jpg");
			
			if ($foto) {

				// LOCAL
				$url =  base_url ("assets/img/produto/500x500/".$foto->profot_id.".".$foto->profot_extensao);
			}

			$urlFicha = site_url("produto/".$p->prod_id."/".$p->prod_urlseo);

			$precoPromocional = array();
			$valorFinal = $p->prod_valor;

			if ( ($p->prod_valorpromocional > 0) && ($p->prod_valorpromocional < $p->prod_valor) ) {
				$precoPromocional[] = array(
					"VALORANTIGO" => number_format($p->prod_valor, 2, ",", ".")
				);	
							
				$valorFinal = $p->prod_valorpromocional;
			}
			
			$coluna[] = array(
				"URLFOTO" => $url,
				"URLPRODUTO" => $urlFicha,
				"NOMEPRODUTO" => $p->prod_nome,
				"BLC_PRECOPROMOCIONAL" => $precoPromocional,
				"VALOR"	=> number_format($valorFinal, 2, ",", ".")
			);
			
			$produtosExibidos++;
			
			if ($produtosExibidos === 4) {
				$produtosExibidos = 0;
				$data["BLC_LINHA"][] = array(
					"BLC_COLUNA" => $coluna
				);
				
				$coluna = array();
			}
		}
		
		if ($produtosExibidos > 0) {
			$data["BLC_LINHA"][] = array(
				"BLC_COLUNA" => $coluna
			);
		}
		
		$html = $CI->parser->parse($screen, $data, TRUE);
		
		return $html;
	}
}

if (!function_exists('montaListaVerticalProduto')) {
	function montaListaVerticalProduto($produto) {
		$CI = &get_instance ();
		
		$CI->load->model("ProdutoFoto_Model");

        $CI->load->config('configS3');

        $ambiente = $CI->config->config['ambiente'];
        $bucket = $CI->config->config[$ambiente]['bucket'];
        $amazon_url = $CI->config->config['amazon_url'];
        $pasta_produtos = $CI->config->config['produtos'];
        $tamanho_fotos = $CI->config->config['tamanhos'];
		
		$data = array(
			"BLC_LINHA" => array()
		);
		
		$produtosExibidos = 0;
		$coluna = array();
		
		/*$this->load->model("ProdutoFoto_Model");*/
		
		foreach($produto as $p) {
			
			$filtroFoto = array("p.profot_idproduto" => $p->prod_id);
			
			$foto = $CI->ProdutoFoto_Model->get($filtroFoto, TRUE);
			
			$url = base_url("assets/img/foto-indisponivel.jpg");
			
			if ($foto) {

				// LOCAL
				$url =  base_url ( "assets/img/produto/500x500/".$foto->profot_id.".".$foto->profot_extensao);

				// SERVIDOR AMAZON
				// $url =  $amazon_url.'/'.$bucket.'/'.$pasta_produtos.'/'.$tamanho_fotos[2].'/'.$foto->profot_id.".".$foto->profot_extensao;
			}

			$urlFicha = site_url("produto/".$p->prod_id."/".$p->prod_urlseo);

			$precoPromocional = array();
			$valorFinal = $p->prod_valor;

			if ( ($p->prod_valorpromocional > 0) && ($p->prod_valorpromocional < $p->prod_valor) ) {
				$precoPromocional[] = array(
					"VALORANTIGO" => number_format($p->prod_valor, 2, ",", ".")
				);	
							
				$valorFinal = $p->prod_valorpromocional;
			}
			
			$coluna[] = array(
				"URLFOTO" => $url,
				"URLPRODUTO" => $urlFicha,
				"NOMEPRODUTO" => $p->prod_nome,
				"BLC_PRECOPROMOCIONAL" => $precoPromocional,
				"VALOR"	=> number_format($valorFinal, 2, ",", ".")
			);
			
			$produtosExibidos++;
			
			if ($produtosExibidos === 4) {
				$produtosExibidos = 0;
				$data["BLC_LINHA"][] = array(
					"BLC_COLUNA" => $coluna
				);
				
				$coluna = array();
			}
		}
		
		if ($produtosExibidos > 0) {
			$data["BLC_LINHA"][] = array(
				"BLC_COLUNA" => $coluna
			);
		}
//		$this->showDepartamento = FALSE;
		$html = $CI->parser->parse("lista_vertical", $data, TRUE);
		
		return $html;
	}
}


if (!function_exists('getMenu')) {
    function getMenu($menu, $submenu = null) {
        $CI = &get_instance ();
        return $CI->load->view('layouts/menu_backoffice', ['menu_array' => $CI->session->userdata('menu'), 'menu'=> $menu, 'submenu'=> $submenu], TRUE);
    }
}

if (!function_exists('pedidoStatus')) {
	function pedidoStatus($status) {
		$st = '';
    switch ($status) {
        case '0':
            $st = '<span class="label label-warning">Pendente</span>';
            break;
        case '1':
            $st = '<span class="label label-success">Aprovado </span>';
            break;
    }
		return $st;
	}
}

if (!function_exists('pedidoTipo')) {
	function pedidoTipo($tipo) {
		$tp = '';
    switch ($tipo) {
        case '1':
            $tp = '<span class="label label-info">Ativação</span>';
            break;
        
    }
		return $tp;
	}
}

if (!function_exists('creditoDebito')) {
	function creditoDebito($valor) {    
    	return $valor >= 0
    	? '<span class="label label-primary">C</span>'
    	: '<span class="label label-danger">D</span>';
	}
}


if (!function_exists('criaAlerta')) {
	function criaAlerta($mensagem, $tipo, $titulo) 
	{
		$html = "<div class=\"alert alert-{$tipo} alert-dismissable\">\n";
    	$html .= "\t<button aria-hidden=\"true\" data-dismiss=\"alert\" class=\"close\" type=\"button\">×</button>\n";
    	$html .= "\t<strong>{$titulo}! </strong> {$mensagem}.\n";
		$html .= "</div>";
		
		return $html;
	}
}

if (!function_exists('verCarrinho')) {
	function verCarrinho($carrinho)
	{
		$CI = &get_instance ();
		$CI->load->model("Produto_Model");

    $data['QUANTITEM'] = 0;
    $ordemTotal = 0;

    foreach ( $carrinho as $prod_id => $quantidade ) 
    {
        $infoProduto = $CI->Produto_Model->get(array('prod_id' => $prod_id), TRUE);
        
        if (($infoProduto->prod_valorpromocional > 0) && ($infoProduto->prod_valor > $infoProduto->prod_valorpromocional)) {
            $valorFinal = $infoProduto->prod_valorpromocional;
        } else {
            $valorFinal = $infoProduto->prod_valor;
        }

        $valorTotal = $valorFinal * $quantidade;
        
        $referencia = "";
        
        if (! empty ( $infoProduto->prod_resumo )) {
            $referencia = " - " . $infoProduto->prod_resumo;
        }
        
        $urlFoto = ""; 
        
        if (! empty ( $infoProduto->profot_id )) {
            $urlFoto = base_url ( "assets/img/produto/80x80/" . $infoProduto->profot_id . "." . $infoProduto->profot_extensao);
        } else {
            $urlFoto = base_url ( "assets/img/produto/80x80/" . 'indisponivel.jpg' );
        }
        
        $data ["BLC_PRODUTOS"] [] = array (
                "URLFOTO" => $urlFoto,
                "IDPRODUTO" => $infoProduto->prod_id,
                "NOMEPRODUTO" => $infoProduto->prod_nome,
                "NOMEITEM" => $infoProduto->prod_nome,
                "QUANTIDADE" => $quantidade,
                "QTYMAX" => $infoProduto->prodest_quantidade,
                "VALOR" => number_format ( $valorFinal, 2, ",", "." ),
                "VALORTOTAL" => number_format ( $valorTotal, 2, ",", "." ),
                "URLAUMENTAQTD" => site_url ( "checkout/aumenta/" . $infoProduto->prod_id ),
                "URLDIMINUIQTD" => site_url ( "checkout/diminui/" . $infoProduto->prod_id ),
                "URLREMOVEQTD" => site_url ( "checkout/remove/" . $infoProduto->prod_id ) 
        );

        $ordemTotal = $ordemTotal + $valorTotal;
        $data ["TOTALPEDIDO"] = number_format($ordemTotal, 2, ",", "." );
        $data['QUANTITEM']++;
    }
    
    $data ["BLC_FINALIZAR"] [] = array (
        "URLFINALIZAR" => site_url ( 'checkout/formaentrega' ) 
    );

    $data ["URLFINALIZAR"] = site_url ( 'checkout/formaentrega' ) ;

    return $data;
	}
}

