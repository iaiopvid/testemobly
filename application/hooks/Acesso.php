<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acesso
{
    public function ValidarAcesso()
    {
        $this->ci =& get_instance();

        $classe = $this->ci->router->class;
        $metodo = $this->ci->router->method;
        $pasta = $this->ci->router->directory;

        if ($pasta == 'backoffice/') {
            if(!$this->ci->login_business->logged()) {
                redirect('logout');
            } 
            else {
                if (!$this->ci->login_business->checkAtivo()) {
                    redirect('ativacao');
                }
            }

            $this->ci->session->set_userdata('menu', $this->getMenu());
        } 
        if ($pasta == 'ativacao/') {
            if(!$this->ci->login_business->logged()) {
                redirect('logout');
            }
        }

        if ($pasta == 'admin/') {
            if(!$this->ci->login_business->loggedAdmin()) {
                redirect('logoutAdmin');
            } else {
                if (!$this->ci->login_business->checkAtivoAdmin()) {
                    redirect('logoutAdmin');
                }
            }

            $this->ci->session->set_userdata('menu', $this->getMenuAdmin());
        }
    }

    public function getMenu(){

        $this->ci =& get_instance();

        /************************ Admin **************************/
        $menu['Dashboard'] = [
            'display'   => 'Dashboard',
            'url'   => 'backoffice',
            'icon'      => 'fa-th-large',
            'label' => '',
            'submenu'   => []
        ];
        /**************************************************************/
        
        /************************ Conta ***************************/
        $idUsuario = $this->ci->session->userdata('usuario')->usu_id;
        // debug($idUsuario, true);
        $menu['Conta'] = [
            'display'   => 'Minha Conta',
            'url'       => '',
            'icon'      => 'fa-user',
            'label'     => '',
            'submenu'   => []
        ];
        $menu['Conta']['submenu']['Editar'] = [
            'display'   => 'Editar Dados',
            'url'   => 'backoffice/cadastro/editarDadosPessoais/'.$idUsuario,
            'icon'  => ''
        ];
        $menu['Conta']['submenu']['Banco'] = [
            'display'   => 'Dados Bancários',
            'url'   => 'backoffice/cadastro/editarDadosBancarios/'.$idUsuario,
            'icon'  => ''
        ];
        /***************************************************************/
        
        /************************ CADASTROS ***************************/
        $menu['Cadastros'] = [
            'display'   => 'Cadastros',
            'url'       => '',
            'icon'      => 'fa-user-plus',
            'label'     => ''
        ];
        $menu['Cadastros']['submenu']['Cadastrar'] = [
            'display'   => 'Cadastrar Novo',
            'url'   => 'backoffice/cadastro/novo',
            'icon'  => '',
        ];
        $menu['Cadastros']['submenu']['Cadastros_Ativos'] = [
            'display'   => 'Cadastros Clientes',
            'url'   => 'backoffice/cadastro/listarAtivos',
            'icon'  => '',
        ];
        /***************************************************************/
        
        /************************ PEDIDOS ***************************/
        $menu['Pedidos'] = [
            'display'   => 'Pedidos',
            'url'       => '',
            'icon'      => 'fa-cart-plus',
            'label'     => ''
        ];
        $menu['Pedidos']['submenu']['Lista'] = [
            'display'   => 'Meus Pedidos',
            'url'   => 'backoffice/pedidos/lista',
            'icon'  => '',
        ];
        $menu['Pedidos']['submenu']['Novo'] = [
            'display'   => 'Novo Pedido',
            'url'   => 'backoffice/pedidos/novo',
            'icon'  => '',
            'status'  => 'lock',
        ];                                
        /***************************************************************/
        
        /************************ Rede ***************************/
        $menu['Rede'] = [
            'display'   => 'Rede',
            'url'       => '',
            'icon'      => 'fa-sitemap',
            'label'     => '',
            'submenu'   => []
        ];
        $menu['Rede']['submenu']['Unilevel'] = [
            'display'   => 'Minha Rede',
            'url'   => 'backoffice/rede/unilevel',
            'icon'  => ''
        ];
        /***************************************************************/
        
        /************************ TOKEN ***************************/
        $menu['Token'] = [
            'display'   => 'Token',
            'url'       => '',
            'icon'      => 'fa-sitemap',
            'label'     => '',
            'submenu'   => []
        ];
        $menu['Token']['submenu']['Gerar'] = [
            'display'   => 'Gerar token',
            'url'   => 'backoffice/token/gerar',
            'icon'  => ''
        ];
        /***************************************************************/
        
        /************************ Financeiro ***************************/
        $menu['Financeiro'] = [
            'display'   => 'Financeiro',
            'url'       => '',
            'icon'      => 'fa-dollar',
            'label'     => '',
            'submenu'   => []
        ];
        $menu['Financeiro']['submenu']['Saque'] = [
            'display'   => 'Saque',
            'url'   => 'backoffice/financeiro/saque',
            'icon'  => 'fa-lock',
            'status'  => 'lock',
        ];
        $menu['Financeiro']['submenu']['Extrato'] = [
            'display'   => 'Meu Extrato',
            'url'   => 'backoffice/financeiro/extrato',
            'icon'  => '',
        ];
        $menu['Financeiro']['submenu']['creditos'] = [
            'display'   => 'Meus Créditos',
            'url'   => 'backoffice/financeiro/creditos',
            'icon'  => '',
            // 'status'  => 'lock',
        ];
        /***************************************************************/


        return $menu;
    }

    public function getMenuAdmin(){

        /************************ ADMIN **************************/
        $menu['Admin'] = [
            'display'   => 'Administração',
            'url'   => 'admin',
            'icon'      => 'fa-th-large',
            'label' => '',
            'submenu'   => []
        ];
        /**************************************************************/
        
        /************************ PEDIDOS ***************************/
        $menu['Pedidos'] = [
            'display'   => 'Pedidos',
            'url'       => '',
            'icon'      => 'fa-cart-plus',
            'label'     => ''
        ];
        $menu['Pedidos']['submenu']['Lista'] = [
            'display'   => 'Pedidos',
            'url'   => 'admin',
            'icon'  => '',
        ];                     
        /***************************************************************/
        
        /************************ CADASTROS ***************************/
        $menu['Cadastros'] = [
            'display'   => 'Cadastros',
            'url'       => '',
            'icon'      => 'fa-user-plus',
            'label'     => ''
        ];

        $menu['Cadastros']['submenu']['Cadastros_Ativos'] = [
            'display'   => 'Cadastros',
            'url'   => 'admin/cadastro/listarAtivos',
            'icon'  => '',
        ];                    
        /***************************************************************/

        /************************ LOJA ***************************/
        $menu['LOJA'] = [
            'display'   => 'LOJA',
            'url'       => '',
            'icon'      => 'fa-cart-plus',
            'label'     => '',
            'submenu'   => []
        ];
        $menu['LOJA']['submenu']['produtos'] = [
            'display'   => 'Produtos',
            'url'   => 'admin/produto',
            'icon'  => '',
            'status'  => '',
        ];
        $menu['LOJA']['submenu']['Categorias'] = [
            'display'   => 'Categorias',
            'url'   => 'admin/departamento',
            'icon'  => '',
            'status'  => '',
        ];
        /***************************************************************/
        
        return $menu;
    }

    public function getMenuCVD(){

        $this->ci =& get_instance();

        /************************ Admin **************************/
        $menu['Dashboard'] = [
            'display'   => 'Dashboard',
            'url'   => 'pdv',
            'icon'      => 'fa-th-large',
            'label' => '',
            'submenu'   => []
        ];
        /**************************************************************/
        
        /************************ Conta ***************************/
        $idUsuario = $this->ci->session->userdata('usuario_cvd')->cvd_id;
        // debug($idUsuario, true);
        $menu['Conta'] = [
            'display'   => 'Minha Conta',
            'url'       => '',
            'icon'      => 'fa-user',
            'label'     => '',
            'submenu'   => []
        ];
        $menu['Conta']['submenu']['Editar'] = [
            'display'   => 'Editar Dados',
            'url'   => 'cvd/cadastro/editarDadosPessoais/'.$idUsuario,
            'icon'  => ''
        ];
        
        /************************ CADASTROS ***************************/
        $menu['Cadastros'] = [
            'display'   => 'Cadastros',
            'url'       => '',
            'icon'      => 'fa-user-plus',
            'label'     => ''
        ];
        $menu['Cadastros']['submenu']['Cadastrar'] = [
            'display'   => 'Cadastrar Novo',
            'url'   => 'cvd/cadastro/novo',
            'icon'  => '',
        ];
        $menu['Cadastros']['submenu']['Cadastros_Ativos'] = [
            'display'   => 'Cadastros Ativos',
            'url'   => 'cvd/cadastro/listarAtivos',
            'icon'  => '',
        ];
        /***************************************************************/
        
        /************************ PEDIDOS ***************************/
        $menu['Pedidos'] = [
            'display'   => 'Pedidos',
            'url'       => '',
            'icon'      => 'fa-cart-plus',
            'label'     => ''
        ];
        $menu['Pedidos']['submenu']['Lista'] = [
            'display'   => 'Meus Pedidos',
            'url'   => 'cvd/pedidos/lista',
            'icon'  => '',
        ];
        $menu['Pedidos']['submenu']['Novo'] = [
            'display'   => 'Novo Pedido',
            'url'   => 'cvd/pedidos/novo',
            'icon'  => '',
            'status'  => 'lock',
        ];                                
        /***************************************************************/
        
        /************************ TOKEN ***************************/
        $menu['Token'] = [
            'display'   => 'Token',
            'url'       => '',
            'icon'      => 'fa-sitemap',
            'label'     => '',
            'submenu'   => []
        ];
        $menu['Token']['submenu']['Gerar'] = [
            'display'   => 'Gerar token',
            'url'   => 'cvd/token/gerar',
            'icon'  => ''
        ];
        /***************************************************************/
        
        /************************ Financeiro ***************************/
        $menu['Financeiro'] = [
            'display'   => 'Financeiro',
            'url'       => '',
            'icon'      => 'fa-dollar',
            'label'     => '',
            'submenu'   => []
        ];
        $menu['Financeiro']['submenu']['Saque'] = [
            'display'   => 'Saque',
            'url'   => 'cvd/financeiro/saque',
            'icon'  => 'fa-lock',
            'status'  => 'lock',
        ];
        $menu['Financeiro']['submenu']['Extrato'] = [
            'display'   => 'Meu Extrato',
            'url'   => 'cvd/financeiro/extrato',
            'icon'  => '',
        ];
        $menu['Financeiro']['submenu']['creditos'] = [
            'display'   => 'Meus Créditos',
            'url'   => 'cvd/financeiro/creditos',
            'icon'  => '',
            // 'status'  => 'lock',
        ];
        /***************************************************************/


        return $menu;
    }
}