<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Template {
	public function init() {

		$CI = &get_instance();
		$output = $CI->output->get_output();

		if (isset($CI->layout)) {

			if ($CI->layout) {
				
				$layoutOriginal = $CI->layout;



				// if ($layoutOriginal == LAYOUT_TESTE) {
				// 	$sessaoUsuaio = $CI->session->userdata("loginatendimento");

				// 	if (!$sessaoUsuaio) {
				// 		redirect("painel/login");
				// 	}
				// }

				// if ($CI->layout == LAYOUT_TESTE) {
				// 	$erroDash 	  = $CI->session->flashdata('erro');
				// 	$sucessoDash  = $CI->session->flashdata('sucesso');
				// }

				if ($layoutOriginal == LAYOUT_DASHBOARD) {
					$sessaoUsuaio = $CI->session->userdata("loginatendimento");

					if (!$sessaoUsuaio) {
						redirect("painel/login");
					}
				}

				if ($CI->layout == LAYOUT_DASHBOARD) {
					$erroDash 	  = $CI->session->flashdata('erro');
					$sucessoDash  = $CI->session->flashdata('sucesso');
				}

				if (! preg_match ( '/(.+).php$/', $CI->layout )) {
					$CI->layout .= '.php';
				}

				$template = APPPATH . 'templates/' . $CI->layout;

				if (file_exists($template)) {

					$layout = $CI->load->file ( $template, TRUE );

				} else {

					die('Template invalido.');
				}

				$html = str_replace ( "{CONTEUDO}", $output, $layout );
				
				// TRATAMENTO PARA A LOJA
				/*if ($layoutOriginal == LAYOUT_LOJA) {
					$title = isset($CI->title) ? $CI->title : 'Ecommerce - JP';
					
					$html = str_replace ( "{TITLE}", $title, $html );
				}*/
				if ($layoutOriginal == LAYOUT_LOJA) {
					$this->tratamentoLoja($CI, $html);	
					$html = str_replace ( "{SITEURL}", site_url(), $html );
					$erroDash 	  = $CI->session->flashdata('erro');
					$sucessoDash  = $CI->session->flashdata('sucesso');
				} elseif ($layoutOriginal == LAYOUT_DASHBOARD) {
					$this->tratamentoDashboard($CI, $html);
					$html = str_replace ( "{NOMEUSUARIOLOGIN}", $sessaoUsuaio->usu_nome, $html );
					$html = str_replace ( "{SITEURL}", site_url(), $html );
				} elseif ($layoutOriginal == LAYOUT_TESTE) {
					$this->tratamentoTeste($CI, $html);
					$html = str_replace ( "{SITEURL}", site_url(), $html );
					$erroDash 	  = $CI->session->flashdata('erro');
					$sucessoDash  = $CI->session->flashdata('sucesso');
				}

				if ($erroDash) {
					$erroDash = $this->criaAlerta($erroDash, 'alert-error', 'Error');
					$html = str_replace ( "{MENSAGEM_SISTEMA_ERROR}", $erroDash, $html );
				} else {
					$html = str_replace ( "{MENSAGEM_SISTEMA_ERROR}", NULL, $html );
				}

				if ($sucessoDash) {
					$sucessoDash = $this->criaAlerta($sucessoDash, 'alert-success', 'Sucesso');
					$html = str_replace ( "{MENSAGEM_SISTEMA_SUCESSO}", $sucessoDash, $html );
				} else {
					$html = str_replace ( "{MENSAGEM_SISTEMA_SUCESSO}", NULL, $html );
				}

			} else {
          $erroDash = $CI->session->flashdata ( 'erro' );
          
          if ($erroDash) {
              $erroDash = $this->criaAlerta ( $erroDash, 'alert-error', 'Ops' );
              $output = str_replace ( "{MENSAGEM_SISTEMA_ERROR}", $erroDash, $output );
          } else {
              $output = str_replace ( "{MENSAGEM_SISTEMA_ERROR}", null, $output );
          }

				$html = $output;
			}
		} else {
        $erroDash = $CI->session->flashdata ( 'erro' );
        
        if ($erroDash) {
            $erroDash = $this->criaAlerta ( $erroDash, 'alert-error', 'Ops' );
            $output = str_replace ( "{MENSAGEM_SISTEMA_ERROR}", $erroDash, $output );
        } else {
            $output = str_replace ( "{MENSAGEM_SISTEMA_ERROR}", null, $output );
        }

			$html = $output;
		}

		$CI->output->_display($html);
	}

	private function criaAlerta($mensagem, $tipo, $titulo) {
		$html = "<div class=\"alert {$tipo}\">\n";
		$html .= "\t<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>\n";
		$html .= "\t<strong>{$titulo}!</strong> {$mensagem}\n";
		$html .= "</div>";
		return $html;
	}
	
	private function deleteAllBetween($beginning, $end, $string) {
		$beginningPos = strpos($string, $beginning);
		$endPos = strpos($string, $end);

		if (!$beginningPos || !$endPos) {
			return $string;
		}

		$textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);

		return str_replace($textToDelete, '', $string);
	}

	private function tratamentoLoja($CI, &$html) {
		$title = isset($CI->title) ? $CI->title.' - '.'E-commerce OFMG' : 'E-commerce OFMG';

		$this->setVariable("URLBASE", base_url(), $html);
		$this->setVariable("URLSITE", site_url(), $html);
					
		$this->setVariable("TITLE", $title, $html);
		$this->setVariable("URLBUSCA", base_url("busca"), $html);

		// $this->setVariable("NOMECLIENTE", 'Caralho', $html);

		$cliente = $CI->session->userdata('loja');
		// debug($cliente, true);

		if ($cliente) {
			$this->setVariable("NOMECLIENTE", $cliente["cli_nome"], $html);
			$html = $this->deleteAllBetween("<naologado>", "</naologado>", $html);
		} else {
			$this->setVariable("NOMECLIENTE", "Visitante", $html);
			$html = $this->deleteAllBetween("<logado>", "</logado>", $html);
		}
		
		$showDepartamento = isset($CI->showDepartamento) ? $CI->showDepartamento : TRUE;
		
		if ($showDepartamento) {
			$this->setVariable("EXIBEDEPARTAMENTO", NULL, $html);
			$this->setVariable("COLUNACONTEUDO", 10, $html);
		} else {
			$this->setVariable("EXIBEDEPARTAMENTO", 'hide', $html);
			$this->setVariable("COLUNACONTEUDO", 12, $html);
		}
	}

	private function tratamentoDashboard($CI, &$html) {
		$title = isset($CI->title) ? $CI->title.' - '.'E-commerce OFMG' : 'E-commerce OFMG';

		$this->setVariable("URLBASE", base_url(), $html);
		$this->setVariable("URLSITE", site_url(), $html);
	}



	private function tratamentoTeste($CI, &$html) {
		$title = isset($CI->title) ? $CI->title.' - '.'E-commerce OFMG' : 'E-commerce OFMG';

		$this->setVariable("URLBASE", base_url(), $html);
		$this->setVariable("URLSITE", site_url(), $html);
					
		$this->setVariable("TITLE", $title, $html);
		$this->setVariable("URLBUSCA", site_url("busca"), $html);

		// $this->setVariable("NOMECLIENTE", 'Caralho', $html);

		$cliente = $CI->session->userdata('loja');
		// debug($cliente, true);

		if ($cliente) {
			$this->setVariable("NOMECLIENTE", $cliente["cli_nome"], $html);
			$html = $this->deleteAllBetween("<naologado>", "</naologado>", $html);
		} else {
			$this->setVariable("NOMECLIENTE", "Visitante", $html);
			$html = $this->deleteAllBetween("<logado>", "</logado>", $html);
		}
		
		$showDepartamento = isset($CI->showDepartamento) ? $CI->showDepartamento : TRUE;
		
		if ($showDepartamento) {
			$this->setVariable("EXIBEDEPARTAMENTO", NULL, $html);
			$this->setVariable("COLUNACONTEUDO", 10, $html);
		} else {
			$this->setVariable("EXIBEDEPARTAMENTO", 'hide', $html);
			$this->setVariable("COLUNACONTEUDO", 12, $html);
		}
	}
	
	private function setVariable($name, $value, &$html) {
		$html = str_replace("{".$name."}", $value, $html);
	}
}