<!DOCTYPE html>
<html>
<head>
    <title>Frente de Loja</title>
    <meta name="description" content="<?= $meta_description ?>"> 
    <meta name="keywords" content="<?= $meta_keywords ?>">
    <meta http-equiv="expires" content="0" />
    <meta name="classification" content="<?= $meta_classification ?>" />
    <meta name="Robots" content="index,follow">
    <meta name="revisit-after" content="2 Days">
    <meta name="language" content="en-us">
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <title><?= $pageTitle ?></title>
    
    <link href=<?= base_url()."public/inspinia/css/bootstrap.min.css" ?> media="screen" rel="stylesheet" type="text/css">
    <link href=<?= base_url()."public/inspinia/font-awesome/css/font-awesome.css" ?> media="screen" rel="stylesheet" type="text/css">

    <link href=<?= base_url()."public/inspinia/assets/css/plugins/toastr/toastr.min.css" ?> media="screen" rel="stylesheet" type="text/css">


    <!-- Data Tables -->
    <link href=<?= base_url()."public/inspinia/css/plugins/dataTables/dataTables.bootstrap.css" ?> media="screen" rel="stylesheet" type="text/css">
    <link href=<?= base_url()."public/inspinia/css/plugins/dataTables/dataTables.responsive.css" ?> media="screen" rel="stylesheet" type="text/css">
    <link href=<?= base_url()."public/inspinia/css/plugins/dataTables/dataTables.tableTools.min.css" ?> media="screen" rel="stylesheet" type="text/css">


    <link href=<?= base_url()."public/inspinia/css/animate.css" ?> media="screen" rel="stylesheet" type="text/css">
    <link href=<?= base_url()."public/inspinia/css/style.css" ?> media="screen" rel="stylesheet" type="text/css">
    
    <link href=<?= base_url()."public/inspinia/css/plugins/steps/jquery.steps.css" ?> media="screen" rel="stylesheet" type="text/css">
    <link href=<?= base_url()."public/css/custom_style.css" ?> media="screen" rel="stylesheet" type="text/css">
    
    {styles} 
      <link href=<?= base_url()."{url}"?> media="screen" rel="stylesheet" type="text/css">
    {/styles}

    
                
</head>

<body class="pace-done skin-1">
    <div id="wrapper">
        <?php $submenu = isset($submenu) ? $submenu : ''; 
        echo getMenu($menu, $submenu); ?>

        <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
            <!-- <form role="search" class="navbar-form-custom" method="post" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form> -->
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Bem vindo, 
                        <b style="font-size: 1.5em">
                            <?php if($this->uri->segment(1) == 'admin') : ?>
                                <?= strtoupper($this->session->userdata('usuario_admin')->usu_login); ?>
                            <?php elseif($this->uri->segment(1) == 'backoffice') : ?>
                                <?= strtoupper($this->session->userdata('usuario')->usu_login); ?>
                            <?php endif; ?>
                        </b></span>
                </li>
                
                <li>
                    <a href="<?php if($this->uri->segment(1) == 'admin') : ?>
                                <?= base_url("logoutAdmin"); ?>
                            <?php elseif($this->uri->segment(1) == 'backoffice') : ?>
                                <?= base_url("logout"); ?>
                            <?php endif; ?>">
                        <i class="fa fa-sign-out"></i> Sair
                    </a>
                </li>
            </ul>

        </nav>
        </div>
          
        <?= $content_body ?>
          

                <div class="footer">
                    <div>
                        <strong>Desenvolvido pela</strong> Pixel Interface &copy; 2015
                    </div>
                </div>

        </div>
    </div>
    
  <script type="text/javascript" src=<?= base_url()."public/inspinia/js/jquery-2.1.1.js" ?>></script>
  <script type="text/javascript" src=<?= base_url()."public/inspinia/js/bootstrap.min.js" ?>></script>
  <script type="text/javascript" src=<?= base_url()."public/inspinia/js/plugins/metisMenu/jquery.metisMenu.js" ?>></script>
  <script type="text/javascript" src=<?= base_url()."public/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js" ?>></script>
  <script type="text/javascript" src=<?= base_url()."public/inspinia/js/plugins/peity/jquery.peity.min.js" ?>></script>
  <script type="text/javascript" src=<?= base_url()."public/inspinia/js/demo/peity-demo.js" ?>></script>

    <script type="text/javascript" src=<?= base_url()."public/inspinia/js/plugins/jeditable/jquery.jeditable.js" ?>></script>
    <script type="text/javascript" src=<?= base_url()."public/inspinia/assets/js/plugins/toastr/toastr.min.js" ?>></script>
    <!-- Data Tables -->
    <script type="text/javascript" src=<?= base_url()."public/inspinia/js/plugins/dataTables/jquery.dataTables.js" ?>></script>
    <script type="text/javascript" src=<?= base_url()."public/inspinia/js/plugins/dataTables/dataTables.bootstrap.js" ?>></script>
    <script type="text/javascript" src=<?= base_url()."public/inspinia/js/plugins/dataTables/dataTables.responsive.js" ?>></script>
    <script type="text/javascript" src=<?= base_url()."public/inspinia/js/plugins/dataTables/dataTables.tableTools.min.js" ?>></script>

  <script type="text/javascript" src=<?= base_url()."public/inspinia/js/inspinia.js" ?>></script>
  <script type="text/javascript" src=<?= base_url()."public/inspinia/js/plugins/pace/pace.min.js" ?>></script>
  <script type="text/javascript" src=<?= base_url()."public/inspinia/js/plugins/jquery-ui/jquery-ui.min.js" ?>></script>
  <script type="text/javascript" src=<?= base_url()."public/inspinia/js/plugins/gritter/jquery.gritter.min.js" ?>></script>
  <script type="text/javascript" src=<?= base_url()."public/inspinia/js/plugins/sparkline/jquery.sparkline.min.js" ?>></script>
  <script type="text/javascript" src=<?= base_url()."public/inspinia/js/demo/sparkline-demo.js" ?>></script>

  <script type="text/javascript" src=<?= base_url()."public/js/jquery.maskedinput.js" ?>></script>
  <script type="text/javascript" src=<?= base_url()."public/js/plugins/jquery-validation-1.14.0/dist/jquery.validate.min.js" ?>></script>
  <script type="text/javascript" src=<?= base_url()."public/js/plugins/jquery-validation-1.14.0/dist/localization/messages_pt_BR.js" ?>></script>
  <script type="text/javascript" src=<?= base_url()."public/js/mlm.js" ?>></script>
  
  {scripts} 
    <script type="text/javascript" src=<?= base_url()."{url}" ?>></script>
  {/scripts}
  
  <script type="text/javascript">
      var base_url = "<?= base_url();?>";
      $(function(){ 
      });
  </script>

    <style>
        body.DTTT_Print {
            background: #fff;

        }
        .DTTT_Print #page-wrapper {
            margin: 0;
            background:#fff;
        }

        button.DTTT_button, div.DTTT_button, a.DTTT_button {
            border: 1px solid #e7eaec;
            background: #fff;
            color: #676a6c;
            box-shadow: none;
            padding: 6px 8px;
        }
        button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
            border: 1px solid #d2d2d2;
            background: #fff;
            color: #676a6c;
            box-shadow: none;
            padding: 6px 8px;
        }

        .dataTables_filter label {
            margin-right: 5px;

        }
    </style>
    <script>
        $(document).ready(function() {
            $('.dataTables-example').dataTable({
                responsive: true,
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": base_url + "public/js/plugins/copy_csv_xls_pdf/copy_csv_xls_pdf.swf"
                }
            });

            $('.dataTables-extrato').dataTable({
                "ordering": false,
                responsive: true,
                "dom": 'T<"clear">lfrtip',
                "tableTools": {
                    "sSwfPath": base_url + "public/js/plugins/copy_csv_xls_pdf/copy_csv_xls_pdf.swf"
                }
            });

            /* Init DataTables */
            var oTable = $('#editable').dataTable();

            /* Apply the jEditable handlers to the table */
            oTable.$('td').editable( '../example_ajax.php', {
                "callback": function( sValue, y ) {
                    var aPos = oTable.fnGetPosition( this );
                    oTable.fnUpdate( sValue, aPos[0], aPos[1] );
                },
                "submitdata": function ( value, settings ) {
                    return {
                        "row_id": this.parentNode.getAttribute('id'),
                        "column": oTable.fnGetPosition( this )[2]
                    };
                },

                "width": "90%",
                "height": "100%"
            } );


        });

        function fnClickAddRow() {
            $('#editable').dataTable().fnAddData( [
                "Custom row",
                "New row",
                "New row",
                "New row",
                "New row" ] );

        }
    </script>
    <style>
        body.DTTT_Print {
            background: #fff;

        }
        .DTTT_Print #page-wrapper {
            margin: 0;
            background:#fff;
        }

        button.DTTT_button, div.DTTT_button, a.DTTT_button {
            border: 1px solid #e7eaec;
            background: #fff;
            color: #676a6c;
            box-shadow: none;
            padding: 6px 8px;
        }
        button.DTTT_button:hover, div.DTTT_button:hover, a.DTTT_button:hover {
            border: 1px solid #d2d2d2;
            background: #fff;
            color: #676a6c;
            box-shadow: none;
            padding: 6px 8px;
        }

        .dataTables_filter label {
            margin-right: 5px;

        }
</body>
</html>
