 <nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                    <img alt="image" class="img-circle" src="<?= base_url() ?>public/feirarecife/images/logo-wolf.png" />
                     </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="<?= base_url() ?>">
                    <span class="clear"> 
                     <span class="block m-t-xs"> 
                        <strong class="font-bold"> 
                            <?php if($this->uri->segment(1) == 'admin') : ?>
                                <?= strtoupper($this->session->userdata('usuario_admin')->pes_nome); ?>
                            <?php elseif($this->uri->segment(1) == 'backoffice') : ?>
                                <!--<?= strtoupper($this->session->userdata('usuario')->pes_nome); ?>-->
                            <?php endif; ?>
                        </strong>
                     </span> 
                         
                    </span> 
                    </a>
                </div>
                <div class="logo-element">
                    WF
                </div>
            </li>
            <?php if(is_array($menu_array)): ?>
                <?php foreach ($menu_array as $key => $menuItem):?>
                    <?php $isSub = !empty($menuItem['submenu']) && is_array($menuItem['submenu']) && count($menuItem['submenu']) > 0;?>
                    <li <?php echo $key == $menu ? 'class="active"' : '';?>>
                        <a href="<?= $isSub ? '#' : base_url().$menuItem['url']; ?>">
                            <i class="fa <?php echo $menuItem['icon']; ?>"></i> <span class="nav-label"><?php echo $menuItem['display']; ?></span> 
                            <?php echo ($isSub)
                                ? '<span class="fa arrow"></span>'
                                : $menuItem['label']; ?>
                        </a>
                        <?php if($isSub): ?>
                            <ul class="nav nav-second-level">
                                <?php foreach ($menuItem['submenu'] as $keySub => $submenuItem): ?>
                                    <li <?php echo $keySub == $submenu ? 'class="active"' : ''; ?>>
                                        <?php if(isset($submenuItem['status']) and $submenuItem['status'] == 'lock'): ?>
                                        <a class="menu-lock">
                                            <?php echo $submenuItem['display']; ?>
                                            <?php if(isset($submenuItem['status']) and $submenuItem['status'] == 'lock'): ?>
                                                <span 
                                                    class="fa fa-lock pull-right" 
                                                ></span>
                                            <?php endif; ?>
                                        </a>
                                        <?php else: ?>
                                            <a href="<?= base_url().$submenuItem['url'] ?>">
                                                <?php echo $submenuItem['display']; ?>
                                            </a>    
                                        <?php endif; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>                        
                        <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</nav>
