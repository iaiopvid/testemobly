<div class="container text-center">
		<div class="logo-404">
			<a href="index.html"><img src="images/home/logo.png" alt="" /></a>
		</div>
		<div class="content-404">
			<img src="images/404/404.png" class="img-responsive" alt="" />
			<h1><b>OPPS!</b> Não foi possível encontrar esta página!</h1>
			<p> A página que você está procurando está inativada no momento.</p>
			<h2><a href="{SITEURL}">Me retorna a página inicial</a></h2>
		</div>
	</div>