<!-- BEGIN Main Container -->
  <div class="main-container col1-layout wow bounceInUp animated">
    <div class="main">
      <div class="col-main">
        <div class="product-view wow bounceInUp animated" itemscope="" itemtype="http://schema.org/Product" itemid="#product_base">
          <div id="messages_product_view"></div>
          <!--product-next-prev-->
          <div class="product-essential container">
            <div class="row">
              <div class="product-next-prev"> <a class="product-next" title="Next" href="#"><span></span></a> <a class="product-prev" title="Previous" href="#"><span></span></a> </div>
              <form action="{SITEURL}" method="post" id="product_addtocart_form">
                <input type="hidden" value="{IDPRODUTO}" name="prod_id">
                <!--End For version 1, 2, 6 -->
                <!-- For version 3 -->
                <div class="product-img-box col-sm-6 col-xs-12">
                  <div class="new-label new-top-left"> New </div>
                  <div class="product-image">
                    {BLC_FOTOINDISPONIVEL}
                    <div class="large-image"> <a href="{FOTOINDISPONIVEL}" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img src="{FOTOINDISPONIVEL}"> </a> </div>
                    {/BLC_FOTOINDISPONIVEL}
                    {BLC_GALERIA}
                      {BLC_FOTOS}
                      <div class="large-image"> <a href="{URLFOTONORMAL}" class="cloud-zoom" id="zoom1" rel="useWrapper: false, adjustY:0, adjustX:20"> <img src="{URLFOTONORMAL}"> </a> </div>
                      {/BLC_FOTOS} 
                    {/BLC_GALERIA} 
                          
                  </div>
                  <!-- end: more-images -->
                </div>
                <!--End For version 1,2,6-->
                <!-- For version 3 -->
                <div class="product-shop col-sm-6 col-xs-12">
                
                  <div class="product-name">
                    <h1 itemprop="name">{NOMEPRODUTO}</h1>
                  </div>

                  <div class="price-block">
                    <div class="price-box"> <span class="regular-price" id="product-price-123"> <span class="price">R$ {VALORFINALPRODUTO}</span> </span> </div>
                    <p class="availability {CLASS_STOCK}">
                      <link itemprop="availability" href="#">
                      <span>{MSG_STOCK}</span></p>
                  </div>
                  
                  <!--price-block-->
                  <div class="short-description">
                    <h2>Detalhes do produto</h2>
                    <p> {DESCRICAOBASICA} </p>
                  </div>
                  <div class="add-to-box">
                    <div class="add-to-cart">
                      <div class="pull-left">
                        <div class="custom pull-left">
                          <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="reduced items-count" type="button"><i class="icon-minus">&nbsp;</i></button>
                          <input type="number" targetId="{IDPRODUTO}" min="0" max="{QTYMAX}" name="qty" id="qty" maxlength="12" value="1" title="Quantity:" class="input-text qty">
                          <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="increase items-count" type="button"><i class="icon-plus">&nbsp;</i></button>
                        </div>
                        <!--custom pull-left-->
                      </div>
                      <!--pull-left-->
                      <button type="submit" title="Comprar" class="button btn-cart" onClick="productAddToCartForm.submit(this)" {BTN_COMPRAR}><span><i class="icon-basket"></i>Comprar</span></button>
                    </div>
                    <!--add-to-cart-->
                    
                    <!--email-addto-box-->
                  </div>
                  <!--add-to-box-->
                  <!-- thm-mart Social Share-->
                  <div class="social">
                    <ul class="link">
                      <li class="fb"> <a href="http://www.facebook.com/" rel="nofollow" target="_blank" style="text-decoration:none;"> </a> </li>
                      <li class="linkedin"> <a href="http://www.linkedin.com/" rel="nofollow" target="_blank" style="text-decoration:none;"> </a> </li>
                      <li class="tw"> <a href="http://twitter.com/" rel="nofollow" target="_blank" style="text-decoration:none;"> </a> </li>
                      <li class="pintrest"> <a href="http://pinterest.com/" rel="nofollow" target="_blank" style="text-decoration:none;"> </a> </li>
                      <li class="googleplus"> <a href="https://plus.google.com/" rel="nofollow" target="_blank" style="text-decoration:none;"> </a> </li>
                    </ul>
                  </div>
                  <!-- thm-mart Social Share Close-->
                </div>
                <!--product-shop-->
                <!--Detail page static block for version 3-->
              </form>
            </div>
          </div>
          <!-- end related product -->
        </div>
        <!--box-additional-->
        <!--product-view-->
      </div>
    </div>
    <!--col-main-->
  </div>
  <!--main-container-->
</div>
<!--col1-layout-->