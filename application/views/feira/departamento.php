<div class="page-heading">
  <div class="breadcrumbs">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <ul>
            <li class="home"> <a href="<?= base_url() ?>" title="Página Inicial">Home</a> <span>—› </span> </li>
            <li class="category1599"> <a href="#" title="">Categoria</a> 
          </ul>
        </div>
        <!--col-xs-12--> 
      </div>
      <!--row--> 
    </div>
    <!--container--> 
  </div>
  <div class="page-title">
    <h2>Frente Loja</h2>
  </div>
</div>
<section class="main-container col2-left-layout bounceInUp animated"> 
  <div class="container">
    <div class="row">
      <div class="col-main col-sm-9 col-sm-push-3 product-grid">
      <div class="pro-coloumn">
        
        <article class="col-main">
          <div class="toolbar">

            <div class="pager">

              <div class="pages pull-right">
                <label>Page:</label>
                <ul class="pagination">
                  {BLC_PAGINACAO}
                    {BLC_PAGINA}
                      <li class="active"><a href="{URLPAGINA}">{INDICE}</a></li>
                    {/BLC_PAGINA}
                  {/BLC_PAGINACAO}
                </ul>
              </div>

            </div>

          </div>

          {LISTAGEM}
          
        </article>

        </div>
      </div>
      <aside class="col-left sidebar col-sm-3 col-xs-12 col-sm-pull-9 wow bounceInUp animated"> 
        <div class="side-nav-categories">
          <div class="block-title"> Categorias </div>
          <div class="box-content box-category">
            <ul>
              {BLC_DEPARTAMENTOS}
              <li> <a class="active" href="#">{NOMEDEPARTAMENTO}</a> <span class="subDropdown plus"></span>
                <ul class="level0_415" style="display:none">
                  {BLC_DEPARTAMENTOSFILHOS}
                  <li> <a href="{URLDEPARTAMENTO_FILHO}"> {NOMEDEPARTAMENTO_FILHO} </a> 
                  </li>
                  {/BLC_DEPARTAMENTOSFILHOS}
                </ul>
              </li>
              {/BLC_DEPARTAMENTOS}
              
            </ul>
          </div>
          <!--box-content box-category--> 
        </div>
      </aside>
      <!--col-right sidebar--> 
    </div>
    <!--row--> 
  </div>
  <!--container--> 
</section>
<!--main-container col2-left-layout--> 