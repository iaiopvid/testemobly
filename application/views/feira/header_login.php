<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Frente de Loja</title>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no">
    <meta name="description" content=" ">
    <meta name="keywords" content=" ">
    <meta name="robots" content="*">
    <link rel="icon" href="#" type="image/x-icon">
    <link rel="shortcut icon" href="#" type="image/x-icon">

    <link href="<?= base_url().'public/feirarecife/stylesheet/bootstrap.min.css' ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url().'public/feirarecife/stylesheet/font-awesome.css' ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url().'public/feirarecife/stylesheet/revslider.css' ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url().'public/feirarecife/stylesheet/owl.carousel.css' ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url().'public/feirarecife/stylesheet/owl.theme.css' ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url().'public/feirarecife/stylesheet/jquery.bxslider.css' ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url().'public/feirarecife/stylesheet/jquery.mobile-menu.css' ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url().'public/feirarecife/stylesheet/style.css' ?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url().'public/feirarecife/stylesheet/responsive.css' ?>" rel="stylesheet" type="text/css">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        
    <link href=<?= base_url()."public/inspinia/css/plugins/steps/jquery.steps.css" ?> media="screen" rel="stylesheet" type="text/css">
    <link href=<?= base_url()."public/css/custom_style.css" ?> media="screen" rel="stylesheet" type="text/css">

    <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>

    <meta name="viewport" content="initial-scale=1.0, width=device-width">

</head>
<body>
    <header>
        <div class="header-banner">
            <div class="assetBlock">
              <div style="height: 20px; overflow: hidden;" id="slideshow">
                <p style="display: block;">FAÇA <span>LOGIN</span> PARA ACESSAR O SISTEMA! &gt;</p>
                <p style="display: none;">FAÇA <span>LOGIN</span> PARA ACESSAR O SISTEMA! &gt;</p>
              </div>
             </div>
        </div>
        <div id="header">
            <div class="header-container container">
            <!-- <div class="row"> -->
                <a href="<?= base_url('ecommerce') ?>" >
                    <button style="margin: 10px; color: #88be4c"class="btn pull-right">Voltar</button>
                </a>
            <!-- </div> -->
                <div class="row">
                    <div class="logo">
                        <a href="<?= base_url() ?>" title="Flavours HTML">
                            <div>
                                <img src="<?= base_url().'public/feirarecife/images/logo.png' ?>" alt="Flavours Store">
                            </div>
                        </a> 
                    </div>
              
                </div>
            </div>

        </div>
    </header>