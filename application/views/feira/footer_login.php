<footer>
   
    <!--footer-middle-->
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-xs-12 coppyright">© Desenvolvido pela Pixel Interface.</div>
         
        </div>
        <!--row-->
      </div>
      <!--container-->
    </div>
    <!--footer-bottom-->
    <!-- BEGIN SIMPLE FOOTER -->
</footer>
  <!-- End For version 1,2,3,4,6 -->
  
</div>
<!--page-->
<!-- Mobile Menu-->
<div id="mobile-menu">
  <ul>
    <li>
      <div class="home"><a href="#"><i class="icon-home"></i>Início</a> </div>
    </li>
    <li><a href="#">Sub-menu</a>
      <ul>
        <li><a href="#">Sub-1</a></li>
        <li> <a href="#">Sub-2</a></li>
      </ul>
    </li>
  </ul>
</div>
     

<!-- JavaScript -->
<!--<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/jquery.min.js' ?>"></script>-->
<script type="text/javascript" src="<?= base_url()."public/inspinia/js/jquery-2.1.1.js" ?>"></script>
<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/bootstrap.min.js' ?>"></script>
<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/parallax.js' ?>"></script>
<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/revslider.js' ?>"></script>
<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/common.js' ?>"></script>
<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/jquery.bxslider.min.js' ?>"></script>
<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/owl.carousel.min.js' ?>"></script>
<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/jquery.mobile-menu.min.js' ?>"></script>
<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/mascara.js' ?>"></script>

{scripts}
<script type="text/javascript" src="{url}"></script>
{/scripts}

<script type="text/javascript">
        jQuery(document).ready(function(){

            jQuery('#thm-rev-slider').show().revolution({
                dottedOverlay: 'none',
                delay: 5000,
                startwidth: 0,
                startheight:900,

                hideThumbs: 200,
                thumbWidth: 200,
                thumbHeight: 50,
                thumbAmount: 2,

                navigationType: 'thumb',
                navigationArrows: 'solo',
                navigationStyle: 'round',

                touchenabled: 'on',
                onHoverStop: 'on',
                
                swipe_velocity: 0.7,
                swipe_min_touches: 1,
                swipe_max_touches: 1,
                drag_block_vertical: false,
            
                spinner: 'spinner0',
                keyboardNavigation: 'off',

                navigationHAlign: 'center',
                navigationVAlign: 'bottom',
                navigationHOffset: 0,
                navigationVOffset: 20,

                soloArrowLeftHalign: 'left',
                soloArrowLeftValign: 'center',
                soloArrowLeftHOffset: 20,
                soloArrowLeftVOffset: 0,

                soloArrowRightHalign: 'right',
                soloArrowRightValign: 'center',
                soloArrowRightHOffset: 20,
                soloArrowRightVOffset: 0,

                shadow: 0,
                fullWidth: 'on',
                fullScreen: 'on',

                stopLoop: 'off',
                stopAfterLoops: -1,
                stopAtSlide: -1,

                shuffle: 'off',

                autoHeight: 'on',
                forceFullWidth: 'off',
                fullScreenAlignForce: 'off',
                minFullScreenHeight: 0,
                hideNavDelayOnMobile: 1500,
            
                hideThumbsOnMobile: 'off',
                hideBulletsOnMobile: 'off',
                hideArrowsOnMobile: 'off',
                hideThumbsUnderResolution: 0,

                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0,
                fullScreenOffsetContainer: ''
            });
        });
        </script>
<script type="text/javascript">
    function HideMe()
    {
        jQuery('.popup1').hide();
        jQuery('#fade').hide();
    }
</script> 
<script type="text/javascript">
    // <![CDATA[
        jQuery(function($) {
            $.mask.definitions['~']='[+-]';
            //Inicio Mascara Telefone
            $('#telefone').focusout(function(){
                var phone, element;
                element = $(this);
                element.unmask();
                phone = element.val().replace(/\D/g, '');
                if(phone.length > 10) {
                    element.mask("(99) 99999-999?9");
                } else {
                    element.mask("(99) 9999-9999?9");
                }
            }).trigger('focusout');
            //Fim Mascara Telefone
            $("#cpf").mask("999.999.999-99");
            $("#cep").mask("18.999-999");
        });
    // ]]>
    </script>
</body>

</html>
