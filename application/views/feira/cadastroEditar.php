<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?php echo $titulo;?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('backoffice/dashboard');?>">Dashboard</a>
            </li>
            <li class="active">
                <strong><?php echo $titulo;?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<?php 
    $errorMessage = $this->session->flashdata('errorMessage');
    if(!empty($errorMessage)): 
    ?>
        <div class="alert alert-danger" style="">
                <h3><i class="fa fa-time-cicle" style="margin-right: 15px;"></i><?= $errorMessage; ?></h3>
        </div>
    <?php endif; ?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <?php $erro = $this->session->flashdata('error'); ?>
                <?php if($erro): ?>
                    <?= '<div class="alert alert-danger">'.$erro.'</div>' ?>
                <?php endif; ?>
                <div class="ibox-title">
                    <h5>Patrocinador: </h5>
                        <span class="label label-success" style="font-size: 1.5em; margin-top: -5px;" >
                            <?php echo strtoupper($parent); ?>
                        </span>                    
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>                               
                    </div>
                </div>
                <div class="ibox-content">
                    <p>
                        Preencha os campos e pressione o botão "Finalizar"
                    </p>
                    <?php
                        $validation_errors = validation_errors();
                        if ( ! empty($validation_errors))
                        {
                            echo "<div class=\"alert alert-danger\">";
                            echo "<h4>Preencha corretamente os seguintes campos:</h4>";
                            echo "<ul>";
                            echo validation_errors();
                            echo "</ul>";
                            echo "</div>";
                        }
                    ?>
                    <?php if($isInterno): ?>
                        <form id="form" method="post" action="<?php echo base_url('backoffice/cadastro/editar'); ?>" class="wizard-big">
                    <?php else: ?>
                        <form id="form" method="post" action="<?php echo base_url('cadastro/'.$parent); ?>" class="wizard-big">
                    <?php endif; ?>
                        <h1>Acesso</h1>
                        <fieldset>
                            <h2>Dados da Conta</h2>
                            <?php echo form_hidden('id', $cadastro['id']); ?>

                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <?php 
                                            $attr['usuario'] = ' 
                                                id="usuario"
                                                class="form-control required"
                                                placeholder="Escolha um usuário para login"
                                            '; 
                                        ?>
                                        <?php echo form_label('Usuário*', 'usuario'); ?>
                                        <?php echo form_input('usuario', set_value('usuario', $cadastro['usuario']), $attr['usuario']); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php 
                                            $attr['email'] = ' 
                                                id="email"
                                                class="form-control required email"
                                                placeholder="Informe um e-mail válido"
                                            '; 
                                        ?>
                                        <?php echo form_label('E-mail*', 'email'); ?>
                                        <?php echo form_input('email', set_value('email', $cadastro['email']), $attr['email'])?>
                                    </div>
                                    <div class="form-group">
                                        <?php 
                                            $attr['senha'] = ' 
                                                id="senha"
                                                class="form-control required"
                                                placeholder="Informe a sua senha"
                                            '; 
                                        ?>
                                        <?php echo form_label('Senha*', 'senha'); ?>
                                        <?php echo form_password('senha', '', $attr['senha'])?>
                                    </div>
                                    <div class="form-group">
                                        <?php 
                                            $attr['confirmarSenha'] = ' 
                                                id="confirmarSenha"
                                                class="form-control required"
                                                placeholder="Confirme a sua senha"
                                            '; 
                                        ?>
                                        <?php echo form_label('Confirmar senha*', 'confirmarSenha'); ?>
                                        <?php echo form_password('confirmarSenha', '', $attr['confirmarSenha'])?> 
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="text-center">
                                        <div style="margin-top: 20px">
                                            <i class="fa fa-sign-in" style="font-size: 180px;color: #e5e5e5 "></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                        <h1>Perfil</h1> 
                        <fieldset>
                            <h2>Informações Pessoais</h2>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['nome'] = ' 
                                                id="nome"
                                                class="form-control required"
                                                placeholder="Informe o seu nome"
                                            '; 
                                        ?>
                                        <?php echo form_label('Nome*', 'nome'); ?>
                                        <?php echo form_input('nome', set_value('nome', $cadastro['nome']), $attr['nome'])?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['cpf'] = ' 
                                                id="cpf"
                                                class="form-control required cpfMask"
                                                placeholder="Informe o seu CPF"
                                            '; 
                                        ?>
                                        <?php echo form_label('CPF*', 'cpf'); ?>
                                        <?php echo form_input('cpf', set_value('cpf', $cadastro['cpf']), $attr['cpf'])?>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['rg'] = ' 
                                                id="rg"
                                                class="form-control"
                                                placeholder="Informe o seu RG"
                                            '; 
                                        ?>
                                        <?php echo form_label('RG', 'rg'); ?>
                                        <?php echo form_input('rg', set_value('rg', $cadastro['rg']), $attr['rg'])?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $maxDate = date('Y-m-d', strtotime(Now(). ' - 18 years'));
                                            $minDate = date('Y-m-d', strtotime(Now(). ' - 115 years'));
                                        
                                            $attr['dataNascimento'] = '
                                                id="dataNascimento" 
                                                class="form-control required"
                                                placeholder="Informe a sua data de nascimento"
                                                type="date"
                                            '; 
                                        ?>
                                        <?php echo form_label('Data de nascimento*', 'dataNascimento'); ?>
                                        <?php echo '<input type="date" name="dataNascimento" id="dataNascimento" '.$attr['dataNascimento'].' value="'.set_value('dataNascimento', $cadastro['dataNascimento']).'"/>'; ?>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-3 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['telefone'] = ' 
                                                id="telefone"
                                                class="form-control telefoneMask required"
                                                placeholder="Informe o número do seu telefone"
                                            '; 
                                        ?>
                                        <?php echo form_label('Telefone*', 'telefone'); ?>
                                        <?php echo form_input('telefone', set_value('telefone', $cadastro['telefone']), $attr['telefone'])?>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-3 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['celular'] = ' 
                                                id="celular"
                                                class="form-control celularMask required"
                                                placeholder="Informe o número do seu celular"
                                            '; 
                                        ?>
                                        <?php echo form_label('Celular*', 'celular'); ?>
                                        <?php echo form_input('celular', set_value('celular', $cadastro['celular']), $attr['celular'])?>
                                    </div>
                                </div>
                            </div>                            
                        </fieldset>

                        <h1>Endereço</h1>
                        <fieldset>
                            <h2>Informações de Endereço</h2>
                            <div class="row">
                                <div class="col-lg-3 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['cep'] = ' 
                                                id="cep"
                                                class="form-control required cepMask cepAutoComplete"
                                                placeholder="Informe o seu CEP"
                                            '; 
                                        ?>
                                        <?php echo form_label('CEP*', 'cep'); ?>
                                        <?php echo form_input('cep', set_value('cep', $cadastro['cep']), $attr['cep'])?>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-8 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['endereco'] = ' 
                                                id="endereco"
                                                class="form-control required"
                                                placeholder="Informe o seu endereço"
                                            '; 
                                        ?>
                                        <?php echo form_label('Endereço*', 'endereco'); ?>
                                        <?php echo form_input('endereco', set_value('endereco', $cadastro['endereco']), $attr['endereco'])?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['numero'] = ' 
                                                id="numero"
                                                class="form-control required"
                                                placeholder="Número"
                                                type="number"
                                                min="0"
                                            '; 
                                        ?>
                                        <?php echo form_label('Número*', 'numero'); ?>
                                        <?php echo form_input('numero', set_value('numero', $cadastro['numero']), $attr['numero'])?>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['complemento'] = ' 
                                                id="complemento"
                                                class="form-control"
                                                placeholder="Informe o complemento do endereço"
                                            '; 
                                        ?>
                                        <?php echo form_label('Complemento', 'complemento'); ?>
                                        <?php echo form_input('complemento', set_value('complemento', $cadastro['complemento']), $attr['complemento'])?>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['bairro'] = ' 
                                                id="bairro"
                                                class="form-control required"
                                                placeholder="Informe o bairro"
                                            '; 
                                        ?>
                                        <?php echo form_label('Bairro*', 'bairro'); ?>
                                        <?php echo form_input('bairro', set_value('bairro', $cadastro['bairro']), $attr['bairro'])?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['cidade'] = ' 
                                                id="cidade"
                                                class="form-control required"
                                                placeholder="Informe a cidade"
                                            '; 
                                        ?>
                                        <?php echo form_label('Cidade*', 'cidade'); ?>
                                        <?php echo form_input('cidade', set_value('cidade', $cadastro['cidade']), $attr['cidade'])?>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['estado'] = ' 
                                                id="estado"
                                                class="form-control required"
                                                placeholder="Informe o estado"
                                            '; 
                                        ?>
                                        <?php echo form_label('Estado*', 'estado'); ?>
                                        <?php echo form_dropdown('estado', $estados, set_value('estado', $cadastro['estado']), $attr['estado'])?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h1>Contrato</h1>
                        <fieldset>
                            <h2>Termos e Condições de Uso</h2>
                            <input id="licenca" name="licenca" type="hidden" value="<?php set_value('licenca', $cadastro['licenca']); ?>" />
                            <div class="row">

                                <?php $cont = 0; foreach ($licencas as $key => $item): $cont ++;?>
                                    <div class="col-lg-4 plano-option" tag="<?= $key ?>" style="cursor: pointer">
                                        <div class="widget-head-color-box lazur-bg p-lg text-center">
                                            <div class="m-b-md">
                                            <h2 class="font-bold no-margins">
                                                <?= $item->lic_nome ?>
                                            </h2>
                                                <h1>R$ <?= convertToValorBR($item->lic_valor); ?></h1>
                                            </div>
                                            <img style="width: 200px" src="<?= base_url(); ?>public/img/default-avatar.jpg" class="img-circle circle-border m-b-md" alt="profile">
                                            <div>
                                                <?php if($item->lic_bonus_adesao > 0): ?>
                                                    <span><?= convertToValorBR($item->lic_bonus_adesao * 100); ?> % por indicação</span> |
                                                <?php endif; ?>
                                                <?php if($item->lic_carreira > 0): ?>
                                                    <span>Plano de Carreira</span> |
                                                <?php endif; ?>|
                                                <span>Bônus de Indicação</span>
                                            </div>
                                        </div>
                                    </div>
                                <?php if($cont >= 3): $cont = 0; ?>
                                    </div>
                                    <div class="row">
                                <?php endif; ?>
                                <?php endforeach;?>
                            </div>
                            <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required"><label for="acceptTerms">Eu aceito os termos e condições de uso.</label>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>




