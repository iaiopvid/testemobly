<div class="row">
    <form action="<?= base_url('checkout/finalizar_checkout') ?>" method="post">
        <fieldset>
            <?php if(count($carrinho['itens'])): ?>
                <table id="shopping-cart-table" class="data-table cart-table table-striped">
                    <colgroup>
                        <col width="1">
                        <col>
                        <col width="1">
                        <col width="1">
                        <col width="1">
                        <col width="1">
                        <col width="1">
                    </colgroup>
                    <thead>
                        <tr class="first last">
                            <th style="vertical-align: middle" rowspan="1">&nbsp;</th>
                            <th style="vertical-align: middle" rowspan="1"><span class="nobr">Nome do Produto</span></th>
                            <th style="vertical-align: middle" rowspan="1"></th>
                            <th style="vertical-align: middle" class="a-center" colspan="1"><span class="nobr">Preço</span></th>
                            <th style="vertical-align: middle" rowspan="1" class="a-center qtd">Quantidade</th>
                            <th style="vertical-align: middle" class="a-center" colspan="1">Subtotal</th>
                            <th style="vertical-align: middle" rowspan="1" class="a-center">&nbsp;</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr class="first last">
                            <td colspan="50" class="a-right last">
                                <a href="<?= base_url() ?>"><button type="button" title="Continue Shopping" class="button btn-continue pull-right" onClick=""><span><span>Continuar Comprando</span></span></button></a>
                                                           
                            </td>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php if (count($carrinho['itens']) > 0): ?>
                            <?php foreach ($carrinho['itens'] as $key => $item): ?>
                                <tr class="odd">
                                    <td style="vertical-align: middle" class="image hidden-table"><a href="#" title="Nome do Produto" class="product-image"><img src="<?= $item['url'] ?>" width="75" alt="Nome do Produto"></a></td>
                                    <td style="vertical-align: middle" >
                                        <h2 class="product-name">
                                            <a href="#"><?= $item['descricao'] ?></a>
                                        </h2>
                                    </td>
                                    <td style="vertical-align: middle" class="a-center hidden-table">    
                                    </td>
                                    <td style="vertical-align: middle" class="a-right hidden-table">
                                        <span class="cart-price">
                                            <span class="price">R$ <?= convertToValorBR($item['valor']) ?></span>                
                                        </span>
                                    </td>
                                    <td style="vertical-align: middle; align: right" class="a-center movewishlist">
                                        <!-- <button targetMinus="{IDPRODUTO}" id="reduced-{IDPRODUTO}" class="reduced items-count" type="button" size="2"><i class="icon-minus"></i></button> -->
                                          <button onclick="var result = document.getElementById('qty-<?= $item['id'] ?>'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty > 0 ) result.value--;return false;" class="reduced items-count" type="button"><i class="icon-minus">&nbsp;</i>
                                          </button>
                                          <input type="number" targetid="<?= $item['id'] ?>" name="qty-<?= $item['id'] ?>" id="qty-<?= $item['id'] ?>" value="<?= $item['qtd'] ?>" size="4" min="1" max="" title="Qty" class="input-text qty" maxlength="12">
                                          <button onclick="var result = document.getElementById('qty-<?= $item['id'] ?>'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="increase items-count" type="button"><i class="icon-plus">&nbsp;</i>
                                          </button>

                                        <span onclick="atualizaItem(<?= $item['id'] ?>, document.getElementById('qty-<?= $item['id'] ?>'));" style="cursor: pointer;" class="" type="button"><i class="icon-refresh pull-right" title="Atualizar Item" >&nbsp;</i>
                                        </span>
                                        <!-- <input type="number" targetid="<?= $item['id'] ?>" name="qty" id="qty-<?= $item['id'] ?>" value="<?= $item['qtd'] ?>" size="4" min="1" max="" title="Qty" class="input-text qty" maxlength="12"> -->
                                    </td>
                                    <td style="vertical-align: middle" class="a-right movewishlist">
                                         <span class="cart-price">
                                            <span class="price-total price" id="valor-total">R$ <?= convertToValorBR($item['valor'] * $item['qtd']) ?>
                                            </span>                            
                                        </span>
                                    </td>
                                    <td style="vertical-align: middle" class="a-center last">
                                        <a href="<?= base_url('carrinho/removeItem').'/'.$item['id'] ?>" title="Remover Item" class="button remove-item"><span><span>Remover Item</span></span></a>
                                    </td>
                                </tr>  
                            <?php endforeach; ?>
                        <?php else: ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            <?php else: ?>
                <div style="padding: 100px" class="col-sm-8">
                    <div class="col-lg-4 text-right" style="margin-top: 30px">
                        <span style="font-size: 7em; padding-top: 40px">:(</span>
                    </div>
                    <div class="col-lg-8 text-left">
                        <article>
                            <div>
                                Ops! Seu carrinho está vazio.
                            </div>
                            <div>
                                Para inserir produtos no seu carrinho, navegue pela loja. Ao encontrar os produtos desejados, clique no botão 
                                <b>"Comprar"</b>.
                            </div>
                            <div>
                                <a href="<?= base_url() ?>ecommerce">
                                    <button type="button" title="Continuar comprando" class="button btn-proceed-checkout" onClick="" disable="disabled"><span>Continuar comprando</span></button>
                                </a>
                            </div>
                        </article>
                    </div>
                </div>
            <?php endif; ?>         
        </fieldset>

        <?php if($logged AND count($carrinho['itens']) > 0): ?>
            <div class="col-sm-8">
                <div class="row">                            
                    <div class="col-sm-12 col-md-12 col-lg-6">  
                        <div class="">
                            <h3>Endereço de Entrega</h3>
                            
                            <div class="col-sm-12" style="padding-left: 0">
                                <div class="shipping-form">
                                    <p></p>
                                    <ul class="form-list" style="background-color:rgb(242, 242, 242); border-radius:2px; padding:5px; color:#ffff;">   
                                        <li><strong>Rua:</strong> <?= $endereco['end_logradouro'] ?></label></li>
                                        <li><strong>Bairro:</strong> <?=  $endereco['end_bairro'] ?></label></li>
                                        <li><strong>Número:</strong> <?= $endereco['end_numero'] ?></label></li>
                                        <li><strong>Complemento:</strong> <?= $endereco['end_complemento'] ? $endereco['end_complemento'] : '' ?></label></li>
                                        <li><strong>Cidade:</strong> <?= $endereco['end_cidade'].' - '.$endereco['est_sigla'] ?> </label></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="">
                            </fieldset>
                                <h3>Escolha a forma de pagamento!</h3>
                                <label for="coupon_code">Formas de pagamento:</label>
                                <ul class="payment-methods">
                                  <li class="payment-method paypal" >
                                    <input name="forma-pagamento" type="radio" id="paypal" value="1">
                                    <label for="paypal" 
                                        style="background-image: url(<?= base_url('public/img/paypal2.png') ?>);
                                                background-size: 100px 50px;
                                                background-repeat: no-repeat;">PayPal</label>
                                  </li>

                                  <li class="payment-method pagseguro">
                                    <input name="forma-pagamento" type="radio" id="pagseguro" value="2">
                                    <label for="pagseguro"
                                        style="background-image: url(<?= base_url('public/img/pagseguro.png') ?>);
                                                background-size: 100px 50px;
                                                background-repeat: no-repeat;">PagSeguro</label>
                                  </li>

                                  <li class="payment-method bankslip">
                                    <input name="forma-pagamento" type="radio" id="bankslip" value="3">
                                    <label for="bankslip"
                                        style="background-image: url(<?= base_url('public/img/boleto.png') ?>);
                                                background-size: 100px 50px;
                                                background-repeat: no-repeat;">Boleto</label>
                                  </li>
                                </ul>
                            </fieldset>
                        </div>
                    </div>

                </div> 
            </div>
        <?php endif; ?>
        <?php if (!$logged): ?>
            <div class="col-sm-6 alert alert-danger">  
                <h4>Para fazer suas compras é preciso estar logado! <a href="<?= base_url('login_ecommerce/2'); ?>" style="color: #94cc56;"><b>Fazer Login</b></a> <h4>
            </div>
        <?php endif; ?>
        <div class="col-sm-4 pull-right">
            <?php if (count($carrinho['itens']) > 0 AND $carrinho['limite_minimo'] > 0): ?>
                <?php if ($carrinho['status'] == 2): ?>
                <div class="alert alert-warning">
                    Caso o seu desconto não seja suficiente para cobrir o valor total do seu boleto, o valor do boleto não pode ser menor que R$ <?= convertToValorBR($carrinho['limite_minimo']); ?>
                </div>
                <?php endif; ?>
            <?php endif; ?>
            <?php if(count($carrinho['itens'])): ?>
                <div class="alert alert-warning">
                    <strong>Frete valor fixo para toda cidade de R$ 15,00</strong>
                </div>
                <div class="totals">
                    <h3>Total das Compras</h3>
                    <div class="inner">
                        <table id="shopping-cart-totals-table" class="table shopping-cart-table-total">
                            <colgroup>
                            <col>
                            <col width="1">
                            </colgroup>
                            <tfoot>
                                <tr class="produto" style="border-top: solid 1px #ddd">
                                    <td style="width: 50%" class="a-left" colspan="1">
                                        <strong>Total</strong>
                                    </td>
                                    <td style="width: 50%" class="a-right">
                                        <strong><span id="subtotal" class="price-total-pedido">R$ <?= convertToValorBR($carrinho['total']); ?></span></strong>
                                    </td>
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr class="produto">
                                    <td style="width: 50%" class="a-left" colspan="1">
                                        Subtotal    </td>
                                    <td style="width: 50%" class="a-right">
                                        <span id="valor-total-pedido" class="price-total-pedido">R$ <?= convertToValorBR($carrinho['subtotal']); ?></span>
                                    </td>
                                </tr>
                                <tr class="produto">
                                    <td style="width: 50%" class="a-left" colspan="1">
                                        Desconto    </td>
                                    <td style="width: 50%" class="a-right">
                                        <span id="desconto-pedido" class="price-desconto">R$ <?= convertToValorBR($carrinho['desconto']); ?></span>
                                    </td>
                                </tr>
                                <tr class="produto">
                                    <td style="width: 50%" class="a-left" colspan="1">
                                        Frete    </td>
                                    <td style="width: 50%" class="a-right">
                                        <span id="desconto-pedido" class="price-desconto">R$ <?= $carrinho['frete'] ?></span>
                                    </td>
                                </tr>
                                <tr class="produto">
                                    <td style="width: 50%" class="a-left" colspan="1">
                                        Crédito Usado    </td>
                                    <td style="width: 50%" class="a-right">
                                        <span id="desconto-credito" class="price-credito">R$ <?= convertToValorBR($carrinho['credito']); ?></span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <?php if($logged AND count($carrinho['itens']) > 0): ?>
                            <ul class="checkout">
                                <li>
                                <?php //if($carrinho['total'] > 0 AND $carrinho['credito'] == $carrinho['total']): ?>
                                    <a href="<?= base_url() ?>checkout/finalizar_checkout">
                                        <button type="submit" title="Realizar Pagamento" class="button btn-proceed-checkout" onClick="" disable="disabled"><span>Finalizar Compra</span></button>
                                    </a>
                                <?php //endif; ?>
                                </li>
                                <br>
                            </ul>
                        <?php endif; ?>                
                    </div><!--inner-->  
                 </div><!--totals-->
            <?php endif; ?>
        </div> <!--col-sm-4-->  
    </form> 
</div>

