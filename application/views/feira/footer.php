<footer>
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-xs-12 coppyright">© J.P.</div>         
        </div>
      </div>
    </div>
  </footer>
  
</div>
<div id="mobile-menu">
  <ul>
    <li>
      <div class="home"><a href="#"><i class="icon-home"></i>Início</a> </div>
    </li>
    <li><a href="#">Sub-menu</a>
      <ul>
        <li><a href="#">Sub-1</a></li>
        <li> <a href="#">Sub-2</a></li>
      </ul>
    </li>
  </ul>
</div>
     

<!-- JavaScript -->
<script type="text/javascript" src="<?= base_url()."public/inspinia/js/jquery-2.1.1.js" ?>"></script>
<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/bootstrap.min.js' ?>"></script>
<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/parallax.js' ?>"></script>
<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/revslider.js' ?>"></script>
<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/common.js' ?>"></script>
<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/jquery.bxslider.min.js' ?>"></script>
<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/owl.carousel.min.js' ?>"></script>
<script type="text/javascript" src="<?= base_url().'public/feirarecife/js/jquery.mobile-menu.min.js' ?>"></script>

{scripts}
<script type="text/javascript" src="{url}"></script>
{/scripts}


<script type="text/javascript">

jQuery(function($) {
    $.mask.definitions['~']='[+-]';
    //Inicio Mascara Telefone
    $("#dataNascimento").mask("99-99-9999");
});

var base_url = "<?= base_url(); ?>";
    jQuery(document).ready(function(){
        jQuery('#thm-rev-slider').show().revolution({
            dottedOverlay: 'none',
            delay: 5000,
            startwidth: 0,
            startheight:900,

            hideThumbs: 200,
            thumbWidth: 200,
            thumbHeight: 50,
            thumbAmount: 2,

            navigationType: 'thumb',
            navigationArrows: 'solo',
            navigationStyle: 'round',

            touchenabled: 'on',
            onHoverStop: 'on',
            
            swipe_velocity: 0.7,
            swipe_min_touches: 1,
            swipe_max_touches: 1,
            drag_block_vertical: false,
        
            spinner: 'spinner0',
            keyboardNavigation: 'off',

            navigationHAlign: 'center',
            navigationVAlign: 'bottom',
            navigationHOffset: 0,
            navigationVOffset: 20,

            soloArrowLeftHalign: 'left',
            soloArrowLeftValign: 'center',
            soloArrowLeftHOffset: 20,
            soloArrowLeftVOffset: 0,

            soloArrowRightHalign: 'right',
            soloArrowRightValign: 'center',
            soloArrowRightHOffset: 20,
            soloArrowRightVOffset: 0,

            shadow: 0,
            fullWidth: 'on',
            fullScreen: 'on',

            stopLoop: 'off',
            stopAfterLoops: -1,
            stopAtSlide: -1,

            shuffle: 'off',

            autoHeight: 'on',
            forceFullWidth: 'off',
            fullScreenAlignForce: 'off',
            minFullScreenHeight: 0,
            hideNavDelayOnMobile: 1500,
        
            hideThumbsOnMobile: 'off',
            hideBulletsOnMobile: 'off',
            hideArrowsOnMobile: 'off',
            hideThumbsUnderResolution: 0,

            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            startWithSlide: 0,
            fullScreenOffsetContainer: ''
        });
    });
</script>
<script type="text/javascript">
    function HideMe()
    {
        jQuery('.popup1').hide();
        jQuery('#fade').hide();
    }
</script> 
</body>

</html>
