{BLC_LINHA}
  {BLC_COLUNA}
  <a href="{URLPRODUTO}">
    <div class="item">
        <div class="item-inner"> 
          <div class="item-img">
            <div class="item-img-info"><a href="{URLPRODUTO}" title="Nome do Produto" class="product-image"><img src="{URLFOTO}" alt="{NOMEPRODUTO}"></a>
              <div class="new-label new-top-left">Novo</div>
              <div class="item-box-hover">
                <div class="box-inner">
                  <div class="product-detail-bnt"><a href="{URLPRODUTO}" class="button detail-bnt"><span>Ver</span></a></div>                                 
                </div>
              </div>
            </div>
          </div>
          <div class="item-info">
            <div class="info-inner">
              <div class="item-title"><a href="{URLPRODUTO}" title="Nome do Produto">{NOMEPRODUTO}</a> </div>
              <div class="item-content">
                <div class="item-price">
                  <div class="price-box"><span class="regular-price" ><span class="price">R$ {VALOR}</span> </span> </div>
                </div>
  <div class="add_cart">
                    <a href="{URLPRODUTO}">
                      <button class="button btn-cart" type="button"><span>Comprar</span></button>
                    </a>
                  </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </a>
  {/BLC_COLUNA}
{/BLC_LINHA}
