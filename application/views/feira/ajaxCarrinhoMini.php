  <div class="fl-cart-contain conteudo-carrinho">
  <div class="mini-cart">
    <div class="basket"> <a href="<?= base_url() ?>checkout"><span><?= count($carrinho['itens']) ?></span></a> </div>
    <div class="fl-mini-cart-content" style="display: none;">
      <div class="block-subtitle">
        <div class=""><?= count($carrinho['itens']); ?> Item(ns), <span class="price">R$ <?= convertToValorBR($carrinho['total']); ?></span> </div>
      </div>
      <?php foreach ($carrinho['itens'] as $key => $item): ?>
        <ul class="mini-products-list" id="cart-sidebar">
          <li class="item first">
            <div class="item-inner"><a class="product-image" title="<?= $item['descricao'] ?>" href="#"><img alt="<?= $item['descricao'] ?>" src="<?= $item['url']; ?>"></a>
              <div class="product-details">
                <div class="access"><a class="btn-remove1" title="Remover este item" href="<?= base_url('carrinho/removeItem').'/'.$item['id'] ?>">Remover</a></div>
                <!--access-->
                <strong><?= $item['qtd']; ?></strong> x <span class="price">R$ <?= convertToValorBR($item['valor']); ?></span>
                <p class="product-name"><a href="#"><?= $item['descricao']; ?></a></p>
              </div>
            </div>
          </li>
        </ul>
      <?php endforeach; ?>

      <div class="actions">
        <a href="<?= base_url() ?>checkout">
        <button class="btn-checkout" title="Checkout" type="button" style="display: <?= count($carrinho['itens']) > 0 ? 'block' : 'none' ?>"><span>Checkout</span></button>
        </a>
      </div>

    </div>            
  </div>
</div>