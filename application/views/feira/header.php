<!DOCTYPE html>
<html lang="pt-br">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Frente Loja</title>
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=no">
        <meta name="description" content=" ">
        <meta name="keywords" content=" ">
        <meta name="robots" content="*">
        <link rel="icon" href="#" type="image/x-icon">
        <link rel="shortcut icon" href="#" type="image/x-icon">

        <link href="<?= base_url() . 'public/feirarecife/stylesheet/bootstrap.min.css' ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url() . 'public/feirarecife/stylesheet/font-awesome.css' ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url() . 'public/feirarecife/stylesheet/revslider.css' ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url() . 'public/feirarecife/stylesheet/owl.carousel.css' ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url() . 'public/feirarecife/stylesheet/owl.theme.css' ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url() . 'public/feirarecife/stylesheet/jquery.bxslider.css' ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url() . 'public/feirarecife/stylesheet/jquery.mobile-menu.css' ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url() . 'public/feirarecife/stylesheet/style.css' ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url() . 'public/feirarecife/stylesheet/responsive.css' ?>" rel="stylesheet" type="text/css">

        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700,800' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

        <link href=<?= base_url() . "public/inspinia/css/plugins/steps/jquery.steps.css" ?> media="screen" rel="stylesheet" type="text/css">
        <link href=<?= base_url() . "public/css/custom_style.css" ?> media="screen" rel="stylesheet" type="text/css">

        <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>

        <meta name="viewport" content="initial-scale=1.0, width=device-width">

        <link rel="icon" href="dayd.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="<?= base_url() ?>dayd.ico" type="image/x-icon" />
    </head>   
    <body>
        <div id="page" data-toggle="modal" data-target="#myModal"> 
            <header>
                <div class="header-banner">
                    <div class="assetBlock">
                        <div style="height: 20px; overflow: hidden;" id="slideshow">
                            {BLC_DESLOGADO}
                            <p style="display: block;"><a href="{URLLOGAR}" ><span>LOGIN</span></a></p>
                            <p style="display: none;"><a href="{URLLOGAR}" ><span>LOGIN</span></a></p>
                            {/BLC_DESLOGADO}

                            {BLC_LOGADO}
                            <p style="display: block;">Olá {USUARIO} <a href="{URLDESLOGAR}" ><span> SAIR</span></a></p>
                            <p style="display: none;">Olá {USUARIO}</p>
                            {/BLC_LOGADO}
                        </div>
                    </div>
                </div>
                <div id="header">
                    <div class="header-container container">
                        <div class="row">
                            <div class="logo"> <a href="<?= base_url() ?>" title="Flavours HTML">
                                    <div><img src="<?= base_url() . 'public/feirarecife/images/logo.png' ?>" alt="Flavours Store"></div>
                                </a> </div>
                            <div class="fl-nav-menu">

                                <nav>
                                    <div class="mm-toggle-wrap">
                                        <div class="mm-toggle"><i class="icon-align-justify"></i><span class="mm-label">Menu</span> </div>
                                    </div>
                                    <div class="nav-inner">
                                        <ul id="nav" class="hidden-xs">
                                            <li id="nav-home" class="level0 parent drop-menu"><a class="level-top active" href="<?= base_url() ?>"><span>Início</span></a></li>
                                            <li class="level0 parent drop-menu"><a class="level-top active"><span>CATEGORIAS</span> </a> 
                                                <ul class="level1">
                                                    {BLC_DEPARTAMENTOS}
                                                            <li class="level1 nav-10-2"> <a href="{URLDEPARTAMENTO}"> <span>{NOMEDEPARTAMENTO}</span> </a> </li>  
                                                    {/BLC_DEPARTAMENTOS}
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                            <!--row-->
                        </div>
                        <div class="fl-header-right">
                            <div class="fl-links">
                                <div class="no-js"> <a title="" class="clicker"></a>
                                    <div class="fl-nav-links">

                                        <ul class="links">
                                            <li><a href="{URLUSERCONTA}" title="Minha Conta">{USERCONTA}</a></li>
                                            <li><a href="{URLCARRINHO}" title="Carrinho">{CARRINHO}</a></li>
                                            <li class="last"><a href="{URLUSERACAO}" title="Login"><span>{USERACAO}</span></a></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div id="conteudo-carrinho" class='fl-cart-contain'>
                            <!--mini-cart-->
                            <div class="collapse navbar-collapse">
                                <form class="navbar-form form-search top-search" method="get" action="<?= base_url() ?>busca">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="pesquisa" id="pesquisa" style="height:75px; border-radius:0px;"  placeholder="Pesquisar">
                                        <span class="input-group-btn">
                                            <button type="submit" class="search-btn search-query"> <span class="glyphicon glyphicon-search"> <span class="sr-only">Pesquisar</span> </span> </button>
                                        </span> </div>
                                </form>
                            </div>
                            <!--links-->
                        </div>
                    </div>
                </div>	
            </header>