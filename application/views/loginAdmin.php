<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Frente de Loja | Login</title>

   
    <link href=<?= base_url()."public/inspinia/css/bootstrap.min.css" ?> media="screen" rel="stylesheet" type="text/css">
    <link href=<?= base_url()."public/inspinia/font-awesome/css/font-awesome.css" ?> media="screen" rel="stylesheet" type="text/css">

    <link href=<?= base_url()."public/inspinia/css/animate.css" ?> media="screen" rel="stylesheet" type="text/css">
    <link href=<?= base_url()."public/inspinia/css/style.css" ?> media="screen" rel="stylesheet" type="text/css">
</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen  animated fadeInDown">

        <div>
            <h2>Bem vindo ao seu painel administrativo</h2>
        </br>
            <p>Efetue o login para acessar o painel administrativo.
            </p>
            <form name="form" class="m-t" method="post" role="form" action="<?php echo base_url('loginAdmin'); ?>">
                <div class="form-group">
                    <input type="text" class="form-control" name="usuario" id="usuario" value="<?php echo set_value('usuario_admin'); ?>" placeholder="Usuário" required>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="senha" id="senha" placeholder="Senha" required>
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>

            </form>
            <p class="m-t"> <small></small> </p>
            <?php if (isset($errorMessage) AND !empty($errorMessage)): ?>
                <div class="alert alert-danger" style="width: 300px; margin-top: 20px;"><?php echo $errorMessage ?></div>
            <?php endif; ?>
        </div>
    </div>

    <!-- Mainly scripts -->

    <script type="text/javascript" src=<?= base_url()."public/inspinia/js/jquery-2.1.1.js" ?>></script>
    <script type="text/javascript" src=<?= base_url()."public/inspinia/js/bootstrap.min.js" ?>></script>
    

</body>

</html>
