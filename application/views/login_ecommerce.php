<!-- BEGIN Main Container -->  
              
<div class="main-container col1-layout wow bounceInUp animated">     
                      
    <div class="main">                     
        <div class="cart wow bounceInUp animated">
            <div class="row">
                <div class="container col-lg-offset-2 col-lg-4">
                    <p>
                        <h2><?= $descricao ?></h2>
                    </p>
                    <form name="form" class="m-t" method="post" role="form" action="#">
                        <div class="form-group">
                            <input type="text" class="form-control" name="usuario" id="usuario" value="<?php echo set_value('usuario'); ?>" placeholder="Usuário" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="senha" id="senha" placeholder="Senha" required>
                        </div>
                        <button style="color: #88be4c" type="submit" class="btn btn-primary block full-width m-b">Login</button>
                    <p class="m-t"> <small></small> </p>
                    <?php $errorMessage = $this->session->flashdata('errorMessage'); ?>
                    <?php if (isset($errorMessage) AND !empty($errorMessage)): ?>
                        <div class="alert alert-danger" style="">
                            <?php echo $errorMessage ?>
                        </div>
                    <?php endif; ?>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-offset-2 col-lg-8 ">
            <div class="ibox float-e-margins">
                <?php $erro = $this->session->flashdata('error'); ?>
                <?php $sucesso = $this->session->flashdata('sucesso'); ?>
                <?php if($erro): ?>
                    <?= '<div class="alert alert-danger">'.$erro.'</div>' ?>
                <?php endif; ?>
                <?php if($sucesso): ?>
                    <?= '<div class="alert alert-success">'.$sucesso.'</div>' ?>
                <?php endif; ?>
                <div class="ibox-content">
                    <p>
                        <h2>Não é cadastrado? Faça o seu cadastro abaixo:</h2>
                    </p>
                    <?php
                        $validation_errors = validation_errors();
                        if ( ! empty($validation_errors))
                        {
                            echo "<div class=\"alert alert-danger\">";
                            echo "<h4>Preencha corretamente os seguintes campos:</h4>";
                            echo "<ul>";
                            echo validation_errors();
                            echo "</ul>";
                            echo "</div>";
                        }
                    ?>

                    <form id="form" method="post" action="<?php echo base_url('login/cadastro'); ?>" class="wizard-big">
                        <fieldset>
                            <h2>Dados da Conta</h2>
                            <?php echo form_hidden('id', $cadastro['id']); ?>

                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="form-group">
                                        <?php 
                                            $attr['usuario'] = ' 
                                                id="usuario"
                                                class="form-control required"
                                                placeholder="Escolha um usuário para login"
                                            '; 
                                        ?>
                                        <?php echo form_label('Usuário*', 'usuario'); ?>
                                        <?php echo form_input('usuario', set_value('usuario', $cadastro['usuario']), $attr['usuario']); ?>
                                    </div>
                                    <div class="form-group">
                                        <?php 
                                            $attr['email'] = ' 
                                                id="email"
                                                class="form-control required email"
                                                placeholder="Informe um e-mail válido"
                                            '; 
                                        ?>
                                        <?php echo form_label('E-mail*', 'email'); ?>
                                        <?php echo form_input('email', set_value('email', $cadastro['email']), $attr['email'])?>
                                    </div>
                                    <div class="form-group">
                                        <?php 
                                            $attr['senha'] = ' 
                                                id="senha"
                                                class="form-control required"
                                                placeholder="Informe a sua senha"
                                            '; 
                                        ?>
                                        <?php echo form_label('Senha*', 'senha'); ?>
                                        <?php echo form_password('senha', '', $attr['senha'])?>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="text-center">
                                        <div style="margin-top: 20px">
                                            <i class="fa fa-sign-in" style="font-size: 180px;color: #e5e5e5 "></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </fieldset>
                        <fieldset>
                            <h2>Informações Pessoais</h2>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['nome'] = ' 
                                                id="nome"
                                                class="form-control required"
                                                placeholder="Informe o seu nome"
                                            '; 
                                        ?>
                                        <?php echo form_label('Nome*', 'nome'); ?>
                                        <?php echo form_input('nome', set_value('nome', $cadastro['nome']), $attr['nome'])?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['cpf'] = ' 
                                                id="cpf"
                                                name="cpf" 
                                                class="form-control required cpfMask"
                                                placeholder="Informe o seu CPF"
                                            '; 
                                        ?>
                                        <?php echo form_label('CPF*', 'cpf'); ?>
                                        <?php echo form_input('cpf', set_value('cpf', $cadastro['cpf']), $attr['cpf'])?>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['telefone'] = '
                                                id="telefone"
                                                class="form-control telefoneMask required"
                                                placeholder="Informe o número do seu telefone"
                                            '; 
                                        ?>
                                        <?php echo form_label('Telefone*', 'telefone'); ?>
                                        <?php echo form_input('telefone', set_value('telefone', $cadastro['telefone']), $attr['telefone'])?>
                                    </div>
                                </div>
                            </div>                          
                        </fieldset>
                        <fieldset>
                            <h2>Informações de Endereço</h2>
                            <div class="row">
                                <div class="col-lg-3 col-md-4 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['cep'] = ' 
                                                id="cep"
                                                class="form-control required cepMask cepAutoComplete"
                                                placeholder="Informe o seu CEP"
                                            '; 
                                        ?>
                                        <?php echo form_label('CEP*', 'cep'); ?>
                                        <?php echo form_input('cep', set_value('cep', $cadastro['cep']), $attr['cep'])?>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-8 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['endereco'] = ' 
                                                id="endereco"
                                                class="form-control required"
                                                placeholder="Informe o seu endereço"
                                            '; 
                                        ?>
                                        <?php echo form_label('Endereço*', 'endereco'); ?>
                                        <?php echo form_input('endereco', set_value('endereco', $cadastro['endereco']), $attr['endereco'])?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 col-md-2 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['numero'] = ' 
                                                id="numero"
                                                class="form-control required"
                                                placeholder="Número"
                                                type="number"
                                                min="0"
                                            '; 
                                        ?>
                                        <?php echo form_label('Número*', 'numero'); ?>
                                        <?php echo form_input('numero', set_value('numero', $cadastro['numero']), $attr['numero'])?>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['complemento'] = ' 
                                                id="complemento"
                                                class="form-control"
                                                placeholder="Informe o complemento do endereço"
                                            '; 
                                        ?>
                                        <?php echo form_label('Complemento', 'complemento'); ?>
                                        <?php echo form_input('complemento', set_value('complemento', $cadastro['complemento']), $attr['complemento'])?>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['bairro'] = ' 
                                                id="bairro"
                                                class="form-control required"
                                                placeholder="Informe o bairro"
                                            '; 
                                        ?>
                                        <?php echo form_label('Bairro*', 'bairro'); ?>
                                        <?php echo form_input('bairro', set_value('bairro', $cadastro['bairro']), $attr['bairro'])?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['cidade'] = ' 
                                                id="cidade"
                                                class="form-control required"
                                                placeholder="Informe a cidade"
                                            '; 
                                        ?>
                                        <?php echo form_label('Cidade*', 'cidade'); ?>
                                        <?php echo form_input('cidade', set_value('cidade', $cadastro['cidade']), $attr['cidade'])?>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <?php 
                                            $attr['estado'] = ' 
                                                id="estado"
                                                class="form-control required"
                                                placeholder="Informe o estado"
                                            '; 
                                        ?>
                                        <?php echo form_label('Estado*', 'estado'); ?>
                                        <?php echo form_dropdown('estado', $estados, set_value('estado', $cadastro['estado']), $attr['estado'])?>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <button style="color: #88be4c" type="submit" class="btn btn-primary block full-width m-b">Confirmar Cadastro</button>
                            <p class="m-t"> <small></small> </p>
                            <?php $errorMessage = $this->session->flashdata('errorMessage'); ?>
                            <?php if (isset($errorMessage) AND !empty($errorMessage)): ?>
                                <div class="alert alert-danger" style="">
                                    <?php echo $errorMessage ?>
                                </div>
                            <?php endif; ?>
                        </fieldset>                        
                    </form>
                </div>
            </div>
        </div>
    </div>