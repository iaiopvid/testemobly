<div id="header">
    <h1 title="Loud Songs Logo">LoudSongs search - hard to find obscure lyrics</h1>

    <ul title="navigation">
        <li <?php if($navTab == "about"){echo " id=\"active\"";}?>><a href="<?= base_url(); ?>about" title="About Page">About</a></li>
        <li <?php if($navTab == "add"){echo " id=\"active\"";}?>><a href="<?= base_url(); ?>add" title="Add Lyrics">Add Lyrics</a></li>
        <li <?php if($navTab == "home"){echo " id=\"active\"";}?>><a href="<?= base_url(); ?>" title="Home Page">Home</a></li>
    </ul>
</div>