<div class="category-products">
  <ul class="products-grid">
    {BLC_LINHA}
      {BLC_COLUNA}
        <li class="item col-lg-4 col-md-3 col-sm-4 col-xs-6">
          <div class="item-inner">
            <div class="item-img">
              <div class="item-img-info"><a href="{URLPRODUTO}" title="{NOMEPRODUTO}" class="product-image"><img src="{URLFOTO}" alt="Nome do Produto"></a>
                <div class="item-box-hover">
                  <div class="box-inner">
                    <div class="product-detail-bnt"><a class="button detail-bnt" type="button"><span>Ver</span></a></div>                                
                  </div>
                </div>
              </div>
            </div>
            <div class="item-info">
              <div class="info-inner">
                <div class="item-title"><a href="{URLPRODUTO}" title="Nome do Produto">{NOMEPRODUTO}</a> </div>
                <div class="item-content">
                  <div class="item-price">
                    <div class="price-box"><span class="regular-price" id="product-price-1"><span class="price">R$ {VALOR}</span></span></div>
                  </div>
                    <div class="add_cart">
                      <a href="{URLPRODUTO}" class="productcart"><button class="button btn-cart" type="button"><span>Comprar</span></button></a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </li>
      {/BLC_COLUNA}
    {/BLC_LINHA}
  </ul>
</div>