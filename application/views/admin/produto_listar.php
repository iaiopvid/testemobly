<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Produtos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url('admin/dashboard');?>">Administração</a>
            </li>
            <li class="active">
                <strong>Produtos</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12">
      {MSGERROR} {MSGSUCCESS}
<!-- <div class="alert alert-danger alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    A wonderful serenity has taken possession. <a class="alert-link" href="#">Alert Link</a>.
</div> -->
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background: #1c84c6; color: #fff">
                    <h5>Lista de produtos da sua loja</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up" style="color: #fff"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
								<div class="pull-right">
									<a href="{URLLISTAR}" title="Listar produtos" class="btn btn-primary btn-flat btn-sm"><em class="fa fa-list"></em> Listar</a>
									<a href="{URLADICIONAR}" title="Adicionar produto" class="btn btn-primary btn-flat btn-sm"><em class="fa fa-plus"></em> Adicionar</a>
								</div>

                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>Código</th>
                                <th>Img</th>
                                <th>Nome</th>
                                <th>Qtd</th>
                                <th>Preço</th>
                                <th>Promoção</th>
                                <th>Habilitado</th>
								<th class="coluna-acao text-center">Ação</th>
                            </tr>
                        </thead>
                        <tbody>
                            {BLC_DADOS}
                                <tr class="gradeX clickable pedidoItem" style="cursor: pointer;">
                                    <td style="vertical-align: middle; text-align: center">{IDPRODUTO}</td>
                                    <td style="vertical-align: middle; text-align: center"><img src="{URLTHUMB}"></td>
									<td style="vertical-align: middle; text-align: center">{NOME}</td>
                                    <td style="vertical-align: middle; text-align: center">{QTD}</td>
                                    <td style="vertical-align: middle; text-align: right">{PRECO}</td>
                                    <td style="vertical-align: middle; text-align: right">{VALORPROMOCIONAL}</td>
                                    <td style="vertical-align: middle; text-align: center">{HABILITADO}</td>
									<td style="vertical-align: middle; text-align: center" class="alinha-centro">
                                        <a href="{URLSTATUS}" title="Desativar produto" targetId="{IDPRODUTO}" class="btn btn-flat btn-{BTN} desativaProduto" style="margin-right: 5px">{BTNVALUE}</a>  
                                        <a href="{URLEDITAR}" title="Editar produto" class="btn btn-info btn-flat" style="margin-right: 5px;">Editar</a>
                                        <a href="{URLEXCLUIR}" title="Excluir produto" class="link-excluir btn btn-danger btn-flat" onclick="return confirm('Deseja realmente excluir?')">Excluir</a>
                                    </td>
                                </tr>
                            {/BLC_DADOS}
                        </tbody>

                        <div class="modal inmodal fade" id="modalItem" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div id="modal-content" class="modal-content">
                                    <!-- Content -->
                                </div>
                            </div>
                        </div>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>