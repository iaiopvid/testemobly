<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Departamentos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('backoffice/dashboard');?>">Administração</a>
            </li>
            <li class="active">
                <strong>Edição  </strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12">
      <div class="pull-right">
        <a href="{URLLISTAR}" title="Listar departamentos" class="btn btn-primary"><em class="fa fa-arrow-left"></em> Voltar</a>
      </div>
    </div>
    <div class="col-lg-12">
      {MSGERROR} {MSGSUCCESS}
    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background: #1c84c6; color: #fff">
                    <h5>Manutenção de departamentos - Edição</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up" style="color: #fff"></i>
                        </a>
                        <!-- <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a> -->
                    </div>
                </div>
                <div class="ibox-content">

                <section class="content">
                  <div class="row">

                    <div class="col-md-12">
                      <div class="box">
                          <div class="box-header">
                            <div class="pull-right">
                              <a href="{URLLISTAR}" title="Listar departamentos" class="btn btn-primary"><em class="fa fa-arrow-left"></em> Voltar</a>
                            </div>
                            <form role="form" action="{ACAOFORM}" method="post" enctype="multipart/form-data" class="form-horizontal">
                              
                              <input type="hidden" name="dep_id" id="dep_id" value="{dep_id}"> 

                              <div class="box-body">
                                <div class="form-group">
                                  <label class="control-label text-right col-sm-3 text-right col-sm-3" for="dep_nome">Nome:<span class="required" style="color:red"> *</span></label>
          												<div class="col-lg-4">
          													<input type="text" class="form-control" id="dep_nome" name="dep_nome" value="{dep_nome}" required placeholder="Nome">
          												</div>
                                </div>
          											
          											<div class="form-group">
          												<label class="control-label text-right col-sm-3" for="dep_departamentopai">Departamento pai:<span class="required" style="color:red"> *</span></label>
          												<div class="col-lg-4">
          													<select class="form-control" name="dep_departamentopai" id="dep_departamentopai">
          														<option value="">Selecione um departamento</option>
          													{BLC_DEPARTAMENTOS}
          														<option value="{IDDEPARTAMENTO}" {sel_iddepartamentopai}>{NOME}</option>
          													{/BLC_DEPARTAMENTOS}
          													</select>
          												</div>
          											</div>

                                <div class="form-group" id="dep_foto">
                                  <label for="dep_foto" class="control-label text-right col-sm-3">foto:<span class="required" style="color:red"> *</span></label>
                                  <div class="col-lg-4">
                                    <input type="file" class="form-control set-integer" id="dep_foto" name="dep_foto" size="40" value="{dep_foto}" placeholder="Escolha uma foto para o produto">
                                  </div>
                                </div>

          										</div>
          										<div class="box box-footer box-primary text-left">
          											<div class="form-group">
          		                    <div class="col-sm-offset-3 col-lg-4" style="margin-top:10px">
          		                      <button type="submit" class="btn btn-info"><em class="fa fa-save"></em> Salvar</button>
          		                    </div>
          	                    </div>
          	                  </div>
                            </form>

                      </div>   <!-- /.row -->
                    </section><!-- /.content -->

                </div>
            </div>
        </div>
    </div>
</div>