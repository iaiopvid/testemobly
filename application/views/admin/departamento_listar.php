<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Departamentos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('backoffice/dashboard');?>">Administração</a>
            </li>
            <li class="active">
                <strong>Departamentos</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12">
      {MSGERROR} {MSGSUCCESS}
<!-- <div class="alert alert-danger alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    A wonderful serenity has taken possession. <a class="alert-link" href="#">Alert Link</a>.
</div> -->
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background: #1c84c6; color: #fff">
                    <h5>Lista de categorias da sua loja</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up" style="color: #fff"></i>
                        </a>
                        <!-- <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a> -->
                    </div>
                </div>
                <div class="ibox-content">
                <div class="pull-right">
                  <a href="{URLLISTAR}" title="Listar produtos" class="btn btn-primary btn-flat btn-sm"><em class="fa fa-list"></em> Listar</a>
                  <a href="{URLADICIONAR}" title="Adicionar produto" class="btn btn-primary btn-flat btn-sm"><em class="fa fa-plus"></em> Adicionar</a>
                </div>

                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                              <th>Imagem</th>
                              <th>Nome</th>
                              <th>Departamento pai</th>
                              <th class="coluna-acao text-center"></th>
                              <th class="coluna-acao text-center"></th>
                            </tr>
                        </thead>
                        <tbody>
                            {BLC_DADOS}
                                <tr class="gradeX clickable pedidoItem" style="cursor: pointer;">
                                  <td><img src="{URLTHUMB}"></td>
                                  <td>{NOME}</td>
                                  <td>{NOMEPAI}</td>
                                  <td class="alinha-centro" style="width:25px">
                                    <a href="{URLEDITAR}" title="Editar" class="btn btn-warning btn-sm btn-flat"><em class="fa fa-pencil"></em></a>
                                  </td>
                                  <td class="alinha-centro" style="width:25px">
                                    <a href="{URLEXCLUIR}" title="Excluir" class="link-excluir btn btn-danger btn-sm btn-flat" onclick="return confirm('Deseja realmente excluir?')"><em class="fa fa-trash"></em></a>
                                  </td>
                                </tr>
                            {/BLC_DADOS}
                        </tbody>

                        <div class="modal inmodal fade" id="modalItem" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div id="modal-content" class="modal-content">
                                    <!-- Content -->
                                </div>
                            </div>
                        </div>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>