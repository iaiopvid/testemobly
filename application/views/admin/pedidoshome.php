<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?php echo $titulo;?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('backoffice/dashboard');?>">Dashboard</a>
            </li>
            <li class="active">
                <strong><?php echo $titulo;?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-12">
        {MSGERROR} {MSGSUCCESS}
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background: #1c84c6; color: #fff">
                    <h5>Lista de pedidos realizados</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up" style="color: #fff"></i>
                        </a>
                        <!-- <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#">Config option 1</a>
                            </li>
                            <li><a href="#">Config option 2</a>
                            </li>
                        </ul>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a> -->
                    </div>
                </div>
                <div class="ibox-content">

                    <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th style="vertical-align: middle; text-align: center">ID</th>
                                <th style="vertical-align: middle; text-align: center">Status</th>
                                <th style="vertical-align: middle; text-align: center">Subtotal</th>
                                <th style="vertical-align: middle; text-align: center">Descontos</th>
                                <th style="vertical-align: middle; text-align: center">Frete</th>
                                <th style="vertical-align: middle; text-align: center">Total</th>
                                <th style="vertical-align: middle; text-align: center">Data</th>
                                <th style="vertical-align: middle; text-align: center">Ação</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($pedidos) > 0): ?>
                                <?php foreach ($pedidos as $key => $item): ?>
                                <!-- <a href="<?= base_url() ?>backoffice/rede/unilevel/{ID_FILHO}"> 
                                </a>-->

                                <tr class="gradeX" style="cursor: pointer;">
                                    <td style="vertical-align: middle; text-align: center"><?= str_pad($item->ped_id, 6, 0,  STR_PAD_LEFT); ?></td>
                                    <td style="vertical-align: middle; text-align: center"><?= pedidoStatus($item->ped_status); ?></td>
                                    <td style="vertical-align: middle; text-align: right"><?= convertToValorBR($item->ped_subtotal) ?></td>
                                    <td style="vertical-align: middle; text-align: right"><?= convertToValorBR($item->ped_desconto) ?></td>
                                    <td style="vertical-align: middle; text-align: right"><?= convertToValorBR($item->ped_frete) ?></td>
                                    <td style="vertical-align: middle; text-align: right"><?= convertToValorBR($item->ped_total) ?></td>
                                    <td style="vertical-align: middle; text-align: center"><?= dateMySQL2BR($item->ped_data_criacao); ?></td>

                                    <td class="alinha-centro" style="vertical-align: middle; text-align: center">
                                        <a href="#" title="Clique aqui para visualizar os detalhes do pedido." targetId="<?= $item->ped_id ?>" class="btn btn-sm btn-flat btn-success clickable pedidoItem" style="margin-right: 5px">Visualizar</a>  
                                        <?php if($item->ped_status == 0) : ?>
                                            <a href="<?= base_url().'pedido/aprovarPedido/'.$item->ped_id ?>" title="Clique aqui para aprovar manualmente o pedido." class="link-aprovar btn btn-sm btn-flat btn-primary" style="margin-right: 5px" >Aprovar</a>
                                            <a href="<?= base_url().'pedido/cancelarPedido/'.$item->ped_id ?>"" title="Clique aqui para cancelar o pedido." class="link-excluir btn btn-danger btn-sm btn-flat" style="margin-right: 5px" >Cancelar</a>
                                        <?php endif; ?>
                                    </td>

                                </tr>
                                
                                <?php endforeach; ?>
                            <?php endif; ?>
                        
                        </tbody>

                        <div class="modal inmodal fade" id="modalItem" tabindex="-1" role="dialog"  aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div id="modal-content" class="modal-content">
                                    <!-- Content -->
                                </div>
                            </div>
                        </div>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>