        
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
        <h2><?php echo $titulo;?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?php echo base_url('admin/dashboard');?>">Dashboard</a>
            </li>
            <li class="active">
                <strong><?php echo $titulo;?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<?php 
    $errorMessage = $this->session->flashdata('errorMessage');
    if(!empty($errorMessage)): 
    ?>
        <div class="alert alert-danger" style="">
                <h3><i class="fa fa-time-cicle" style="margin-right: 15px;"></i><?= $errorMessage; ?></h3>
        </div>
    <?php endif; ?>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title" style="background: #1ab394; color: #fff">
                    <h5>Cadastros ativos e posicionados na rede</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up" style="color: #fff"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                     <table class="table table-striped table-bordered table-hover dataTables-example" >
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Usuário</th>
                                <th>Email</th>
                                <th style="text-align: center">Data de Cadastro</th>
                                <th style="text-align: center">Data de Ativação</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php foreach ($cadastros as $key => $item): ?>
                            <!-- <a href="<?= base_url() ?>backoffice/rede/unilevel/{ID_FILHO}"> 
                            </a>-->
                            <tr class="gradeX clickable">
                                <td><?= str_pad($item->usu_id, 6, 0,  STR_PAD_LEFT); ?></td>
                                <td><?= strtoupper($item->usu_login); ?></td>
                                <td><?= strtoupper($item->usu_email); ?></td>
                                <td style="text-align: center"><?= dateMySQL2BR($item->usu_data_criacao); ?></td>
                                <td style="text-align: center"><?= dateMySQL2BR($item->usu_data_alteracao); ?></td>
                            </tr>
                            
                            <?php endforeach; ?>
                        
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
