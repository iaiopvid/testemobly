<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-content text-left p-md">

                <div class="row wrapper border-bottom white-bg page-heading">
                  <div class="col-lg-10">
                      <h2>Produtos - {ACAO}</h2>
                      <ol class="breadcrumb">
                          <li>
                              <a href="<?= base_url('admin/dashboard') ?>">Administração</a>
                          </li>
                          <li>
                              <a href="<?= base_url('admin/produto');?>">Produtos</a>
                          </li>
                          <li class="active">
                              <strong>Adicionar Produtos</strong>
                          </li>
                      </ol>
                  </div>
                  <div class="col-lg-12">
                    <div class="pull-right">
                      <a href="{URLLISTAR}" title="Listar departamentos" class="btn btn-primary"><em class="fa fa-arrow-left"></em> Voltar</a>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    {MSGERROR} {MSGSUCCESS}
                  </div>
                </div>
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="row">
                    <form role="form" action="{ACAOFORM}" method="post" enctype="multipart/form-data" class="form-horizontal">       
                    <input type="hidden" name="prod_id" id="prod_id" value="{prod_id}">
                    <div class="col-lg-6">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Produtos <small>Insira as fiformações do produto</small></h5>
                                <div class="ibox-tools">
                                    <a class="collapse-link">
                                        <i class="fa fa-chevron-up"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-sm-12 "><h3 class="m-t-none m-b">Dados do produto</h3>
                                      <!-- <form role="form"> -->
                                          <div class="form-group">
                                            <label for="prod_nome">Nome<span class="required" style="color:red"> *</span></label> 
                                            <input type="text" id="prod_nome" name="prod_nome" value="{prod_nome}" required placeholder="Nome" class="form-control">
                                          </div>
                                          <div class="form-group">
                                            <label for="prod_nome">Resumo<span class="required" style="color:red"> *</span></label> 
                                            <textarea class="form-control" rows="3" id="prod_resumo" name="prod_resumo" value="{prod_resumo}" {required} placeholder="Resumo"></textarea>
                                          </div>
                                          <div class="form-group">
                                            <label>Valor<span class="required" style="color:red"> *</span></label> <input type="number" step="0.01" min="0" class="form-control set-numeric" id="prod_valor" name="prod_valor" value="{prod_valor}" required placeholder="Valor ">
                                          </div>
                                          <div class="form-group">
                                            <label>Promoção</label> 
                                            <input type="number" step="0.01" min="0" class="form-control set-numeric" id="prod_valorpromocional" name="prod_valorpromocional" value="{prod_valorpromocional}" placeholder="Valor promocional">
                                          </div>
                                          <div class="form-group">
                                            <label>Quantidade<span class="required" style="color:red"> *</span></label> 
                                            <input type="number" min="0" class="form-control set-integer" id="prod_quantidade" name="prod_quantidade" value="{prod_quantidade}" required placeholder="Quantidade">
                                          </div>
                                          <div class="form-group" id="prod_foto">
                                            <label for="prod_foto">foto:<span class="required" style="color:red"> *</span></label>
                                            <input type="file" class="form-control set-integer" id="prod_foto" name="prod_foto" size="40" value="{prod_foto}" placeholder="Escolha uma foto para o produto">
                                          </div>

                                          <div class="form-group" id="prod_status">
                                            <div class="checkbox">
                                              <label for="prod_status" class="checkbox">
                                                <input name="prod_status" type="checkbox" class="" id="prod_status" value="1" {CHECKED}>HABILITAR
                                              </label>
                                            </div>
                                          </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="col-lg-6">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Categorias</h5>
                                    <div class="ibox-tools">
                                        <a class="collapse-link">
                                            <i class="fa fa-chevron-up"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="ibox-content">
                                <!-- <form class="form-horizontal"> -->
                                    <p>Selecione as categorias do produto.</p>

                                    <h3 class="box-title" style="margin-top:10px">Departamentos</h3>
                                    <ul class='lista-departamentos'>
                                      {BLC_DEPARTAMENTOPAI}

                                      <li>
                                        <label for="departamento-{IDDEPARTAMENTO}" class="checkbox">
                                          <input {chk_departamentopai} name="departamento[]" type="checkbox" class="set-departamento-pai" id="departamento-{IDDEPARTAMENTO}" value="{IDDEPARTAMENTO}">{NOMEDEPARTAMENTO}
                                        </label>
                                      </li>

                                      <!-- <li> -->
                                        <ul class='lista-departamentos'>
                                          {BLC_DEPARTAMENTOFILHO}
                                          <li>
                                            <label for="departamento-{IDDEPARTAMENTOFILHO}" class="checkbox">
                                              <input {chk_departamentofilho} name="departamento[]" type="checkbox" class="set-departamento-filho" data-pai="{IDDEPARTAMENTOPAI}"id="departamento-{IDDEPARTAMENTOFILHO}" value="{IDDEPARTAMENTOFILHO}">{NOMEDEPARTAMENTOFILHO}
                                            </label>
                                          </li>
                                          {/BLC_DEPARTAMENTOFILHO}
                                        </ul>
                                        
                                      <!-- </li> -->
                                      
                                      {/BLC_DEPARTAMENTOPAI}
                                    </ul>

                                <!-- </form> -->
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                          <div class="box box-footer box-primary text-left">
                            <div class="form-group">
                              <div class="col-sm-offset col-md-6 text-right" style="margin-top:10px;">
                                <button type="submit" class="btn btn-info btn-flat" style="margin-right:10px"><em class="fa fa-save"></em> Salvar</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>

                </div>
            </div>
        </div>
  </div>