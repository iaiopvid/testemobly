<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Checkout extends CI_Controller {

    public function __construct() {

        parent::__construct ();
        $this->load->library ( 'form_validation' );
        $this->load->model('business/login_business');
        $this->load->model('business/pedido_business', 'PedidoBusiness');
        $this->load->model('business/carrinho_business', 'CarrinhoBusiness');

        $this->load->model('business/endereco_business', 'EnderecoBusiness');
        $this->load->model ( "Produto_Model");
        $this->load->model("Departamento_Model");
        $this->load->model ( "Cliente_Model");
        $this->load->model ( "CarrinhoItem_Model" );
        $this->load->model ( "Estado_Model" );
    }

    public function index() {
        $carrinho = $this->CarrinhoBusiness->getCarrinho();
        $logged = $this->login_business->logged();
        $data = array (
          'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
          'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
          'meta_url' => base_url(),
          'meta_classification' => "home",
          'pageTitle' => "LoudSon.gs",
          'searchInput' => "",
          'searchOptions' => "",
          'menu' => "Dashboard",

          'styles' => [
              ['url' => base_url().'assets/css/loja.css'],
          ],

          'scripts' => [
              ['url' => base_url().'public/feirarecife/js/feira.js']
          ],
        );

        $data['BLC_LOGADO'] = array();
        $data['BLC_DESLOGADO'] = array();
        $data['BLC_CREDITO'] = array();
        $data['BLC_SEMCREDITO'] = array();

        if ($logged) 
        {
          $user = $this->session->userdata('usuario');
          $usuario = $user->usu_login;
          $creditosPontos = $this->CreditosBusiness->getSaldoPontos($user->usu_id);
          $cotacao = $this->CreditosBusiness->getCotacao(1);
          $cotacao = (!is_null($cotacao) AND $cotacao > 0) ? $cotacao : 1;
          $creditos = convertToValorBR($creditosPontos / $cotacao);
          $data['BLC_LOGADO'][] = array(
            'USUARIO' => $usuario,
            'URLDESLOGAR' => base_url().'logout/1',
            'ACAO' => 'Login',
          );
          $data['USERCONTA'] = 'MINHA CONTA';
          $data['URLUSERCONTA'] = base_url().'backoffice';
          $data['CARRINHO'] = 'CARRINHO';
          $data['URLCARRINHO'] = base_url().'ckeckout';
          $data['USERACAO'] = 'SAIR';
          $data['URLUSERACAO'] = base_url().'logout/1';
          $data['BLC_CREDITO'][] = array(
            'CREDITO'=> $creditos ? $creditos : '0',
          );
          $data['PONTOS']= convertToValorBR($creditosPontos);
        } else {
          $data['BLC_DESLOGADO'][] = array(
            'URLLOGAR' => base_url().'login_ecommerce',
          );
          $data['USERCONTA'] = 'MEU BACKOFFICE';
          $data['URLUSERCONTA'] = base_url().'backoffice';
          $data['CARRINHO'] = 'CARRINHO';
          $data['URLCARRINHO'] = base_url().'ckeckout';
          $data['USERACAO'] = 'LOGIN';
          $data['URLUSERACAO'] = base_url().'login_ecommerce';
          $data['BLC_SEMCREDITO'][] = array(
            'MSG' => 'Para user os bônus é preciso estar logado!',
          );
        }
        $data ["BLC_PRODUTOS"] = array ();  
        $data ["BLC_FINALIZAR"] = array ();
        $data ["BLC_SEMPRODUTOS"] = array ();
        $data ["TOTALPEDIDO"] = NULL;
        $data ["DISPLAY"] = NULL;
        $data["BLC_DEPARTAMENTOS"] = array();
        
        
        $departamentos = $this->Departamento_Model->getDepartamentosPais(array('dep_departamentopai IS NULL' => null), true);
        
        foreach($departamentos as $dep) {
            
            $filhos = array();
            
            $departamentosFihos = $this->Departamento_Model->getDepartamentosFilhos($dep->dep_id);
        
            foreach($departamentosFihos as $depf) {
                
                $filhos[] = array(
                    "URLDEPARTAMENTO_FILHO" => site_url("departamento/".$depf->dep_id),
                    "NOMEDEPARTAMENTO_FILHO" => $depf->dep_nome
                    
                );
            } 
            
            $data["BLC_DEPARTAMENTOS"][] = array(
                "URLDEPARTAMENTO" => site_url("departamento/".$dep->dep_id),
                "NOMEDEPARTAMENTO" => $dep->dep_nome,
                "BLC_DEPARTAMENTOSFILHOS" => $filhos,
            );
        }

        $layout_data['content_header'] = $this->parser->parse('feira/header', $data, true);
        $layout_data['content_body'] = $this->parser->parse('feira/carrinho', $data, true);
        $layout_data['content_footer'] = $this->parser->parse('feira/footer', $data, true);

        $this->parser->parse("layouts/feiraRecife", $layout_data);
    }

    public function adicionar() {
        
        $prod_id = $this->input->post ( "prod_id" ); 
        $prod_qty = $this->input->post ( "qty" );

        $infoProduto = $this->Produto_Model->get(array('prod_id' => $prod_id), TRUE);

        $carrinho = $this->CarrinhoBusiness->getCarrinho();
        if ($infoProduto) {
            $disponivel = $infoProduto->prodest_quantidade > 0;
            if (!$disponivel) {
                if (isset($carrinho['itens'][$prod_id])) {
                    $this->CarrinhoBusiness->removeItem($prod_id);
                }
            } else {
                if (!isset($carrinho['itens'][$prod_id])) {
                    $this->CarrinhoBusiness->changeItem($prod_id, $prod_qty);
                    
                } else {
                    $this->CarrinhoBusiness->changeItem($prod_id, $prod_qty, true);
                }    
            }
            
        }
        redirect ("checkout");
    }

    public function formaentrega() {

        $carrinho = $this->CarrinhoBusiness->getCarrinho();

        $usuario = $this->session->userdata('usuario');

        if (!$usuario) {
            redirect('login_ecommerce/1');
        }

        $enderecoentrega = $this->EnderecoBusiness->getEnderecoByPessoa($usuario->usu_pessoa_id);
        
        if (count($carrinho['itens']) === 0) {
            redirect('');
        } 

        $data = array (
          'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
          'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
          'meta_url' => base_url(),
          'meta_classification' => "home",
          'pageTitle' => "LoudSon.gs",
          'searchInput' => "",
          'searchOptions' => "",
          'menu' => "Dashboard",

          'styles' => [
              ['url' => base_url().'assets/css/loja.css'],
          ],

          'scripts' => [
                ['url' => base_url().'public/feirarecife/js/feira.js'],
                ['url' => base_url().'public/js/jquery.maskedinput.js'],
                ['url' => base_url().'public/js/mlm.js']
          ],
        );
        
        $endereco['endereco'] = $enderecoentrega->end_logradouro;
        $endereco['numero'] = $enderecoentrega->end_numero;
        $endereco['complemento'] = $enderecoentrega->end_complemento;
        $endereco['cep'] = $enderecoentrega->end_cep;
        $endereco['bairro'] = $enderecoentrega->end_bairro;
        $endereco['cidade'] = $enderecoentrega->end_cidade;
        $endereco['estado'] = $enderecoentrega->est_sigla;
        
        $data ["endereco"] = $endereco;

        $this->session->set_userdata('endereco_pedido', $endereco);

        $data ["estados"] = [
            "PE"=> "Pernambuco - PE"
        ];

        $layout_data['content_header'] = $this->parser->parse('feira/header_login', $data, true);
        $layout_data['content_body'] = $this->parser->parse('formaentrega', $data, true);
        $layout_data['content_footer'] = $this->parser->parse('feira/footer_login', $data, true);

        $this->parser->parse("layouts/feiraRecife", $layout_data);
    }

    public function alterar_endereco() {
        
        $this->form_validation->set_rules('cep', 'CEP', 'trim|required');
        $this->form_validation->set_rules('endereco', 'Endereço', 'trim|required');
        $this->form_validation->set_rules('numero', 'Número', 'trim|required');
        $this->form_validation->set_rules('complemento', 'Complemento', 'trim');
        $this->form_validation->set_rules('bairro', 'Bairro', 'trim|required');
        $this->form_validation->set_rules('cidade', 'Cidade', 'trim|required');
        $this->form_validation->set_rules('estado', 'Estado', 'trim|required');

        $this->form_validation->set_error_delimiters('<li>','</li>');
          
        if ($this->form_validation->run())
        {
            $post = $this->input->post();
            $this->session->set_userdata('endereco_pedido', $post);
            redirect ( 'checkout/finalizar_checkout' );
        } else {
            $this->session->set_flashdata ( 'erro', 'Informe os campos corretamente.' . validation_errors () );
            redirect ( 'checkout/formaentrega' );
        }
        
    }
    
    private function getPesoCarrinho() {
        $carrinho = $this->session->userdata ( "carrinho" );
        
        $peso = 0;
        
        if ($carrinho) {
            $carrinho = unserialize ( $carrinho );
            
            if (sizeof ( $carrinho ) > 0) { // debug($carrinho);
                foreach ( $carrinho as $sku_id => $quantidade ) {
                    $infoSKU = $this->Sku_Model->getPorSKU ( $sku_id );
                    
                    if ($quantidade <= 0) {
                        continue;
                    }
                    
                    if ($quantidade > $infoSKU->sku_quantidade) {
                        continue;
                    }
                    
                    $pesoCubico = ($infoSKU->prod_altura * $infoSKU->prod_largura * $infoSKU->prod_comprimento) / 6000;
                    
                    if ($pesoCubico > $infoSKU->prod_peso) {
                        $peso += $pesoCubico;
                    } else {
                        $peso += $infoSKU->prod_peso;
                    }
                }
            }
        }
        
        return convertToValorDB($peso);
    }
    
    public function aumenta($prod_id) {
        $carrinho = $this->session->userdata ( "carrinho" );
        
        if (! $carrinho) {
            $carrinho = array ();
        } else {
            $carrinho = unserialize ( $carrinho );
        }
        
        $infoProduto = $this->Produto_Model->get(array('prod_id' => $prod_id), TRUE);
        
        if ($infoProduto) {
            
            if (! isset ( $carrinho [$prod_id] )) {
                if ($infoProduto->prodest_quantidade > 0) {
                    $carrinho [$prod_id] = 1;
                }
            } else {
                
                if ($infoProduto->prodest_quantidade > $carrinho [$prod_id] + 1) {
                    $carrinho [$prod_id] = $carrinho [$prod_id] + 1;
                }
            }
        }
        
        $carrinho = serialize ( $carrinho );
        $this->session->set_userdata ( "carrinho", $carrinho );
        
        redirect ( "checkout" );
    }

    public function diminui($prod_id) {
        $carrinho = $this->session->userdata ( "carrinho" );
        
        if (! $carrinho) {
            $carrinho = array ();
        } else {
            $carrinho = unserialize ( $carrinho );
        }
        
        $infoProduto = $this->Sku_Model->getPorSKU ( $prod_id );
        
        if ($infoProduto) {
            
            if (! isset ( $carrinho [$prod_id] )) {
                if ($infoProduto->prodest_quantidade > 0) {
                    $carrinho [$prod_id] = 1;
                }
            } else {
                if (($carrinho [$prod_id] - 1) <= 0) {
                    $carrinho [$prod_id] = 1;
                } elseif ($infoProduto->prodest_quantidade > $carrinho [$prod_id] - 1) {
                    $carrinho [$prod_id] = $carrinho [$prod_id] - 1;
                }
            }
        }
        
        $carrinho = serialize ( $carrinho );
        $this->session->set_userdata ( "carrinho", $carrinho );
        
        redirect ( "checkout" );
    }

    public function remove($sku_id) {
        $carrinho = $this->session->userdata ( "carrinho" );
        
        if (! $carrinho) {
            $carrinho = array ();
        } else {
            $carrinho = unserialize ( $carrinho );
        }
        
        unset ( $carrinho [$sku_id] );
        
        $carrinho = serialize ( $carrinho );
        $this->session->set_userdata ( "carrinho", $carrinho );
        
        redirect ( "checkout" );
    }

    public function pagamento() {
        clienteLogado ( true ); //debug($_POST);

        $codformaentrega = $this->input->post ( "codformaentrega" );
        $dadosEntrega = unserialize($this->session->userdata('enderecoentrega'));
        
        if ($codformaentrega) {
            $this->session->set_userdata ( "codformaentrega", $codformaentrega );
        } else {
            $codformaentrega = $this->session->userdata ( "codformaentrega" );
            
            if (! $codformaentrega) {
                redirect ( "checkout/formaentrega" );
            }
        }
        
        $data = array ();
        $data ["SUBTOTAL"] = null;
        $data ["FRETE"] = null;
        $data ["TOTAL"] = null;
        $data ["URLPAGAMENTO"] = site_url ( "checkout/finalizar" );


        $data ["ENDERECO"] = $dadosEntrega['cli_endereco'];
        $data ["CIDADE"] = $dadosEntrega['cli_cidade'];
        $data ["UF"] = $dadosEntrega['cli_uf'];
        $data ["CEP"] = $dadosEntrega['cli_cep'];
        
        $data ["BLC_FORMAPAGAMENTO"] = array ();
        
        $formasPagamento = $this->FormaPagamento_Model->get ( array (
                    "fpag_habilita" => 'S' 
                ) 
            );

        $checked = false;
        
        $valorTotal = 0;

        $valorFrete = 0;
        $valorSubTotal = 0;
        
        $enderecoentrega = $this->session->userdata ( "enderecoentrega" );
        $enderecoentrega = unserialize ( $enderecoentrega ); 
        
        $valorFrete = $this->getPrecoEntrega ( $codformaentrega, $enderecoentrega ['cli_cep'] ); 
        $valorSubTotal = $this->getPrecoCarrinho (); 
        
        $valorTotal += $valorSubTotal + $valorFrete; 
        
        $data ["FRETE"] = number_format ( $valorFrete, 2, ",", "." );
        $data ["SUBTOTAL"] = number_format ( $valorSubTotal, 2, ",", "." );
        $data ["TOTAL"] = number_format ( $valorTotal, 2, ",", "." );
        
        $data ["BLC_PARCELACARTAO"] = array ();
        
        for($i = 1; $i <= 6; $i ++) {
            if ((($valorSubTotal / $i) < 10) && ($i > 1)) {
                continue;
            }
            $data ["BLC_PARCELACARTAO"] [] = array (
                    "NUMEROPARCELA" => $i,
                    "VALORPARCELA" => number_format ( $valorSubTotal / $i, 2, ",", "." ) 
            );
        }

        $arrayCartao = array("15", "16");
        
        if ($formasPagamento) {
            foreach ( $formasPagamento as $fp ) { 
                
                $valorComDesconto = $valorSubTotal;
                
                if ($fp->fpag_desconto > 0) {
                    $valorComDesconto = $valorComDesconto - (($valorSubTotal * $fp->fpag_desconto) / 100);
                }
                
                $valorComDesconto += $valorFrete;
                
                $data ["BLC_FORMAPAGAMENTO"] [] = array (
                        "CODFORMAPAGAMENTO" => $fp->fpag_id,
                        "NOMEFORMAPAGAMENTO" => $fp->fpag_nome,
                        "CHECKED_FE" => (! $checked) ? "checked" : null,
                        "TIPO" => $fp->fpag_tipo,
                        "VALOR" => number_format ( $valorComDesconto, 2, ",", "." ) 
                );
                if ((! $checked)) {
                    $checked = true;
                }
            }
        }

        $data ["BLC_PERMITECOMPRAR"] = array ();
        
        if ($checked) {
            $data ["BLC_PERMITECOMPRAR"] [] = array ();
        } 

        $this->parser->parse ( "formapagamento", $data );
    }

    private function getPrecoCarrinho($codformapagamento = false) {
        $carrinho = $this->session->userdata ( "carrinho" );
        
        if (! $carrinho) {
            $carrinho = array ();
        } else {
            $carrinho = unserialize ( $carrinho );
        }
        
        $valorTotal = 0;
        
        foreach ( $carrinho as $codsku => $quantidade ) {
            $infoSKU = $this->Sku_Model->getPorSKU ( $codsku );
            
            if (($infoSKU->prod_valorpromocional > 0) && ($infoSKU->prod_valor > $infoSKU->prod_valorpromocional)) {
                $valorFinal = $infoSKU->prod_valorpromocional;
            } else {
                $valorFinal = $infoSKU->prod_valor;
            }
            $valorTotal += $valorFinal * $quantidade;
        }
        
        if ($codformapagamento) {
            $this->load->model ( "FormaPagamento_Model" );
            
            $formasPagamento = $this->FormaPagamento_Model->get ( array (
                    "fpag_id" => $codformapagamento 
            ), TRUE );
            
            if ($formasPagamento->fpag_desconto > 0) {
                $valorTotal = $valorTotal - (($valorTotal * $formasPagamento->fpag_desconto) / 100);
            }
        }
        
        return $valorTotal;
    }

    private function getPrecoEntrega($codformaentrega, $cep) {
        $pesoCarrinhoOriginal = $this->getPesoCarrinho (); 

        $formasEntrega = $this->PrecoFormaEntrega_Model->getPrecoEntrega ( $cep, $pesoCarrinhoOriginal, $codformaentrega );

        if ($formasEntrega) {
            return $formasEntrega->pfent_valor; 
        } else {
            $this->load->model ( 'FormaEntrega_Model' );
            
            $formasEntrega = $this->FormaEntrega_Model->get(array(
                    "fent_id" => $codformaentrega 
            ), TRUE ); 
                
            $totalPacotes = 1;
                
            if ($pesoCarrinhoOriginal > 30) {
                $totalPacotes = $pesoCarrinhoOriginal / 30;
                $totalPacotes = ceil ( $totalPacotes );
                $pesoCarrinho = 30;
            } else {
                $pesoCarrinho = $pesoCarrinhoOriginal;
            }

            $url = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?";
            $url .= "&sCepOrigem=39508000";
            $url .= "&sCepDestino=" . $cep; 
            $url .= "&nVlPeso=" . $pesoCarrinho;
            $url .= "&nCdServico=" . $formasEntrega->fent_codcorreios;
            $url .= "&nCdFormato=1&nVlComprimento=25&nVlAltura=2&nVlLargura=11";
            $url .= "&sCdMaoPropria=n&nVlValorDeclarado=0&sCdAvisoRecebimento=n";
            $url .= "&nVlDiametro=0&StrRetorno=xml";
            
            $ch = curl_init ();
            curl_setopt ( $ch, CURLOPT_URL, $url );
            curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 2 );
            curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 3 );
            curl_setopt ( $ch, CURLOPT_USERAGENT, 'Checkout' );
            $query = curl_exec ( $ch ); 
            curl_close ( $ch );
            
            if ($query) {
                
                $xml = new SimpleXMLElement ( $query );

                foreach ( $xml as $cor ) {
                    if ($cor->Erro == '0') {
                        $preco = $totalPacotes * ( float ) $cor->Valor;
                        return convertToValorDB($preco);
                    }
                }
            }
        }
    }

    public function finalizar() {
        clienteLogado ( true );
        $codformapagamento = $this->input->post ( "codformapagamento" );
        $codformaentrega = $this->session->userdata ( "codformaentrega" ); 
        
        $this->load->model ( "FormaPagamento_Model", "FormaPagamento_Model" );
        
        $formasPagamento = $this->FormaPagamento_Model->get ( array (
                "fpag_id" => $codformapagamento 
        ), TRUE ); 
        
        $numerocartao = $this->input->post ( "numerocartao" );
        $codigoverificador = $this->input->post ( "codigoverificador" );
        $mescartao = $this->input->post ( "mescartao" );
        $anocartao = $this->input->post ( "anocartao" );
        $parcela = $this->input->post ( "parcela" );

        if ($formasPagamento->fpag_tipo == 2)  {
            
            $this->load->library ( 'form_validation' );
            
            $this->form_validation->set_rules ( 'numerocartao', 'Número do Cartão', 'required' );
            $this->form_validation->set_rules ( 'codigoverificador', 'Código Verificador', 'required' );
            $this->form_validation->set_rules ( 'mescartao', 'Mês do Cartão', 'required' );
            $this->form_validation->set_rules ( 'anocartao', 'Ano do Cartão', 'required' );
            $this->form_validation->set_rules ( 'parcela', 'Parcela', 'required' );
            
            if ($this->form_validation->run () == FALSE) {
                $this->session->set_flashdata ( 'erro', 'Sequência de números do cartão inválida.' . validation_errors () );
                redirect ( "checkout/pagamento" );
            }
        } 
        
        if (! $codformaentrega) {
            redirect ( "checkout/pagamento" );
        }
        
        $carrinho = $this->session->userdata ( "carrinho" );
        $enderecoentrega = $this->session->userdata ( "enderecoentrega" );
        $comprador = $this->session->userdata ( 'loja' ); //debug($comprador, false);
        
        if ((! $carrinho) || (sizeof ( $carrinho ) === 0)) {
            redirect ();
        }
        
        if ((! $enderecoentrega) || (sizeof ( $enderecoentrega ) === 0)) {
            redirect ();
        }
        
        $carrinho = unserialize ( $carrinho );
        $enderecoentrega = unserialize ( $enderecoentrega ); 
        $valorFrete = $this->getPrecoEntrega ( $codformaentrega, $enderecoentrega ['cli_cep'] );
        $valorSubTotal = $this->getPrecoCarrinho ( $codformapagamento );
        
        $valorTotal = $valorFrete + $valorSubTotal;
        
        $this->load->model ( "Carrinho_Model" );
        $this->load->model ( "CarrinhoItem_Model" );
        
        $objeto = array (
                "car_datacompra" => date ( "Y-m-d H:i:s" ),
                "car_valorcompra" => convertToValorBR($valorSubTotal),
                "car_valorfrete" => $valorFrete,
                "car_valorcomprafinal" => convertToValorBR($valorTotal),
                "car_idcliente" => $comprador ["cli_id"],
                "car_idformaentrega" => $codformaentrega,
                "car_idformapagamento" => $codformapagamento,
                "car_endentrega" => $enderecoentrega ['cli_endereco'],
                "car_cidadeentrega" => $enderecoentrega ['cli_cidade'],
                "car_ufentrega" => $enderecoentrega ['cli_uf'],
                "car_cepentrega" => $enderecoentrega ['cli_cep'] 
        ); 
        
        $codCarrinho = $this->Carrinho_Model->post ( $objeto ); 

        if($codCarrinho) {
            echo 'True';
        } else {
            echo 'false';
        }

        
        foreach ( $carrinho as $codsku => $quantidade ) {
            
            $infoSKU = $this->Sku_Model->getPorSKU ( $codsku );
            
            if (($infoSKU->prod_valorpromocional > 0) && ($infoSKU->prod_valor > $infoSKU->prod_valorpromocional)) {
                $valorFinal = $infoSKU->prod_valorpromocional;
            } else {
                $valorFinal = $infoSKU->prod_valor;
            }
            
            if ($formasPagamento->fpag_desconto > 0) {
                $valorFinal = $valorFinal - (($valorFinal * $formasPagamento->fpag_desconto) / 100);
            }
            
            $objetoItem = array (
                    "item_valor" => $valorFinal,
                    "item_quantidade" => $quantidade,
                    "item_idcarrinho" => $codCarrinho,
                    "item_idsku" => $codsku 
            ); 
            
            $this->CarrinhoItem_Model->post ( $objetoItem );
        } 
        
        if ($formasPagamento->fpag_tipo == 2) {
            
            $this->load->library ( 'WebServiceCielo' );
            
            $dataHoraPedido = date('c'); 
            $dataHoraPedido = substr($dataHoraPedido, 0, 19); 
            
            switch($codformapagamento) {
            	case 3:
            	    $bandeira = 'mastercard';
            	    break;
        	    case 2:
        	        $bandeira = 'visa';
        	        break;
            }
            
            if ($parcela == 1) {
                $produto = 1;
            } else {
                $produto = 2;
            }
            
            $cartao = new stdClass();
            $pedido = new stdClass();
            
            $cartao->numero = $numerocartao;
            $cartao->ano = $anocartao;
            $cartao->mes = $mescartao;
            $cartao->codigoseguranca = $codigoverificador;
            
            $pedido->codcarrinho = $codCarrinho;
            $pedido->valor = $valorTotal * 100;
            $pedido->datahora = $dataHoraPedido;
            $pedido->bandeira = $bandeira;
            $pedido->produto = $produto;
            $pedido->parcelas = $parcela;
            
            $autorizacao = $this->webservicecielo->enviaAutorizacao($cartao, $pedido);
            
            if (!$autorizacao->error) {
                redirect("conta/resumo/".$codCarrinho);
            } else {
                $this->session->set_flashdata ( 'erro', "Não foi possível processar o seu pagamento." );
                redirect ( "checkout/pagamento" );
            }
        }

        $this->session->unset_userdata('carrinho');
        
        redirect("conta/resumo/".$codCarrinho);
    }

    public function finalizar_checkout() 
    {
        if (!$this->login_business->logged()) {
            redirect('login_ecommerce/1');
        }

        $usuario = $this->session->userdata('usuario');
        $carrinho = $this->CarrinhoBusiness->getCarrinho();
        $endereco = $this->session->userdata('endereco_pedido');
        $forma_pagamento = $this->input->post()['forma-pagamento'];

        $this->db->trans_begin();

        if ($carrinho['total'] > 0) {
            $message = $this->PedidoBusiness->gerarPedidoRecompra($usuario->usu_id, $carrinho, $endereco);
        } else {
            $this->db->trans_rollback();
            redirect('checkout');
        }
        /*******************************************************************/
        $url = 'payment/confirmation';

        if ($message['status'] == 2) {
            $message = $this->PedidoBusiness->aprovar($message['idPedido']);
            if ($message['status'] <> 1) {
                $this->db->trans_rollback();
                $this->session->set_flashdata ( 'erro', $message['descricao'] );
                redirect('checkout');
            }
            $this->session->set_userdata('payment_order', $message['idPedido']);
        }
        if ($message['status'] <> 1 AND $message['status'] <> 2) {
            $this->db->trans_rollback();
            $this->session->set_flashdata ( 'erro', $message['descricao'] );
            redirect('checkout');
        }
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } 

        $idPedido = $message['idPedido'];

        $this->db->trans_commit();

        $usuario = $this->session->userdata('carrinho');

        $this->session->unset_userdata('carrinho');
        $this->session->unset_userdata('endereco_pedido');
        
        redirect();       
    }
}