<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Departamento extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->library('user_agent');
		$this->load->model('Produto_Model');
		$this->load->model('Departamento_Model');
	}

	public function listagem($dep_id) 
	{
		$carrinho = $this->session->userdata ( "carrinho" );
    
    $logged = $this->login_business->logged();

    if (! $carrinho) {
        $carrinho = array ();
    } else {
        $carrinho = unserialize ( $carrinho );
    }

		$pg = $this->input->get("pg");
		$to = $this->input->get("to");
		$oc = $this->input->get("oc");

		if (!$pg) {
			$pg = 0;
		} else {
			$pg--;
		}

		$offset = LINHAS_PESQUISA_DASHBOARD * $pg;

		switch ($to) {
			case 'asc':
			case 'desc':
					$to = $to;
					break;
			default:
					$to = "asc";
		}

		switch ($oc) {
			case 'nome':
					$oc = 'prod_nome';
					break;
			case 'valor':
					$oc = 'prod_valor';
					break;
			default:
					$oc = "prod_nome";
		}
		
		$filtro = array("dp.pdep_pdep_id" => $dep_id);

		$produto = $this->Produto_Model->getDepartamento($filtro, FALSE, $offset, 
		LINHAS_PESQUISA_DASHBOARD, $oc, $to); 

		if (!$produto) {
			$this->session->set_flashdata ( 'erro', 'Nenhum produto encontrado para este Departamento.' );
			redirect();
		}
		
		$html = montaListaProduto($produto, "lista_busca");
		
		$data = array(); 
    $data = array(
      'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
      'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
      'meta_url' => base_url(),
      'meta_classification' => "home",
      'pageTitle' => "LoudSon.gs",
      'searchInput' => "",
      'searchOptions' => "",
      'menu' => "Dashboard",

      'styles' => [
          ['url' => base_url().'assets/css/loja.css'],
      ],

      'scripts' => [
          ['url' => base_url().'assets/js/loja.js'],
          ['url' => base_url().'public/feirarecife/js/feira.js'],
      ],
    );

    $data['BLC_LOGADO'] = array();
    $data['BLC_DESLOGADO'] = array();

    if ($logged) 
    {
      $user = $this->session->userdata('usuario');
      $usuario = $user->usu_login;
      $data['BLC_LOGADO'][] = array(
        'USUARIO' => $usuario,
        'URLDESLOGAR' => base_url().'logout/1',
        'ACAO' => 'Login',
      );
      $data['USERCONTA'] = 'MINHA CONTA';
      $data['URLUSERCONTA'] = base_url().'backoffice';
      $data['CARRINHO'] = 'CARRINHO';
      $data['URLCARRINHO'] = base_url().'checkout';
      $data['USERACAO'] = 'SAIR';
      $data['URLUSERACAO'] = base_url().'logout/1';
    } else {
      $data['BLC_DESLOGADO'][] = array(
        'URLLOGAR' => base_url().'login_ecommerce',
      );
      $data['USERCONTA'] = 'MEU BACKOFFICE';
      $data['URLUSERCONTA'] = base_url().'backoffice';
      $data['CARRINHO'] = 'CARRINHO';
      $data['URLCARRINHO'] = base_url().'checkout';
      $data['USERACAO'] = 'LOGIN';
      $data['URLUSERACAO'] = base_url().'login_ecommerce';
    }

		$data["LISTAGEM"] = $html;
		$data["BLC_DEPARTAMENTOS"] = array();
    $data ["BLC_PRODUTOS"] = array();  
    $data ["BLC_FINALIZAR"] = array();
    $data ["BLC_SEMPRODUTOS"] = array();
    $data ["TOTALPEDIDO"] = NULL;

		$this->load->model("Departamento_Model");
		
	$departamentos = $this->Departamento_Model->getDepartamentosPais(array('dep_departamentopai IS NULL' => null), true);

		foreach($departamentos as $dep) {
			
			$filhos = array();
			
			$departamentosFihos = $this->Departamento_Model->getDepartamentosFilhos($dep->dep_id);

			foreach($departamentosFihos as $depf) {
				
				$filhos[] = array(
					"URLDEPARTAMENTO_FILHO" => site_url("departamento/".$depf->dep_id),
					"NOMEDEPARTAMENTO_FILHO" => $depf->dep_nome
					
				);
			} 
			
			$data["BLC_DEPARTAMENTOS"][] = array(
				"URLDEPARTAMENTO" => site_url("departamento/".$dep->dep_id),
				"NOMEDEPARTAMENTO" => $dep->dep_nome,
				"BLC_DEPARTAMENTOSFILHOS" => $filhos,
				
			);
		}
		

		$totalItens = $this->Produto_Model->getDepartamentoTotal($filtro);
		$queryString = "?".$_SERVER['QUERY_STRING'];

		if (!$queryString) {
			$queryString = "?";
		}

		$data["BLC_ORDENACAO"] = array();
		$data["BLC_ORDENACAO"][] = array(
			"ITENSEXIBICAO" => sizeof($produto),
			"TOTALITENS" => $totalItens,
			"URLATUAL" => current_url().$queryString
		);

		$totalPaginas = ceil($totalItens / LINHAS_PESQUISA_DASHBOARD);

		$data["BLC_PAGINACAO"] = array();

		$paginas = array();

		for ($i = 1; $i <= $totalPaginas; $i++) {
			$paginas[] = array(
				"URLPAGINA" => current_url()."?pg={$i}&oc={$oc}&to={$to}",
				"INDICE" => $i
			);
		}

		$data["BLC_PAGINACAO"][] = array(
			"BLC_PAGINA" => $paginas
		);
		

		$layout_data['content_header'] = $this->parser->parse('feira/header', $data, true);
		$layout_data['content_body'] = $this->parser->parse('feira/departamento', $data, true);
		$layout_data['content_footer'] = $this->parser->parse('feira/footer', $data, true);
		$html = $this->parser->parse("layouts/feiraRecife", $layout_data);
		
	}
	
	public function _remap($method, $params = array()) {
		if (method_exists($this, $method)) {
			return call_user_func(array($this, $method), $params);
		}else {
			$this->listagem($method);
		}
	}
}