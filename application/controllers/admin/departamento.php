<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Departamento extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->layout = LAYOUT_DASHBOARD;
		$this->load->library('user_agent');
		$this->load->model('Departamento_Model');
		$this->load->model('DepartamentoFoto_Model');
		$this->load->library('image_lib');
	}

	public function index() 
	{
		$ferror = $this->session->flashdata('erro');
		$fsucesso = $this->session->flashdata('sucesso');

		$data	= array(
      'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
      'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
      'meta_url' => base_url(),
      'meta_classification' => "home",
      'pageTitle' => "LoudSon.gs",
      'searchInput' => "",
      'searchOptions' => "",
      'menu' => "Carrinho",

			'BLC_DADOS' 	 		=> array(),
			'BLC_SEMDADOS' 		=> array(),
			'BLC_PAGINAS' 		=> array(),
			'URLADICIONAR'    => site_url('admin/departamento/adicionar/'),
			'URLLISTAR'    		=> site_url('admin/departamento'),
		);

    $layout_data = array(
        'styles' => [

        ],

        'scripts' => [
            
        ],
    );

		$data['MSGERROR'] = null;
		$data['MSGSUCCESS'] = null;

    if ($ferror) {
    	$data['MSGERROR'] = criaAlerta($ferror, 'danger', 'Erro');
    }
    if ($fsucesso) {
    	$data['MSGSUCCESS'] = criaAlerta($fsucesso, 'success', 'Sucesso');
    }

		$pagina = $this->input->get('pagina');

		if (!$pagina) {
			$pagina = 0;
		} else {
			$pagina = ($pagina - 1) * LINHAS_PESQUISA_DASHBOARD;
		}

		$res = $this->Departamento_Model->get(array(), FALSE, $pagina, FALSE);

		if ($res) {
			foreach ($res as $r) {
				$data['BLC_DADOS'][] = array(
					"NOME" 			 => $r->dep_nome,
					"URLTHUMB" 			 => base_url("assets/img/departamento/80x80/".$r->depfot_id.".".$r->depfot_extensao),
					"NOMEPAI" 	 => (empty($r->dep_departamentopai)) ? '-' : $r->nomepai,
					"URLEDITAR"  => site_url('admin/departamento/editar/'.$r->dep_id),
					"URLEXCLUIR" => site_url('admin/departamento/excluir/'.$r->dep_id)
				);
			}
		} else {
			$data['BLC_SEMDADOS'][] = array();
		}

		$totalItens 		= $this->Departamento_Model->getTotal();
		$totalPaginas 	= ceil($totalItens/LINHAS_PESQUISA_DASHBOARD);

		$pagina			= $this->input->get('pagina');
	
		$indicePg		= 1;
		if (!$pagina) {
		    $pagina = 1;
		}
		$pagina			= ($pagina==0)?1:$pagina;

		if ($totalPaginas > $pagina) {
			$data['HABAPROXIMO']		= NULL;
			$data['URLPROXIMO' ]		= site_url('admin/departamento?pagina='.($pagina+1));
		} else {
			$data['HABAPROXIMO']		= 'disabled';
			$data['URLPROXIMO' ]		= '#';
		}

		if ($pagina <= 1) {
			$data['HABANTERIOR']	= 'disabled';
			$data['URLANTERIOR' ]	= '#';
		} else {
			$data['HABANTERIOR']	= NULL;
			$data['URLANTERIOR' ]	= site_url('admin/departamento?pagina='.($pagina-1));
		}

		while ($indicePg <= $totalPaginas) {
			$data['BLC_PAGINAS'][] = array(
				'LINK'		=> ($indicePg == $pagina) ? 'active' : NULL,
				'INDICE'	=> $indicePg,
				'URLLINK' => site_url('admin/departamento?pagina='.$indicePg)
			);

			$indicePg++;
		}

		// $this->parser->parse('admin/departamento_listar', $data);
        
    $layout_data['content_body'] = $this->parser->parse ('admin/departamento_listar', $data, true);
    $this->parser->parse ("layouts/inspinia", $layout_data);

	}

	public function adicionar() 
	{
		$ferror = $this->session->flashdata('erro');
		$fsucesso = $this->session->flashdata('sucesso');

		$data = array(
      'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
      'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
      'meta_url' => base_url(),
      'meta_classification' => "home",
      'pageTitle' => "LoudSon.gs",
      'searchInput' => "",
      'searchOptions' => "",
      'menu' => "Carrinho",
      
			'ACAO' 			 	 					 => 'Novo',
			'BLC_DEPARTAMENTOS'			 => array(),
			'sel_iddepartamentopai'	 => NULL,
			'hab_iddepartamentopai'  => NULL,
			'dep_id' 			 					 => NULL,
			'dep_nome' 		 					 => '',
			'dep_departamentopai' 	 => NULL,
			'required'   	 					 => ''
		);

    $layout_data = array(
        'styles' => [

        ],

        'scripts' => [
            
        ],
    );

		$data['MSGERROR'] = null;
		$data['MSGSUCCESS'] = null;

    if ($ferror) {
    	$data['MSGERROR'] = criaAlerta($ferror, 'danger', 'Erro');
    }
    if ($fsucesso) {
    	$data['MSGSUCCESS'] = criaAlerta($fsucesso, 'success', 'Sucesso');
    }

		$dep = $this->Departamento_Model->get(array("d.dep_departamentopai IS NULL" => NULL));

		foreach ($dep as $d) {
			$data['BLC_DEPARTAMENTOS'][] = array(
				"IDDEPARTAMENTO"				=> $d->dep_id,
				"NOME" 									=> $d->dep_nome,
				"sel_iddepartamentopai" => NULL
			);
		}

		$this->setURL($data);

		// $this->parser->parse('admin/departamento_form', $data);
        
    $layout_data['content_body'] = $this->parser->parse ('admin/departamento_form', $data, true);
    $this->parser->parse ("layouts/inspinia", $layout_data);
	}

	public function editar($id) 
	{
		$ferror = $this->session->flashdata('erro');
		$fsucesso = $this->session->flashdata('sucesso');

		$data = array(
      'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
      'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
      'meta_url' => base_url(),
      'meta_classification' => "home",
      'pageTitle' => "LoudSon.gs",
      'searchInput' => "",
      'searchOptions' => "",
      'menu' => "Carrinho",
      
			'ACAO' 								=> 'Edição',
			'BLC_DEPARTAMENTOS' 	=> array()
		);

    $layout_data = array(
        'styles' => [

        ],

        'scripts' => [
            
        ],
    );

		$data['MSGERROR'] = null;
		$data['MSGSUCCESS'] = null;

    if ($ferror) {
    	$data['MSGERROR'] = criaAlerta($ferror, 'danger', 'Erro');
    }
    if ($fsucesso) {
    	$data['MSGSUCCESS'] = criaAlerta($fsucesso, 'success', 'Sucesso');
    }

		$totalFilhos = $this->Departamento_Model->getTotal(array('dep_departamentopai' => $id));

		$res = $this->Departamento_Model->get(array('d.dep_id' => $id), TRUE);

		if ($totalFilhos > 0) {
			$data['BLC_DEPARTAMENTOS'] 			= array();
			$data['hab_iddepartamentopai']	= 'disabled="disabled"';
		} else {
			$dep = $this->Departamento_Model->get(array("d.dep_departamentopai IS NULL" => NULL, "d.dep_id != " => $id));

			foreach ($dep as $d) {
				$data['BLC_DEPARTAMENTOS'][] = array(
					"IDDEPARTAMENTO"				=> $d->dep_id,
					"NOME" 									=> $d->dep_nome,
					"sel_iddepartamentopai" => ($res->dep_departamentopai == $d->dep_id) ? 'selected="selected"' : NULL
				);
			}
		}

		if ($res) {
			foreach ($res as $chave => $valor) {
				$data[$chave] = $valor;
			}

		} else {
			show_error(utf8_decode("DEPARTAMENTO NÃO ENCONTRADOS!"), 500, utf8_decode("Dados relacionados não foram encontrados!"));
		}

		$this->setURL($data);
        
    $layout_data['content_body'] = $this->parser->parse ('admin/departamento_form', $data, true);
    $this->parser->parse ("layouts/inspinia", $layout_data);
	}

	public function salvar() {

		$dep_id 							= $this->input->post('dep_id');
		$dep_nome 					  = $this->input->post('dep_nome');
		$dep_departamentopai 	= $this->input->post('dep_departamentopai');
		$dep_foto							= $this->input->post('dep_foto');

		$errors   = FALSE;
		$mensagem = NULL;

		if (!$dep_nome) {
			$errors		= TRUE;
			$mensagem .= "Informe nome do departamento\n";
		}

		if (!$errors) {

			$itens = array(
				'dep_nome'   					 => $dep_nome,
				'dep_departamentopai'  => (($dep_departamentopai) ? $dep_departamentopai : NULL)
			);

			if ($dep_id) {
				$dep_id = $this->Departamento_Model->update($itens, $dep_id);
			} else {
				$dep_id = $this->Departamento_Model->post($itens);
			}


			if ($dep_id) {

				$this->fotoUpload($dep_id);

				$this->session->set_flashdata('sucesso', 'Novo departamento inserido com sucesso!');
				redirect('admin/departamento');
			} else {
				$this->session->set_flashdata('erro', 'Ocorreu um erro na operação!');

				if ($dep_id) {
					redirect('admin/departamento/editar'.$dep_id);
				} else {
					redirect('admin/departamento/adicionar');
				}

			}

		} else {
			$this->session->set_flashdata('erro', nl2br($mensagem));

			if ($dep_id) {
				redirect('admin/departamento/editar/'.$dep_id);
			} else {
				redirect('admin/departamento/adicionar');
			}

		}

	}

	private function setURL(&$data) {
		$data['URLLISTAR'] = site_url('admin/departamento');
		$data['ACAOFORM']  = site_url('admin/departamento/salvar');
	}

	public function excluir($id) 
	{
		$foto_id = $this->DepartamentoFoto_Model->get(array('depfot_iddepartamento' => $id)); 
		
		$this->removefoto($foto_id[0]->depfot_id, $id);
		$this->DepartamentoFoto_Model->delete($foto_id[0]->depfot_id, $id);

		$res = $this->Departamento_Model->delete($id);

		if ($res) {
			$this->session->set_flashdata('sucesso', 'departamento removido!');
		} else {
			$this->session->set_flashdata('erro', 'departamento não pode ser removido!');
		}

		redirect ('admin/departamento');

	}

	public function fotoUpload($codepartamento = null) 
	{
		if (!$codepartamento) {
			$codepartamento	= $this->input->post('dep_id');
		}

		$arquivo	= $_FILES['dep_foto']; 
		
		if ($arquivo['error']) {
			return FALSE;
		}
		
		$arquivoNome = $arquivo["name"];
		
		$extensao	= strtolower(pathinfo($arquivoNome, PATHINFO_EXTENSION));
		
		$foto = array(
			"depfot_iddepartamento" => $codepartamento,
			"depfot_extensao"	=> $extensao
		); 

		$foto_id = $this->DepartamentoFoto_Model->get(array('depfot_iddepartamento' => $codepartamento)); 
		
		if ($foto_id) 
		{
			$this->removefoto($foto_id[0]->depfot_id, $codepartamento);

			$codfoto = $this->DepartamentoFoto_Model->update($foto, $foto_id[0]->depfot_id); 
			
			$enderecoFoto	= 'assets/img/departamento/original/'.$codfoto.'.'.$extensao;
			
			move_uploaded_file($arquivo['tmp_name'], FCPATH.$enderecoFoto);
			
			//CRIA A MINIATURA DA GALERIA DE FOTOS
			$this->redimensionaFoto($codfoto, $extensao, 80, 80);
			
			//CRIA A MINIATURA DA PÁGINA INICIAL
			$this->redimensionaFoto($codfoto, $extensao, 150, 150); 

			//CRIA A FOTO PARA A GRID DE CARRINHO
			$this->redimensionaFoto($codfoto, $extensao, 378, 327);
			
			//CRIA A FOTO PARA A FICHA DE PRODUTOS
			$this->redimensionaFoto($codfoto, $extensao, 500, 500);
		} 
		else 
		{
			$codfoto = $this->DepartamentoFoto_Model->post($foto); 
			
			$enderecoFoto	= 'assets/img/departamento/original/'.$codfoto.'.'.$extensao;
			
			move_uploaded_file($arquivo['tmp_name'], FCPATH.$enderecoFoto);
			
			//CRIA A MINIATURA DA GALERIA DE FOTOS
			$this->redimensionaFoto($codfoto, $extensao, 80, 80);
			
			//CRIA A MINIATURA DA PÁGINA INICIAL
			$this->redimensionaFoto($codfoto, $extensao, 150, 150); 

			//CRIA A FOTO PARA A GRID DE CARRINHO
			$this->redimensionaFoto($codfoto, $extensao, 378, 327);
			
			//CRIA A FOTO PARA A FICHA DE PRODUTOS
			$this->redimensionaFoto($codfoto, $extensao, 500, 500);
		}
	
		
	}

	// REDIMENSIONA IMAGEM
	private function redimensionaFoto($coddepartamentofoto, $extensao, $altura, $largura) {
	
		if (!is_dir(FCPATH."assets/img/departamento/{$altura}x{$largura}/")){
			mkdir(FCPATH."assets/img/departamento/{$altura}x{$largura}/");
		}
		
		$configImagem['image_library']	= 'gd2'; //BIBLIOTECA RESPONSÁVEL PELO REDIMENSIONAMENTO
		$configImagem['source_image']	= FCPATH.'assets/img/departamento/original/'.$coddepartamentofoto.'.'.$extensao;
		$configImagem['new_image']		= FCPATH."assets/img/departamento/{$altura}x{$largura}/".$coddepartamentofoto.'.'.$extensao;
		$configImagem['create_thumb']	= FALSE;
		$configImagem['maintain_ratio']	= TRUE;
		$configImagem['width']			= $largura;
		$configImagem['height']			= $altura;
		
		$this->load->library('image_lib');
		$this->image_lib->clear();
		$this->image_lib->initialize($configImagem);
		
		$this->image_lib->resize();
	}

	// REMOVE FOTOS DO BD E DO SERVER
	public function removefoto($idfoto, $iddepartamento) {
		$condicao = array(
			"depfot_id" 			 => $idfoto,
			"depfot_iddepartamento" => $iddepartamento
		);

		$infoFoto = $this->DepartamentoFoto_Model->get($condicao, TRUE);
		
			unlink(FCPATH.'assets/img/departamento/original/'.$idfoto.'.'.$infoFoto->depfot_extensao);
			unlink(FCPATH.'assets/img/departamento/80x80/'.$idfoto.'.'.$infoFoto->depfot_extensao);
			unlink(FCPATH.'assets/img/departamento/150x150/'.$idfoto.'.'.$infoFoto->depfot_extensao);
			unlink(FCPATH.'assets/img/departamento/378x327/'.$idfoto.'.'.$infoFoto->depfot_extensao);
			unlink(FCPATH.'assets/img/departamento/500x500/'.$idfoto.'.'.$infoFoto->depfot_extensao);

		return $idfoto;

	}

}