<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    
/**
 * Description of cadastro
 *
 * @author JOAO PAULO
 */
class pedido extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('business/pedido_business', 'PedidoBusiness');
    }

    public function aprovar($idPedido) {
        $this->db->trans_begin(); 

        $message = $this->PedidoBusiness->aprovar($idPedido);
        // debug($message); die;
        
        $status = $message['status'];

        if ($status <> 1) {
            $this->db->trans_rollback();
            $this->session->set_flashdata('message', '<div class="alert alert-danger">Erro '.$message['status'].' - '.$message['mensagem'].'</div>');
            //Mensagem de erro 
        }
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            //Mensagem de erro 
        } else {
            
            $this->db->trans_commit();
            
            // $idPedido = $message['idPedido'];

            $this->session->set_flashdata('message', '<div class="alert alert-success">Cadastro aprovado com sucesso.');   
            //redirecionar para a tela de sucesso
        }

        redirect('admin/cadastro/listarPendentes');
    } 
}