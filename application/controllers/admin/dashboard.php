<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        $this->load->model('business/pedido_business', 'PedidoBusiness');       
    }

    public function index()
    {
        $ferror = $this->session->flashdata('erro');
        $fsucesso = $this->session->flashdata('sucesso');

        $body_data['MSGERROR'] = null;
        $body_data['MSGSUCCESS'] = null;

        if ($ferror) {
            $body_data['MSGERROR'] = criaAlerta($ferror, 'danger', 'Erro');
        }
        if ($fsucesso) {
            $body_data['MSGSUCCESS'] = criaAlerta($fsucesso, 'success', 'Sucesso');
        }

        $base_url = base_url();

        // Menu superior de navegação
        $navigation_data['menuNavegacao'] = "home";
        
        $data = array(
          'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
          'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
          'meta_url' => "$base_url",
          'meta_classification' => "home",
          'pageTitle' => "LoudSon.gs",
          'searchInput' => "",
          'searchOptions' => "",
          'menu' => "Admin",

          'styles' => [
              #['url' => 'teste'],
              #['url' => 'teste2']
          ],

          'scripts' => [
              ['url'=> 'public/js/pedido.js'],
              #['url'=> 'script']
          ],
        );
        // Informações da <header>
        $body_data['titulo'] = 'Administração';

        $body_data['pedidos'] = $this->PedidoBusiness->getAll();

        $body_data['data'] = array();

          // Carregando as variáveis de conteúdo
        $data['content_navigation'] = $this->load->view('navigation', $navigation_data, true);
        $data['content_body'] = $this->parser->parse('admin/pedidoshome', $body_data, true);

        $this->parser->parse('layouts/inspinia', $data);
    }   

    public function pedidoDetalhes($idPedido)
    {
         $base_url = base_url();

          // Menu superior de navegação
          $navigation_data['menuNavegacao'] = "home";
          
          $data = array(
            'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
            'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
            'meta_url' => "$base_url",
            'meta_classification' => "home",
            'pageTitle' => "LoudSon.gs",
            'searchInput' => "",
            'searchOptions' => "",
            'menu' => "Admin",

            'styles' => [
                #['url' => 'teste'],
                #['url' => 'teste2']
            ],

            'scripts' => [
                ['url'=> 'public/js/pedido.js'],
                #['url'=> 'script']
            ],
          );
          // Informações da <header>
          $body_data['titulo'] = 'Administração';

          $body_data['pedido'] = $this->PedidoBusiness->getDadosDoPedido($idPedido);

          $body_data['data'] = array();

          // Carregando as variáveis de conteúdo
        $data['content_navigation'] = $this->load->view('navigation', $navigation_data, true);
        // $data['content_body'] = $this->parser->parse('admin/pedidodetalhes', $body_data, true);
        $data['content_body'] = $this->parser->parse('admin/invoice', $body_data, true);

        $this->parser->parse('layouts/inspinia', $data);
    }  

    public function getItemHtml($idPedido) 
    {
        $data = array();
        $pedido = $this->PedidoBusiness->getPedidos(array('ped_id' => $idPedido));
        $data['pedido'] = $pedido[0];
        debug($idPedido, true);

        $data['itensPedido'] = $this->PedidoBusiness->getDadosDoPedido($idPedido);

        echo $this->load->view('admin/invoice', $data, true);
    }

    public function imprimirPedido($idPedido) 
    {
        $data = array();
        $pedido = $this->PedidoBusiness->getPedidos(array('ped_id' => $idPedido));
        $data['pedido'] = $pedido[0];

        $data['itensPedido'] = $this->PedidoBusiness->getDadosDoPedido($idPedido);
        
        echo $this->load->view('admin/printPedido', $data, true);
    }
}
