<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produto extends CI_Controller {

    var $bucket = '';
    var $pasta_produtos = '';
    var $amazon_url = '';
    var $tamanho_fotos = '';

	public function __construct() {
		parent::__construct();

		$this->layout = LAYOUT_DASHBOARD;
		$this->load->library('user_agent');
		$this->load->model('Produto_Model');
		$this->load->model('Departamento_Model');
		$this->load->model('ProdutoDepartamento_Model');
		$this->load->model('ProdutoFoto_Model');
		$this->load->library('image_lib');
	    $this->load->config('configS3');

	    $ambiente = $this->config->config['ambiente'];
	    $this->bucket = $this->config->config[$ambiente]['bucket'];
	    $this->amazon_url = $this->config->config['amazon_url'];
	    $this->pasta_produtos = $this->config->config['produtos'];
	    $this->tamanho_fotos = $this->config->config['tamanhos'];
	}

	public function index() 
	{
		$ferror = $this->session->flashdata('erro');
		$fsucesso = $this->session->flashdata('sucesso');

		$data	= array(
      'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
      'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
      'meta_url' => base_url(),
      'meta_classification' => "home",
      'pageTitle' => "LoudSon.gs",
      'searchInput' => "",
      'searchOptions' => "",
      'menu' => "Produto",

			'BLC_DADOS' 	 		=> array(),
			'BLC_SEMDADOS' 		=> array(),
			'BLC_PAGINAS' 		=> array(),
			'URLADICIONAR'    => site_url('admin/produto/adicionar/'),
			'URLLISTAR'    		=> site_url('admin/produto')
		);

    $layout_data = array(
        'styles' => [

        ],

        'scripts' => [
            
        ],
    );

		$data['MSGERROR'] = null;
		$data['MSGSUCCESS'] = null;

    if ($ferror) {
    	$data['MSGERROR'] = criaAlerta($ferror, 'danger', 'Erro');
    }
    if ($fsucesso) {
    	$data['MSGSUCCESS'] = criaAlerta($fsucesso, 'success', 'Sucesso');
    }

		$pagina			= $this->input->get('pagina');
	
		if (!$pagina) {
			$pagina = 0;
		} else {
			$pagina = ($pagina-1) * LINHAS_PESQUISA_DASHBOARD;
		}  

		// $res = $this->Produto_Model->get(array(), FALSE, $pagina, FALSE);
		$res = $this->Produto_Model->get(array(), FALSE, $pagina, FALSE);
		
		if ($res) {
			foreach ($res as $r) {
				$data['BLC_DADOS'][] = array(
					'IDPRODUTO'	 					=> $r->prod_id,
					"NOME" 			 					=> $r->prod_nome,
					"QTD" 			 					=> $r->prodest_quantidade,
					"PRECO" 			 				=> convertToValorBR($r->prod_valor),
					"VALORPROMOCIONAL" 		=> convertToValorBR($r->prod_valorpromocional),
					"HABILITADO" 					=> $r->prod_status ? 'Sim' : 'Não',
					"BTN" 								=> $r->prod_status ? 'warning' : 'primary',
					"BTNVALUE" 						=> $r->prod_status ? 'Desabilitar' : 'Habilitar',
					"URLTHUMB" 			 			=> base_url("assets/img/produto/80x80/".$r->profot_id.".".$r->profot_extensao),
					"URLEDITAR"  					=> site_url('admin/produto/editar/'.$r->prod_id),
					"URLEXCLUIR" 					=> site_url('admin/produto/excluir/'.$r->prod_id),
					"URLSTATUS" 					=> site_url('admin/produto/alterarStatus/'.$r->prod_id.'/'.$r->prod_status),
					"URLATRIBUTOS"  			=> site_url('admin/produto/configuracao/'.$r->prod_id),
					"URLUPLOAD"     			=> site_url('admin/produto/uploadfoto/'.$r->prod_id),
					"URLVINCULAIMAGEMSKU" => site_url('admin/produto/fotosku/'.$r->prod_id),
				);
			}
		} else {
			$data['BLC_SEMDADOS'][] = array();
		}

		$totalItens		= $this->Produto_Model->getTotal();
		$totalPaginas	= ceil($totalItens/LINHAS_PESQUISA_DASHBOARD);
		
		$pagina			= $this->input->get('pagina');
	
		$indicePg		= 1;
		if (!$pagina) {
		    $pagina = 1;
		}
		$pagina			= ($pagina==0)?1:$pagina;
	
		if ($totalPaginas > $pagina) {
			$data['HABAPROXIMO']	= null;
			$data['URLPROXIMO']	= site_url('admin/produto?pagina='.($pagina+1));
		} else {
			$data['HABAPROXIMO']	= 'disabled';
			$data['URLPROXIMO']	= '#';
		}
	
		if ($pagina <= 1) {
			$data['HABANTERIOR']= 'disabled';
			$data['URLANTERIOR']= '#';
		} else {
		    $paginaVoltar = 99999;
		    
		    if ($pagina > 1) {
		        $paginaVoltar = $pagina - 1;
		    }
			$data['HABANTERIOR']= null;
			$data['URLANTERIOR']= site_url('admin/produto?pagina='.($pagina-1));
		}

		while ($indicePg <= $totalPaginas) {
			$data['BLC_PAGINAS'][] = array(
					"LINK"		=> ($indicePg==$pagina)?'active':null,
					"INDICE"	=> $indicePg,
					"URLLINK"	=> site_url('admin/produto?pagina='.$indicePg)
			);
				
			$indicePg++;
		}
        
    $layout_data['content_body'] = $this->parser->parse ('admin/produto_listar', $data, true);
    $this->parser->parse ("layouts/inspinia", $layout_data);

	}

	public function configuracao($idProduto) {
        $data = array(
            'IDPRODUTO' => $idProduto
        );
		$this->parser->parse('admin/produto_config', $data);
	}

	public function adicionar() 
	{
		$ferror = $this->session->flashdata('erro');
		$fsucesso = $this->session->flashdata('sucesso');

		$data = array(
			'ACAO' 			 	 					   => 'Novo',
			'URLLISTAR'								 => 'LISTAR'	,
			'prod_id' 			 					 => NULL,
			'prod_nome' 		 					 => NULL,
			'prod_foto' 		 					 => NULL,
			'prod_resumo' 			 			 => NULL,
			'prod_valor' 			 				 => '0,00',
			'prod_valorpromocional' 	 => '0,00',
			'prod_urlseo'			 				 => NULL,
			'prod_quantidade'			 		 => NULL,
			'BLC_TIPOSATRIBUTOS'			 => array(),
			'BLC_DEPARTAMENTOPAI'			 => array(),
			'readonly' 								 => NULL,
			'required'   	 					 	 => 'required',

      'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
      'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
      'meta_url' => base_url(),
      'meta_classification' => "home",
      'pageTitle' => "LoudSon.gs",
      'searchInput' => "",
      'searchOptions' => "",
      'menu' => "Produto",
		);

    $layout_data = array(
        'styles' => [

        ],

        'scripts' => [
            ['url' => 'assets/js/dashboard.js']
        ],
    );

	$data['MSGERROR'] = null;
	$data['MSGSUCCESS'] = null;

    if ($ferror) {
    	$data['MSGERROR'] = criaAlerta($ferror, 'danger', 'Erro');
    }
    if ($fsucesso) {
    	$data['MSGSUCCESS'] = criaAlerta($fsucesso, 'success', 'Sucesso');
    }

		setURL($data, 'produto');

		// BUSCA DEPARTAMENTOS PAI
		$depPai = $this->Departamento_Model->get(array("d.dep_departamentopai IS NULL" => NULL), FALSE, 0, FALSE);
		
		if($depPai) {
			foreach ($depPai as $dp) {

				// FEFINE OS ELEMENTOS DOS FILHOS - INICIO
				$aFilhos = array();

				$depFilho = $this->Departamento_Model->get(array("d.dep_departamentopai" => $dp->dep_id), FALSE, 0, FALSE);

				if ($depFilho) {
					foreach ($depFilho as $df) {
						$aFilhos[] = array(
							"IDDEPARTAMENTOFILHO"		 => $df->dep_id,
							"NOMEDEPARTAMENTOFILHO"  => $df->dep_nome,
							"IDDEPARTAMENTOPAI" 		 => $df->dep_departamentopai,
							"chk_departamentofilho"	 => NULL
						);
					}
				}
				// FEFINE OS ELEMENTOS DOS FILHOS - FIM

				// DEFINE OS ELEMENTOS PAI
				$data['BLC_DEPARTAMENTOPAI'][] = array(
					"IDDEPARTAMENTO"	 		  => $dp->dep_id,
					"NOMEDEPARTAMENTO" 		  => $dp->dep_nome,
					"BLC_DEPARTAMENTOFILHO" => $aFilhos,
					"chk_departamentopai"	  => NULL
				);
			}
		}
		
    $layout_data['content_body'] = $this->parser->parse ('admin/produto_form', $data, true);
    $this->parser->parse ("layouts/inspinia", $layout_data);
	}

	public function editar($id) 
	{
		$ferror = $this->session->flashdata('erro');
		$fsucesso = $this->session->flashdata('sucesso');

		$data = array(
			'ACAO' 			 	 					   => 'Editar',
			'ACAOFORM' 			 	 					   => 'Novo',
			'URLLISTAR'								 => 'LISTAR'	,
      'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
      'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
      'meta_url' => base_url(),
      'meta_classification' => "home",
      'pageTitle' => "LoudSon.gs",
      'searchInput' => "",
      'searchOptions' => "",
      'menu' => "Produto",

			'ACAO' 								=> 'Edição',
			'BLC_TIPOSATRIBUTOS' 	=> array(),
			'des_tipoatributo'		=> 'disabled="disabled"',
			"prod_quantidade"			=> NULL, 
			'readonly' 						=> 'readonly'
		);

    $layout_data = array(
        'styles' => [

        ],

        'scripts' => [
            ['url' => 'assets/js/dashboard.js']
        ],
    );

		$data['MSGERROR'] = null;
		$data['MSGSUCCESS'] = null;

    if ($ferror) {
    	$data['MSGERROR'] = criaAlerta($ferror, 'danger', 'Erro');
    }
    if ($fsucesso) {
    	$data['MSGSUCCESS'] = criaAlerta($fsucesso, 'success', 'Sucesso');
    }

		$res = $this->Produto_Model->get(array('prod_id' => $id), TRUE);

		if ($res) {
			foreach ($res as $chave => $valor) {
				$data[$chave] = $valor;
			}

		} else {
			show_error(utf8_decode("PRODUTO NÃO ENCONTRADO!"), 500, utf8_decode("Dados relacionados não foram encontrados!"));
		} 

    $data['CHECKED'] 					 		 = $res->prod_status ? 'checked' : '';
		$data['prod_nome'] 						 = $res->prod_nome;
		$data['prod_foto'] 						 = $res->prod_foto;
    $data['prod_resumo'] 					 = $res->prod_resumo;
    $data['prod_ficha'] 					 = $res->prod_ficha;
    $data['prodest_quantidade'] 	 = $res->prodest_quantidade;
		$data['prod_valor'] 					 = $res->prod_valor;
		$data['prod_valorpromocional'] = $res->prod_valorpromocional;
		$data['prod_quantidade'] 	 		 = $res->prodest_quantidade;

		setURL($data, 'produto');

		$atributosVinculados = array(); 

		// ARMAZENA OS DEPARTAMENTOS VINCULADOS
		$aDepartamentosVinculados = array();

		$depVinc = $this->ProdutoDepartamento_Model->get(array("dp.pdep_prod_id" => $id)); // debug($depVinc, true);

		if ($depVinc) {
			foreach ($depVinc as $depv) {
				array_push($aDepartamentosVinculados, $depv->pdep_pdep_id);
			}
		}

		// BUSCA DEPARTAMENTOS PAI
		$depPai = $this->Departamento_Model->get(array("d.dep_departamentopai IS NULL" => NULL), FALSE, 0, FALSE);
		
		if($depPai) {
			foreach ($depPai as $dp) {

				// FEFINE OS ELEMENTOS DOS FILHOS - INICIO
				$aFilhos = array();

				$depFilho = $this->Departamento_Model->get(array("d.dep_departamentopai" => $dp->dep_id), FALSE, 0, FALSE);

				if ($depFilho) {
					foreach ($depFilho as $df) {
						$aFilhos[] = array(
							"IDDEPARTAMENTOFILHO"		 => $df->dep_id,
							"NOMEDEPARTAMENTOFILHO"  => $df->dep_nome,
							"IDDEPARTAMENTOPAI" 		 => $df->dep_departamentopai,
							"chk_departamentofilho"	 => (in_array($df->dep_id, $aDepartamentosVinculados)) ? 'checked="checked"' : NULL 
						);
					}
				}
				// FEFINE OS ELEMENTOS DOS FILHOS - FIM


				// DEFINE OS ELEMENTOS PAI
				$data['BLC_DEPARTAMENTOPAI'][] = array(
					"IDDEPARTAMENTO"	 		  => $dp->dep_id,
					"NOMEDEPARTAMENTO" 		  => $dp->dep_nome,
					"BLC_DEPARTAMENTOFILHO" => $aFilhos,
					"chk_departamentopai"	  => (in_array($dp->dep_id, $aDepartamentosVinculados)) ? 'checked="checked"' : NULL
				);
			}
		}
        
    $layout_data['content_body'] = $this->parser->parse ('admin/produto_form', $data, true);
    $this->parser->parse ("layouts/inspinia", $layout_data);
	}

	public function salvar() 
	{
		$erroDash 	  = $this->session->flashdata('erro');
		$sucessoDash  = $this->session->flashdata('sucesso');

		$prod_id 								=	$this->input->post('prod_id');
		$prod_nome 					  	= $this->input->post('prod_nome');
		$prod_resumo 						= $this->input->post('prod_resumo');
		$prod_valor 						= $this->input->post('prod_valor');
		$prod_valorpromocional 	= $this->input->post('prod_valorpromocional');
		$departamento 				  = $this->input->post('departamento'); 
		$prod_status 				    = $this->input->post('prod_status'); 

		$quantidade 						= $this->input->post('prod_quantidade'); 

		$prod_valor  						= $prod_valor; 
		$prod_valorpromocional	= $prod_valorpromocional; 

		$arquivo	= $_FILES['prod_foto']; 

		if ($prod_id) {
			$res = $this->Produto_Model->get(array('prod_id' => $prod_id), TRUE); 
		}

		$errors   = FALSE;
		$mensagem = NULL;

		if (!$prod_status) {
			$prod_status = 0;
		}
		if (!$prod_nome) {
			$errors		= TRUE;
			$mensagem .= "Informe o nome do produto.\n";
		}
		if (!$prod_resumo) {
			if ($prod_id) {
				$prod_resumo = $res->prod_resumo; 
			} else {
				$errors		= TRUE;
				$mensagem .= "Informe o resumo do produto.\n";
			}
		}
		if (!$quantidade) {
			$errors		= TRUE;
			$mensagem .= "Informe a quantidade do estoque do produto.\n";
		}
		if (!$prod_valor) {
			$errors		= TRUE;
			$mensagem .= "Informe o valor do produto.\n";
		} else {
			if ($prod_valorpromocional) {
				if ($prod_valorpromocional > $prod_valor) {
					$errors		= TRUE;
					$mensagem .= "Valor de promoção não pode ser maior que o valor do produto.\n";
				}
			}
		}
		if (!$departamento) {
			$errors		= TRUE;
			$mensagem .= "Informe a(s) categoria(s) do produto.\n";
		}

		if (!$errors) {

			$itens = array(
				'prod_nome' 		 					 => $prod_nome,
				'prod_resumo' 			 			 => $prod_resumo,
				'prod_valor' 			 				 => $prod_valor,
				'prod_valorpromocional' 	 => ($prod_valorpromocional == '' OR $prod_valorpromocional == 0) 
																					? NULL : $prod_valorpromocional,
				'prod_status'							 => $prod_status,
			); 

			if (!$prod_id) {
				$itens['prod_urlseo'] = url_title(strtolower($prod_nome));
			}

			if ($prod_id) 
			{
				$this->Produto_Model->update($itens, $prod_id);

				$itensEstoque = array(
					'prodest_idproduto' 		 	 => $prod_id,
					'prodest_quantidade' 			 => $quantidade,
				);

				$prodest_id = $this->Produto_Model->updateEstoque($itensEstoque, $prod_id);
			} else {

				$prod_id = $this->Produto_Model->post($itens);

				$itensEstoque = array(
					'prodest_idproduto' 		 	 => $prod_id,
					'prodest_quantidade' 			 => $quantidade,
				);

				$prodest_id = $this->Produto_Model->postEstoque($itensEstoque);

			}

			if ($prod_id) {
				
				$this->fotoUpload($prod_id);

				$this->session->set_flashdata('sucesso', 'Novo produto inserido com sucesso!');

				$this->ProdutoDepartamento_Model->delete($prod_id);

				// VINCULA DEPARTAMENTOS
				foreach ($departamento as $dep) {
					$itenDepProd = array(
						'pdep_prod_id' => $prod_id,
						'pdep_pdep_id' => $dep
					);

					$this->ProdutoDepartamento_Model->post($itenDepProd);

				}

				redirect('admin/produto');

			} else {


				$this->session->set_flashdata('erro', 'Ocorreu um erro na operação!');

				if ($prod_id) {
					redirect('admin/produto/editar'.$prod_id);
				} else {
					redirect('admin/produto/adicionar');
				}

			}

		} else {
			$this->session->set_flashdata('erro', nl2br($mensagem));

			if ($prod_id) {
				redirect('admin/produto/editar/'.$prod_id);
			} else {
				redirect('admin/produto/adicionar');
			}

		}

	}

	public function fotoUpload($codproduto = null) 
	{
		if (!$codproduto) {
			$codproduto	= $this->input->post('prod_id');
		}
		 
		$arquivo	= $_FILES['prod_foto']; 
		
		if ($arquivo['error'] !== 0) {
			return FALSE;
		}
		
		$arquivoNome = $arquivo["name"];
		
		$extensao	= strtolower(pathinfo($arquivoNome, PATHINFO_EXTENSION));
		
		$foto = array(
			"profot_idproduto" => $codproduto,
			"profot_extensao"	=> $extensao
		); 

		$foto_id = $this->ProdutoFoto_Model->get(array('profot_idproduto' => $codproduto)); 

		if ($foto_id) 
		{
			$this->removefoto($foto_id[0]->profot_id, $codproduto);

			$codfoto = $this->ProdutoFoto_Model->update($foto, $foto_id[0]->profot_id); 
			
			$enderecoFoto	= 'assets/img/produto/original/'.$codfoto.'.'.$extensao;
			
			move_uploaded_file($arquivo['tmp_name'], FCPATH.$enderecoFoto);
			
			//CRIA A MINIATURA DA GALERIA DE FOTOS
			$this->redimensionaFoto($codfoto, $extensao, 80, 80);
			
			//CRIA A MINIATURA DA PÁGINA INICIAL
			$this->redimensionaFoto($codfoto, $extensao, 150, 150); 

			//CRIA A FOTO PARA A GRID DE CARRINHO
			$this->redimensionaFoto($codfoto, $extensao, 378, 327);
			
			//CRIA A FOTO PARA A FICHA DE PRODUTOS
			$this->redimensionaFoto($codfoto, $extensao, 500, 500);
		} 
		else 
		{
			$codfoto = $this->ProdutoFoto_Model->post($foto); 
			
			$enderecoFoto	= 'assets/img/produto/original/'.$codfoto.'.'.$extensao;

			move_uploaded_file($arquivo['tmp_name'], FCPATH.$enderecoFoto);
			
			//CRIA A MINIATURA DA GALERIA DE FOTOS
			$this->redimensionaFoto($codfoto, $extensao, 80, 80);
			
			//CRIA A MINIATURA DA PÁGINA INICIAL
			$this->redimensionaFoto($codfoto, $extensao, 150, 150); 

			//CRIA A FOTO PARA A GRID DE CARRINHO
			$this->redimensionaFoto($codfoto, $extensao, 378, 327);
			
			//CRIA A FOTO PARA A FICHA DE PRODUTOS
			$this->redimensionaFoto($codfoto, $extensao, 500, 500);
		}
	}

	public function alterarStatus($id, $status) 
	{
		if ($status == 1) {
			$itens = array(
				'prod_status'	=> 0,
			);
		} elseif ($status == 0) {
			$itens = array(
				'prod_status'	=> 1,
			);			
		}

		$res = $this->Produto_Model->update($itens, $id);

		if ($res) {
			$this->session->set_flashdata('sucesso', 'Status do produto alterado com sucesso!');
		} else {
			$this->session->set_flashdata('erro', 'status do produto não pode ser alterado!');
		}

		redirect ('admin/produto');

	}

	public function excluir($id) 
	{
		$foto_id = $this->ProdutoFoto_Model->get(array('profot_idproduto' => $id)); 

		if ($foto_id) {
			$this->removefoto($foto_id[0]->profot_id, $id);
			$this->ProdutoFoto_Model->delete($foto_id[0]->profot_id, $id);
		}
		
		$res = $this->Produto_Model->delete($id);

		if ($res) {
			$this->session->set_flashdata('sucesso', 'Produto removido!');
		} else {
			$this->session->set_flashdata('erro', 'Produto não pode ser removido!');
		}

		redirect ('admin/produto');

	}

	public function atributos($id) {
		$data 	= array(
			"BLC_SEMVINCULADOS"  => array(),
			"BLC_VINCULADOS" 		 => array(),
			"BLC_SEMDISPONIVEIS" => array(),
			"BLC_DISPONIVEIS" 	 => array(),
			"URLSALVAATRIBUTO" 	 => site_url('admin/produto/salvaatributo'),
			"URLLISTAR"					 => site_url('admin/produto'),

      'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
      'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
      'meta_url' => base_url(),
      'meta_classification' => "home",
      'pageTitle' => "LoudSon.gs",
      'searchInput' => "",
      'searchOptions' => "",
      'menu' => "Produto",
		);

    $layout_data = array(
        'styles' => [

        ],

        'scripts' => [
            
        ],
    );
		
		$infoProduto  = $this->Produto_Model->get(array('prod_id' => $id), TRUE);

		if ($infoProduto) {
			$data['NOMEPRODUTO'] = $infoProduto->prod_nome;
			$data['IDPRODUTO']   = $infoProduto->prod_id;
		} else {
			show_error('Não foram encontrados dados.', 500, 'Ops error');
		}

			$resExistente = $this->Sku_Model->getPorProdutoSimples($id);
			// echo '<pre>';
			// print_r($resExistente);
			// die();

		if (empty($infoProduto->prod_idatributotipo)) {
			$variosSKUs = FALSE;
			$resExistente = $this->Sku_Model->getPorProdutoSimples($id);
		} else {
			$variosSKUs = TRUE;
			$resExistente = $this->Sku_Model->getPorProdutoAtributo($id);
		}

		if ($resExistente) {
			if ($variosSKUs) {
				foreach ($resExistente as $rEx) {
					$data['BLC_VINCULADOS'][] = array(
							"CODSKU" 		 => $rEx->sku_id,
							"REFERENCIA" => $rEx->sku_referencia,
							"QUANTIDADE" => $rEx->sku_quantidade,
							"DESCRICAO"  => $rEx->atr_nome
						);
				}
			} else {
				$data['BLC_VINCULADOS'][] = array(
						"CODSKU" 		 => $resExistente->sku_id,
						"REFERENCIA" => $resExistente->sku_referencia,
						"QUANTIDADE" => $resExistente->sku_quantidade,
						"DESCRICAO"  => $resExistente->atr_nome
					);
			}
				
		}	else {
			$data['BLC_SEMVINCULADOS'][] = array();
		}

		// ATRIBUTOS DISPONÍVEIS

		if (empty($infoProduto->prod_idatributotipo)) {
			$data['BLC_SEMDISPONIVEIS'][] = array();
		} else {

			$atribDisponivel = $this->Sku_Model->getAtributosDisponiveis($id);

			if ($atribDisponivel) {
				foreach ($atribDisponivel as $aD) {
					$data['BLC_DISPONIVEIS'][] = array(
						"DESCRICAO"  => $aD->atr_nome,
						"IDATRIBUTO" => $aD->atr_id
					);
				}
			} else {
				$data['BLC_SEMDISPONIVEIS'][] = array();
			}
		}
			

		// $this->parser->parse('admin/produtoatributo_listar', $data); 
		        
    $layout_data['content_body'] = $this->parser->parse ('admin/produtoatributo_listar', $data, true);
    $this->parser->parse ("layouts/inspinia", $layout_data);
	}

	public function salvaatributo() {
		$idproduto = $this->input->post('prod_id');

		// INSERE ARIBUTOS E TRANSFORMA EM SKU
		$atributo = $this->input->post('atributo');

		foreach ($atributo as $idatributo => $valores) {
			if ( (!empty($valores['referencia'])) || (!empty($valores['quantidade'])) ) {
				$sku = array(
					"sku_referencia" => $valores['referencia'],
					"sku_quantidade" => $valores['quantidade'],			
					"sku_idproduto"	 => $idproduto
				);

				$idsku = $this->Sku_Model->post($sku);

				if ($idsku) {
					$atributoSku = array(
						"skuatb_skuid" 			=> $idsku,
						"skuatb_atributoid" => $idatributo
					);

					$this->Sku_Model->postAtributo($atributoSku);
				}
			}
		}

		// ATUALIZA SKUS
		$skuExistente = $this->input->post('sku');

		if ($skuExistente) {
			// echo '<pre>';
			// print_r($skuExistente);
			// die();

			foreach ($skuExistente as $sku_id => $valores) {
				if (isset($valores['remover']) AND $valores['remover'] === 'S') {
					$this->Sku_Model->delete($sku_id);
				} else {
					$skuAtualiza = array(
						"sku_referencia" => $valores['referencia'],
						"sku_quantidade" => $valores['quantidade']
					);

					$this->Sku_Model->update($sku_id, $skuAtualiza);
				}
			}

		} else {

		}

		$this->session->set_flashdata('sucesso', 'SKUs salvos com sucesso!');

		redirect('admin/produto/atributos/'.$idproduto);
	}


	// FORMULARIO DE UPLOAD DE FOTOS ------------------------------------------------
	public function uploadfoto($id) {
		
		$infoProduto  = $this->Produto_Model->get(array('prod_id' => $id), TRUE);

		if (!$infoProduto) {
			show_error('Não foram encontrados dados.', 500, 'Ops error');
		}

		$data = array(
			"IDPRODUTO" 	=> $id,
			"NOMEPRODUTO" => $infoProduto->prod_nome,
			"URLLISTAR" 	=> site_url('admin/produto'),
			"URLUPLOAD" 	=> site_url('admin/produto/salvafoto')
		);

		$this->parser->parse('admin/produto_foto_upload', $data );

	}
	// -----------------------------------------------------------------------------

	// CRIA FOTOS NO SERVIDOR ------------------------------------------------------
	public function salvafoto() {
		// $idproduto = $this->input->post('idproduto');

		$idproduto = $this->input->post('prod_foto');


        $config['upload_path']   = 'uploads';
        $config['allowed_types'] = 'jpg|jpeg|pdf';
        $config['encrypt_name']  = TRUE;
        $config['max_size']      = '4096';
        $config['overwrite']     = FALSE;
        $this->load->library('upload', $config);

        $files = $_FILES; 
        $cpt = count($_FILES['fotos']['name']);
        for($i=0; $i<$cpt; $i++)
        {
            $_FILES['fotos']['name']= $files['fotos']['name'][$i];
            $_FILES['fotos']['type']= $files['fotos']['type'][$i];
            $_FILES['fotos']['tmp_name']= $files['fotos']['tmp_name'][$i];
            $_FILES['fotos']['error']= $files['fotos']['error'][$i];
            $_FILES['fotos']['size']= $files['fotos']['size'][$i];


            $this->upload->initialize($config);
            $this->upload->do_upload('fotos');

            $arquivo_upload = $this->upload->data();

            $extensao 	 = str_replace('.','',$arquivo_upload['file_ext']);

            $foto = array(
                "profot_idproduto" => $idproduto,
                "profot_extensao"  => $extensao

            );

            $idfoto = $this->ProdutoFoto_Model->post($foto);
            $urlsImagens = array();

            for($i=0; $i < count($this->tamanho_fotos);$i++){
                if($i == 0){
                    $upload_uri = $this->pasta_produtos.'/'.$this->tamanho_fotos[0].'/'.$idfoto.'.'.$extensao;
                    $this->s3->putObjectFile($arquivo_upload['full_path'], $this->bucket, $upload_uri, 'public-read');
                    $urlsImagens[] = $this->amazon_url.'/'.$this->bucket.'/'.$upload_uri;
                } else {
                    $altura = explode('x',$this->tamanho_fotos[$i])[0];
                    $largura = explode('x',$this->tamanho_fotos[$i])[1];
                    $this->redimensionaFoto($arquivo_upload['file_path'], $idfoto.'.'.$extensao,$arquivo_upload['file_name'],$altura, $largura);
                    $upload_uri = $this->pasta_produtos.'/'.$this->tamanho_fotos[$i].'/'.$idfoto.'.'.$extensao;
                    $urlsImagens[] = $this->amazon_url.'/'.$this->bucket.'/'.$upload_uri;
                    $this->s3->putObjectFile($arquivo_upload['file_path'].$altura.$largura.$idfoto.'.'.$extensao, $this->bucket, $upload_uri, 'public-read');
                    unlink($arquivo_upload['file_path'].$altura.$largura.$idfoto.'.'.$extensao);
                }

         }
            unlink($arquivo_upload['full_path']);
        }

		$jsonRetorno = array( "files" => array( array(
					"name"				 => $files['fotos']["name"][0],
					"type"				 => $files['fotos']["type"][0],
					"url"				 => $urlsImagens[2],
					"thumbnailUrl"       => $urlsImagens[2],
					"deleteUrl"		     => site_url('admin/produto/removefoto/'.$idfoto.'/'.$idproduto),
					"deleteType"	     => 'DELETE'
				) 
			)
		);

		$this->layout = '';
		echo json_encode($jsonRetorno);
		die();
		
	}

	// REMOVE FOTOS DO BD E DO SERVER
	public function removefoto($idfoto, $idproduto) {
		$condicao = array(
			"profot_id" 			 => $idfoto,
			"profot_idproduto" => $idproduto
		);

		$infoFoto = $this->ProdutoFoto_Model->get($condicao, TRUE);
		
		// debug($infoFoto, true);

		// if ($this->ProdutoFoto_Model->delete($idfoto, $idproduto)) {
			
			unlink(FCPATH.'assets/img/produto/original/'.$idfoto.'.'.$infoFoto->profot_extensao);
			unlink(FCPATH.'assets/img/produto/80x80/'.$idfoto.'.'.$infoFoto->profot_extensao);
			unlink(FCPATH.'assets/img/produto/150x150/'.$idfoto.'.'.$infoFoto->profot_extensao);
			unlink(FCPATH.'assets/img/produto/378x327/'.$idfoto.'.'.$infoFoto->profot_extensao);
			unlink(FCPATH.'assets/img/produto/500x500/'.$idfoto.'.'.$infoFoto->profot_extensao);

		return $idfoto;
	
		// }

		// if ($this->ProdutoFoto_Model->delete($idfoto, $idproduto)) {
  //           for($i=0; $i < count($this->tamanho_fotos);$i++) {
  //               $this->s3->deleteObject($this->bucket, $this->pasta_produtos.'/'.$this->tamanho_fotos[$i].'/'.$idfoto.'.'.$infoFoto->profot_extensao);
  //           }
		// }
	}

	// REDIMENSIONA IMAGEM
	private function redimensionaFoto($codprodutofoto, $extensao, $altura, $largura) {
	// ($file_path, $nome_arquivo, $nome_original, $altura, $largura) {

		// $configImagem = array(
		// 	'image_library'  => 'gd2',
		// 	'source_image'   => $file_path.$nome_original,
		// 	'new_image'      => $file_path.$altura.$largura.$nome_arquivo,
		// 	'create_thumb'   => FALSE,
		// 	'maintain_ratio' => TRUE,
		// 	'width'          => $largura,
		// 	'height'         => $altura
		// );

		// $this->image_lib->clear();
		// $this->image_lib->initialize($configImagem);
  //   $this->image_lib->resize();
		
		if (!is_dir(FCPATH."assets/img/produto/{$altura}x{$largura}/")){
			mkdir(FCPATH."assets/img/produto/{$altura}x{$largura}/");
		}
		
		$configImagem['image_library']	= 'gd2'; //BIBLIOTECA RESPONSÁVEL PELO REDIMENSIONAMENTO
		$configImagem['source_image']	= FCPATH.'assets/img/produto/original/'.$codprodutofoto.'.'.$extensao;
		$configImagem['new_image']		= FCPATH."assets/img/produto/{$altura}x{$largura}/".$codprodutofoto.'.'.$extensao;
		$configImagem['create_thumb']	= FALSE;
		$configImagem['maintain_ratio']	= TRUE;
		$configImagem['width']			= $largura;
		$configImagem['height']			= $altura;
		
		$this->load->library('image_lib');
		$this->image_lib->clear();
		$this->image_lib->initialize($configImagem);
		
		$this->image_lib->resize();
	}

	// EXIBE PAGINA DE VINCULAÇÃO DE FOTOS COM ATRIBUTS/SKUs
	public function fotosku($idproduto) {
		$res = $this->Produto_Model->get(array('prod_id' => $idproduto), TRUE);
		// debug($res, true);

		if (!$res) {
			show_error('Departamento não encontrado.', 500, 'Error!');
		}

		$fotos = $this->ProdutoFoto_Model->get(array("profot_idproduto" => $idproduto));
		// debug($fotos, true);
				
		$data = array(
			"NOMEPRODUTO" => $res->prod_nome,
			"BLC_FOTOS" => array(),
			"BLC_SEMFOTOS" => array(),
			"URLSALVAFOTOATRIBUTO" => site_url('admin/produto/salvafotosku'),
			"URLLISTAR" 	=> site_url('admin/produto'),
		);
		
		$sku = $this->Sku_Model->getPorProdutoAtributo($idproduto);
		// debug($sku, true);
		
		/*getSkusFoto*/
		
		if ($fotos) {
			foreach($fotos as $f) {
				
				$skusvinculados = $this->FotoSku_Model->getSkusFoto($f->profot_id);
				
				$aSKUsVinculados = array();
				
				if ($skusvinculados) {
					foreach($skusvinculados as $sv) {
						array_push($aSKUsVinculados, $sv->pfsku_idsku);
					}
				}
				
				$aSku = array();
				
				if ($sku) {
					foreach($sku as $s) {
						$aSku[] = array(
							"IDSKU" => $s->sku_id,
							"NOMESKU" => $s->atr_nome,
							"SEL_SKU" => in_array($s->sku_id, $aSKUsVinculados) ? 'selected="selected"' : NULL
						);
					}
				}
				
				$data['BLC_FOTOS'][] = array(
					'URLIMAGEM'	 => $this->amazon_url.'/'.$this->bucket.'/'.$this->pasta_produtos.'/'.$this->tamanho_fotos[1].'/'.$f->profot_id.'.'.$f->profot_extensao,
					'BLC_SKUSPRODUTO' => $aSku,
					"IDPRODUTOFOTO" => $f->profot_id
				);
				
			}
		} else {
			$data['BLC_SEMFOTOS'][] = array();
		}
		
		$data['IDPRODUTO'] = $idproduto;
		
		$this->parser->parse('admin/produtoatributofoto_form', $data);
	}
	
	// VINCULA AS FOTOS COM O SKU NO BANCO DE DADOS
	public function salvafotosku() {
		
		$skus = $this->input->post('skus');
		$idproduto = $this->input->post('prod_id');
		$remover = $this->input->post('remover');
		
		$aImagensRemover = array();
		
		if ($remover) {
			foreach($remover as $idprodutofoto => $remove) {
				array_push($aImagensRemover, $idprodutofoto);
				
				$this->session->set_flashdata('sucesso', 'Imagens modificadas com sucesso.');
				$this->ProdutoFoto_Model->delete($idprodutofoto, $idproduto);
			}
		}
		
		foreach($skus as $idprodutofoto => $codigossku) {
			
			if (in_array($idprodutofoto, $aImagensRemover)) {
				continue;
			}
			
			$this->FotoSku_Model->limpaImagens($idprodutofoto);
			
			foreach($codigossku as $idsku) {
				
				$itemFoto = new stdClass();
				$itemFoto->pfsku_idprodutofoto = $idprodutofoto;
				$itemFoto->pfsku_idsku = $idsku;
				
				$this->session->set_flashdata('sucesso', 'Imagens modificadas com sucesso.');
				$this->FotoSku_Model->post($itemFoto);
			}
		}
		
		redirect('admin/produto/fotosku/'.$idproduto);
	}

    public function associar_atributo($idproduto)
    {
        $res = $this->Produto_Model->get(array('prod_id' => $idproduto), TRUE);

        if (!$res) {
            show_error('Departamento não encontrado.', 500, 'Error!');
        }

        $atributosVinculados = $this->Atributo_Model->getAtributosVinculados($idproduto);
        $atributosTipos = $this->Atributo_Model->getAtributoTipo();
        $this->session->set_userdata('atributosTipos', $atributosTipos);
        $this->session->set_userdata('atributos', $atributosVinculados);
        $atrVinculo = array();
        if(!is_null($atributosVinculados)) {
            foreach ($atributosVinculados AS $row) {
                array_push($atrVinculo, $row->pat_idatributotipo);
            }
        }

        foreach($atributosTipos AS $row){
            $tipos[] = array(
                "IDTIPO" => $row->atip_id,
                "NOMETIPO" => $row->atip_nome,
                "SEL_TIPO" => in_array($row->atip_id, $atrVinculo) ? 'selected="selected"' : NULL
            );
        }

        $listaAtributosProduto = array();

        if(count($tipos) > 0) {
            foreach ($tipos as $row) {

                $atributosPorTipo = $this->Atributo_Model->getAtributosTipo($row['IDTIPO']);
                $atributosProduto = $this->Atributo_Model->getAtributosProduto($row['IDTIPO'], $idproduto);


                $atrProduto = array();
                if(!is_null($atributosProduto)) {
                    foreach ($atributosProduto AS $atrp) {
                        array_push($atrProduto, $atrp->patr_id);
                    }
                }

                $listaAttr = array(
                    'IDTIPOATTR'      => $row['IDTIPO'],
                    'NOMETIPOATTR'    => $row['NOMETIPO'],
                );

                if(!is_null($atributosPorTipo)) {
                    $listaAttr = array(
                        'IDTIPOATTR'      => $row['IDTIPO'],
                        'NOMETIPOATTR'    => $row['NOMETIPO'],
                    );

                    foreach ($atributosPorTipo AS $atrTip) {
                        $listaAttr['BLC_LISTAATTR'][] = array(
                            'IDATTR'   => $atrTip->patr_id,
                            'NOMEATTR' => $atrTip->patr_nome,
                            'SEL_ATTR' => in_array($atrTip->patr_id, $atrProduto) ? 'selected="selected"' : NULL,
                        );
                    }
                } else {
                    $listaAttr['BLC_LISTAATTR'] = array();
                }
                array_push($listaAtributosProduto, $listaAttr);
            }
        }

        $data = array(
            "NOMEPRODUTO"    => $res->prod_nome,
            'BLC_ATRPRODUTO' => $tipos,
            'IDPRODUTO'      => $idproduto,
            'BLC_ATRTIPO'    => $listaAtributosProduto,
        );

        $this->parser->parse('admin/produtoatributoassociar', $data);
    }

    public function novoTipo($idproduto, $tipo)
    {
        $atributosPorTipo = $this->Atributo_Model->getAtributosTipo($tipo);
        $atributoTipo = $this->Atributo_Model->getAtributoTipo($tipo);

        $listaAttr = array(
            'IDTIPOATTR'      => $tipo,
            'NOMETIPOATTR'    => $atributoTipo[0]->atip_nome,
        );

        if(!is_null($atributosPorTipo)) {
            foreach ($atributosPorTipo AS $atrTip) {
                $listaAttr['BLC_LISTAATTR'][] = array(
                    'IDATTR' => $atrTip->patr_id,
                    'NOMEATTR' => $atrTip->patr_nome,
                );
            }
        } else {
            $listaAttr['BLC_LISTAATTR'] = array();
        }

        $infoJson = json_encode($listaAttr);

        echo $infoJson; die;
    }


  public function getAtributosByIdProduto($idProduto) {


    $atributosTipos = $this->Atributo_Model->getAtributoTipo();
    $atributosVinculados = $this->Atributo_Model->getAtributosVinculados($idProduto);

  	// $atributosPorTipo = $this->Atributo_Model->getAtributosTipo($row['IDTIPO']);
   //  $atributosProduto = $this->Atributo_Model->getAtributosProduto($row['IDTIPO'], $idProduto);

    $data = array($atributosTipos, $atributosVinculados);
  	echo json_encode($data);
  	die;
  }
	// -----------------------------------------------------------------------------

}
