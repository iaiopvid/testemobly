<?php

class login extends CI_Controller {
	
	function __construct() {
        parent::__construct();  
        $this->load->model('business/cadastro_business', 'CadastroBusiness');
    }

    public function index() {
        $this->form_validation->set_rules('usuario', 'Usuário', 'trim|required');
        $this->form_validation->set_rules('senha', 'Senha', 'trim|required|md5');

        $this->form_validation->set_error_delimiters('<li>','</li>');
          
        if ($this->form_validation->run())
        {
            $post = $this->input->post();
            if ($this->login_business->logar($post['usuario'], $post['senha'], $post['senha'])) {
                redirect('backoffice');
            } else {
                $data = ['errorMessage'=> "Usuário ou senha são inválidos"];
                $this->load->view('login', $data);
            }
        } else {
            $this->load->view('login');
        }
    }

    public function ecommerce($opt = '') {
        $url = '';
        switch ($opt) {
            case '1':
                $url = 'checkout/formaentrega';
                break;
            case '2':
                $url = 'checkout';
                break;
            default:
                $url = 'ecommerce';
                break;
        }

        $this->form_validation->set_rules('usuario', 'Usuário', 'trim|required');
        $this->form_validation->set_rules('senha', 'Senha', 'trim|required|md5');

        $this->form_validation->set_error_delimiters('<li>','</li>');
        
        if ($this->form_validation->run())
        {
            $post = $this->input->post();
            
            if ($this->login_business->logar($post['usuario'], $post['senha'])) {
                redirect($url);
            } else {
                $this->session->set_flashdata("errorMessage", "Usuário ou senha são inválidos");
                redirect(!empty($opt) ? 'login_ecommerce/'.$opt : 'login_ecommerce');
            }
        } else {
            $data = array (
              'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
              'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
              'meta_url' => base_url(),
              'meta_classification' => "home",
              'pageTitle' => "LoudSon.gs",
              'searchInput' => "",
              'searchOptions' => "",
              'menu' => "Dashboard",

              'styles' => [
                    ['url' => base_url().'assets/css/loja.css'],
                    ['url' => "public/inspinia/css/plugins/iCheck/custom.css"],
                    ['url' => "public/inspinia/css/plugins/steps/jquery.steps.css"],
                    ['url' => "public/css/custom_style.css"]
              ],

              'scripts' => [
                    ['url' => base_url().'public/feirarecife/js/feira.js'],
                    ['url' => "public/inspinia/js/plugins/staps/jquery.steps.min.js"],
                    ['url' => base_url().'public/feirarecife/js/mascara.js'],
              ],
            ); 

            $cadastro = [
                'id' => ''
                ,'usuario' => ''
                ,'email' => ''
                ,'senha' => ''
                ,'confirmarSenha' => ''
                ,'nome' => ''
                ,'cpf' => ''
                ,'rg' => ''
                ,'dataNascimento' => ''
                ,'telefone' => ''
                ,'celular' => ''
                ,'nomeMae' => ''
                ,'nomePai' => ''
                ,'cep' => ''
                ,'endereco' => ''
                ,'numero' => ''
                ,'complemento' => ''
                ,'bairro' => ''
                ,'cidade' => ''
                ,'estado' => ''
                ,'licenca'=> 0
            ];

            $estados = $this->CadastroBusiness->getEstados();
            $estadosArr = [
                "" => "Selecione"
            ];
            foreach ($estados as $key => $estado) {
                $estadosArr[$estado->est_sigla] = $estado->est_nome. ' - ' . $estado->est_sigla;
            }

            $data['descricao'] = "Faça o login para finalizar sua compra.";
            $data['url'] = $url;

            $data['cadastro'] = $cadastro;
            $data['estados'] = $estadosArr;

            $layout_data['content_header'] = $this->parser->parse('feira/header_login', $data, true);
            $layout_data['content_body'] = $this->parser->parse('login_ecommerce', $data, true);
            $layout_data['content_footer'] = $this->parser->parse('feira/footer_login', $data, true);

            $this->parser->parse("layouts/feiraRecife", $layout_data);

        }
    }

    public function logout($opt = "") {
        $url = '';
        switch ($opt) {
            case '1':
                $url = 'ecommerce';
                break;
            
            default:
                $url = 'login';
                break;
        }
    	$this->login_business->logOut();
    	redirect($url);
    }

    public function lembrarSenha() {

        
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('usuario', 'Usuário', 'trim|required');

        $this->form_validation->set_error_delimiters('<li>','</li>');
          
        if ($this->form_validation->run())
        {
            $post = $this->input->post();
            if ($this->login_business->logar($post['usuario'], $post['senha'])) {
                redirect('backoffice');
            } else {
                $data = ['errorMessage'=> "Usuário ou email são inválidos"];
                $this->load->view('redefinir_senha_backoffice', $data);
            }
        } else {
            $this->load->view('lembrar_senha_backoffice');
        }
    }

    public function cadastro($idUsuarioLogado = 0) {

        $base_url = base_url();
        $usuarioLogado = $this->session->userdata('usuario');

        $this->form_validation->set_rules('usuario', 'Usuário', 'trim|required|min_length[5]|callback_validarUsuario');
        $this->form_validation->set_rules('senha', 'Senha', 'trim');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
        
        $this->form_validation->set_rules('nome', 'Nome', 'trim|required|min_length[5]|callback_validarNome');
        $this->form_validation->set_rules('cpf', 'CPF', 'trim|required|callback_validarCpf');
        $this->form_validation->set_rules('rg', 'RG', 'trim');
        $this->form_validation->set_rules('telefone', 'Telefone', 'trim|required');
        
        $this->form_validation->set_rules('cep', 'CEP', 'trim|required');
        $this->form_validation->set_rules('endereco', 'Endereço', 'trim|required');
        $this->form_validation->set_rules('numero', 'Número', 'trim|required');
        $this->form_validation->set_rules('complemento', 'Complemento', 'trim');
        $this->form_validation->set_rules('bairro', 'Bairro', 'trim|required');
        $this->form_validation->set_rules('cidade', 'Cidade', 'trim|required');
        $this->form_validation->set_rules('estado', 'Estado', 'trim|required');

        $this->form_validation->set_error_delimiters('<li>','</li>');

        if ($this->form_validation->run())
        {
            $post = $this->input->post();
            $this->db->trans_begin();
            $ret = $this->CadastroBusiness->salvarCadastro($post);

            $status = $ret['status'];
            if ($status <> 1) {
                $this->db->trans_rollback();
                $this->session->set_flashdata('error', 'Erro '.$ret['status'].' - '.$ret['mensagem']);
                
                redirect('login/ecommerce');

                //Mensagem de erro 
            }
            if ($this->db->trans_status() === FALSE) {
                $this->db->trans_rollback();
                //Mensagem de erro 
            } else { 
                $this->db->trans_commit();

                $this->session->set_flashdata('sucesso', 'Sucesso '.$ret['status'].' - Cadastro Inserido com sucesso!'.$ret['mensagem']);
                // $idPedido = $ret['idPedido'];
                redirect('login/ecommerce');
                //redirecionar para a tela de sucesso
            }                
        }  else { 
            $this->session->set_flashdata("errorMessage", "Dados informados inválidos, Favor corrija e reenvia o formulário");
            redirect(!empty($opt) ? 'login_ecommerce/'.$opt : 'login_ecommerce');
        }
        
        $body_data['isInterno'] = true;
        $data['cadastro'] = $cadastro;
        $data['estados'] = $estadosArr;  

          // Carregando as variáveis de conteúdo
        // $data['content_navigation'] = $this->load->view('navigation', $navigation_data, true);
        $data['content_body'] = $this->parser->parse('backoffice/cadastro_editar_dados', $body_data, true);
        $this->parser->parse('layouts/inspinia', $data); 
            
    } 

}
