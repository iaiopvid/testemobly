<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class Carrinho extends CI_Controller {

    public function __construct() {
        parent::__construct ();
        $this->load->Model('Produto_Model');
        $this->load->model('business/creditos_business', 'CreditosBusiness');
        $this->load->model('business/carrinho_business', 'CarrinhoBusiness');
        $this->load->model('business/endereco_business', 'EnderecoBusiness');
    }

    public function index() {

        $carrinho = $this->session->userdata ( "carrinho" );
        
        $logged = $this->login_business->logged();
        
        if (! $carrinho) {
            $carrinho = array ();
        } else {
            $carrinho = unserialize ( $carrinho );
        }
        
        $data ["BLC_PRODUTOS"] = array ();  
        $data ["BLC_FINALIZAR"] = array ();
        $data ["BLC_SEMPRODUTOS"] = array ();
        $data ["TOTALPEDIDO"] = NULL;
        $data ["DISPLAY"] = NULL;
        
        if (sizeof ( $carrinho ) === 0) {
            $data['QUANTITEM'] = 0;
            $data ["BLC_SEMPRODUTOS"] [] = array ();
            $data ["URLFINALIZAR"] = site_url ( 'checkout/finalizar_checkout' ) ;
        } else {
              $verCarrinho             = verCarrinho($carrinho);
              $data ["BLC_PRODUTOS"]   = $verCarrinho["BLC_PRODUTOS"];
              $data ['QUANTITEM']      = $verCarrinho['QUANTITEM'];
              $data ["TOTALPEDIDO"]    = $verCarrinho['TOTALPEDIDO'];
              $data ["BLC_FINALIZAR"]  = $verCarrinho['BLC_FINALIZAR'];
              $data ["URLFINALIZAR"]   = $verCarrinho['URLFINALIZAR'];
        }

        if ( $data['QUANTITEM'] > 0) {
             $data['DISPLAY'] = 'show';
        } else {
            $data['DISPLAY'] = 'none';
        }
        $this->parser->parse("feira/ajaxCarrinhoMini", $data);
    }

    public function getCarrinhoFormatado($isMini = false) {
        
        $redirect = $isMini 
            ? "feira/ajaxCarrinhoMini"
            : "feira/ajaxCarrinho";

        $carrinho = $this->CarrinhoBusiness->getCarrinho();

        $logged = $this->login_business->logged();
        if ($logged) {
            $usuario = $this->session->userdata('usuario');
            if ($usuario) {
                $endereco = $this->EnderecoBusiness->getEnderecoByPessoa($usuario->usu_pessoa_id);
                $enderecoArr = [];
                foreach ($endereco as $key => $value) {
                    $enderecoArr[$key] = $value;
                }
                $creditosPontos = $this->CreditosBusiness->getSaldoPontos($usuario->usu_id);

                $cotacao = $this->CreditosBusiness->getCotacao(1);
                $cotacao = (!is_null($cotacao) AND $cotacao > 0) ? $cotacao : 1;
                $creditos = $creditosPontos / $cotacao;
                $data['creditoDisponivel'] = $creditos;
                $data['endereco'] = $enderecoArr;
            }
        }
        $data['carrinho'] = $carrinho;
        $data['logged'] = $logged;
        $this->parser->parse($redirect, $data);
    }

    public function getCarrinhoMiniFormatado() {
        $this->getCarrinhoFormatado(TRUE);
    }

    public function getCarrinho() {
        return $this->CarrinhoBusiness->getCarrinho();
    } 

    public function setCarrinho($carrinho) {
        $this->CarrinhoBusiness->setCarrinho();
    }

    public function removeItem($idProduto) {
        $this->CarrinhoBusiness->removeItem($idProduto);
        redirect ("checkout");
    }

    public function changeItem($idProduto, $qtd) {
        $this->CarrinhoBusiness->changeItem($idProduto, $qtd);
        $carrinho = $this->getCarrinho();
        echo $carrinho['status'];
    }

    public function atualizarCredito() {
        $usuario = $this->session->userdata('usuario');
        $creditosPontos = $this->CreditosBusiness->getSaldoPontos($usuario->usu_id);
        $carrinho = $this->getCarrinho();
        $valor = $carrinho['total'];
        $this->CarrinhoBusiness->atualizarCredito($valor);
        $carrinho = $this->getCarrinho();
        echo $carrinho['status'];
    }

    public function atualizaItem($prod_id, $prod_qty) {
        // debug($prod_id.' '.$qty, true);

        $infoProduto = $this->Produto_Model->get(array('prod_id' => $prod_id), TRUE);
        debug($infoProduto);

        $carrinho = $this->CarrinhoBusiness->getCarrinho();
        if ($infoProduto) {
            $disponivel = $infoProduto->prodest_quantidade > 0;
            if (!$disponivel) {
                if (isset($carrinho['itens'][$prod_id])) {
                    $this->CarrinhoBusiness->removeItem($prod_id);
                }
            } else {
                $this->CarrinhoBusiness->changeItem($prod_id, $prod_qty);
            }            
        }
        return $carrinho;
    }

    public function adicionar() {

        $prod_id = $this->input->post ( "prod_id" ); 
        $prod_qty = $this->input->post ( "prod_qty" );
        
        $carrinho = $this->session->userdata ( "carrinho" );
        if (! $carrinho) {
            $carrinho = array();
        } 
        else {
            $carrinho = unserialize ( $carrinho );
        }

        $infoProduto = $this->Produto_Model->get(array('prod_id' => $prod_id), TRUE);
        
        if ($infoProduto) {
            
            if (! isset ( $carrinho [$prod_id] )) {
                if ($infoProduto->prodest_quantidade > 0) {
                    $carrinho [$prod_id] = $prod_qty;
                }
            } else {
                $carrinho [$prod_id] = $prod_qty;
            }
        }
        
        $carrinho = serialize ( $carrinho ); 
        
        $this->session->set_userdata ( "carrinho", $carrinho );
        
        return $carrinho;
    }
    
    public function aumenta($prod_id) {
        $carrinho = $this->session->userdata ( "carrinho" );
        
        if (! $carrinho) {
            $carrinho = array ();
        } else {
            $carrinho = unserialize ( $carrinho );
        }
        
        $infoProduto = $this->Produto_Model->get(array('prod_id' => $prod_id), TRUE);
        
        if ($infoProduto) {
            
            if (! isset ( $carrinho [$prod_id] )) {
                if ($infoProduto->prodest_quantidade > 0) {
                    $carrinho [$prod_id] = 1;
                }
            } else {
                
                if ($infoProduto->prodest_quantidade > $carrinho [$prod_id] + 1) {
                    $carrinho [$prod_id] = $carrinho [$prod_id] + 1;
                }
            }
        }
        
        return $carrinho;
    }

    public function diminui($prod_id) {
        $carrinho = $this->session->userdata ( "carrinho" );
        
        if (! $carrinho) {
            $carrinho = array ();
        } else {
            $carrinho = unserialize ( $carrinho );
        }
        
        $infoProduto = $this->Sku_Model->getPorSKU ( $prod_id );
        
        if ($infoProduto) {
            
            if (! isset ( $carrinho [$prod_id] )) {
                if ($infoProduto->prodest_quantidade > 0) {
                    $carrinho [$prod_id] = 1;
                }
            } else {
                if (($carrinho [$prod_id] - 1) <= 0) {
                    $carrinho [$prod_id] = 1;
                } elseif ($infoProduto->prodest_quantidade > $carrinho [$prod_id] - 1) {
                    $carrinho [$prod_id] = $carrinho [$prod_id] - 1;
                }
            }
        }
        
        $carrinho = serialize ( $carrinho );
        $this->session->set_userdata ( "carrinho", $carrinho );
        
        redirect ( "checkout" );
    }

    public function remove($sku_id) {
        $carrinho = $this->session->userdata ( "carrinho" );
        
        if (! $carrinho) {
            $carrinho = array ();
        } else {
            $carrinho = unserialize ( $carrinho );
        }
        
        unset ( $carrinho [$sku_id] );
        
        $carrinho = serialize ( $carrinho );
        $this->session->set_userdata ( "carrinho", $carrinho );
        
        redirect ( "checkout" );
    }
}