<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends CI_Controller {

    private $methodsPayment = array(
        // 'pagseguro',
        'boleto'
    );

    private $variaveis;

    public function __construct() {
        $this->variaveis = array();
        parent::__construct();

        $this->load->model('business/banco_business','BancoBusiness');
        $this->load->model('business/pedido_business','PedidoBusiness');
    }

    private function setVariables($key, $value)
    {
        $this->variaveis[$key] = $value;
    }

    public function layout($page, $titulo)
    {

        $pedido = $this->session->userdata('pedido');
        if (!$pedido) {

        }
        $base_url = base_url();
        $data = array(
            'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
            'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
            'meta_url' => "$base_url",
            'meta_classification' => "home",
            'pageTitle' => "Pagamento",
            'searchInput' => "",
            'searchOptions' => "",
            'menu' => "dashboard",
            'submenu' => "",

            'styles' => [
                #['url' => 'teste'],
                #['url' => 'teste2']
            ],

            'scripts' => [
                #['url'=> 'script'],
                #['url'=> 'script']
            ],
        );

        $navigation_data['menuNavegacao'] = "home";

        $body_data['data'] = array();
        $body_data['titulo'] = $titulo;
        $body_data['valorPedido'] = convertToValorBR($pedido->ped_total);
        $body_data['tipoPedido'] = $pedido->ped_tipo_bonus;
        foreach($this->variaveis AS $key=>$value){
            $body_data[$key] = $value;
        }
        // Carregando as variáveis de conteúdo
        $data['content_navigation'] = $this->load->view('navigation', $navigation_data, true);
        $data['content_body'] = $this->parser->parse('payment/'.$page, $body_data, true);
        $this->parser->parse('layouts/clean', $data);
    }

    public function index($idPedido = '')
    {
        /*
         * Verifica se foi passado o ID do pedido
         */
        if(empty($idPedido) OR $idPedido <= 0){
            /*
             * Tratamento caso o pedido não seja passado
             * todo TRATAR
             */
            $this->session->set_flashdata('errorMessage', 'Número do Pedido é Inválido!');
            redirect('backoffice/dashboard');
        }

        $pedido = $this->PedidoBusiness->getById($idPedido);

        if (is_null($pedido)) {
            $this->session->set_flashdata('errorMessage', 'Pedido não encontrado!');
            redirect('backoffice/dashboard');
        }

        $this->session->set_userdata('pedidoPagamento',$idPedido);

        $this->session->set_userdata('payment_order',$idPedido);

        $this->session->set_userdata('pedido', $pedido);

        $this->layout('choose', 'Forma de Pagamento');
    }

    public function method($metodo)
    {
        $idPedido = $this->session->userdata('payment_order');

        if(empty($idPedido)){
            /*
             * Tratamento caso o pedido não seja passado
             * todo TRATAR
             */
            redirect('backoffice');
        }

        /*
         * Se a forma de pagamento selecionada não existir
         */
        if(!in_array($metodo, $this->methodsPayment)) {
            /*
             * todo TRATAR
             */
            redirect('backoffice');
        }

        $this->session->set_userdata('payment_method', $metodo);

        redirect('payment/confirmation');
    }

    public function confirmation()
    {
        $idPedido = $this->session->userdata('payment_order');
        $pedido = $this->PedidoBusiness->getById($idPedido);
        $this->session->set_userdata('pedido', $pedido);
        
        $pedidoArr = [];
        $pedidoArr['ped_id'] = $pedido->ped_id;
        $pedidoArr['ped_status'] = $pedido->ped_status;
        $pedidoArr['ped_idusu'] = $pedido->ped_idusu;
        $pedidoArr['ped_subtotal'] = $pedido->ped_subtotal;
        $pedidoArr['ped_desconto'] = $pedido->ped_desconto;
        $pedidoArr['ped_total'] = $pedido->ped_total;
        $pedidoArr['ped_tipo_bonus'] = $pedido->ped_tipo_bonus;
        $pedidoArr['ped_data_criacao'] = $pedido->ped_data_criacao;
        $pedidoArr['ped_data_alteracao'] = $pedido->ped_data_alteracao;
        $pedidoArr['ped_data_vencimento'] = $pedido->ped_data_vencimento;

        if(empty($idPedido)){
            /*
             * Tratamento caso o pedido não seja passado
             * todo TRATAR
             */
            redirect('backoffice');
        }

        // $metodo = $this->session->userdata('payment_method');
        $metodo = 'boleto';
        $this->setVariables('pedido', $pedidoArr);
        $this->setVariables('method', $metodo);
        $this->setVariables('valorPedido', convertToValorBR($pedido->ped_total));

        $this->layout('confirmation', 'Confirmação do Pedido');
    }

    public function boleto($pedidoTipo = 3)
    {
        $idPedido = $this->session->userdata('payment_order');
        // $pedidoTipo = $this->session->userdata('pedido'); 

        if(empty($idPedido)){
            /*
             * Tratamento caso o pedido não seja passado
             * todo TRATAR
             */
            redirect('backoffice');
        }

        $pedidoCompleto = $this->PedidoBusiness->getPedidoCompleto($idPedido, $pedidoTipo);
        
        // debug($this->db->last_query()); die;
        if (is_null($pedidoCompleto)) {
            /*
             * Tratamento caso o pedido não seja passado
             * todo TRATAR
             */
            redirect('backoffice');   
        }
        /*
         * todo Carregar as informações do Pedido e do Cliente para passar como parâmetro
         */
        $this->BancoBusiness->gerarBoleto($pedidoCompleto, 'bradesco');

    }
}