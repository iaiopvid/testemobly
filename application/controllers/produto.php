<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produto extends CI_Controller {

    var $bucket = '';
    var $pasta_produtos = '';
    var $amazon_url = '';
    var $tamanho_fotos = '';
    var $url_produto = '';

	public function __construct() {
		parent::__construct();
		$this->load->model("Produto_Model");
		$this->load->model("ProdutoFoto_Model");
		// $this->load->model("Sku_Model");
		$this->load->model("FotoSku_Model");

	    $this->load->config('configS3');

	    $ambiente = $this->config->config['ambiente'];
	    $this->bucket = $this->config->config[$ambiente]['bucket'];
	    $this->amazon_url = $this->config->config['amazon_url'];
	    $this->pasta_produtos = $this->config->config['produtos'];
	    $this->tamanho_fotos = $this->config->config['tamanhos'];
	    $this->url_produto = $this->amazon_url.'/'.$this->bucket.'/'.$this->pasta_produtos.'/';
	}

	public function getFicha($prod_id, $sku = null) 
	{
    	$logged = $this->login_business->logged();
    
		$data = array();  
	    $data = array(
	      'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
	      'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
	      'meta_url' => base_url(),
	      'meta_classification' => "home",
	      'pageTitle' => "LoudSon.gs",
	      'searchInput' => "",
	      'searchOptions' => "",
	      'menu' => "Dashboard",

	      'styles' => [
	      ],

			'scripts' => [
				['url' => base_url().'public/feirarecife/js/feira.js'],
			],

	      'SITEURL' => base_url().'checkout/adicionar',
	    );

	    $data['BLC_LOGADO'] = array();
	    $data['BLC_DESLOGADO'] = array();

	    if ($logged) 
	    {
	      $user = $this->session->userdata('usuario');
	      $usuario = $user->usu_login;
	      $data['BLC_LOGADO'][] = array(
	        'USUARIO' => $usuario,
	        'URLDESLOGAR' => base_url().'logout/1',
	        'ACAO' => 'Login',
	      );
	      $data['USERCONTA'] = 'MINHA CONTA';
	      $data['URLUSERCONTA'] = base_url().'backoffice';
	      $data['CARRINHO'] = 'CARRINHO';
	      $data['URLCARRINHO'] = base_url().'ckeckout';
	      $data['USERACAO'] = 'SAIR';
	      $data['URLUSERACAO'] = base_url().'logout/1';
	    } else {
	      $data['BLC_DESLOGADO'][] = array(
	        'URLLOGAR' => base_url().'login_ecommerce',
	      );
	      $data['USERCONTA'] = 'MEU BACKOFFICE';
	      $data['URLUSERCONTA'] = base_url().'backoffice';
	      $data['CARRINHO'] = 'CARRINHO';
	      $data['URLCARRINHO'] = base_url().'ckeckout';
	      $data['USERACAO'] = 'LOGIN';
	      $data['URLUSERACAO'] = base_url().'login_ecommerce';
	    }
	        
	    $data ["BLC_PRODUTOS"] = array();  
	    $data ["BLC_FINALIZAR"] = array();
	    $data ["BLC_SEMPRODUTOS"] = array();
	    $data ["TOTALPEDIDO"] = NULL;

	    // if (sizeof ( $carrinho ) === 0) {
	    // 		$data['QUANTITEM'] = 0;
	    //     $data ["BLC_SEMPRODUTOS"] [] = array ();
	    //     $data ["URLFINALIZAR"] = site_url ( 'checkout/formaentrega' ) ;
	    // } else {
	    //   $verCarrinho             = verCarrinho($carrinho);
	    //   $data ["BLC_PRODUTOS"]   = $verCarrinho["BLC_PRODUTOS"];
	    //   $data ['QUANTITEM']      = $verCarrinho['QUANTITEM'];
	    //   $data ["TOTALPEDIDO"]    = $verCarrinho['TOTALPEDIDO'];
	    //   $data ["BLC_FINALIZAR"]  = $verCarrinho['BLC_FINALIZAR'];
	    //   $data ["URLFINALIZAR"]   = $verCarrinho['URLFINALIZAR'];
	    // }

	    // if ( $data['QUANTITEM'] > 0) {
	    //      $data['DISPLAY'] = 'show';
	    // } else {
	    //     $data['DISPLAY'] = 'none';
	    // }

		$data['COMPRARVISIBILIDADE'] = NULL;
		$data['MSGVISIBILIDADE'] = NULL;
		$data['INDISPONIVELVISIBILIDADE'] = "hide";

		$info = $this->Produto_Model->get(array("prod_id" => $prod_id), TRUE); 

		// debug($info, true);

		if (!$info) {
			show_error(utf8_decode("PRODUTO NÃO ENCONTRADO!"), 404, utf8_decode("PRODUTO NÃO ENCONTRADO!"));
		}

		$data["NOMEPRODUTO"] = $info->prod_nome;
		$data["QTYMAX"] = $info->prodest_quantidade;
		$data["DESCRICAOBASICA"] = $info->prod_resumo;
		$data["DESCRICAOCOMPLETA"] = $info->prod_ficha;
		$data["CLASS_STOCK"] = 'in-stock';
		$data["BTN_COMPRAR"] = '';
		$data["MSG_STOCK"] = 'Em estoque';

		if ( !$info->prodest_quantidade OR !$info->prod_status ) {
			$data["CLASS_STOCK"] = 'no-stock';
			$data["BTN_COMPRAR"] = 'disabled';
			$data["MSG_STOCK"] = 'Indisponível';
      		$data['SITEURL'] = '#';
		}

		$data["BLC_PROMOCAO"] = array();
		// $data["BLC_SKUSIMPLES"] = array();
		// $data["BLC_SKUCOMPLEXOS"] = array();
		$data["IDPRODUTO"] = $prod_id;
		$data["BLC_GALERIA"] = array();

		if (($info->prod_valorpromocional > 0) && ($info->prod_valorpromocional < $info->prod_valor)) {
			$valorFinal = number_format($info->prod_valorpromocional, 2, ',', '.');
			$data["BLC_PROMOCAO"][] = array(
				"VALORANTIGO" => $info->prod_valor
			);
			$data["VALORFINALPRODUTO"] = $valorFinal;
		} else {
			$valorFinal = number_format($info->prod_valor, 2, ',', '.');
			$data["VALORFINALPRODUTO"] = $valorFinal;
		}

		$foto = $this->ProdutoFoto_Model->get(array("p.profot_idproduto" => $prod_id), TRUE); 

		if ($foto) {
			$data["FOTOPRINCIPAL"] = base_url("assets/img/produto/500x500/".$foto->profot_id.".".$foto->profot_extensao);
			$data["FOTOZOOM"] = base_url("assets/img/produto/original/".$foto->profot_id.".".$foto->profot_extensao);
			$data["BLC_FOTOINDISPONIVEL"] = array();

		} else {
			$data["FOTOPRINCIPAL"] = base_url("assets/img/foto-indisponivel.jpg");
			$data["FOTOZOOM"] = base_url("assets/img/foto-indisponivel.jpg");
			$data["BLC_FOTOINDISPONIVEL"][] = array(
					"FOTOINDISPONIVEL" => base_url("assets/img/foto-indisponivel.jpg")
			);

		}

		$foto = $this->ProdutoFoto_Model->get(array(
				"p.profot_idproduto" => $prod_id
			), FALSE, 0, 1
		);


		if ($foto) {
			foreach ($foto as $f) {
				$fotoGaleria = array();

				// SERVIDOR LOCAL
				$fotoGaleria[] = array(
					"URLFOTOTHUMB" => base_url("assets/img/produto/80x80/".$f->profot_id.".".$f->profot_extensao),
					"URLFOTOZOOM" => base_url("assets/img/produto/original/".$f->profot_id.".".$f->profot_extensao),
					"URLFOTONORMAL" => base_url("assets/img/produto/500x500/".$f->profot_id.".".$f->profot_extensao),
					"IDPRODUTOFOTO" => $f->profot_idproduto,
				);
			}

			$data["BLC_GALERIA"][] = array(
					"BLC_FOTOS" => $fotoGaleria,
					// "NOMEGALERIA" => "fotos-sem-sku",
					"CLASSEGALERIA" => ""
				);
			$data["BLC_THUMBNAILS"][] = array(
					"BLC_THUMBS" => $fotoGaleria,
					"CLASSEGALERIA" => ""
				);

		}

		$data["BLC_DEPARTAMENTOS"] = array();
		
		$this->load->model("Departamento_Model");
		
		$departamentos = $this->Departamento_Model->getDepartamentosDisponiveis();
		
		foreach($departamentos as $dep) {
			
			$filhos = array();
			
			$departamentosFihos = $this->Departamento_Model->getDepartamentosDisponiveis($dep->dep_id);
		
			foreach($departamentosFihos as $depf) {
				
				$filhos[] = array(
					"URLDEPARTAMENTO_FILHO" => site_url("departamento/".$depf->dep_id),
					"NOMEDEPARTAMENTO_FILHO" => $depf->dep_nome
					
				);
			} 
			
			$data["BLC_DEPARTAMENTOS"][] = array(
				"URLDEPARTAMENTO" => site_url("departamento/".$dep->dep_id),
				"NOMEDEPARTAMENTO" => $dep->dep_nome,
				"BLC_DEPARTAMENTOSFILHOS" => $filhos,
				
			);
		}
		
		$produto = $this->Produto_Model->get(array(), FALSE, 0, 4, "prod_id", "DESC");
		$html = montaListaProduto($produto, 'listagem'); 

		$data["BLC_ORDENACAO"] = array();
		$data["BLC_PAGINACAO"] = array();
		$data["LISTAGEM"] = $html; 

		$layout_data['content_header'] = $this->parser->parse('feira/header', $data, true);
		$layout_data['content_body'] = $this->parser->parse('feira/ficha_produto', $data, true);
		$layout_data['content_footer'] = $this->parser->parse('feira/footer', $data, true);

		$this->parser->parse("layouts/feiraRecife", $layout_data);
	}

	// public function _remap($method, $params = array()) {
	// 	if (method_exists($this, $method)) {
	// 		return call_user_func_array(array($this, $method), $params);
	// 	} else {
	// 		$this->getFicha($method);
	// 	}
	// }

}