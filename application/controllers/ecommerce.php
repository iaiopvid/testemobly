<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ecommerce extends CI_Controller {

    public function __construct() {
        //var_dump('antes');
        parent::__construct();
        $this->load->model("business/login_business");
    }

    public function index() { 
        $logged = $this->session->userdata('logged');

        $this->load->model("Vitrine_Model");

        $dataAtual = date("Y-m-d");

        $filtro = array(
            "vit_ativa" => "S",
            "('$dataAtual' BETWEEN vit_data_inicio AND vit_data_final)" => NULL
        );

        $vitrine = $this->Vitrine_Model->get($filtro, TRUE);
        $produto = FALSE;

        if (!$produto) {
            $this->load->model("Produto_Model");

            $produto = $this->Produto_Model->get(array(), FALSE, 0, LINHAS_PESQUISA_DASHBOARD, "prod_id", "DESC");
        }

        $html = montaListaProduto($produto, "lista_busca");

        $data = array(
            'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
            'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
            'meta_url' => base_url(),
            'meta_classification' => "home",
            'pageTitle' => "LoudSon.gs",
            'searchInput' => "",
            'searchOptions' => "",
            'menu' => "Dashboard",
            'styles' => [
                ['url' => base_url() . 'assets/css/loja.css'],
            ],
            'scripts' => [
                ['url' => base_url() . 'assets/js/loja.js'],
                ['url' => base_url() . 'public/feirarecife/js/feira.js'],
            ],
        );

        $data['BLC_LOGADO'] = array();
        $data['BLC_DESLOGADO'] = array();

        if ($logged) {
            $user = $this->session->userdata('usuario');
            $usuario = $user->usu_login;
            $data['BLC_LOGADO'][] = array(
                'USUARIO' => $usuario,
                'URLDESLOGAR' => base_url() . 'logout/1',
                'ACAO' => 'Login',
            );
            $data['USERCONTA'] = 'MINHA CONTA';
            $data['URLUSERCONTA'] = base_url() . 'backoffice';
            $data['CARRINHO'] = 'CARRINHO';
            $data['URLCARRINHO'] = base_url() . 'checkout';
            $data['USERACAO'] = 'SAIR';
            $data['URLUSERACAO'] = base_url() . 'logout/1';
        } else {
            $data['BLC_DESLOGADO'][] = array(
                'URLLOGAR' => base_url() . 'login_ecommerce',
            );
            $data['USERCONTA'] = 'MEU BACKOFFICE';
            $data['URLUSERCONTA'] = base_url() . 'backoffice';
            $data['CARRINHO'] = 'CARRINHO';
            $data['URLCARRINHO'] = base_url() . 'checkout';
            $data['USERACAO'] = 'LOGIN';
            $data['URLUSERACAO'] = base_url() . 'login_ecommerce';
        }

        $data["LISTAGEM"] = $html;
        $data["BLC_DEPARTAMENTOS"] = array();

        $this->load->model("Departamento_Model");

        $departamentos = $this->Departamento_Model->getDepartamentosPais(array('dep_departamentopai IS NULL' => null), true);

        foreach ($departamentos as $dep) {

            $filhos = array();

            $departamentosFihos = $this->Departamento_Model->getDepartamentosFilhos($dep->dep_id);

            foreach ($departamentosFihos as $depf) {

                $filhos[] = array(
                    "URLFOTODEPARTAMENTO_FILHO" => base_url("assets/img/departamento/80x80/" . $depf->depfot_id . "." . $depf->depfot_extensao),
                    "URLDEPARTAMENTO_FILHO" => site_url("departamento/" . $depf->dep_id),
                    "NOMEDEPARTAMENTO_FILHO" => $depf->dep_nome,
                );
            }

            $data["BLC_DEPARTAMENTOS"][] = array(
                "URLDEPARTAMENTO" => site_url("departamento/" . $dep->dep_id),
                "NOMEDEPARTAMENTO" => $dep->dep_nome,
                "BLC_DEPARTAMENTOSFILHOS" => $filhos,
            );
        }

        $data["BLC_ORDENACAO"] = array();
        $data["BLC_PAGINACAO"] = array();

        $data ["BLC_PRODUTOS"] = array();
        $data ["BLC_FINALIZAR"] = array();
        $data ["BLC_SEMPRODUTOS"] = array();
        $data ["TOTALPEDIDO"] = NULL;
        
        $data['logged'] = ''; 
        $data['modal'] = ''; 

        $layout_data['content_header'] = $this->parser->parse('feira/header', $data, true);
        $layout_data['content_body'] = $this->parser->parse('feira/home', $data, true);
        $layout_data['content_footer'] = $this->parser->parse('feira/footer', $data, true);
        $html = $this->parser->parse("layouts/feiraRecife", $layout_data);
    }
     

    public function getItemHtml($isAdmin = false) 
    {
        $data['isAdmin'] = $isAdmin;

        echo $this->load->view('feira/newsletter', $data, true);
    }

}
