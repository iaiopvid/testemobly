<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastro extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('business/cadastro_business', 'CadastroBusiness');
        $this->load->model('business/licenca_business', 'LicencaBusiness');
        $this->load->model('business/pedido_business', 'PedidoBusiness');
    }

    public function listarAtivos() {
        $usuarioLogado = $this->session->userdata('usuario');
        $base_url = base_url();

        // Menu superior de navegação
        $navigation_data['menuNavegacao'] = "home";
          
        $data = array(
            'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
            'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
            'meta_url' => "$base_url",
            'meta_classification' => "home",
            'pageTitle' => "LoudSon.gs",
            'searchInput' => "",
            'searchOptions' => "",
            'menu' => "Cadastros",
            'submenu' => "Cadastros_Ativos"
        );

        $styles = [
            ['url' => "public/inspinia/css/plugins/iCheck/custom.css"],
            ['url' => "public/inspinia/css/plugins/steps/jquery.steps.css"],
            ['url' => "public/css/custom_style.css"]
        ];
        $scripts = [
            ['url' => "public/inspinia/js/plugins/staps/jquery.steps.min.js"],
            ['url' => "public/js/jquery.maskedinput.js"],
            ['url' => "public/js/plugins/jquery-validation-1.14.0/dist/jquery.validate.min.js"],
            ['url' => "public/js/plugins/jquery-validation-1.14.0/dist/localization/messages_pt_BR.js"],
            ['url' => "public/inspinia/js/form_wizard.js"]
        ];
        $data['styles'] = $styles;
        $data['scripts'] = $scripts;
        $body_data['data'] = array();
        $body_data['titulo'] = 'Cadastros Ativos';

        $cadastros = $this->CadastroBusiness->getCadastrosAtivosAll();
        $body_data['cadastros'] = $cadastros;
          // Carregando as variáveis de conteúdo
        $data['content_navigation'] = $this->load->view('navigation', $navigation_data, true);
        $data['content_body'] = $this->parser->parse('admin/cadastros_ativos', $body_data, true);
        $this->parser->parse('layouts/inspinia', $data);

    }
    
    public function listarAtivacoes(){
        $usuarioLogado = $this->session->userdata('usuario');
        $base_url = base_url();

        // Menu superior de navegação
        $navigation_data['menuNavegacao'] = "home";
          
        $data = array(
            'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
            'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
            'meta_url' => "$base_url",
            'meta_classification' => "home",
            'pageTitle' => "LoudSon.gs",
            'searchInput' => "",
            'searchOptions' => "",
            'menu' => "Cadastros",
            'submenu' => "Ativacoes"
        );

        $styles = [
            ['url' => "public/inspinia/css/plugins/iCheck/custom.css"],
            ['url' => "public/inspinia/css/plugins/steps/jquery.steps.css"],
            ['url' => "public/css/custom_style.css"]
        ];
        $scripts = [
            ['url' => "public/inspinia/js/plugins/staps/jquery.steps.min.js"],
            ['url' => "public/js/jquery.maskedinput.js"],
            ['url' => "public/js/plugins/jquery-validation-1.14.0/dist/jquery.validate.min.js"],
            ['url' => "public/js/plugins/jquery-validation-1.14.0/dist/localization/messages_pt_BR.js"],
            ['url' => "public/inspinia/js/form_wizard.js"]
        ];
        $data['styles'] = $styles;
        $data['scripts'] = $scripts;
        $body_data['data'] = array();
        $body_data['titulo'] = 'Ativações';

        $cadastros = $this->PedidoBusiness->getPedidosAtivacao()->result();
        // var_dump($cadastros); die;
        // var_dump($this->db->last_query()); die;

        $body_data['cadastros'] = $cadastros;
          // Carregando as variáveis de conteúdo
        $data['content_navigation'] = $this->load->view('navigation', $navigation_data, true);
        $data['content_body'] = $this->parser->parse('admin/cadastros_pendentes', $body_data, true);
        $this->parser->parse('layouts/inspinia', $data);

    }

    public function listarPendentes() {
        $usuarioLogado = $this->session->userdata('usuario');
        $base_url = base_url();

        // Menu superior de navegação
        $navigation_data['menuNavegacao'] = "home";
          
        $data = array(
            'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
            'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
            'meta_url' => "$base_url",
            'meta_classification' => "home",
            'pageTitle' => "LoudSon.gs",
            'searchInput' => "",
            'searchOptions' => "",
            'menu' => "Cadastros",
            'submenu' => "Cadastros_Pendentes"
        );

        $styles = [
            ['url' => "public/inspinia/css/plugins/iCheck/custom.css"],
            ['url' => "public/inspinia/css/plugins/steps/jquery.steps.css"],
            ['url' => "public/css/custom_style.css"]
        ];
        $scripts = [
            ['url' => "public/inspinia/js/plugins/staps/jquery.steps.min.js"],
            ['url' => "public/js/jquery.maskedinput.js"],
            ['url' => "public/js/plugins/jquery-validation-1.14.0/dist/jquery.validate.min.js"],
            ['url' => "public/js/plugins/jquery-validation-1.14.0/dist/localization/messages_pt_BR.js"],
            ['url' => "public/inspinia/js/form_wizard.js"]
        ];
        $data['styles'] = $styles;
        $data['scripts'] = $scripts;
        $body_data['data'] = array();
        $body_data['titulo'] = 'Cadastros Pendentes';

        $cadastros = $this->CadastroBusiness->getCadastrosPendentesAll();

        // var_dump($this->db->last_query()); die;

        $body_data['cadastros'] = $cadastros;
          // Carregando as variáveis de conteúdo
        $data['content_navigation'] = $this->load->view('navigation', $navigation_data, true);
        $data['content_body'] = $this->parser->parse('admin/cadastros_pendentes', $body_data, true);
        $this->parser->parse('layouts/inspinia', $data);
    }       
}