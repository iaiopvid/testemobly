<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author JOAO PAULO
 */
class loginAdmin extends CI_Controller {
	
	function __construct() {
        parent::__construct();
    }

    public function index() 
    {
    	$this->form_validation->set_rules('usuario', 'Usuário', 'trim|required');
        $this->form_validation->set_rules('senha', 'Senha', 'trim|required|md5');

        $this->form_validation->set_error_delimiters('<li>','</li>');
          
        if ($this->form_validation->run())
        {
        	$post = $this->input->post();
    		if ($this->login_business->logarAdmin($post['usuario'], $post['senha'])) {
    			redirect('admin');
	    	} else {
	    		$data = ['errorMessage'=> "Usuário ou senha são inválidos"];

	    		$this->load->view('loginAdmin', $data);
			}
		} else {
			$this->load->view('loginAdmin');
		}
    }

    public function logOutAdmin() {
    	$this->login_business->logOutAdmin();
    	$this->load->view('loginAdmin');
    }

}
