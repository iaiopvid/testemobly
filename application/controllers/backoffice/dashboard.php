<?php defined('BASEPATH') OR exit('No direct script access allowed');


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of cadastro
 *
 * @author JOAO PAULO
 */
class dashboard extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('business/pedido_business', 'PedidoBusiness');
        $this->load->model('business/cadastro_business', 'CadastroBusiness');
        $this->load->model('business/rede_business', 'RedeBusiness');   
        $this->load->model('business/extrato_business', 'ExtratoBusiness');
        $this->load->model('business/licenca_business', 'LicencaBusiness');   
    }

    public function index()
    {
        $base_url = base_url();
        $usuarioLogado = $this->session->userdata('usuario');
        $isAdmin = ($usuarioLogado->usu_admin == '1') ? TRUE : FALSE;
        // Menu superior de navegação
        $navigation_data['menuNavegacao'] = "home";
          
        $data = array(
            'meta_description' => "Under Ground Lyrics, hardcore, metal, emo, rock",
            'meta_keywords' => "lyrics,song,songs,words,hardore,metal,emo,rock",
            'meta_url' => "$base_url",
            'meta_classification' => "home",
            'pageTitle' => "LoudSon.gs",
            'searchInput' => "",
            'searchOptions' => "",
            'menu' => "Dashboard",

            'styles' => [
                #['url' => 'teste'],
                #['url' => 'teste2']
            ],

            'scripts' => [
                #['url'=> 'script'],
                #['url'=> 'script']
            ],
        );
        // Informações da <header>

        $body_data['data'] = array();
        $licencaVigente = $this->CadastroBusiness->getLicencaVigente($usuarioLogado->usu_id);
        // debug($this->db->last_query());
        // debug($licencaVigente); die;
        $ultimaAtivacao = $this->CadastroBusiness->getUltimaAtivacao($usuarioLogado->usu_id);

        if ($isAdmin) {
            $datetime1 = date_create(date('Y-m-d'));
        } else {
            $datetime1 = date_create($ultimaAtivacao->ati_data_validade);
        }
    
        $datetime2 = date_create(date('Y-m-d'));
        
        $interval = date_diff($datetime1, $datetime2);
        
        $daysDiff = $interval->format('%a');
        // debug($daysDiff); die;

        $body_data['informarAtivacao'] = ($daysDiff >= 1 and $daysDiff <= 5);
        $body_data['informarExpiracao'] = $isAdmin ? FALSE : ($daysDiff < 1);

        $numAtivos = $this->CadastroBusiness->getNumCadastrosAtivos();
        $numPendentes = $this->CadastroBusiness->getNumCadastrosPendentes();
        $numTotal = $this->RedeBusiness->getNumDiretosTotalById($usuarioLogado->usu_id);
        //$numAtivos + $numPendentes;
        $numDiretos = $this->RedeBusiness->getNumFilhosDiretos($usuarioLogado->usu_id);
        $numRede = $this->RedeBusiness->getNumRedeById($usuarioLogado->usu_id);

        $numPedidos = $this->PedidoBusiness->getNumAprovadosById($usuarioLogado->usu_id);
        $totalPedidos = $this->PedidoBusiness->getTotalPedidosByUsuario($usuarioLogado->usu_id);

        $saldo = $this->ExtratoBusiness->getSaldo($usuarioLogado->usu_id);
        $ganhos = $this->ExtratoBusiness->getGanhosAteHoje($usuarioLogado->usu_id);
        $saldoPontos = $this->ExtratoBusiness->getSaldoPontos($usuarioLogado->usu_id);
        
        if ($usuarioLogado->usu_id <> 1) {
            $tipoPlano = $this->LicencaBusiness->getLicencaByUsuarioId($usuarioLogado->usu_id)->lic_nome;
            // debug($this->db->last_query(), true);
        } else {
            $tipoPlano = 'Admin';
        }

        $dataValidadeLicenca = !is_null($licencaVigente) ? $licencaVigente->ati_data_validade : null;
        $body_data['validade']      =  !is_null($licencaVigente) 
            ? dateMySQL2BR($licencaVigente->ati_data_validade) 
            : '--/--/----';

        // debug($body_data['validade']); die;

        $body_data['plano']        =  $tipoPlano;
        $body_data['ativos']        =  convertToInt($numAtivos);
        $body_data['pendentes']     =  convertToInt($numPendentes);
        $body_data['total']         =  convertToInt($numTotal);
        $body_data['diretos']       =  convertToInt($numDiretos);
        $body_data['rede']          =  convertToInt($numRede);
        $body_data['numPedidos']    =  convertToInt($numPedidos);
        $body_data['totalPedidos']  =  convertToValorBR($totalPedidos);
        $body_data['saldo']         =  convertToValorBR($saldo);
        $body_data['ganhos']        =  convertToValorBR($ganhos);
        $body_data['saldoPontos']   =  convertToValorBR($saldoPontos);




        // Carregando as variáveis de conteúdo
        $data['content_navigation'] = $this->load->view('navigation', $navigation_data, true);
        $data['content_body'] = $this->parser->parse('backoffice/dashboard', $body_data, true);

        $this->parser->parse('layouts/inspinia', $data);
    }    
}
