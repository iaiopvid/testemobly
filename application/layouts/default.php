<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<title>{title_for_layout}</title>
{css_for_layout}
{js_for_layout}
</head>

<body>

    <div id="geral">

        <div id="topo">
            
        </div>

        <div id="meio">
	
			<ul id="menu">
                <li><a href="<?php echo base_url(); ?>principal">Principal</a></li>
                <li><a href="<?php echo base_url(); ?>principal/teste">Teste</a></li>
            </ul>

			<div id="conteudo">
				{content_for_layout}
			</div>
		
		</div>

        <br style="clear: both;" />

        <div id="rodape">
            <p class="rodape">Todos os direitos reservados - Bla Bla Bla</p>
        </div>

    </div>

</body>
</html>