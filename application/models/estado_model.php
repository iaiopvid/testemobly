<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estado_Model extends CI_Model {

	public function getTotal($condicao = array()) {
		$this->db->where($condicao);
		$this->db->from('estado');
		return $this->db->count_all_results();
	}

	public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0, $limite = LINHAS_PESQUISA_DASHBOARD) {
		$this->db->select('est_id, est_nome, est_sigla');
		$this->db->where($condicao);
		$this->db->from('estado');
		return $this->db->get()->result();
	}

}