<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of UsuarioDao
 *
 * @author JOAO PAULO
 */
class Pedido_Dao extends CI_Model {
    
    

    public function __construct() {
        parent::__construct();
    }

    public function gerarPedidoAdesao($idUsuario, $packAdesao) {
        
        
    }

    public function getPedidoAtivacaoPendente($idUsuario) {
        $this->db->where('ped_status', '0');
        $this->db->where('ped_tipo_bonus IN(1,2)');
        $this->db->where('ped_idusu', $idUsuario);
        $this->db->limit('1');
        $this->db->order_by('ped_data_criacao', 'DESC');
        $result = $this->db->get('pedidos');
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return null;
    }

    public function getAll($tipo = '3')
    {
        $this->db->where('ped_status <>', '99');
        $this->db->where('ped_tipo_bonus', $tipo, false); 
        return $this->db->get('pedidos')->result();
    }

    public function getTotalPedidosByUsuario($idUsuario) {
        $this->db->select('SUM(ped_total) AS total');
        $this->db->where('ped_tipo_bonus','3', false); 
        $this->db->where('ped_status', '1');
        $this->db->where('ped_idusu', $idUsuario);
        $result = $this->db->get('pedidos');
        return $result->num_rows() > 0 ? $result->row()->total : 0;
    }

    public function getAprovadosById($idUsuario)
    {
        $this->db->where('ped_status', '1');
        $this->db->where('ped_tipo_bonus','3', false); 
        $this->db->where('ped_idusu', $idUsuario);
        return $this->db->get('pedidos');
    }


    public function getLicencaDoItem($idItem) {
        $this->db->select('lic.*');
        $this->db->where('pedite_id', $idItem);
        $this->db->join('produto_licenca', 'pedite_idproduto = prolic_idproduto');
        $this->db->join('licencas lic', 'prolic_idlicenca = lic_id');
        $result = $this->db->get('pedidos_item');
        return $result->num_rows() > 0 ? $result->row() : null;
    }

    public function addEndereco($idPedido, $idEndereco) {
        $this->db->insert('pedidos_endereco', ['pedend_idpedido' => $idPedido, 'pedend_idendereco' => $idEndereco]);
        return $this->db->insert_id();

    }

    public function insertPedido($pedido) {
        $this->db->insert('pedidos', $pedido);
        return $this->db->insert_id();
    }

    public function addItemPedido($item, $idPedido) {
        $item['pedite_idpedido'] = $idPedido;
        return $this->insertItem($item);
    }

    public function insertItem($item) {
        $this->db->insert('pedidos_item', $item);
        return $this->db->insert_id();
    }

    public function insertUltimaAtivacao($dataAtivacao) {
        $this->db->insert('ultima_ativacao', $dataAtivacao);
        return $this->db->insert_id();
    }

    public function getByIdUsuario($idUsuario) {
    	$this->db->where('ped_idusu', $idUsuario);
        $this->db->where('ped_tipo_bonus','3', false); 
    	return $this->db->get('pedidos')->result();
    }

    public function getById($idPedido)
    {
    	$this->db->where('ped_id', $idPedido); 
    	$result = $this->db->get('pedidos');
    	if($result->num_rows() > 0) {
    		return $result->row();
    	}
    	return null;
    }

    public function getItens($idPedido)
    {
    	$this->db->where('pedite_idpedido', $idPedido);
    	return $this->db->get('pedidos_item')->result();
    }

    public function cancelar($idPedido) {
    	$this->setStatus($idPedido, '99');
    }

    public function aprovar($idPedido) {

    	$this->setStatus($idPedido, '1');
    }

    private function setStatus($idPedido, $status){
        // debug($idPedido); debug($status); die;
    	$this->db->where('ped_id', $idPedido, TRUE);
    	$this->db->update('pedidos', array('ped_status' => $status));
    }

    private function getEndereco($idPedido) {
        $this->db->where('pedend_pedido_id', $idPedido);
        $this->db->join('endereco', 'end_id = pedend_endereco_id');
        $result = $this->db->get('pedidos_endereco');
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return null;
    }

    public function getPedidoCompleto($idPedido, $peditoTipo = 3) {
        $this->db->select("
            ped_id, 
            ped_idusu, 
            ped_status, 
            ped_tipo_bonus, 
            ped_subtotal, 
            ped_desconto, 
            ped_total, 
            ped_data_criacao, 
            ped_data_alteracao,
            pes_nome, 
            pes_email, 
            pes_cpf_cnpj,
            end_id,
            end_logradouro,
            end_numero,
            end_complemento,
            end_cep,
            end_bairro,
            end_cidade,
            end_estado,
            end_pais,
            est_sigla
        ");
        $this->db->where('ped_id', $idPedido);
        $this->db->where('ped_tipo_bonus', $peditoTipo, false); 
        $this->db->join('pedidos_endereco', 'pedend_pedido_id = ped_id');
        $this->db->join('endereco', 'pedend_endereco_id = end_id');
        $this->db->join('estado', 'end_estado = est_id');
        $this->db->join('usuario_mmn', 'ped_idusu = usu_id');
        $this->db->join('pessoa', 'usu_pessoa_id = pes_id');
        $result = $this->db->get('pedidos');
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return null;
    }

    public function getPedidos($condicao = array()) 
    {
        $this->db->select("
            ped_id, 
            ped_idusu, 
            ped_status, 
            ped_tipo_bonus, 
            ped_subtotal, 
            ped_desconto, 
            ped_total, 
            ped_data_criacao, 
            ped_data_vencimento,
            ped_data_alteracao,
            usu_id, 
            usu_pai_id, 
            usu_login,
            pes_nome, 
            pes_email, 
            pes_cpf_cnpj,
            end_id,
            end_logradouro,
            end_numero,
            end_complemento,
            end_cep,
            end_bairro,
            end_cidade,
            end_estado,
            end_pais,
            end_data_criacao,
            end_data_alteracao,
            tel_numero, 
            tel_tipo, 
            tel_descricao"
        );
        $this->db->from("
            pedidos,
            usuario_mmn,
            pessoa,
            pessoa_endereco,
            endereco,
            pessoa_telefone,
            telefone"
        );
        $this->db->where($condicao, false);
        $this->db->where('ped_tipo_bonus','3', false); 
        $this->db->where('ped_idusu','usu_id', false); 
        $this->db->where('usu_pessoa_id', 'pes_id', false);
        $this->db->where('pes_id', 'pesend_idpessoa', false);
        $this->db->where('end_id', 'pesend_idendereco', false);
        $this->db->where('pes_id', 'pestel_idpessoa', false);
        $this->db->where('tel_id', 'pestel_idtelefone', false);

        return $this->db->get()->result();

    }

    public function getDadosDoPedido($idPedido = NULL) {
        $this->db->select("
            ped_id, 
            ped_idusu, 
            ped_status, 
            ped_tipo_bonus, 
            ped_subtotal, 
            ped_desconto, 
            ped_total, 
            ped_data_criacao, 
            ped_data_alteracao,
            usu_id, 
            usu_pai_id, 
            usu_login,
            pes_nome, 
            pes_email, 
            pes_cpf_cnpj,
            pedite_id, 
            pedite_idproduto, 
            pedite_quantidade, 
            pedite_valor_unitario,
            pedite_valor_desconto,
            prod_id, 
            prod_nome,
            prod_resumo, 
            prod_valor"  
        );
        $this->db->from("
            pedidos,
            usuario_mmn,
            pessoa,
            pedidos_item,
            produto"
        );
        $this->db->where('ped_idusu','usu_id', false);
        $this->db->where('ped_tipo_bonus','3', false); 
        $this->db->where('usu_pessoa_id', 'pes_id', false);
        $this->db->where('pedite_idpedido', 'ped_id', false);
        $this->db->where('pedite_idproduto', 'prod_id', false);
        $this->db->where('ped_id', $idPedido, false);

        return $this->db->get()->result();

    }

    function getPedidosAtivacao() {
        $this->db->join('pedidos', 'ped_idusu = usu_id', 'LEFT');
        $this->db->join('tipo_bonus', 'ped_tipo_bonus = tipbon_id');
        $this->db->join('pessoa', 'pes_id = usu_pessoa_id');
        $this->db->where('ped_tipo_bonus', '2');
        $this->db->where('ped_status', '0');
        return $this->db->get("usuario_mmn");
    }
    
}
    