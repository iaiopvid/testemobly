<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of UsuarioDao
 *
 * @author JOAO PAULO
 */
class Creditos_Dao extends CI_Model {

    public function getAll() 
    {
    	return $this->db->get('saldos_em_ponto')->result();
    }

    public function getGanhosAteHoje($idUsuario) {
        $this->db->select('SUM(sep_pontos) total');
        $this->db->where('sep_idusuario', $idUsuario);
        $this->db->where('sep_inicio_vigencia <= NOW()');
        // $this->db->where_in('sep_idtipo', array('1, 2')); // Tipos de Saldos_em_ponto que são bonus.
        $result = $this->db->get('saldos_em_ponto');
        if ($result->num_rows() > 0) {
            return $result->row()->total;
        }
        return 0;
    }

    public function getSaldoPontos($idUsuario) {
        $this->db->select('SUM(sep_pontos) total');
        $this->db->where('sep_idusuario', $idUsuario);
        $this->db->where('sep_inicio_vigencia <= NOW()');
        $result = $this->db->get('saldos_em_ponto');
        if ($result->num_rows() > 0) {
            $valor = $result->row()->total;
            return $valor > 0 ? $valor : 0;
        }
        return 0;
    }
    
    public function getCotacao($idLicenca){
        $this->db->where('cot_idlicenca', $idLicenca);
        $result = $this->db->get('cotacao');
        if ($result->num_rows() > 0) {
            return $result->row()->cot_valor;
        }
        return null;
    }

    public function getSaldo($idUsuario) {
        $this->db->select('SUM(sal_valor) total');
        $this->db->where('sal_idusuario', $idUsuario);
        $this->db->where('sal_inicio_vigencia <= NOW()');
        $result = $this->db->get('saldos');
        if ($result->num_rows() > 0) {
            $valor = $result->row()->total;
            return $valor > 0 ? $valor : 0;
        }
        return 0;
    }

    public function getById($idUsuario) 
    {
        $this->db->where('sep_idusuario', $idUsuario);
        $result = $this->db->get('saldos_em_ponto');
        return $result->num_rows() > 0
            ? $result->row()
            : null;
    }

    public function getExtratoById($idUsuario, $condicao = array(), $ordenacao = FALSE, $tipoOrdem = 'ASC') 
    {
        $this->db->where('sep_idusuario', $idUsuario);
        $this->db->where($condicao);

        if ($ordenacao) {
            $this->db->order_by($ordenacao, $tipoOrdem);
        }

        return $this->db->get('saldos_em_ponto')->result();
    }

    public function adicionarCreditoProvisionado($idUsuario, $idPedido, $credito, $descricao) {
        $data = date('Y-m-d H:i:s');
        $cotacao = $this->getCotacao(1);
        $db_creditos_aprovisionados = [
            'cap_idusuario'     => $idUsuario,
            'cap_idpedido'      => $idPedido,
            'cap_cotacao'       => $cotacao,
            'cap_pontos'        => convertToValorDb($credito * $cotacao),
            'cap_descricao'     => $descricao,
            'cap_data_criacao'  => $data
        ];
        $this->db->insert('creditos_aprovisionados', $db_creditos_aprovisionados);
        return $this->db->insert_id();
    }

    public function adicionarCreditoPonto($idUsuario, $idPedido, $credito, $descricao)
    {
        $data = date('Y-m-d H:i:s');
        $cotacao = $this->getCotacao(1);
        $db_saldo_pontos = [
            'sep_idusuario' => $idUsuario,
            'sep_cotacao'   => $cotacao,
            'sep_pontos'    => $credito * $cotacao,
            'sep_descricao' => $descricao,
            'sep_data_criacao' => $data,
            'sep_inicio_vigencia' => $data
        ];
        // debug($db_saldo_pontos); die;
        $this->db->insert('saldos_em_ponto', $db_saldo_pontos);
        return $this->db->insert_id();
    }
}