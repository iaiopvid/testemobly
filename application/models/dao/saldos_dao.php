<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of UsuarioDao
 *
 * @author JOAO PAULO
 */
class Saldos_Dao extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    function getSaldos() {
        $this->db->select('usu_id, usu_login, cba_agencia, cba_conta, cba_titular, cba_cpf, sum(sal_valor) as saldo');
        $this->db->from('saldos');
        $this->db->join('usuario_mmn', 'sal_idusuario = usu_id');
        $this->db->join('pessoa', 'usu_pessoa_id = pes_id');
        $this->db->join('conta_banco', 'cba_idpessoa = pes_id');
        $this->db->join('bancos', 'cba_idbanco = ban_id');
        // $this->db->where('MONTH(sal_data_criacao)');
        $this->db->group_by('sal_idusuario');
        $this->db->having('saldo > 0');
        return $this->db->get();
    }
    
    function getSaldosPagosAll() {
        $this->db->select('log_idusuario, usu_login, cba_agencia, cba_conta, cba_titular, cba_cpf, log_idsaldo, log_valor, log_descricao, log_data_criacao');
        $this->db->from('log_saldos_pagamento');
        $this->db->join('usuario_mmn', 'log_idusuario = usu_id');
        $this->db->join('pessoa', 'usu_pessoa_id = pes_id');
        $this->db->join('conta_banco', 'cba_idpessoa = pes_id');
        $this->db->join('bancos', 'cba_idbanco = ban_id');
        // $this->db->where('MONTH(sal_data_criacao)');
        return $this->db->get();
    }
    
    function pagarSaldo($dados) {
        $valor = $dados['sal_valor'];
        $now = date('Y-m-d H:i:s');
        $dados['sal_valor'] = -$dados['sal_valor'];
        
        if ($valor > 0) {
            $dados['sal_data_criacao'] = $now; 
            $dados['sal_inicio_vigencia'] = $now; 
            $this->db->insert('saldos', $dados);
            $idSaldo = $this->db->insert_id();

            $logDados = array(
                'log_idusuario' => $dados['sal_idusuario'],
                'log_idsaldo' => $idSaldo,
                'log_valor' => $valor,
                'log_descricao' => 'Pagamento de saldo devedor',
                'log_data_criacao' => $dados['sal_data_criacao'],
            );

            $this->insetLogSaldosPagamento($logDados);

            return $idSaldo;
        }
        return false;
    }
    
    function insetLogSaldosPagamento($logDados) {
        $valor = $logDados['log_valor'];
        
        if ($valor > 0) {
            $this->db->insert('log_saldos_pagamento', $logDados);
            $idLog = $this->db->insert_id();

            return $idLog;
        }
        return false;
    }

}
