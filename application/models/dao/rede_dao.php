<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of UsuarioDao
 *
 * @author JOAO PAULO
 */
class Rede_Dao extends CI_Model {

    public function getAll() 
    {
    	return $this->db->get('redes')->result();
    }

    function getById($idRede) 
    {
        $this->db->where('red_idusuario', $idUsuario);
        $result = $this->db->get('redes');
        return $result->num_rows() > 0
            ? $result->row()
            : null;
    }
    
    function checkFilho($idParent, $idFilho) {
        $this->db->from("redes p");
        $this->db->from("redes f");
        $this->db->where("p.red_idusuario", $idParent, false);
        $this->db->where("f.red_idusuario", $idFilho, false);
        $this->db->where("f.red_lft >=", "p.red_lft", false);
        $this->db->where("f.red_rht <=", "p.red_rht", false);
        return $this->db->get()->num_rows() > 0;
    }
    
    function getNumFilhosDiretos($idUsuario) {
        $this->db->select("COUNT(red_idusuario) AS total");
        $this->db->from("redes");
        $this->db->where("red_parent", $idUsuario, false);
        return $this->db->get()->row()->total;
    }

    public function getFilhosDiretos($idUsuario) 
    {
    	$this->db->select("
            red_idusuario AS idfilho,
            red_parent AS idpai,
            usu_status, 
            usu_login, 
            pes_nome, 
            pes_email, 
            pes_rg, 
            pes_cpf_cnpj, 
            pes_tipo, 
            pes_data_nascimento,
			pes_nome_mae, 
            pes_nome_pai, 
            end_logradouro, 
            end_numero, 
            end_complemento, 
            end_cep, 
            end_bairro,
			end_cidade, 
            end_estado, 
            end_pais, 
            tel_numero, 
            tel_descricao"  
        );
    	$this->db->from("redes");
    	$this->db->join("usuario_mmn", "red_idusuario = usu_id", "LEFT");
    	$this->db->join("pessoa", "usu_pessoa_id = pes_id", "LEFT");
        $this->db->join("pessoa_endereco", "pes_id = pesend_idpessoa", "LEFT");
    	$this->db->join("endereco", "pesend_id = end_id", "LEFT");
        $this->db->join("pessoa_telefone", "pes_id = pestel_idpessoa", "LEFT");
    	$this->db->join("telefone", "pestel_id = tel_id AND tel_tipo = 1", "LEFT");
    	$this->db->where("red_parent", $idUsuario);
        
    	return $this->db->get();
    }

    function getRedeById($idUsuario, $lvl = 0) 
    {
        $this->db->select("
            p.red_id AS idpai, 
            f.red_idusuario as idfilho, 
            (f.red_lvl - p.red_lvl) as nivel, 
            uf.usu_login as usu_login,
            uf.usu_data_criacao,
            uf.usu_data_alteracao,
            up.usu_login as usu_login_pai,
            pes_nome, 
            pes_email, 
        	pes_rg, 
            pes_cpf_cnpj, 
            pes_tipo, 
            pes_data_nascimento, 
            pes_nome_mae, 
            pes_nome_pai, 
            end_logradouro, 
        	end_numero, 
            end_complemento, 
            end_cep, 
            end_bairro, 
            end_cidade, 
            end_estado, 
            end_pais, 
            tel_numero, 
        	tel_descricao, 
            (((f.red_rht-f.red_lft) -1)/2) as equipe"
        );
    	$this->db->from("redes p");
    	$this->db->join("redes f", "f.red_lft > p.red_lft and f.red_rht < p.red_rht", "LEFT");
        $this->db->join("usuario_mmn uf", "uf.usu_id = f.red_idusuario", "LEFT");
        $this->db->join("usuario_mmn up", "uf.usu_pai_id = up.usu_id", "LEFT");
    	$this->db->join("pessoa", "uf.usu_pessoa_id = pes_id", "LEFT");
        $this->db->join("pessoa_endereco", "pes_id = pesend_idpessoa", "LEFT");
    	$this->db->join("endereco", "pesend_id = end_id", "LEFT");
        $this->db->join("pessoa_telefone", "pes_id = pestel_idpessoa", "LEFT");
    	$this->db->join("telefone", "pestel_idtelefone = tel_id", "LEFT");
        $this->db->where('p.red_idusuario', $idUsuario, false);
        $this->db->where('tel_tipo', 1, false);
        $this->db->order_by("(f.red_lvl - p.red_lvl) asc, f.red_parent");        
        return $this->db->get();

    }

    function getNumDiretosTotalById($idUsuario) 
    {
        $this->db->select('COUNT(1) AS total');
        $this->db->from('usuario_mmn');
        $this->db->where('usu_pai_id', $idUsuario);
        return $this->db->get()->row()->total;
    }

    function getNumRedeById($idUsuario, $lvl = 0) 
    {
        $this->db->select("FORMAT((((red_rht-red_lft)-1)/2),0) AS total", FALSE);
        $this->db->from("redes");
        $this->db->where("red_idusuario", $idUsuario, false);
        return $this->db->get()->row()->total;
    }
    
    public function adicionarUnilevel($usuario, $usuarioPai) {
        return $this->db->query("call rede_adicionar(".$usuario.",".$usuarioPai.")");
    }

    public function pagarBonusRecompra($usuario, $idPedido, $valor) {
        return true;
    }
}