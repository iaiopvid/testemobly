<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of UsuarioDao
 *
 * @author JOAO PAULO
 */
class Extrato_Dao extends CI_Model {

    public function getAll() 
    {
    	return $this->db->get('saldos')->result();
    }

    public function getGanhosAteHoje($idUsuario) {
        $this->db->select('SUM(sal_valor) total');
        $this->db->where('sal_idusuario', $idUsuario);
        $this->db->where('sal_inicio_vigencia <= NOW()');
        $this->db->where_in('sal_idtipobonus', array('1, 2')); // Tipos de Saldos que são bonus.
        $result = $this->db->get('saldos');
        if ($result->num_rows() > 0) {
            return $result->row()->total;
        }
        return 0;
    }

    public function getSaldoPontos($idUsuario) {
        $this->db->select('SUM(sep_pontos) total');
        $this->db->where('sep_idusuario', $idUsuario);
        $this->db->where('sep_inicio_vigencia <= NOW()');
        $result = $this->db->get('saldos_em_ponto');
        if ($result->num_rows() > 0) {
            return $result->row()->total;
        }
        return 0;
    }

    public function getSaldo($idUsuario) {
        $this->db->select('SUM(sal_valor) total');
        $this->db->where('sal_idusuario', $idUsuario);
        $this->db->where('sal_inicio_vigencia <= NOW()');
        $result = $this->db->get('saldos');
        if ($result->num_rows() > 0) {
            return $result->row()->total;
        }
        return 0;
    }

    public function getById($idUsuario) 
    {
        $this->db->where('sal_idusuario', $idUsuario);
        $result = $this->db->get('saldos');
        return $result->num_rows() > 0
            ? $result->row()
            : null;
    }

    public function getExtratoById($idUsuario, $condicao = array(), $ordenacao = FALSE, $tipoOrdem = 'ASC') 
    {
        $this->db->where('sal_idusuario', $idUsuario);
        $this->db->where($condicao);

        if ($ordenacao) {
            $this->db->order_by($ordenacao, $tipoOrdem);
        }

        return $this->db->get('saldos')->result();
    }
    
}