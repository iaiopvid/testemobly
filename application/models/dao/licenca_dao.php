<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of UsuarioDao
 *
 * @author JOAO PAULO
 */
class Licenca_Dao extends CI_Model {
    
    /**
     * 8001 - Licença sem renovação a mais de 30 dias. 
     */

    public function __construct() {
        parent::__construct();
        $this->load->model('dao/config_dao', 'ConfigDao');
    }

    public function getProdutoByLicenca($idLicenca) {
    	$this->db->where('prolic_idlicenca', $idLicenca);
    	$this->db->join('produto', 'prolic_idproduto = prod_id');
    	$this->db->join('licencas', 'prolic_idlicenca = lic_id');
    	$result = $this->db->get('produto_licenca');
    	if ($result->num_rows() > 0) {
    		return $result->row();
    	}
    	return null;
    }

    public function getLicencaById($idLicenca) {
        $this->db->where('lic_id', $idLicenca);
        $result = $this->db->get('licencas');
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return null;
    }

    public function addAtivacao($idUsuario, $idLicenca, $idPedido, $valor) 
    {
        // debug($idUsuario); debug($idLicenca); debug($idPedido); debug($valor);

        $ultimaAtivacao = $this->getUltimaAtivacao($idUsuario); 
        $dayInMonth = date('d') <= 15 ? 5 : 20;

        $lastValidade = is_null($ultimaAtivacao) 
            ? date('Y-m-'.$dayInMonth) 
            : $ultimaAtivacao->ultati_data;

        $newValidade = date('Y-m-d', strtotime($lastValidade. ' + 1 month'));
        $dateLimite = date('Y-m-d', strtotime($lastValidade. ' + 2 month'));

        #$interval = date_diff(date_create(date('Y-m-d')), date_create($lastValidade));
        #$days = $interval->days;
        
        #if ($days > 30) {
         #   return 8001;
        #}
        if (date('Y-m-d') > $dateLimite) {
            return 8001;
        }
        $ativacao = [
            'ati_idlicenca'     => $idLicenca,
            'ati_idusuario'     => $idUsuario,
            'ati_idpedido'      => $idPedido, 
            'ati_valor'         => $valor,
            'ati_data_criacao'  => date('Y-m-d H:i:s'),
            'ati_data_validade' => date('Y-m-d', strtotime($newValidade. ' + 1 month'))
        ];
        // debug($ativacao); die;
        $this->insertAtivacao($ativacao);
        return 1;
    }

    public function insertAtivacao($dados) {
        $this->db->insert('ativacoes', $dados);
        return $this->db->insert_id();
    }

    public function getAll()
    {
        return $this->db->get('licencas')->result();
    }

    public function getUltimaAtivacao($idUsuario) {
        $this->db->where('ultati_idusuario', $idUsuario);
        // $this->db->limit('1');
        // $this->db->order_by('ati_data_vencimento', 'DESC');
        $result = $this->db->get('ultima_ativacao');
        return $result->num_rows() > 0 ? $result->row() : null;
    }

    public function getLicencaByUsuarioId($idUsuario) {
        $this->db->join('ultima_ativacao', 'ultati_idlicenca = lic_id');
        $this->db->where('ultati_idusuario', $idUsuario);
        $result = $this->db->get('licencas');
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return null;
    }

    public function getAtivacoesExpiradas()
    {
        $prazo = $this->ConfigDao->getAll()["PRAZO_LIMITE_ATIVACAO"];
        $this->db->select("*");
        $this->db->where('datediff(now(), ultati_data_validade) > '.$prazo);
        $result = $this->db->get("ultima_ativacao");
        if ($result->num_rows() > 0) {
            return $result->result();
        } 
        return [];
    }
    
    public function getLicencasAtivacoes($idLicenca) {
        $this->db->where("lic_id >=", $idLicenca);
        return $this->db->get('licencas')->result();
    }
}