<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of UsuarioDao
 *
 * @author JOAO PAULO
 */
class Cadastro_Dao extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    function getByParent($usuario, $status) {
        $this->db->where('usu_pai', $usuario);
        $this->db->where('usu_status', $status);
        $this->db->join('pessoa', 'usu_pessoa_id = pes_id');
        return $this->db->get('usuario_mmn')->result();
    }
    
    function getDadosUsuario($usuario) {
        $this->db->where('usu_id', $usuario);
        $this->db->join('pessoa', 'usu_pessoa_id = pes_id', 'LEFT');
        $this->db->join('pessoa_endereco', 'pes_id = pesend_idpessoa', 'LEFT');
        $this->db->join('endereco', 'pesend_idendereco = end_id', 'LEFT');
        $this->db->join('estado', 'end_estado = est_id', 'LEFT');
        $this->db->join('pessoa_telefone', 'pes_id = pestel_idpessoa', 'LEFT');
        $this->db->join('telefone', 'pestel_idtelefone = tel_id AND tel_tipo = 1', 'LEFT');
        return $this->db->get('usuario_mmn')->row();
    }
    
    function getNumTelefoneByTipo($idPessoaTelefone, $tipo) {
        $this->db->where('pestel_idpessoa', $idPessoaTelefone, false);
        $this->db->where('tel_tipo', $tipo, false);
        $this->db->join('telefone', 'pestel_idtelefone = tel_id');
        return $this->db->get('pessoa_telefone')->row();
    }

    function aprovarCadastro($idUsuario) {
        return $this->setStatus($idUsuario, '1');
    }

    function getCadastrosAtivos($parent=null) {
        if (!is_null($parent)) {
            $this->db->where("usu_pai_id", $parent);
        }
        $this->db->join('pedidos', 'ped_idusu = usu_id AND ped_tipo_bonus = 1', 'LEFT');
        $this->db->join('pessoa', 'pes_id = usu_pessoa_id');
        $this->db->where("usu_status", '1');
        return $this->db->get("usuario_mmn");
    }

    function getCadastrosPendentes($parent=null) {
        if (!is_null($parent)) {
            $this->db->where("usu_pai_id", $parent);
        }
        $this->db->join('pedidos', 'ped_idusu = usu_id AND ped_tipo_bonus = 1', 'LEFT');
        $this->db->join('tipo_bonus', 'ped_tipo_bonus = tipbon_id', 'LEFT');
        $this->db->join('pessoa', 'pes_id = usu_pessoa_id');
        $this->db->where("usu_status", '0');
        return $this->db->get("usuario_mmn");
    }
    
    function getUsuarioById($idUsuario) {
        $this->db->where('usu_id', $idUsuario);
        $result = $this->db->get('usuario_mmn');
        return $result->num_rows() > 0
            ? $result->row()
            : null;
    }
    
    function getUsuarioByIdAdmin($idUsuario) {
        $this->db->where('usu_id', $idUsuario);
        $result = $this->db->get('usuario_admin');
        return $result->num_rows() > 0
            ? $result->row()
            : null;
    }
    
    function salvarPessoa($dados) {
        $idPessoa = $dados['pes_id'];
        $now = date('Y-m-d H:i:s');
        $dados['pes_data_alteracao'] = $now;
        if ($idPessoa > 0) {
            $this->db->where('pes_id', $idPessoa);
            $this->db->update('pessoa', $dados);
        } else {
            unset($dados->pes_id);
            $dados['pes_data_criacao'] = $now;   
            $this->db->insert('pessoa', $dados);
            $idPessoa = $this->db->insert_id();
        }
        return $idPessoa;
    }
    
    public function salvarUsuario($dados) {
        $idUsuario = $dados['usu_id'];
        $now = date('Y-m-d H:i:s');

        $dados['usu_data_alteracao'] = $now;
        if ($idUsuario > 0) {
            unset($dados['usu_pai_id']);
            $this->db->where('usu_id', $idUsuario);
            $this->db->update('usuario_mmn', $dados);
        } else {
            $dados['usu_data_criacao'] = $now;
            unset($dados['usu_id']);
            $this->db->insert('usuario_mmn', $dados);
            $idUsuario = $this->db->insert_id();
        }
        return $idUsuario;
    }
    
    public function salvarUsuarioAdmin($dados) {
        $idUsuario = $dados['usu_id'];
        $now = date('Y-m-d H:i:s');
        
        $dados['usu_data_alteracao'] = $now;
        if ($idUsuario > 0) {
            $this->db->where('usu_id', $idUsuario);
            $this->db->update('usuario_admin', $dados);
        } else {
            $dados['usu_data_criacao'] = $now;
            $this->db->insert('usuario_admin', $dados);
            $idUsuario = $this->db->insert_id();
        }
        return $idUsuario;
    }
    
    public function salvarUsuarioCvd($dados) {
        $idCvd = $dados['cvd_id'];
        $now = date('Y-m-d H:i:s');
        
        $dados['cvd_data_alteracao'] = $now;
        if ($idCvd > 0) {
            $this->db->where('cvd_id', $idCvd);
            $this->db->update('cvds', $dados);
        } else {
            $dados['cvd_data_criacao'] = $now;
            $this->db->insert('cvds', $dados);
            $idCvd = $this->db->insert_id();
        }

        return $idCvd;
    }
    
    public function addTelefone($idPessoa, $telefone, $tipo) {

        $now = date('Y-m-d H:i:s');
        $telefoneDados = [
            'tel_numero' => $telefone
            ,'tel_tipo' => $tipo
            ,'tel_descricao' => $tipo == '1' ? 'Fixo' : 'Celular'
            ,'tel_data_criacao' => $tipo = $now
            ,'tel_data_alteracao' => $tipo = $now
        ];
        // debug($tipo, true);
        $this->db->insert('telefone', $telefoneDados);
        $idTelefone = $this->db->insert_id();
        
        $pessoaTelefone = [
            'pestel_idpessoa' => $idPessoa

            ,'pestel_idtelefone' => $idTelefone
        ];
        $this->db->insert('pessoa_telefone', $pessoaTelefone);
        return $idTelefone;
    }

    public function updateTelefone($idTelefone, $telefone, $tipo) {
        $now = date('Y-m-d H:i:s');
        $telefoneDados = [
             'tel_numero' => $telefone
            ,'tel_data_alteracao' => $now
        ];
        
        $this->db->where('tel_id', $idTelefone);
        $this->db->where('tel_tipo', $tipo);
        $this->db->update('telefone', $telefoneDados);
        
        return $idTelefone;
    }
    
    public function addEnderecoUsuario($idUsuario, $dados) {
        $usuarioPessoa = $this->getByIdUsuario($idUsuario);
        
        $idPessoa = $usuarioPessoa->pes_id;
        return $this->addEndereco($idPessoa, $dados);
    }

    public function addEndereco($idPessoa, $dados) {
    	$now = date('Y-m-d H:i:s');
    	$dados['end_data_criacao'] = $now;
    	$dados['end_data_alteracao'] = $now;
        $this->db->insert('endereco', $dados);
        $idEndereco = $this->db->insert_id();
        
        $pessoaEndereco = [
            'pesend_idpessoa' => $idPessoa
            ,'pesend_idendereco' => $idEndereco
            ,'pesend_data_criacao' => $now
            ,'pesend_data_alteracao' => $now
        ];
        $this->db->insert('pessoa_endereco', $pessoaEndereco);
        
        return $idEndereco;
    }
    
    function getEstadoBySigla($sigla) {
        $this->db->where('est_sigla', $sigla);
        $result = $this->db->get('estado');
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return null;
    }
    
    function getEstados() {
        $this->db->order_by('est_nome');
        $result = $this->db->get('estado')->result();

        return $result;
    }
    
    function getBancos() {
        $this->db->order_by('ban_nome');
        $result = $this->db->get('bancos')->result();

        return $result;
    }
    
    function getContaBancoById($usuario) {
        $this->db->where("cba_idpessoa", $usuario);
        $result = $this->db->get("conta_banco");
        if ($result->num_rows() > 0) 
            return $result->row();
        return null;
    }
    
    function getByUsuario($usuario) {
        $this->db->where("UPPER(usu_login)", strtoupper($usuario));
        $this->db->where("usu_status <>", '99');
        $this->db->join('pessoa', 'pes_id = usu_pessoa_id');
        $result = $this->db->get("usuario_mmn");
        if ($result->num_rows() > 0) 
            return $result->row();
        return null;
    }

    function getByUsuarioAdmin($usuario) {
        $this->db->where("UPPER(usu_login)", strtoupper($usuario));
        $this->db->where("usu_status <>", '99');
        $this->db->join('pessoa', 'pes_id = usu_pessoa_id');
        $result = $this->db->get("usuario_admin");

        if ($result->num_rows() > 0) 
            return $result->row();
        
        return null;
    }

    function getByUsuarioCvd($usuario) {
        $this->db->where("UPPER(cvd_login)", strtoupper($usuario));
        $this->db->where("cvd_status <>", '99');
        $this->db->join('pessoa', 'pes_id = cvd_pessoa_id');
        $result = $this->db->get("cvds");

        if ($result->num_rows() > 0) 
            return $result->row();
        
        return null;
    }
    
    function getByIdUsuario($idUsuario) {
        $this->db->where("usu_id", $idUsuario);
        $this->db->where("usu_status <>", '99');
        $this->db->join('pessoa', 'pes_id = usu_pessoa_id');
        $result = $this->db->get("usuario_mmn");
        if ($result->num_rows() > 0) 
            return $result->row();
        return null;
    }
    
    function getUltimaAtivacao($idUsuario) {
        $this->db->where('ati_idusuario', $idUsuario);
        $this->db->limit('1');
        $this->db->order_by('ati_data_validade', 'DESC');
        $result = $this->db->get('ativacoes');
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return null;
    }

    function getLicencaVigente($idUsuario) {
        $this->db->where('ati_idusuario', $idUsuario);
        $this->db->where('ati_data_validade >=', date('Y-m-d'));
        $this->db->limit('1');
        $this->db->order_by('ati_data_validade', 'DESC');
        $result = $this->db->get('ativacoes');
        if ($result->num_rows() > 0) {
            return $result->row();
        } else {
            return $this->getUltimaAtivacao($idUsuario);
        }
    }

    function checkAtivo($idUsuario) {

        $result = $this->getUltimaAtivacao($idUsuario); 
        
        //Se o usuário for admin ele sempre estará ativo.
        return $this->checkRoot($idUsuario) 
            OR (!is_null($result) AND $result->ati_data_validade > date('Y-m-d'));
    }

    function checkRoot($idUsuario) {
        $this->db->where('usu_id', $idUsuario);
        $this->db->where('usu_admin', '1');
        $this->db->where("usu_status <>", '99');
        return $this->db->get("usuario_mmn")->num_rows() > 0;
    }

    function checkAtivoAdmin($usuario) {
        $this->db->where("UPPER(usu_login)", strtoupper($usuario));
        $this->db->where("usu_status <>", '99');
        $result = $this->db->get("usuario_admin");
        if ($result->num_rows() > 0) 
            return true;
        return false;
    }

    function checkAtivoCvd($usuario) {
        $this->db->where("UPPER(cvd_login)", strtoupper($usuario));
        $this->db->where("cvd_status <>", '99');
        $result = $this->db->get("cvds");
        if ($result->num_rows() > 0) 
            return true;
        return false;
    }
    
    function checkUsuario($usuario) {
        return $this->getByUsuario($usuario) != null;
    }
    
    function checkUsuarioAdmin($usuario) {
        return $this->getByUsuarioAdmin($usuario) != null;
    }
    
    function checkEmail($email) {
        $this->db->where("usu_email", $email);
        $this->db->where("usu_status <>", '99');
        return $this->db->get("usuario_mmn")->num_rows() > 0;
    }
    
    function checkEmailAdmin($email) {
        $this->db->where("usu_email", $email);
        $this->db->where("usu_status <>", '99');
        return $this->db->get("usuario_admin")->num_rows() > 0;
    }
    
    function checkCpf($cpf) {
        $this->db->where("pes_cpf_cnpj", $cpf);
        $this->db->where("usu_status <>", '99');
        $this->db->join('pessoa', 'usu_pessoa_id = pes_id');
        return $this->db->get("usuario_mmn")->num_rows() > 0;
    }
    
    function checkCpfAdmin($cpf) {
        $this->db->where("pes_cpf_cnpj", $cpf);
        $this->db->where("usu_status <>", '99');
        $this->db->join('pessoa', 'usu_pessoa_id = pes_id');
        return $this->db->get("usuario_admin")->num_rows() > 0;
    }

    private function setStatus($idUsuario, $status) {
        $this->db->where('usu_id', $idUsuario);
        return $this->db->update('usuario_mmn', array('usu_status'=> $status));
    }

    private function setStatusAdmin($idUsuario, $status) {
        $this->db->where('usuario_admin', $idUsuario);
        return $this->db->update('usu_status', $status);
    }
    
    function salvarDadosBancarios($dados) {
        $idPessoa = $dados['cba_idpessoa'];
        $now = date('Y-m-d H:i:s');
        
        $dados['cba_data_alteracao'] = $now;

        unset($dados->cba_idpessoa);

        $dados['cba_data_criacao'] = $now;   
        $this->db->insert('conta_banco', $dados);
        $idPessoa = $this->db->insert_id();

        return $idPessoa;
    }
    
    function updateDadosBancarios($dados, $idConta) {
        $idPessoa = $dados['cba_idpessoa'];
        $now = date('Y-m-d H:i:s');
        $dados['cba_data_alteracao'] = $now;

        $this->db->where('cba_idpessoa', $idPessoa);
        $this->db->where('cba_id', $idConta);
        $this->db->update('conta_banco', $dados);

        return $idPessoa;
    }

    function cancelarCadastroByIdUsuario($idUsuario) {
        $this->db->where('usu_id', $idUsuario);
        $this->db->update('usuario_mmn', ["usu_status" => "99"]);
    }
}
