<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of UsuarioDao
 *
 * @author JOAO PAULO
 */
class Endereco_Dao extends CI_Model {

	public function insert($dados) {
		$now = date('Y-m-d H:i:s');
    	$dados['end_data_criacao'] = $now;
    	$dados['end_data_alteracao'] = $now;
        $this->db->insert('endereco', $dados);
        return $this->db->insert_id();
    }

    public function update($idEndereco, $dados) {
        $now = date('Y-m-d H:i:s');
        $dados['end_data_alteracao'] = $now;
        $this->db->where('end_id', $idEndereco);
        $this->db->update('endereco', $dados);
        return $this->db->insert_id();
    }

    public function addEnderecoPessoa($idPessoa, $idEndereco) {
    	$this->db->insert('pessoa_endereco', 
    		[
    			'pesend_idpessoa' => $idPessoa, 
    			'pesend_idendereco' => $idEndereco
			]
		);

    	return $this->db->insert_id();
    }

    public function addEnderecoPedido($idPedido, $idEndereco) {
    	$this->db->insert('pedidos_endereco', 
    		[
    			'pedend_pedido_id' => $idPedido,
    			'pedend_endereco_id' => $idEndereco
			]
		);

    	return $this->db->insert_id();
    } 

    public function getEnderecoByPessoa($idPessoa) {
        $this->db->where('pesend_idpessoa', $idPessoa);
        $this->db->where('pesend_padrao', '1');
        $this->db->join('estado', 'est_id = end_estado');
        $this->db->join('pessoa_endereco', 'pesend_idendereco = end_id');
        $result = $this->db->get('endereco');
        if ($result->num_rows() > 0) {
            return $result->row();
        }
        return null;
    }
}
