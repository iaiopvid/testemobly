<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Token_Dao extends CI_Model {

    public function getAll() 
    {
    	return $this->db->get('token')->result();
    }

    function getById($idToken) 
    {
        $this->db->where('tok_idusuario', $idUsuario);
        $result = $this->db->get('tokens');
        return $result->num_rows() > 0
            ? $result->row()
            : null;
    }

    function getTokenBySalt($salt) 
    {
        $this->db->where('tok_salt', $salt);
        $result = $this->db->get('tokens');
        return $result->num_rows() > 0
            ? $result->row()
            : null;
    }
    
    public function salvarToken($dados, $idToken) {

        $idUsuario = $dados['tok_idusuario'];
        $valor = $dados['tok_valor'];        
        $now = date('Y-m-d H:i:s');
        
        $dados['tok_data_criacao'] = $now;
        if ($idToken > 0) {
            echo "token > 0"; die;
            $this->db->where('tok_id', $idUsuario);
            $this->db->where('tok_idusuario', $idUsuario);
            $this->db->update('tokens', $dados);
        } else {
            $dados['tok_data_criacao'] = $now;
            $dados['tok_salt'] = $this->db->query('SELECT UPPER(SUBSTRING(MD5(RAND()) FROM 1 FOR 16)) salt')->result();
            $dados['tok_salt'] = $dados['tok_salt'][0]->salt;
            $dados['tok_valido'] = 's';
            $dados['tok_data_validade'] = date('Y-m-d', strtotime("+1 month", strtotime($now)));
            $this->db->insert('tokens', $dados);
            $idUsuario = $this->db->insert_id();
        }
        return $idUsuario;
    }
    
    public function adicionarUnilevel($usuario, $usuarioPai) {
        return $this->db->query("call rede_adicionar(".$usuario.",".$usuarioPai.")");
    }

}