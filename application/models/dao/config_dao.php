<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of UsuarioDao
 *
 * @author JOAO PAULO
 */
class Config_Dao extends CI_Model {

    public function getAll() 
    {
    	$result  = $this->db->get('config_mmn');
    	if ($result->num_rows() > 0) {
    		return $result->result_array()[0];
    	}
    	return null;
    }
}