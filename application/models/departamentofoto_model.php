<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DepartamentoFoto_Model extends CI_Model {
  public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0, $limite = LINHAS_PESQUISA_DASHBOARD) {
      $this->db->select ( 'd.depfot_id, d.depfot_extensao, d.depfot_iddepartamento' );
      $this->db->where ( $condicao );
      $this->db->from ( 'departamento_foto d' );
      
//      return $this->db->get()->result();
      
      if ($primeiraLinha) {
          return $this->db->get ()->first_row ();
      } else {
          if ($limite !== FALSE) {
              $this->db->limit ( $limite, $pagina );
          }
          return $this->db->get ()->result ();
      }
  }

	public function post($itens) {
		$res = $this->db->insert('departamento_foto', $itens);
		if ($res) {
			return $this->db->insert_id();
		} else {
			return FALSE;
		}
	}

	public function update($itens, $depfot_id) {
		$this->db->where('depfot_id', $depfot_id);
		$res = $this->db->update('departamento_foto', $itens);

		if ($res) {
			return $depfot_id;
		} else {
			return FALSE;
		}
		
	}

	public function delete($idfoto, $idDepartamento) {
		$this->db->where('depfot_iddepartamento', $idDepartamento, FALSE);
		$this->db->where('depfot_id', $idfoto, FALSE);
		return $this->db->delete('departamento_foto');
	}
}