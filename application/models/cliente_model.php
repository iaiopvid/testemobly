<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cliente_Model extends CI_Model {

	public function getTotal($condicao = array()) {
		$this->db->where($condicao);
		$this->db->from('cliente');
		return $this->db->count_all_results();
	}

	public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0, $limite = LINHAS_PESQUISA_DASHBOARD) {
		$this->db->select('cli_id, cli_nome, cli_endereco, cli_cidade, cli_uf, cli_cep, cli_email, cli_telefone, cli_cpf, cli_sexo, cli_senha');
		$this->db->where($condicao);
		$this->db->from('cliente');

		if ($primeiraLinha) {
			return $this->db->get()->first_row();
		} else {
			if ($limite !== FALSE) {
				$this->db->limit($limite, $pagina);
			}
			return $this->db->get()->result();
		}
	}

	public function post($itens) {
		$res = $this->db->insert('cliente', $itens);

		if ($res) {
			return $this->db->insert_id();
		} else {
			return FALSE;
		}

	}
	
	public function update($itens, $cli_id) {
		$this->db->where('cli_id', $cli_id);
		$res = $this->db->update('cliente', $itens);

		if ($res) {
			return $cli_id;
		} else {
			return FALSE;
		}
		
	}

	public function delete($cli_id) {
		$this->db->where('cli_id', $cli_id, FALSE);
		return $this->db->delete('cliente');
	}
	
	public function validaCPFDubplicado($cli_id, $cpf) {
		$this->db->from('cliente');
		$this->db->where('cli_cpf', $cpf, TRUE);
		$this->db->where('cli_id !=', $cli_id, TRUE);
		return $this->db->count_all_results();
	}
	
	public function validaEmailDubplicado($cli_id, $email) {
		$this->db->from('cliente');
		$this->db->where('cli_email', $email, TRUE);
		$this->db->where('cli_id !=', $cli_id, TRUE);
		return $this->db->count_all_results();
	}

}