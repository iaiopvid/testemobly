<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produto_model extends CI_Model {

	public function getTotal($condicao = array()) {
		$this->db->where($condicao);
		$this->db->where("prod_tipo", "0");	
		$this->db->from('produto');
		return $this->db->count_all_results();
	}

	public function getBusca($condicao = array(), $primeiraLinha = FALSE, 
		$pagina = 0, $limite = LINHAS_PESQUISA_DASHBOARD, $ordenacao = FALSE, $tipoOrdem = "ASC") 
	{
		$this->db->select('prod_id , prod_nome , prod_resumo , prod_ficha , prod_valor , prod_valorpromocional, prodest_quantidade, prod_urlseo');
		$this->db->where("prod_id", "prodest_idproduto", FALSE);
		$this->db->where("prod_tipo", "0", FALSE);

		foreach ($condicao as $c) {
			$this->db->like("UPPER(prod_nome)", strtoupper($c), 'both');
		}


		$this->db->from('produto');
		$this->db->from('produto_estoque');

		if ($primeiraLinha) {
			return $this->db->get()->first_row();
		} else {
			if ($limite != FALSE) {
				$this->db->limit($limite, $pagina);
			}
			
			if ($ordenacao) {
				$this->db->order_by($ordenacao, $tipoOrdem);
				
				
			}
//			$this->db->limit(LINHAS_PESQUISA_DASHBOARD, $pagina);
			return $this->db->get()->result();
		}
	}

	public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0, $limite = LINHAS_PESQUISA_DASHBOARD, 
											$ordenacao = FALSE, $tipoOrdem = "ASC") {
		$this->db->select('
			prod_id , 
			prod_nome , 
			prod_resumo , 
			prod_ficha , 
			prod_valor , 
			prod_valorpromocional, 
			prod_peso,
			prod_altura, 
			prod_largura, 
			prod_comprimento, 
			prod_urlseo, 
			prod_tipo, 
			prod_status,
			prodest_quantidade, 
			profot_id, 
			profot_extensao, 
			profot_idproduto'
		); 
		$this->db->from('produto');
		$this->db->join('produto_estoque', 'prod_id = prodest_idproduto', 'left');
		$this->db->join('produto_foto', 'prod_id = profot_idproduto', 'left');
		$this->db->where($condicao);
		$this->db->where("prod_tipo", "0");	

		if ($primeiraLinha) {
			return $this->db->get()->first_row();
		} else {
			if ($limite != FALSE) {
				$this->db->limit($limite, $pagina);
			}
			
			if ($ordenacao) {
				$this->db->order_by($ordenacao, $tipoOrdem);
			}
//			$this->db->limit(LINHAS_PESQUISA_DASHBOARD, $pagina);
			return $this->db->get()->result();
			// return $this->db->get()->first_row();
		}
	}

	public function getDepartamento($condicao = array(), $primeiraLinha = FALSE, $pagina = 0, $limite = LINHAS_PESQUISA_DASHBOARD, 
											$ordenacao = FALSE, $tipoOrdem = "ASC") {
		$this->db->select('prod_id , prod_nome , prod_resumo , prod_ficha , prod_valor , prod_valorpromocional, prod_urlseo, dp.pdep_pdep_id');
		$this->db->from('produto');
		$this->db->from('produto_departamento dp');
		$this->db->where($condicao);
		$this->db->where("dp.pdep_prod_id", "produto.prod_id", FALSE);
		$this->db->where("total_produto(produto.prod_id) >", 0, FALSE);

		if ($primeiraLinha) {
			return $this->db->get()->first_row();
		} else {
			if ($limite != FALSE) {
				$this->db->limit($limite, $pagina);
			}
			
			if ($ordenacao) {
				$this->db->order_by($ordenacao, $tipoOrdem);
				
				
			}
//			$this->db->limit(LINHAS_PESQUISA_DASHBOARD, $pagina);
			return $this->db->get()->result();
		}
	}

	public function getBuscaTotal($condicao = array()) {
		$this->db->from('produto');

		foreach ($condicao as $c) {
			$this->db->like("UPPER(prod_nome)", strtoupper($c), 'both');
		}

		// $this->db->where("total_sku(produto.prod_id) >", 0, FALSE);
			
		return $this->db->count_all_results();
			
	}

	public function getDepartamentoTotal($condicao = array()) {
		$this->db->from('produto');
		$this->db->from('produto_departamento dp');
		$this->db->where($condicao);
		$this->db->where("dp.pdep_prod_id", "produto.prod_id", FALSE);
		$this->db->where("total_produto(produto.prod_id) >", 0, FALSE);
			
		return $this->db->count_all_results();
			
	}

	public function post($itens) {
		$res = $this->db->insert('produto', $itens);

		if ($res) {
			return $this->db->insert_id();
		} else {
			return FALSE;
		}

	}

	public function postEstoque($itensEstoque) {
		$res = $this->db->insert('produto_estoque', $itensEstoque);

		if ($res) {
			return $this->db->insert_id();
		} else {
			return FALSE;
		}

	}

	public function update($itens, $prod_id) {
		$this->db->where('prod_id', $prod_id);
		$res = $this->db->update('produto', $itens);

		if ($res) {
			return $prod_id;
		} else {
			return FALSE;
		}
		
	}

	public function updateEstoque($itensEstoque, $prod_id) {
		$this->db->where('prodest_idproduto', $prod_id);
		$res = $this->db->update('produto_estoque', $itensEstoque);

		if ($res) {
			return $prod_id;
		} else {
			return FALSE;
		}
		
	}

	public function delete($prod_id) {
		$this->db->where('prod_id', $prod_id, FALSE);
		return $this->db->delete('produto');
	}

	public function getValorProduto($idProduto) {
		$this->db->select('prod_valor AS valor');
		$this->db->where('prod_id', $idProduto);
		$result = $this->db->get('produto');
		if ($result->num_rows() > 0) {
			return $result->row()->valor;
		}
		return null;
	}

}