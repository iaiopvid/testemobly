<?php

if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class FotoSku_Model extends CI_Model {
	
	public function post($item) {
		
		$this->db->insert('produto_foto_sku', $item);
	}
	
	public function limpaImagens($pfsku_idprodutofoto) {
		$this->db->where('pfsku_idprodutofoto', $pfsku_idprodutofoto, FALSE);
		$this->db->delete('produto_foto_sku');
	}
	
	public function getSkusFoto($pfsku_idprodutofoto, $primeiraLinha = FALSE, $pagina = 0, $limite = LINHAS_PESQUISA_DASHBOARD) {
		$this->db->select('pfsku_idsku');
		$this->db->from('produto_foto_sku');
		$this->db->where('pfsku_idprodutofoto', $pfsku_idprodutofoto, FALSE);

		if ($primeiraLinha) {
      return $this->db->get ()->first_row ();
    } else {
      if ($limite !== FALSE) {
          $this->db->limit ( $limite, $pagina );
      }
      return $this->db->get ()->result ();
    }

		return $this->db->get()->result();
	}
	
	public function getFotoSKU($sku_id, $primeiraLinha = FALSE,  $pagina = 0, $limite = LINHAS_PESQUISA_DASHBOARD) {
		$this->db->select('profot_id, profot_extensao');
		$this->db->from('produto_foto_sku');
		$this->db->from('produto_foto');
		$this->db->where('pfsku_idsku', $sku_id, FALSE);
		$this->db->where('profot_id', 'pfsku_idprodutofoto', FALSE);

		if ($primeiraLinha) {
      return $this->db->get ()->first_row ();
    } else {
      if ($limite !== FALSE) {
          $this->db->limit ( $limite, $pagina );
      }
      return $this->db->get ()->result ();
    }

		return $this->db->get()->result();
	}
}