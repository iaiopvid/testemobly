<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class carrinhoItem_Model extends CI_Model {

	// public function getTotal($condicao = array()) {
	// 	$this->db->where($condicao);
	// 	$this->db->from('carrinho_item');
	// 	return $this->db->count_all_results();
	// }

	// public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0) {
	// 	$this->db->select('item_id, atr_nome, item_idtipo');
	// 	$this->db->where($condicao);
	// 	$this->db->from('carrinho_item');

	// 	if ($primeiraLinha) {
	// 		return $this->db->get()->first_row();
	// 	} else {
	// 		$this->db->limit(LINHAS_PESQUISA_DASHBOARD, $pagina);
	// 		return $this->db->get()->result();
	// 	}
	// }

	// public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0, $limite = LINHAS_PESQUISA_DASHBOARD) {
	// 	$this->db->select('a.item_id, a.atr_nome, a.item_idtipo');
	// 	$this->db->select('t.atip_nome AS nomepai');
	// 	$this->db->where($condicao);
	// 	$this->db->from('carrinho_item a');
	// 	$this->db->join('carrinho_item_tipo t', 't.atip_id = a.item_idtipo', 'LEFT');

	// 	if ($primeiraLinha) {
	// 		return $this->db->get()->first_row();
	// 	} else {
	// 		if ($limite !== FALSE) {
	// 			$this->db->limit($limite, $pagina);
	// 		}
			
	// 		return $this->db->get()->result();
	// 	}
	// }

	public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0) {
		$this->db->select('prod_nome');
		$this->db->select('item_quantidade, item_valor, item_idcarrinho');
		$this->db->select("item_quantidade * item_valor AS valorfinal", FALSE);
		$this->db->where($condicao);
		$this->db->from('carrinho_item');
		$this->db->from('sku');
		$this->db->from('produto');
		$this->db->where('sku_id', 'item_idsku', FALSE);
		$this->db->where('prod_id', 'sku_idproduto', FALSE);

		if ($primeiraLinha) {
			return $this->db->get()->first_row();
		} else {
			$this->db->limit(LINHAS_PESQUISA_DASHBOARD, $pagina);
			return $this->db->get()->result();
		}
	}

	public function post($itens) {
		$res = $this->db->insert('carrinho_item', $itens);

		if ($res) {
			return $this->db->insert_id();
		} else {
			return FALSE;
		}

	}

	public function update($itens, $item_id) {
		$this->db->where('item_id', $item_id);
		$res = $this->db->update('carrinho_item', $itens);

		if ($res) {
			return $item_id;
		} else {
			return FALSE;
		}
		
	}

	public function delete($item_id) {
		$this->db->where('item_id', $item_id, FALSE);
		return $this->db->delete('carrinho_item');
	}


}