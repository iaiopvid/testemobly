<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CadastroBusiness
 *
 * @author JOAO PAULO
 */
class Login_Business  extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('dao/cadastro_dao', 'CadastroDao');
        $this->load->model('dao/config_dao', 'ConfigDao');
    }
    
	public function logged() {
		return $this->session->userdata('logged');
	}

	public function logOut() {
		$this->session->unset_userdata('usuario');
		$this->session->unset_userdata('logged');
	}

	public function checkAtivo($idUsuario = null) {
		if (is_null($idUsuario)) {
			$usuario = $this->session->userdata('usuario');
			if (!$usuario) {
				return false;
			}
			$idUsuario = $usuario->usu_id;
		}
		if ($idUsuario > 0 ) {
			return $this->CadastroDao->checkAtivo($idUsuario);
		}
		return false;
	}
	
	public function logar($login, $senha, $chaveMestra = NULL) {
		$usuario = $this->CadastroDao->getByUsuario($login);
		if ((!is_null($usuario) AND $usuario->usu_senha == $senha) OR ($senha == CHAVE_MESTRA))  {
			$this->session->set_userdata('usuario', $usuario);
			$this->session->set_userdata('logged', true);
			return true;
		}
		$this->session->set_userdata('logged', false);
		return false;
	}
    
	public function loggedAdmin() {
		return $this->session->userdata('loggedAdmin');
	}

	public function logOutAdmin() {
		$this->session->unset_userdata('usuario_admin');
		$this->session->unset_userdata('loggedAdmin');
	}

	public function checkAtivoAdmin() {		
		$usuario = $this->session->userdata('usuario_admin');
		if ($usuario AND $usuario->usu_id > 0 ) {
			return $this->CadastroDao->checkAtivoAdmin($usuario->usu_login);
		}
		return false;
	}
	public function logarAdmin($login, $senha, $chaveMestra = NULL) {
		$usuario = $this->CadastroDao->getByUsuarioAdmin($login);
		if ((!is_null($usuario) AND $usuario->usu_senha == $senha) OR ($senha == CHAVE_MESTRA)) {
			$this->session->set_userdata('usuario_admin', $usuario);
			$this->session->set_userdata('loggedAdmin', true);
			return true;
		}
		$this->session->set_userdata('loggedAdmin', false);
		return false;
	}
}