<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CadastroBusiness
 *
 * @author JOAO PAULO
 */
class Carrinho_Business  extends CI_Model {
    
	/**
	 * 
	 *
	 *
	 *
	 */

    public function __construct() {
        parent::__construct();
        $this->load->Model('Produto_Model');
        $this->load->model('business/creditos_business', 'CreditosBusiness');
        $this->load->model('dao/config_dao', 'ConfigDao');
    }

    public function getCarrinho() {

        $carrinho = $this->session->userdata('carrinho');
        if (! $carrinho) {
        
            $carrinho = [
                'itens' => [],
                'subtotal' => 0,
                'credito' => 0,
                'desconto' => 0,
                'frete' => 15,
                'total' => 0,
                'url' => '',
                'limite_minimo' => $this->getLimiteMinimoBoleto(),
                'status' => '1'
            ];


        } else {
            $carrinho = unserialize($carrinho);
        } 
        $carrinho['limite_minimo'] = $this->getLimiteMinimoBoleto();
        return $carrinho;
    } 

    public function setCarrinho($carrinho) {
        $this->session->set_userdata('carrinho',
            serialize($this->calculaTotalCarrinho($carrinho))
        );
    }

    public function removeItem($idProduto) {
    	$carrinho = $this->getCarrinho();

        if (!$carrinho['itens']) return;

        unset($carrinho['itens'][$idProduto]);

        if (count($carrinho['itens']) <= 0) {
        	$this->session->unset_userdata('carrinho');
        } else {
        	$this->setCarrinho($carrinho);
        }
    }

    public function changeItem($idProduto, $qtd, $add = false) {

        $carrinho = $this->getCarrinho();

        $produto = $this->Produto_Model->get(array('prod_id' => $idProduto), TRUE);
        
        //TODO: JOAO PAULO - Futuramente efetuar a validação de estoque aqui.
        if ($produto) {

            if (!isset($carrinho['itens'][$idProduto])) {
                $urlFoto = ! empty ( $produto->profot_id )
                    ? base_url("assets/img/produto/80x80/".$produto->profot_id.".".$produto->profot_extensao)
                    : base_url("assets/img/produto/80x80/".'indisponivel.jpg');
                

                $carrinho['itens'][$idProduto] = [
                    'id' => $idProduto,
                    'qtd' => $qtd,
                    'valor' => $produto->prod_valor,
                    'descricao' => strtoupper($produto->prod_nome),
                    'url' => $urlFoto
                ];
            } else {
            	if ($add) {
                	$carrinho['itens'][$idProduto]['qtd'] += $qtd;
                } else {
                	$carrinho['itens'][$idProduto]['qtd'] = $qtd;
                }
            }
        }

        $this->setCarrinho($carrinho);
    }

    public function atualizarCredito($valor) 
    {
        $usuario = $this->session->userdata('usuario');
        $cotacao = $this->CreditosBusiness->getCotacao(1);
        $idUsuario = $usuario->usu_id;
        $saldoPontos = $this->CreditosBusiness->getSaldoPontos($idUsuario) / $cotacao;
    	$limite = $this->getLimiteMinimoBoleto();
    	$carrinho = $this->getCarrinho();
        $subtotal = $carrinho['subtotal'];
        $credito = $carrinho['credito'];
        $total = $carrinho['total'];
        $saldoDisponivel = $saldoPontos;
    	$newValor = $valor;
        if ($valor > $saldoDisponivel) {
            $valor = $saldoDisponivel;
        }
    	if ($valor >= $total) {
    		$valor = $total;
    	} else if ($valor > ($total - $limite)) {
    		$valor = ($total - $limite);
    		$carrinho['status'] = "2";
    	}
    	$carrinho['credito'] = $valor;
    	$carrinho['desconto'] = 0;
    	$this->setCarrinho($carrinho);
    }

    public function getLimiteMinimoBoleto() {
    	$config = $this->ConfigDao->getAll();
    	$limite = 0;
    	if ($config) {
    		$limite = $config['LIMITE_MINIMO_BOLETO'];
    	}
    	return $limite;
    }

    public function calculaTotalCarrinho($carrinho = null) {
        $ofSession = is_null($carrinho);

        $carrinho = $ofSession 
            ? $this->getCarrinho()
            : $carrinho;

        $subtotal = 0;
        foreach ($carrinho['itens'] as $key => $item) {
            $subtotal += $item['qtd'] * $item['valor'];
        }
        $carrinho['subtotal'] = $subtotal;
        $carrinho['total'] = $subtotal - $carrinho['desconto'] + $carrinho['frete'];

        if ($ofSession) {
            $this->setCarrinho($carrinho);
        } 

        return $carrinho;
    }

}