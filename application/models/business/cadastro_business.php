<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CadastroBusiness
 *
 * @author JOAO PAULO
 */
class Cadastro_Business  extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('dao/cadastro_dao', 'CadastroDao');
        $this->load->model('business/pedido_business', 'PedidoBusiness');
        $this->load->model('business/endereco_business', 'EnderecoDao');
        $this->load->model('business/licenca_dao', 'LicencaDao');
    }

    public function getCadastrosAtivosAll() {
        return $this->CadastroDao->getCadastrosAtivos()->result();
    }

    public function getDadosUsuario($idUsuario) {
        return $this->CadastroDao->getDadosUsuario($idUsuario);
    }

    public function getCadastrosPendentesAll() {
        return $this->CadastroDao->getCadastrosPendentes()->result();
    }

    public function getCadastrosAtivos() {
        $usuarioLogado = $this->session->userdata('usuario');
        if (is_null($usuarioLogado)) {
            redirect('login');
        }
        return $this->CadastroDao->getCadastrosAtivos($usuarioLogado->usu_id)->result();
    }
    
    public function getNumCadastrosAtivos() {
        $usuarioLogado = $this->session->userdata('usuario');
        if (is_null($usuarioLogado)) {
            redirect('login');
        }
        return $this->CadastroDao->getCadastrosAtivos($usuarioLogado->usu_id)->num_rows();
    }
    
    public function getCadastrosPendentes() {
        $usuarioLogado = $this->session->userdata('usuario');
        if (is_null($usuarioLogado)) {
            redirect('login');
        }
        return $this->CadastroDao->getCadastrosPendentes($usuarioLogado->usu_id)->result();
    }
    
    public function getNumCadastrosPendentes() {
        $usuarioLogado = $this->session->userdata('usuario');
        if (is_null($usuarioLogado)) {
            redirect('login');
        }
        return $this->CadastroDao->getCadastrosPendentes($usuarioLogado->usu_id)->num_rows();
    }

    public function salvarCadastro($dados) {
        $dados = (object)$dados;
        
        $idUsuario = $dados->id;
        
        $usuario = $this->CadastroDao->getUsuarioById($idUsuario);
        $idPessoa = !is_null($usuario) ? $usuario->usu_pessoa_id : 0;
            
        $pessoaDados = [
            'pes_id' => $idPessoa
            ,'pes_nome' => $dados->nome
            ,'pes_email' => $dados->email
            ,'pes_rg' => null
            ,'pes_cpf_cnpj' => limparMascara($dados->cpf)
            ,'pes_tipo'=> '1'
        ];
        
        $idPessoa = $this->CadastroDao->salvarPessoa($pessoaDados);

        $usuarioDados = [
            'usu_id' => $idUsuario
            ,'usu_pai_id' => null
            ,'usu_login' => $dados->usuario
            ,'usu_email' => $dados->email
            ,'usu_senha' => md5($dados->senha)
            ,'usu_status' => '1'
            ,'usu_salt' => $dados->nome
            ,'usu_chave_ativacao' => $dados->senha
        ];
        
        if ($idUsuario <= 0 && $usuario == null) {
            $usuarioDados['usu_pessoa_id'] = $idPessoa;
        }

        $idUsuario = $this->CadastroDao->salvarUsuario($usuarioDados);
        
        $usuarioDados['usu_id'] = $idUsuario;
        
        if ($dados->id > 0) {
            $this->CadastroDao->updateTelefone($dados->idTelefoneFixo, limparMascara($dados->telefone), '1');
        } else {
            $this->CadastroDao->addTelefone($idPessoa, limparMascara($dados->telefone), '1');
        }
        
        
        $estado = $this->CadastroDao->getEstadoBySigla($dados->estado);

        $endereco = [
            'end_cep' => limparMascara($dados->cep)
            ,'end_logradouro' => $dados->endereco
            ,'end_numero' => $dados->numero
            ,'end_complemento' => $dados->complemento
            ,'end_bairro' => $dados->bairro
            ,'end_cidade' => $dados->cidade
            ,'end_estado' => $estado != null ? $estado->est_id : '0'
        ];

        if ($dados->id > 0) {
            $this->EnderecoDao->update($dados->idEndereco, $endereco);
            // $this->EnderecoDao->addEnderecoPessoa($idPessoa, $idEndereco);
        } else {
            $idEndereco = $this->EnderecoDao->insert($endereco);
            $this->EnderecoDao->addEnderecoPessoa($idPessoa, $idEndereco);
        }

        if ($dados->id == 0) {
            return [
                "status" => 1,
            ];
            return $ret;
        } else {
            return [
                "status" => 1,
            ];
        }
        // debug("AQui"); die;
        // return $ret;
    }
    
    public function getLicencaVigente($idUsuario) {
        return $this->CadastroDao->getLicencaVigente($idUsuario);
    }
    public function getUltimaAtivacao($idUsuario) {
        return $this->CadastroDao->getUltimaAtivacao($idUsuario);
    }

    public function getByUsuario($usuario) {
        return $this->CadastroDao->getByUsuario($usuario);
    }

    public function checkUsuario($usuario) {
        return $this->CadastroDao->checkUsuario($usuario);
    }
    
    public function checkEmail($email) {
        return $this->CadastroDao->checkEmail($email);
    }
    
    public function checkCpf($cpf) {
        return $this->CadastroDao->checkCpf($cpf);
    }
    
    function getEstados() {
        return $this->CadastroDao->getEstados();
    }
    
    function getBancos() {
        return $this->CadastroDao->getBancos();
    }
    
    function getNumTelefoneByTipo($idPessoaTelefone, $tipo) {
        return $this->CadastroDao->getNumTelefoneByTipo($idPessoaTelefone, $tipo);
    }

    public function salvarDadosBancarios($dados, $idParent) {
        
        $dados = (object)$dados;
        
        $idUsuario = $dados->id;
        
        $usuario = $this->CadastroDao->getUsuarioById($idUsuario);

        $idPessoa = !is_null($usuario) ? $usuario->usu_pessoa_id : 0;
            
        $dadosBancarios = [
            'cba_idpessoa' => $idPessoa
            ,'cba_idbanco' => $dados->banco
            ,'cba_agencia' => $dados->agencia
            ,'cba_conta' => $dados->conta
            ,'cba_titular'=> $dados->titular
            ,'cba_cpf'=> $dados->cpfTitular
            ,'cba_idtipo' => '1'
        ];

        $idConta = $this->CadastroDao->getContaBancoById($idPessoa);

        if ($idConta) {
             $idContaBancaria = $this->CadastroDao->updateDadosBancarios($dadosBancarios, $idConta->cba_id);
        } else {
            $idContaBancaria = $this->CadastroDao->salvarDadosBancarios($dadosBancarios);     
        }

        if ($dados-> id > 0) {
            return [
                "status" => 1,
            ];
        }

        // return FALSE;
    }

    public function getContaBancoById($usuario) {
        return $this->CadastroDao->getContaBancoById($usuario);
    }

    public function cancelarCadastrosComLicencasExpiradas()
    {
        $licencasExpiradas = $this->LicencasDao->getAtivacoesExpiradas();
        foreach ($licencasExpiradas as $key => $item) {
            $this->CadastroDao->cancelarCadastroByIdUsuario($item->ultati_idusuario);
        }
    }
}
