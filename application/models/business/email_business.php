<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Email_Business extends CI_Model{

    public function __construct(){

        parent::__construct();
        $this->load->library('email');
        $this->load->model('dao/pedido_dao','pedido_dao');
        // $this->load->model('dao/pedido_item_dao','pedido_item_dao');
    }

    public function Enviar($nome,$email, $assunto, $mensagem)
    {
        $this->email->from('dan_zoem@hotmail.com', 'Clari Clean');
        $this->email->to($email);
        $this->email->subject($assunto);
        $this->email->message($this->load->view('template_email', array('nome' => $nome, 'mensagem'=>$mensagem), true));
        $this->email->send();
    }

    public function PedidoRealizado($idPedido, $email, $nome)
    {
        $mensagem   = 'Seu Pedido foi Realizado!</p><p>Para mais detalhes do pedido acesse:'.site_url()."pedidos/resumo/".$idPedido;
        $assunto = 'Pedido Realizado #'.str_pad($idPedido,'8','0',STR_PAD_LEFT);
        
        $this->Enviar($nome, $email, $assunto, $mensagem);
    } 

    public function PedidoRecompraRealizado($idPedido, $email, $nome)
    {
        $mensagem   = 'Seu Pedido foi Realizado!</p><p>Para mais detalhes do pedido acesse:'.site_url()."pedidos/resumo/".$idPedido;
        $assunto = 'Pedido Realizado #'.str_pad($idPedido,'8','0',STR_PAD_LEFT);
        
        $this->Enviar($nome, $email, $assunto, $mensagem);
    } 

    // public function AvisoPedido($idPedido)
    // {
    //     $pedido = $this->pedido_dao->getPedidoById($idPedido);
    //     $itens = $this->pedido_item_dao->getItensByIdPedido($idPedido);

    //     if(!is_null($pedido) && $itens->num_rows() > 0){

    //         $mensagem = '<p><h2><b>Um novo pedido foi criado</b></h2></p>';
    //         $mensagem .= '<p><b>Forma de Pagamento: </b>'.$pedido->fpag_nome.'</p>';
    //         $mensagem .= '<p><b>Forma de Entrega: </b>'.$pedido->fent_nome.'</p>';
    //         $mensagem .= '<p><b>Endereço: </b>'.$pedido->end_logradouro.', '.$pedido->end_numero.' - '.$pedido->end_bairro;
    //         $mensagem .= ' '.$pedido->end_cidade.'/'.$pedido->est_sigla.'</p>';

    //         $mensagem .= '<p><h2><b>Itens:</b></h2></p>';

    //         foreach($itens->result() AS $row)
    //         {
    //             $mensagem .= '<p><b>Produto: </b>'.$row->prod_nome.' - '.$row->ser_nome.'<b> Qtde</b>: '.$row->item_quantidade.' <b>Valor:</b> R$'.convertToValorBR($row->item_preco).'</p>';
    //         }

    //         $mensagem .= '</br></br>';
    //         $mensagem .= '<p><b>Valor dos Itens:</b> R$'.convertToValorBR($pedido->pe_valorcompra).'</p>';
    //         $mensagem .= '<p><b>Valor do Frete:</b> R$'.convertToValorBR($pedido->pe_valorfrete).'</p>';
    //         $mensagem .= '<p><b>Valor Total do Pedido: </b>R$'.convertToValorBR($pedido->pe_valorcomprafinal).'</p>';

    //         $mensagem .= '</br></br>';
    //         $mensagem .= '<p><h2>ISSO É APENAS UM ALERTA, AGUARDE A APROVAÇÃO DO PEDIDO</h2></p>';

    //         $assunto = 'Novo Pedido Realizado #'.str_pad($idPedido,'8','0',STR_PAD_LEFT);
    //         $email = 'contato@clariclean.com.br';
    //         $nome = 'Ricardo';
    //         $this->Enviar($nome, $email, $assunto,$mensagem);
    //     }
    // }

    // public function Contato($email, $nome, $assunto, $celular, $mensagem)
    // {
    //     $corpo_email = "<p><b>Novo Contato Site:</b></p>";
    //     $corpo_email .= "<p><b>Nome: </b>".$nome."</p>";
    //     $corpo_email .= "<p><b>Celular: </b>".$celular."</p>";
    //     $corpo_email .= "<p><b>Email: </b>".$email."</p>";
    //     $corpo_email .= "<p><b>Mensagem: </b>".$mensagem."</p>";

    //     $email = 'contato@clariclean.com.br';
    //     $nome = 'Ricardo';

    //     $this->Enviar($nome, $email, 'Contato Site ClariClean.com.br',$corpo_email);

    // }


}
