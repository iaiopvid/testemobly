<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of UsuarioBusiness
 *
 * @author JOAO PAULO
 */
class Endereco_Business extends CI_Model {
    

    public function __construct() {
        parent::__construct();
        $this->load->model('dao/endereco_dao', 'EnderecoDao');
    }
    
    public function getEnderecoByPessoa($idPessoa) {
        return $this->EnderecoDao->getEnderecoByPessoa($idPessoa);
    }
}