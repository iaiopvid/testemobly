<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CadastroBusiness
 *
 * @author JOAO PAULO
 */
class Creditos_Business  extends CI_Model {
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('dao/creditos_dao', 'CreditosDao');
        $this->load->model('dao/licenca_dao', 'LicencaDao');
    }

    public function getGanhosAteHoje($idUsuario) {
        return $this->CreditosDao->getGanhosAteHoje($idUsuario);
    }

    public function getSaldoPontos($idUsuario) {
        return $this->CreditosDao->getSaldoPontos($idUsuario);
    }

    public function getCotacao($idLicenca = null){
        if (is_null($idLicenca)) {
            //TODO: Buscar a Licenca do Usuário logado.
            $logged_user = $this->session->userdata('usuario');
            $idUsuario = $logged_user->usu_id;
            $licenca = $this->LicencaDao->getUltimaAtivacao($idUsuario);

            $licenca = $licenca->ultati_idlicenca; 
        }
        return $this->CreditosDao->getCotacao($idLicenca);
    }

    public function getSaldo($idUsuario) {
        return $this->CreditosDao->getSaldo($idUsuario);
    }

    public function getExtratoById($idUsuario, $condicao = array()) {
        return $this->CreditosDao->getExtratoById(
            $idUsuario, 
            $condicao, 
            $ordenacao = 'sep_data_criacao',
            $tipoOrdem = 'ASC'
        );
    }
     
}
