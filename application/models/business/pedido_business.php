<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pedido_Business extends CI_Model {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('produto_model');
        $this->load->model('dao/pedido_dao', 'PedidoDao');
        $this->load->model('dao/cadastro_dao', 'CadastroDao');
        $this->load->model('dao/creditos_dao', 'CreditosDao');
        $this->load->model('dao/rede_dao', 'RedeDao');
        $this->load->model('dao/licenca_dao', 'LicencaDao');
        $this->load->model('dao/endereco_dao', 'EnderecoDao');
        $this->load->model('business/email_business', 'EmailBusiness');
    }

    public function getPedidoAtivacaoPendente($idUsuario) {
        return $this->PedidoDao->getPedidoAtivacaoPendente($idUsuario);
    }

    public function getAprovadosById($idUsuario)
    {
        return $this->PedidoDao->getAprovadosById($idUsuario)->result();
    }
    
    public function getNumAprovadosById($idUsuario)
    {
        return $this->PedidoDao->getAprovadosById($idUsuario)->num_rows();
    }

    public function getTotalPedidosByUsuario($idUsuario) 
    {
        return $this->PedidoDao->getTotalPedidosByUsuario($idUsuario);
    }

    public function getAll() {
    	return $this->PedidoDao->getAll();
    }

    public function getPedidosAtivacao(){
        return $this->PedidoDao->getPedidosAtivacao();
    }

    public function gerarPedidoRecompra($idUsuario, $carrinho, $endereco_pedido) {

        $estado = $this->CadastroDao->getEstadoBySigla($endereco_pedido['estado']);

        $endereco = [
            'end_cep' => limparMascara($endereco_pedido['cep'])
            ,'end_logradouro' => $endereco_pedido['endereco']
            ,'end_numero' => $endereco_pedido['numero']
            ,'end_complemento' => $endereco_pedido['complemento']
            ,'end_bairro' => $endereco_pedido['bairro']
            ,'end_cidade' => $endereco_pedido['cidade']
            ,'end_estado' => $estado != null ? $estado->est_id : '0'
        ];
        $idEndereco = $this->EnderecoDao->insert($endereco);
        $subTotal = 0;

        foreach ($carrinho['itens'] as $key => $item) {

            $valor = $item['valor'];
            if (is_null($valor)) {
                return [
                            "status" => 7001,
                            "descricao" => "Produto do carrinho não encontrado",
                            "mensagem" => ""
                        ];
            }

            $items[] = [
                'pedite_idproduto' => $item['id'],
                'pedite_quantidade' => $item['qtd'],
                'pedite_valor_unitario' => $valor
            ];
        }

        $subTotal = $carrinho['subtotal'];

        $pedido = [
            'ped_tipo_bonus' => '3',
            'ped_idusu' => $idUsuario,
            'ped_subtotal' => $subTotal,
            'ped_desconto' => convertToValorDb($carrinho['desconto']),
            'ped_frete' => convertToValorDb($carrinho['frete']),
            'ped_total' => $carrinho['total']           
        ];

        $idPedido = $this->PedidoDao->insertPedido($pedido);
        
        foreach ($items as $key => $item) {
            $this->PedidoDao->addItemPedido($item, $idPedido);
        }

        $idEndereco = $this->EnderecoDao->insert($endereco);
        $this->EnderecoDao->addEnderecoPedido($idPedido, $idEndereco);

        if ($carrinho['desconto'] >= $carrinho['subtotal']) {
            return [
                "status" => 2,
                "idPedido" => $idPedido,
                'idUsuario' => $idUsuario

            ];
        }
        return [
            "status" => 1,
            "idPedido" => $idPedido,
            'idUsuario' => $idUsuario

        ];

    }

    public function gerarPedidoAdesao($usuario, $idLicenca, $endereco) {
       return $this->gerarPedidoAdesaoAtivacao($usuario, $idLicenca, $endereco, 1);        
    }

    public function gerarPedidoAtivacao($usuario, $idLicenca, $endereco) {
       return $this->gerarPedidoAdesaoAtivacao($usuario, $idLicenca, $endereco, 2); 
    }

    /**
     * $adesaoAtivacao = 1 para Adesão
     * $adesaoAtivacao = 2 para Ativação
     */
    private function gerarPedidoAdesaoAtivacao($usuario, $idLicenca, $endereco, $adesaoAtivacao) {
        $idUsuario = $usuario['usu_id'];

        $produtoLicenca = $this->LicencaDao->getProdutoByLicenca($idLicenca);
        
        if (is_null($produtoLicenca)) {
            return [
                        "status" => 9005,
                        "descricao" => "Produto da Licença não encontrado",
                        "mensagem" => 
                        "   O produto que deveria ser vinculado com a licença não foi encontrado. 
                            Verificar na tabela produtl_licenca se existe um registro que vincula o produto com a licença."
                    ];
        }

        $pedido = [
            'ped_idusu' => $idUsuario,
            'ped_subtotal' => $produtoLicenca->lic_valor,
            'ped_desconto' => '0.00',
            'ped_total' => $produtoLicenca->lic_valor,
            'ped_data_criacao' => date('Y-m-d : H:i:s'),
            'ped_data_alteracao' => date('Y-m-d : H:i:s'),
            'ped_data_vencimento' => date('Y-m-d H:i:s', strtotime('+1 months')),
            'ped_tipo_bonus' => $adesaoAtivacao
        ];
        $idPedido = $this->PedidoDao->insertPedido($pedido);

        $item = [
            'pedite_idpedido' => $idPedido,
            'pedite_idproduto' => $produtoLicenca->prod_id,
            'pedite_quantidade' => '1',
            'pedite_valor_unitario' => $produtoLicenca->lic_valor
        ];

        $idItem = $this->PedidoDao->insertItem($item);

        $idEndereco = $this->EnderecoDao->insert($endereco);
        $this->EnderecoDao->addEnderecoPedido($idPedido, $idEndereco);

        // $dataAtivacao = [
        //     'ultati_idusuario' => $idUsuario,
        //     'ultati_idlicenca' => $idLicenca,
        //     'ultati_data' => date('Y-m-d : H:i:s'),    
        // ];
        // $idItem = $this->PedidoDao->insertUltimaAtivacao($dataAtivacao);

        $this->EmailBusiness->PedidoRealizado($idPedido, $usuario['usu_email'], $usuario['usu_login']);

        return [
            "status" => 1,
            "idPedido" => $idPedido,
            'idUsuario' => $idUsuario

        ];
    }

    private function getEndereco($idPedido) {
        return $this->PedidoDao->getEndereco($idPedido);
    }

    public function getById($idPedido) {
        return $this->PedidoDao->getById($idPedido);
    }

    public function aprovar($idPedido) {

    	$pedido = $this->PedidoDao->getById($idPedido);

    	if (is_null($pedido)) {
    		return [
                        "status" => 9001,
                        "descricao" => "Pedido não encontrado",
                        "mensagem" => 
                        "   Não foi encontradado nenhum pedido com o ID $idPedido na tabela pedidos."
                    ];
    	} else if($pedido->ped_status == '99') {
            return [
                        "status" => 9002,
                        "descricao" => "Pedido cancelado",
                        "mensagem" => 
                        "   O pedido $idPedido já está cancelado. O campo ped_status já está com o valor 99"
                    ];
    	} else if($pedido->ped_status <> '0') {
            return [
                        "status" => 9003,
                        "descricao" => "Pedido aprovado",
                        "mensagem" => 
                        "   O pedido $idPedido já está aprovado. O campo ped_status já está com o valor maior que 0 e diferente de 99"
                    ];
    	} 


    	$tipo = $pedido->ped_tipo_bonus;
    	switch ($tipo) {
            case '1':
                return $this->aprovarAdesaoAtivacao($pedido);//Adesão
                break;
            case '2':
                return $this->aprovarAdesaoAtivacao($pedido);//Ativacao
                break;
            case '3':
                return $this->aprovarRecompra($pedido);
                break;
    	}
    }

    public function cancelar($idPedido) {

        $pedido = $this->PedidoDao->getById($idPedido);

        if (is_null($pedido)) {
            return [
                        "status" => 9001,
                        "descricao" => "Pedido não encontrado",
                        "mensagem" => 
                        "   Não foi encontradado nenhum pedido com o ID $idPedido na tabela pedidos."
                    ];
        } else if($pedido->ped_status == '99') {
            return [
                        "status" => 9002,
                        "descricao" => "Pedido cancelado",
                        "mensagem" => 
                        "   O pedido $idPedido já está cancelado. O campo ped_status já está com o valor 99"
                    ];
        } else if($pedido->ped_status <> '0') {
            return [
                        "status" => 9003,
                        "descricao" => "Pedido aprovado",
                        "mensagem" => 
                        "   O pedido $idPedido já está aprovado. O campo ped_status já está com o valor maior que 0 e diferente de 99"
                    ];
        } 


        $tipo = $pedido->ped_tipo_bonus;
        switch ($tipo) {
            case '1':
                return $this->cancelarAdesaoAtivacao($pedido);//Adesão
                break;
            case '2':
                return $this->cancelarAdesaoAtivacao($pedido);//Ativacao
                break;
            case '3':
                return $this->cancelarRecompra($pedido);
                break;
        }
    }

    private function cancelarRecompra($pedido) {
        $itens = $this->PedidoDao->getItens($pedido->ped_id);
        $count = count($itens);
        
        if ($count <= 0) {
            return [
                        "status" => 9007,
                        "descricao" => "Pedido sem itens",
                        "mensagem" => 
                        "   Não foi encontrado nenhum item na tabela pedidos_item vinculado ao pedido $pedido->ped_id"
                    ];
        }

        $ret = $this->PedidoDao->cancelar($pedido->ped_id);
        
        if ($ret == 8001) {
            return [
                        "status" => 8001,
                        "descricao" => "Licença expirada a mais de 2 meses.",
                        "mensagem" => 
                        "   Quando a licença expira a mais de 2 meses, não é possível aprovar o pedido de licença."
                    ];
        }

        return [
            "status" => 1,
            "idPedido" => $pedido->ped_id,
            'idUsuario' => $pedido->ped_idusu

        ];

        //fazer chamada de bonus de recompra.
    }

    private function aprovarRecompra($pedido) {
        $itens = $this->PedidoDao->getItens($pedido->ped_id);

        $count = count($itens);
        
        if ($count <= 0) {
            return [
                        "status" => 9007,
                        "descricao" => "Pedido sem itens",
                        "mensagem" => 
                        "   Não foi encontrado nenhum item na tabela pedidos_item vinculado ao pedido $pedido->ped_id"
                    ];
        }

        $ret = $this->PedidoDao->aprovar($pedido->ped_id);
        if ($ret == 8001) {
            return [
                        "status" => 8001,
                        "descricao" => "Licença expirada a mais de 2 meses.",
                        "mensagem" => 
                        "   Quando a licença expira a mais de 2 meses, não é possível aprovar o pedido de licença."
                    ];
        }

        return [
            "status" => 1,
            "idPedido" => $pedido->ped_id,
            'idUsuario' => $pedido->ped_idusu

        ];

        //fazer chamada de bonus de recompra.
    }

    private function aprovarAdesaoAtivacao($pedido) 
    {
        $itens = $this->PedidoDao->getItens($pedido->ped_id);
        $count = count($itens);
        
        if ($count <= 0) {
            return [
                        "status" => 9007,
                        "descricao" => "Pedido sem itens",
                        "mensagem" => 
                        "   Não foi encontrado nenhum item na tabela pedidos_item vinculado ao pedido $pedido->ped_id"
                    ];
        }

        if ($count > 1) {
            return [
                        "status" => 9008,
                        "descricao" => "Pedido de Ativação com mais de um Item ",
                        "mensagem" => 
                        "   O pedido de ativação deve possuir apenas um item na tabela pedidos_item, foi encontrado mais de um item para o pedido $pedido->ped_id"
                    ];
        }

        $item = $itens[0];

        $licenca = $this->PedidoDao->getLicencaDoItem($item->pedite_id);

        if (is_null($licenca)) {
            return [
                        "status" => 9009,
                        "descricao" => "Pedido de Ativação sem licença vinculada ao item.",
                        "mensagem" => 
                        "   Não foi encontrada nenhuma licença vinculada ao produto do item."
                    ];
        }

    	$this->PedidoDao->aprovar($pedido->ped_id);

    	$this->CadastroDao->aprovarCadastro($pedido->ped_idusu);

        $parent = $this->CadastroDao->getByIdUsuario($pedido->ped_idusu);
        $parent = $parent->usu_pai_id;

    	$retProc = $this->RedeDao->adicionarUnilevel($pedido->ped_idusu, $parent);
        
        $retLicenca = $this->LicencaDao->addAtivacao($pedido->ped_idusu, $licenca->lic_id, $pedido->ped_id, $licenca->lic_valor);
        // debug($retLicenca); die;

        if ($retLicenca > 1) {
            return $retLicenca; 
        }

        return [
                 "status" => 1,
               ];

    }	

    public function getPedidos($condicao = array()) {
        return $this->PedidoDao->getPedidos($condicao);
    }

    public function getPedidoCompleto($idPedido, $pedidoTipo) {
        return $this->PedidoDao->getPedidoCompleto($idPedido, $pedidoTipo);
    }

    public function getDadosDoPedido($idPedido = NULL) {
        return $this->PedidoDao->getDadosDoPedido($idPedido);
    }
}