<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProdutoDepartamento_model extends CI_Model {

	public function get($condicao = array()) {
		$this->db->select('dp.pdep_prod_id, dp.pdep_pdep_id');
		$this->db->where($condicao);
		$this->db->from('produto_departamento dp');
		return $this->db->get()->result();
	}

	public function getDepartamentoPaiProduto($prod_id) {
		$this->db->select('dp.pdep_prod_id, dp.pdep_pdep_id');
		$this->db->from('produto_departamento dp');
		$this->db->from('departamento d');
		$this->db->where("dp.pdep_prod_id", $prod_id, FALSE);
		$this->db->where("d.dep_id", "dp.pdep_pdep_id", FALSE);
		$this->db->where("d.dep_departamentopai IS NULL", NULL, FALSE);
		return $this->db->get()->result();
	}

	public function post($itens) {
		$res = $this->db->insert('produto_departamento', $itens);
	}

	public function update($itens, $pdep_prod_id) {
		$this->db->where('pdep_prod_id', $pdep_prod_id);
		$res = $this->db->update('produto_departamento', $itens);

		if ($res) {
			return $pdep_prod_id;
		} else {
			return FALSE;
		}
		
	}

	public function delete($pdep_prod_id) {
		$this->db->where('pdep_prod_id', $pdep_prod_id, FALSE);
		return $this->db->delete('produto_departamento');
	}


}