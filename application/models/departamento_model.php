<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Departamento_model extends CI_Model {

	public function getTotal($condicao = array()) {
		$this->db->where($condicao);
		$this->db->from('departamento');
		return $this->db->count_all_results();
	}


	public function getDepartamentosPais($condicao = array(), $primeiraLinha = FALSE, $pagina = 0, $limite = LINHAS_PESQUISA_DASHBOARD) {
		$this->db->select('dep_id, dep_nome');
		$this->db->where($condicao);
		$this->db->from('departamento');

		return $this->db->get()->result();

		// if ($primeiraLinha) {
		// 	return $this->db->get()->first_row();
		// } else {
		// 	if ($limite !== FALSE) {
		// 		$this->db->limit($limite, $pagina);
		// 	}
		// 	return $this->db->get()->result();
		// }
	}

	 public function getDepartamentosFilhos($dep_departamentopai = FALSE) {
	 	$this->db->distinct();
	 	$this->db->select('df.dep_id, df.dep_nome, df.dep_departamentopai, depfot_id, depfot_extensao');
	 	$this->db->from('departamento dp');
	 	$this->db->from('departamento df');
	 	$this->db->from('departamento_foto');
	 	$this->db->where('dp.dep_id', 'df.dep_departamentopai', FALSE);
	 	$this->db->where('df.dep_id', 'depfot_iddepartamento', FALSE);
	 	
		if ($dep_departamentopai) {
			$this->db->where("df.dep_departamentopai", $dep_departamentopai, FALSE);
		} else {
			$this->db->where("df.dep_departamentopai IS NULL", NULL, FALSE);
		}
		
	 	$this->db->order_by("dp.dep_id, df.dep_nome", "ASC");
	 	
	 	return $this->db->get()->result();
	 }

	 public function getDepartamentosDisponiveis($dep_departamentopai = FALSE) {
	 	$this->db->distinct();
	 	$this->db->select('d.dep_id, d.dep_nome, d.dep_departamentopai');
	 	$this->db->from('departamento d');
	 	$this->db->from('produto_departamento pd');
	 	$this->db->where('pd.pdep_pdep_id', 'd.dep_id', FALSE);
	 	$this->db->where('total_produto(pd.pdep_prod_id) >', 0, FALSE);
	 	
		if ($dep_departamentopai) {
			$this->db->where("d.dep_departamentopai", $dep_departamentopai, FALSE);
		} else {
			$this->db->where("d.dep_departamentopai IS NULL", NULL, FALSE);
		}
		
	 	$this->db->order_by("d.dep_nome", "ASC");
	 	
	 	return $this->db->get()->result();
	 }

	public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0, $limite = LINHAS_PESQUISA_DASHBOARD) {
		$this->db->select('d.dep_id, d.dep_nome, d.dep_departamentopai');
		$this->db->select('p.dep_nome AS nomepai');
		$this->db->select('depfot_id, depfot_extensao');
		$this->db->where($condicao);
		$this->db->from('departamento d');
		$this->db->join('departamento p', 'p.dep_id = d.dep_departamentopai', 'LEFT');
		$this->db->join('departamento_foto', 'd.dep_id = depfot_iddepartamento', 'LEFT');

		if ($primeiraLinha) {
			return $this->db->get()->first_row();
		} else {
			if ($limite !== FALSE) {
				$this->db->limit($limite, $pagina);
			}
			return $this->db->get()->result();
		}
	}

	public function post($itens) {
		$res = $this->db->insert('departamento', $itens);

		if ($res) {
			return $this->db->insert_id();
		} else {
			return FALSE;
		}

	}

	public function update($itens, $dep_id) {
		$this->db->where('dep_id', $dep_id);
		$res = $this->db->update('departamento', $itens);

		if ($res) {
			return $dep_id;
		} else {
			return FALSE;
		}
		
	}

	public function delete($dep_id) {
		$this->db->where('dep_id', $dep_id, FALSE);
		return $this->db->delete('departamento');
	}


}