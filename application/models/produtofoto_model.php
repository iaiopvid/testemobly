<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProdutoFoto_Model extends CI_Model {
  public function get($condicao = array(), $primeiraLinha = FALSE, $pagina = 0, $limite = LINHAS_PESQUISA_DASHBOARD) {
      $this->db->select ( 'p.profot_id, p.profot_extensao, p.profot_idproduto' );
      $this->db->where ( $condicao );
      $this->db->from ( 'produto_foto p' );
      
//      return $this->db->get()->result();
      
      if ($primeiraLinha) {
          return $this->db->get ()->first_row ();
      } else {
          if ($limite !== FALSE) {
              $this->db->limit ( $limite, $pagina );
          }
          return $this->db->get ()->result ();
      }
  }

	public function post($itens) {
		$res = $this->db->insert('produto_foto', $itens);
		if ($res) {
			return $this->db->insert_id();
		} else {
			return FALSE;
		}
	}

	public function update($itens, $profot_id) {
		$this->db->where('profot_id', $profot_id);
		$res = $this->db->update('produto_foto', $itens);

		if ($res) {
			return $profot_id;
		} else {
			return FALSE;
		}
		
	}

	public function delete($idfoto, $idproduto) {
		$this->db->where('profot_idproduto', $idproduto, FALSE);
		$this->db->where('profot_id', $idfoto, FALSE);
		return $this->db->delete('produto_foto');
	}
}