<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['ambiente'] = 'sandbox';
$config['sandbox']['email']                            = 'contato.dayd@gmail.com';
$config['sandbox']['token']                            = '18D6F683028B4B9FAE06909CB9971550';
$config['sandbox']['url_pagseguro']                    = 'https://sandbox.pagseguro.uol.com.br';
$config['sandbox']['url_ws_pagseguro']                 = 'https://ws.sandbox.pagseguro.uol.com.br';
$config['sandbox']['url_stc_pagseguro']                = 'https://stc.sandbox.pagseguro.uol.com.br';
$config['sandbox']['url_controller']                   = 'http://clariclean.com.br/carrinho/informacao';

$config['producao']['email']                            = 'santorica@ig.com.br';
$config['producao']['token']                            = '9D1997784AFD4C168ED98EC4438720FC';
$config['producao']['url_pagseguro']                    = 'https://pagseguro.uol.com.br';
$config['producao']['url_ws_pagseguro']                 = 'https://ws.pagseguro.uol.com.br';
$config['producao']['url_stc_pagseguro']                = 'https://stc.pagseguro.uol.com.br';
$config['producao']['url_controller']                   = 'http://clariclean.com.br/carrinho/informacao';