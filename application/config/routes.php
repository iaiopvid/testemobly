<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//$route['boleto/(:any)-(:any)-(:any)'] = 'boleto/index/$1-$2-$3';

//$route['escritorio/relatorios/binario/detalhar/([1-9]|1[012])/([12][0-9]{3})/(:num)/(:any)'] = 'escritorio/relatorios/binariodetalhar/$1/$2/$3';

$route['translate_uri_dashes'] = FALSE;


$route['default_controller'] = "ecommerce";

$route['ativacao'] = 'ativacao/ativacao';
$route['cronjob/(:any)'] = "cronjob/$1";

$route['produto/(:num)/(:any)'] =  'produto/getFicha/$1/$2';
$route['produto/(:num)'] =  'produto/getFicha/$1';

$route['checkout'] = 'checkout';
$route['ecommerce'] = '';
$route['checkout/finalizar_checkout'] = 'checkout/finalizar_checkout';
$route['busca'] = 'busca';

$route['pedido/pedidoDetalhes/(:num)'] = 'pedido/pedidoDetalhes/$1';
$route['pedido/getItemHtml/(:num)'] = 'pedido/getItemHtml/$1';
$route['pedido/imprimirPedido/(:num)'] = 'pedido/imprimirPedido/$1';
$route['pedido/getItemHtml/(:num)/admin'] = 'pedido/getItemHtml/$1/true';


$route['carrinho'] = 'carrinho';
$route['carrinho/index'] = 'carrinho/index';
$route['carrinho/adicionar'] = 'carrinho/adicionar';
$route['carrinho/removItem/(:num)'] = 'carrinho/removeItem/$1';

$route['login'] = 'login';
$route['logout'] = 'login/logout';
$route['login/logar'] = 'login/logar';
$route['login_ecommerce'] = 'login/ecommerce';
$route['login_ecommerce/(:num)'] = 'login/ecommerce/$1';
$route['logout/(:num)'] = 'login/logout/$1';


$route['loginAdmin'] = 'loginAdmin';
$route['logoutAdmin'] = 'loginAdmin/logoutAdmin';
$route['login/logarAdmin'] = 'loginAdmin/logarAdmin';

// CADASTROS
$route['admin/cadastro/listarativos'] = 'admin/cadastro/listarativos';
$route['admin/cadastro/listarpendentes'] = 'admin/cadastro/listarpendentes';

// PEDIDOS
$route['admin/pedido/aprovar/(:num)'] = 'admin/pedido/aprovar/$1';
$route['admin/pedido/cancelar/(:num)'] = 'admin/pedido/cancelar/$1';

// FINANCEIRO
$route['admin/financeiro/saldosapagar'] = 'admin/financeiro/saldosapagar';
$route['admin/financeiro/saldospagos'] = 'admin/financeiro/saldospagos';
$route['admin/financeiro/pagarsaldo/(:num)/(:num)'] = 'admin/financeiro/pagarsaldo/$1/$2';

$route['admin'] = 'admin/dashboard';
$route['admin/pedido'] = 'admin/dashboard';
$route['admin/pedidodetalhes/(:num)'] = 'admin/dashboard/pedidodetalhes/$1';
$route['admin/dashboard/getItemHtml/(:num)'] = 'admin/dashboard/getItemHtml/$1';
$route['admin/dashboard/imprimirPedido/(:num)'] = 'admin/dashboard/imprimirPedido/$1'; 
$route['checkout/remove/(:num)'] = 'checkout/remove/$1'; 
$route['departamento/(:num)'] = 'departamento/$1';


// PRODUTO
$route['admin/produto'] = 'admin/produto';
$route['admin/produto/excluir/(:num)'] = 'admin/produto/excluir/$1'; 
$route['admin/produto/editar/(:num)'] = 'admin/produto/editar/$1'; 
$route['admin/produto/salvar'] = 'admin/produto/salvar';
$route['admin/produto/adicionar'] = 'admin/produto/adicionar'; 

$route['admin/departamento'] = 'admin/departamento';
$route['admin/departamento/excluir/(:num)'] = 'admin/departamento/excluir/$1'; 
$route['admin/departamento/editar/(:num)'] = 'admin/departamento/editar/$1'; 
$route['admin/departamento/salvar'] = 'admin/departamento/salvar';
$route['admin/departamento/adicionar'] = 'admin/departamento/adicionar'; 

$route['admin/bannertitulo'] = 'admin/bannertitulo';
$route['admin/bannertitulo/excluir/(:num)'] = 'admin/bannertitulo/excluir/$1'; 
$route['admin/bannertitulo/editar/(:num)'] = 'admin/bannertitulo/editar/$1'; 
$route['admin/bannertitulo/salvar'] = 'admin/bannertitulo/salvar';
$route['admin/bannertitulo/adicionar'] = 'admin/bannertitulo/adicionar'; 

$route['admin/imagemdestaque'] = 'admin/imagemdestaque';
$route['admin/imagemdestaque/excluir/(:num)'] = 'admin/imagemdestaque/excluir/$1'; 
$route['admin/imagemdestaque/editar/(:num)'] = 'admin/imagemdestaque/editar/$1'; 
$route['admin/imagemdestaque/salvar'] = 'admin/imagemdestaque/salvar';
$route['admin/imagemdestaque/adicionar'] = 'admin/imagemdestaque/adicionar'; 

$route['backoffice'] = 'backoffice/dashboard/index';
$route['backoffice/login'] = 'backoffice/login';


$route['backoffice/pedidos/lista'] = 'backoffice/pedidos/lista';
$route['backoffice/cadastro/listarAtivos'] = 'backoffice/cadastro/listarAtivos';
$route['backoffice/dashboard'] = 'backoffice/dashboard/index';
$route['backoffice/cadastro/novo'] = 'backoffice/cadastro/editar';
$route['backoffice/cadastro/salvar'] = 'backoffice/cadastro/salvar';
$route['backoffice/cadastro/editarDadosPessoais'] = 'backoffice/cadastro/editarDadosPessoais';
$route['backoffice/cadastro/listarAtivos'] = 'backoffice/cadastro/listarAtivos';
$route['backoffice/cadastro/listarPendentes'] = 'backoffice/cadastro/listarPendentes';
$route['inspinia'] = 'inspinia/index';

$route['backoffice/rede/unilevel'] = 'backoffice/rede/unilevel';
$route['backoffice/rede/unilevel/(:num)'] = 'backoffice/rede/unilevel/$1';

// TOKEN
$route['backoffice/token/gerar'] = 'backoffice/token/gerar';
// $route['backoffice/rede/unilevel/(:num)'] = 'backoffice/rede/unilevel/$1';

$route['backoffice/financeiro/extrato'] = 'backoffice/financeiro/extrato';
// $route['backoffice/financeiro/extrato/(:num)'] = 'backoffice/financeiro/extrato/$1';

$route['backoffice/financeiro/creditos'] = 'backoffice/financeiro/creditos';
// $route['backoffice/financeiro/creditos/(:num)'] = 'backoffice/financeiro/creditos/$1';
$route['backoffice/cadastro/ativacao'] = 'backoffice/cadastro/ativacao';

// #########################################################################3

$route['payment/(:num)'] = 'payment/index/$1';
$route['payment/method/boleto'] = 'payment/method/boleto';	
$route['payment/boleto'] = 'payment/boleto';	
$route['payment/confirmation'] = 'payment/confirmation';	
$route['checkout/adicionar'] = 'checkout/adicionar';

$route['admin/(:any)'] = 'admin/$1';
$route['backoffice/(:any)'] = 'backoffice/$1';
$route['cadastro'] = 'cadastro/editar/$1';
$route['cadastro/ativacao'] = 'cadastro/ativacao';


$route['404_override'] = '';

//
//$route['(:any)/(:any)/(:any)'] = '$1/$2/$3';
//$route['(:any)/(:any)'] = '$1/$2';

// $route['(:any)'] = 'home/index/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
