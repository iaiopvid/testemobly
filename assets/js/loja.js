jQuery(document).ready(function () {

//    $('body').click(itemClick); 

    jQuery('.set-integer').change(function (event) {
        campoInteiro(event);
    });

    jQuery('.set-integer').keydown(function (event) {
        campoInteiro(event);
    });

    jQuery('.set-formapagamento').change(function () {
        if (jQuery(this).attr('data-tipo') != "2") {
            jQuery('.info-cartao-credito').addClass("hide");
        } else {
            jQuery('.info-cartao-credito').removeClass("hide");
        }
    });

    jQuery('.busca-cep').focusout(function () {
        var cep = jQuery(this).val(),
                chaveDevMedia = '11VAZ1T0IM';
        jQuery.ajax({
            url: "http://www.devmedia.com.br/devware/cep/service/" + cep,
            dataType: 'json',
            type: 'get'
        }).done(function (data) {
            jQuery('#cli_uf').val(data.uf);
            jQuery('#cli_cidade').val(data.cidade);
            jQuery('#cli_endereco').val(data.logradouro);
        });
    });

});

function campoInteiro(event) {
    if (event.keyCode === 46 || event.keyCode === 8 || event.keyCode === 9 || event.keyCode === 27 || event.keyCode === 13 ||
            (event.keyCode === 64 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)
            ) {
        return;
    } else {
        if (event.shiftKey || ((event.keyCode < 48 || event.keyCode > 57)
                && (event.keyCode < 96 || event.keyCode > 105))) {
            event.preventDefault();
        }
    }
}

function mostraValorServico(TIPOSERVICO) {

}

function itemClick() {
    var modal = $('#modalItem');
    var modalContent = $('#modalItem #modal-content');
    var url = base_url + 'ecommerce/getItemHtml';
    $.get(url, function (response) {
        modalContent.html(response);
        modal.modal();
    });
}

function Mudarestado(el) {
    var display = document.getElementById(el).style.display = 'block';
}
	