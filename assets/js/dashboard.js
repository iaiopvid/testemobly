$(document).ready(function () {

  $('.set-departamento-pai').click(function () {
    var checado = false;
    
    if ($(this).is(':checked')) {
      checado = true;
    }
    
    $(".set-departamento-filho[data-pai='" + $(this).val() + "']").prop('checked', checado);
  });
  
  $('.set-departamento-filho').click(function () {
    
    if ($(this).is(':checked')) {     
      $('#departamento-' + $(this).attr('data-pai')).prop('checked', true);
    }
  });
});