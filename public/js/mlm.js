$(function(){
    configMasks();
    configCepAutoComplete();
   
});

function configMasks() {
    
//            $.mask.definitions['~'] = "[+-]";
    // $(".telefoneMask").mask("(99) 9999-9999");
    // $('.celularMask').mask("(99) 9999-9999?9");
    // $('.celularMask').focusout(function(){
    //     var phone, element;
    //     element = $(this);
    //     element.unmask();
    //     phone = element.val().replace(/\D/g, '');
    //     if(phone.length > 10) {
    //         element.mask("(99) 99999-999?9");
    //     } else {
    //         element.mask("(99) 9999-9999?9");
    //     }
    // }).trigger('focusout');
    // $(".cepMask").mask("99999-999");
    // $(".cpfMask").mask("999.999.999-99");
    
//            $("#product").mask("a*-999-a999", { placeholder: " " });
//            $("#eyescript").mask("~9.99 ~9.99 999");
//            $("#po").mask("PO: aaa-999-***");
//            $("#pct").mask("99%");

    // $("input").blur(function() {
    //     $("#info").html("Unmasked value: " + $(this).mask());
    // }).dblclick(function() {
    //     $(this).unmask();
    // });
}

function limpa_formulário_cep() {
    // Limpa valores do formulário de cep.
    $("#endereco").val("");
    $("#bairro").val("");
    $("#cidade").val("");
    $("#estado").val("");
//    $("#ibge").val("");
}

function configCepAutoComplete() {
    //Quando o campo cep perde o foco.
    $(".cepAutoComplete").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep !== "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#endereco").val("...")
                $("#bairro").val("...")
                $("#cidade").val("...")
                $("#estado").val("...")
    //            $("#ibge").val("...")

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#endereco").val(dados.logradouro);
                        $("#bairro").val(dados.bairro);
                        $("#cidade").val(dados.localidade);
                        $("#estado").val(dados.uf);
                        $("#numero").focus();
    //                    $("#ibge").val(dados.ibge);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        limpa_formulário_cep();
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                limpa_formulário_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulário_cep();
        }  
    });
}

function checkCpf(cpf) {  
    cpf = cpf.replace(/[^\d]+/g,'');    
    if(cpf == '') return false; 
    // Elimina CPFs invalidos conhecidos    
    if (cpf.length != 11 || 
        cpf == "00000000000" || 
        cpf == "11111111111" || 
        cpf == "22222222222" || 
        cpf == "33333333333" || 
        cpf == "44444444444" || 
        cpf == "55555555555" || 
        cpf == "66666666666" || 
        cpf == "77777777777" || 
        cpf == "88888888888" || 
        cpf == "99999999999")
            return false;       
    // Valida 1o digito 
    add = 0;    
    for (i=0; i < 9; i ++)       
        add += parseInt(cpf.charAt(i)) * (10 - i);  
        rev = 11 - (add % 11);  
        if (rev == 10 || rev == 11)     
            rev = 0;    
        if (rev != parseInt(cpf.charAt(9)))     
            return false;       
    // Valida 2o digito 
    add = 0;    
    for (i = 0; i < 10; i ++)        
        add += parseInt(cpf.charAt(i)) * (11 - i);  
    rev = 11 - (add % 11);  
    if (rev == 10 || rev == 11) 
        rev = 0;    
    if (rev != parseInt(cpf.charAt(10)))
        return false;       
    return true;
}


function checkSenha(str)
{
    var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/;
    return re.test(str);
}