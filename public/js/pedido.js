$(function() {
	$('.pedidoItem').click(itemClick);

	$('.link-aprovar').click(function() {
		console.log('Aprovou');
		return confirm('Você confirma a aprovação do deste pedido?');
	});

	$('.link-excluir').click(function() {
		console.log('Cancelou');
		return confirm('Deseja realmente Cancelar?')
	});

});
	
function itemClick() {
	var modal = $('#modalItem');
	var modalContent = $('#modalItem #modal-content');
	var idPedido = this.getAttribute('targetId');
	var url = base_url+'pedido/getItemHtml/'+idPedido;
	$.get(url, function(response){
		modalContent.html(response);
		modal.modal();
	});
}