$(function() {
	$('.pedidoItem').click(itemClick);

});
	
function itemClick() {
	var modal = $('#modalItem');
	var modalContent = $('#modalItem #modal-content');
	var idPedido = this.getAttribute('targetId');
	var url = base_url+'ecommerce/getItemHtml';
	$.get(url, function(response){
		modalContent.html(response);
		modal.modal();
	});
}