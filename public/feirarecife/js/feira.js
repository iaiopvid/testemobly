jQuery(document).ready(function() {

	jQuery("#usa-credito").on('click', function() 
	{
		debugger;
		// var credito = jQuery('#coupon_code').val();
		// var vmax = jQuery('#coupon_code').attr('max');
		// var sub = moneyTextToFloat(jQuery("#valor-total-pedido").text());

		// if (credito <= 0) {
		// 	alert('Nâo é possível aplicar um desconto menor ou igual a zero "0"!');
		// }
		// else if (credito > vmax) {
		// 	alert('Seu crédito disponível é insuficiente!');
		// } 
		// else if(credito > sub) {
		// 	alert('Não é permitir usar um crédito superior ao valor total da compra!');
		// } else {
		// 	var subtotal = moneyTextToFloat(jQuery("#valor-total-pedido").text());
			
		// 	var total = subtotal-credito;
		
		// 	jQuery("#desconto-pedido").text(floatToMoneyText(credito));
		// 	jQuery("#subtotal").text(floatToMoneyText(total));
		// }

	});

	jQuery(".qty").change(function() 
	{
		var max = this.getAttribute('max');
		var min = this.getAttribute('min');

		var prod_id = this.getAttribute("targetid");
		var quantity = this.value;
		var total = (quantity*1);
		if (total > max) {
			jQuery(this).val(max);
			return;
		}
		if (total > min) {
			jQuery(this).val(min);
			return;
		}
		jQuery("#quantity-"+prod_id).text(total);

		postCarrinho(prod_id, quantity);

		this.getAttribute("value", total);
		writeTotal(atualizaCarrinho());
		// jQuery('.conteudo-carrinho').remove();
		// getCarrinho();
	});

	atualizarCarrinho();
});

function atualizaItem(prod_id, qty) {
	console.log(prod_id + ' ' + qty.value);
	var url = 'carrinho/atualizaItem/' + prod_id + '/' +qty.value;
	jQuery.post(url, function() {
		atualizarCarrinho();
		atualizaCarrinho();
	});
	return;
};

var base_url = window.location.origin;

function aplicarCredito() {
	var valor = jQuery('#coupon_code').val();
	var url = 'carrinho/atualizarCredito';
    jQuery.get(url, function(data) {
		if (data) {
			atualizarCarrinho();
			if (data == '2') {
				// alert('O Valor do Boleto não pode ser menor que ')
			}
		}
    });
}

function atualizarCarrinho() {
	var url = base_url + 'carrinho/getCarrinhoFormatado';
    jQuery.get(url, function(data) {
      	jQuery('#content').html('');
      	jQuery('#content').html(data);
    });
	var urlMini = base_url + 'carrinho/getCarrinhoMiniFormatado';
    jQuery.get(urlMini, function(response) {
      	jQuery('#conteudo-carrinho').html('');
      	jQuery('#conteudo-carrinho').html(response);
    });
}

function postCarrinho(prod_id, prod_qty) {
	var url = 'carrinho/changeCarrinho';
    jQuery.post(url, {'prod_id': prod_id, 'prod_qty': prod_qty}, function(data) {
		if (data) {

		}
    });
}

function getCarrinho() {
	var url = 'carrinho/index';
    jQuery.get(url, function(data) {
      	jQuery('.navbar-collapse').before(data);
    });
}

function writeTotal(value) {
	var text = floatToMoneyText(value);
	var atual = jQuery("#valor-total").text();
	jQuery(".price-total-pedido").text(text);
	jQuery("#valor-total-pedido").text(text);
}

function moneyTextToFloat(text) {
	var cleanText = text.replace("R$ ", "").replace(",", ".");
	return parseFloat(cleanText);
}

function floatToMoneyText(value) {
	var text = (value < 1 ? "0" : "") + Math.floor(value * 100);
	text = "R$ " + text;
	return text.substr(0, text.length - 2) + "," + text.substr(-2);
}

function atualizaCarrinho() {

	// jQuery('.conteudo-carrinho').remove();

	var produtos = jQuery(".odd");
	var total = 0;

	jQuery(produtos).each(function(pos, produto) 
	{
		var $produto = jQuery(produto);
		var quantity = moneyTextToFloat($produto.find(".qty").val());
		var price = moneyTextToFloat($produto.find(".price").text());

		var text = floatToMoneyText(quantity * price);
		$produto.find(".price-total").text(text);

		total += quantity * price;
	});

	return total;		
}


