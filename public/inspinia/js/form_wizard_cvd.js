$(document).ready(function(){
    $.validator.addMethod(
        "checkCpf", 
        function(value, element) {
            return checkCpf(value);
        },
        "CPF inválido"
    );
    // $.validator.addMethod(
    //     "checkLicenca", 
    //     function(value, element) {
    //         return value >= 1;
    //     },
    //     "Selecione uma Licença"
    // );

    $("#form").steps(
    {
        labels: {
            current: "passo atual:",
            pagination: "Paginação",
            finish: "Finalizar",
            next: "Próximo",
            previous: "Anterior",
            loading: "Carregando ..."
        },
        bodyTag: "fieldset",
        onStepChanging: function (event, currentIndex, newIndex)
        {
            // Always allow going backward even if the current step contains invalid fields!
            if (currentIndex > newIndex)
            {
                return true;
            }
            // Forbid suppressing "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age").val()) < 18)
            {
                return false;
            }

            var form = $(this);

            // Clean up if user went backward before
            if (currentIndex < newIndex)
            {
                // To remove error styles
                $(".body:eq(" + newIndex + ") label.error", form).remove();
                $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
            }

            // Disable validation on fields that are disabled or hidden.
            form.validate().settings.ignore = ":disabled,:hidden";
            // Start validation; Prevent going forward if false
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {
            // Suppress (skip) "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age").val()) >= 18)
            {
                $(this).steps("next");
            }

            // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3)
            {
                $(this).steps("previous");
            }
            if (currentIndex === 2) {
                configCepAutoComplete();
            }
        },
        onFinishing: function (event, currentIndex)
        {
            var form = $(this);

            // Disable validation on fields that are disabled.
            // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
            form.validate().settings.ignore = ":disabled";

            // Start validation; Prevent form submission if false
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            var form = $(this);

            // Submit form input
            form.submit();
        }
    }).validate({
        lang: 'pt-br',
        errorPlacement: function(error, element)
        {
            switch(element.attr("name")) {
                case 'numero': 
                    error.insertAfter( $("#numero") );
                    break;
                default:
                    element.before(error);
            }
        },       
        rules: {
            confirmarSenha: {
                equalTo: "#senha"
            },
            cpf: {
                checkCpf: true
            }
        }
    });

//            $.mask.definitions['~'] = "[+-]";
    $(".telefoneMask").mask("(99) 9999-9999");
    $('.celularMask').mask("(99) 9999-9999?9");
    $('.celularMask').focusout(function(){
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if(phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    }).trigger('focusout');
    $(".cepMask").mask("99999-999");
    $(".cpfMask").mask("999.999.999-99");
    $(".cpfMask").focusout(function(){
        if (!checkCpf($(".cpfMask").val())) {

        }
    }).trigger('focusout');
//            $("#product").mask("a*-999-a999", { placeholder: " " });
//            $("#eyescript").mask("~9.99 ~9.99 999");
//            $("#po").mask("PO: aaa-999-***");
//            $("#pct").mask("99%");
    $(".floatMask").mask("*9,99");
    $("input").blur(function() {
        $("#info").html("Unmasked value: " + $(this).mask());
    }).dblclick(function() {
        $(this).unmask();
    });


    // $(".plano-option").click(function(){
    //     var id = this.getAttribute('tag');
    //     $(".plano-option").removeClass("active");
    //     $(this).addClass("active");
    //     $("#licenca").val(id);
    // });
});
